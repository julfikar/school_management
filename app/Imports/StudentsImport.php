<?php

namespace App\Imports;

use App\Models\ClassRoom;
use App\Models\Profile;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Row;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class StudentsImport implements OnEachRow, WithHeadingRow
{
    public function onRow(Row $row)
    {
        $class = [];
        foreach (ClassRoom::all('id') as $item){
            $class[] = $item->id;
        }

        if (!in_array($row['class'],$class)){
            $notification = [
                'message' => 'An invalid class you entered',
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }

        $rowIndex = $row->getIndex();
        $row = $row->toArray();
//        dd($row);

        if ($row['phone'] != null && $row['class_roll'] != null) {
            $user = User::create([
                'name' => $row['name'],
                'phone' => $row['phone'],
                'password' => Hash::make(12345678),
                'email' => $row['email'],
                'gender' => $row['gender']
            ]);

            $user->user_type_id = 3;
            $user->save();

            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            Student::create([
                'user_id' => $user->id,
                'height' => $row['height'],
                'class_id' => $row['class'],
                'unique_id' => $uniqueID,
                'class_rol' => $row['class_roll'],
                'admission_date' => Date::excelToDateTimeObject($row['admission_date']),
                'age' => $row['age'],
                'blood_group' => $row['blood_group'],
                'status' => true,
            ]);

            Profile::create([
                'user_id' => $user->id,
                'name_utf8' =>$row['name_utf8'],
                'father_name' => $row['father_name'],
                'father_name_utf8' => $row['father_name_utf8'],
                'mother_name' => $row['mother_name'],
                'mother_name_utf8' => $row['mother_name_utf8'],
                'present_address' => $row['present_address'],
                'permanent_address' => $row['permanent_address'],
                'about' => $row['about'],
            ]);
        }

        return;
    }
}
