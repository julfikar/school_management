<?php

namespace App\Imports;

use App\Models\Profile;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Row;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class TeachersImport implements OnEachRow, WithHeadingRow
{
    public function onRow(Row $row)
    {
        $rowIndex = $row->getIndex();
        $row = $row->toArray();

        if ($row['phone']) {
            $user = User::create([
                'name' => $row['name'],
                'phone' => $row['phone'],
                'password' => Hash::make(12345678),
                'email' => $row['email'],
                'gender' => $row['gender'],
            ]);
            $user->user_type_id = 2;
            $user->save();

            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            Teacher::create([
                'user_id' => $user->id,
                'unique_id' => $uniqueID,
                'join_date' => Date::excelToDateTimeObject($row['join_date']),
                'expert_in' => $row['expert_in'],
                'n_id' => $row['nid_number'],
                'status' => true
            ]);

            Profile::create([
                'user_id' => $user->id,
                'name_utf8' =>$row['name_utf8'],
                'father_name' => $row['father_name'],
                'father_name_utf8' => $row['father_name_utf8'],
                'mother_name' => $row['mother_name'],
                'mother_name_utf8' => $row['mother_name_utf8'],
                'present_address' => $row['present_address'],
                'permanent_address' => $row['permanent_address'],
                'about' => $row['about'],
            ]);

            //assign the user to a role
            $user->assignRole('teacher');
        }

        return;
    }
}
