<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HostelFee extends Model
{
    use HasFactory;

    protected $fillable = ['dweller_id','month','payed_amount','discount','due'];

    public function dweller(){
        return $this->belongsTo(Dweller::class,'dweller_id','id');
    }

}
