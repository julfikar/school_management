<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamFee extends Model
{
    use HasFactory;

    protected $fillable=['class_id','exam_category_id','student_id','payed_amount','discount','due'];

}
