<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransportationFee extends Model
{
    use HasFactory;

    protected $fillable = ['passenger_id','month','payed_amount','discount','due'];

    public function passenger(){
        return $this->belongsTo(Passenger::class,'passenger_id','id');
    }
}
