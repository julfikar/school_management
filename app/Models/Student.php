<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table ='students';
    protected $fillable=['user_id','class_id','class_rol','unique_id','blood_group','height','age','admission_date','status'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function classRoom()
    {
        return $this->belongsTo(ClassRoom::class,'class_id','id');
    }

    public function exam()
    {
        return $this->hasMany(StudentExam::class, 'student_id', 'unique_id');
    }

    public function attendents()
    {
        return $this->morphMany(Attendance::class, 'attentable');
    }

    public function tutionFees()
    {
        return $this->hasMany(TutionFee::class, 'student_id');
    }

    public function examFees(){
        return $this->hasMany(ExamFee::class, 'student_id');
    }


}
