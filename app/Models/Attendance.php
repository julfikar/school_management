<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    use HasFactory;

    protected $fillable=['machine_id','date','attentable_id','attentable_type'];

    public function attentable()
    {
        return $this->morphTo('attendents');
    }



}
