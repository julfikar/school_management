<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalaryAccount extends Model
{
    use HasFactory;

    protected $fillable = ['salary_id', 'payed_date', 'amount', 'gift', 'bonus', 'charge', 'balance'];

    public function employeeSalary()
    {
        return $this->belongsTo(Salary::class, 'salary_id', 'id');
    }
}
