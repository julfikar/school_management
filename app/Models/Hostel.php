<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hostel extends Model
{
    use HasFactory;

    protected $fillable = ['name','phone','employee_id','address'];

    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id');
    }

    public function dwellers(){
        return $this->hasMany(Dweller::class,'hostel_id');
    }


}
