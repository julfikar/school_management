<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
    use HasFactory;

    protected $fillable=['employee_id','vehicle_reg_no','vehicle_no','vehicle_type'];

    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id');
    }

    public function passengers(){
        return $this->hasMany(Passenger::class,'transportation_id');
    }
}
