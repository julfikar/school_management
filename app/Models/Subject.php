<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    protected $fillable=['class_id','teacher_id','name','e_book','thumbnail','total_mark'];

    public function classRoom()
    {
        return $this->belongsTo(ClassRoom::class,'class_id','id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class,'teacher_id','id');
    }

    public function resources()
    {
        return $this->hasMany(Resource::class,'subject_id','id');
    }

    public function classRoutines()
    {
        return $this->hasMany(ClassRoutine::class);
    }

    public function exams()
    {
        return $this->hasMany(Exam::class, 'subject_id', 'id');
    }

    public function studentExam()
    {
        return $this->hasMany(StudentExam::class);
    }

}
