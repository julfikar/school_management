<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamCategory extends Model
{
    use HasFactory;

    protected $fillable = ['parent_id', 'name', 'mark_at_percentage', 'average'];

    public function exam()
    {
        return $this->hasMany(Exam::class, 'category_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(ExamCategory::class, 'parent_id', 'id');
    }

    public function childrens()
    {
        return $this->hasMany(ExamCategory::class, 'parent_id', 'id');
    }
}
