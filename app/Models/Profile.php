<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','name_utf8','father_name','father_name_utf8','mother_name','mother_name_utf8', 'present_address', 'permanent_address', 'about', 'facebook_link', 'whatsapp_link', 'imo_link', 'linked_in'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
