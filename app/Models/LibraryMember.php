<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibraryMember extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'unique_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function book()
    {
        return $this->hasOne(PhysicalBook::class);
    }

    public function libraryFee()
    {
        return $this->hasMany(LibraryFee::class,'member_id','id');
    }

}
