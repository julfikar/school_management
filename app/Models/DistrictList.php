<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DistrictList extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function thana()
    {
        return $this->hasMany(ThanaList::class);
    }

    public function postOffice()
    {
        return $this->hasMany(PostOfficeList::class);
    }
}
