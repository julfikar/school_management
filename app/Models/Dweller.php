<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dweller extends Model
{
    use HasFactory;

    protected $fillable=['hostel_id','user_id','unique_id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function hostel(){
        return $this->belongsTo(Hostel::class,'hostel_id');
    }

    public function hostelFees(){
        return $this->hasMany(HostelFee::class,'dweller_id','id');
    }


}
