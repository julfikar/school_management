<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhysicalBook extends Model
{
    use HasFactory;

    protected $fillable =['name', 'author', 'insert_date', 'out_date', 'library_member_id'];

    public function libraryMember()
    {
        return $this->belongsTo(LibraryMember::class);
    }

}
