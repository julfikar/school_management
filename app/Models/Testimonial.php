<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $fillable =['student_id','name','father_name','mother_name','village','post_office','upazila', 'district','date_of_birth','roll','academic_year','class','headmaster_name','headmaster_phone','headmaster_mobile','status'];

    public function student(){
        return $this->belongsTo(Student::class,'student_id','id');
    }
}
