<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SendSMS extends Model
{
    use HasFactory;

    private $userType;
    private $message;

    public function __construct($userType,$message)
    {
        $this->users = $userType;
        $this->massage = $message;
    }

    public function sent()
    {
        $url = setting('backend.sms.url');
        $authKey = setting('backend.sms.authkey');
        $senderId = setting('backend.sms.sender');
        $route = setting('backend.sms.route');
        $userPhone = [];
        foreach ($this->users as $data){
            $userPhone[] = $data->user->phone;
        }
        $userPhone = implode(' ',$userPhone);
        $userPhone = str_replace(' ',',',$userPhone);
        $massage = urlencode($this->massage);
        $data = array(
            'authkey' => $authKey,
            'mobiles' => $userPhone,
            'message' => $massage,
            'sender' => $senderId,
            'route' => $route,
        );

        try {
            $ch = curl_init();
            curl_setopt_array($ch,array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $data,
            ));

            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);

            $rsponse = curl_exec($ch);
            if (curl_errno($ch)){
                $notification = [
                    'message' => curl_error($ch),
                    'alert-type' => 'error'
                ];
                return $notification;
            }

            curl_close($ch);
            $notification = [
                'message' => 'Message Sent',
                'alert-type' => 'success'
            ];
        }catch (\Throwable $th){
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
        } finally {
            return $notification;
        }
    }
}
