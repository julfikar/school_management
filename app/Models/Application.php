<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable=['teacher_id','student_id','to','date','subject','request_body','request_summary','name','roll','class_name','english'];

    public function student(){
        return $this->belongsTo(Student::class,'student_id','id');
    }

    public function teacher(){
        return $this->belongsTo(Teacher::class,'teacher_id','id');
    }


}
