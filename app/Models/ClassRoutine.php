<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassRoutine extends Model
{
    use HasFactory;

    protected $table = 'cass_routines';

    protected $fillable = ['class_room_id', 'subject_id', 'off_period', 'tiffeen_period', 'name_of_day', 'start_at', 'end_at'];

    public function classRoom()
    {
        return $this->belongsTo(ClassRoom::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
