<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassRoom extends Model
{
    use HasFactory;
    protected $table ='class_rooms';
    protected $fillable = ['teacher_id', 'name', 'room_no'];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class,'class_id','id');
    }

    public function student()
    {
        return $this->hasMany(Student::class,'class_id','id');
    }

    public function studentExam()
    {
        return $this->hasMany(StudentExam::class);
    }

    public function classRoutines()
    {
        return $this->hasMany(ClassRoutine::class);
    }

    public function applyForAdmission()
    {
        return $this->hasOne(Admission::class, 'new_class', 'id');
    }

    public function exams()
    {
        return $this->hasMany(Exam::class, 'class_room_id', 'id');
    }

    public function examFees()
    {
        return $this->hasMany(ExamFee::class, 'class_id', 'id');
    }

}
