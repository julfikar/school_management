<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OneSignal;

class OnesignalNotification extends Model
{
    use HasFactory;

    public function sendNotificationToAll($message, $url)
    {
        try {
            OneSignal::sendNotificationToAll(
                $message,
                $url = $url,
                $data = null,
                $buttons = null,
                $schedule = null
            );
            $notification = [
                'message' => 'Send Successfully',
                'alert-type' => 'success'
            ];
        }catch (\Throwable $th){
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return $notification;
        }
    }

    public function sendNotificationToUser($userID, $message, $url)
    {
        try {
            OneSignal::sendNotificationToUser(
                $message,
                $userID,
                $url = $url,
                $data = null,
                $buttons = null,
                $schedule = null
            );
            $notification = [
                'message' => 'Send Successfully',
                'alert-type' => 'success'
            ];
        }catch (\Throwable $th){
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
        } finally {
            return $notification;
        }
    }
}
