<?php

namespace App\Models;

use App\Mail\EmailNotify;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class SendMail extends Model
{
    use HasFactory;

    private $userType;
    private $subject;
    private $message;

    public function __construct($userType, $subject, $message)
    {
        $this->users = $userType;
        $this->subject = $subject;
        $this->massage = $message;
    }

    public function sent()
    {
        $maleUserName = setting('backend.smtp.mail_username');
        $siteName = setting('backend.general.site_name');
        $userEmail = [];
        foreach ($this->users as $data){
            if ($data->user->email) {
                $userEmail[] = $data->user->email;
            }
        }
        $userEmail = implode(' ',$userEmail);
        $userEmail = str_replace(' ',',',$userEmail);
        $mailSubject = $this->subject;
        $massage = urlencode($this->massage);

        try {
            Mail::to($userEmail)
                ->cc(User::first()->email)
                ->send(new EmailNotify($maleUserName, $siteName, $mailSubject, $massage));
        }catch (\Throwable $th){
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
        } finally {
            return $notification;
        }
    }
}
