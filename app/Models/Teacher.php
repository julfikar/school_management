<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;


    protected $fillable = ['user_id', 'join_date', 'unique_id','expert_in', 'n_id', 'cv_file', 'status', 'father_name', 'mother_name'];

    // protected $casts = [
    //     'join_date' => 'datetime:d-m-Y',
    // ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function classRoom()
    {
        return $this->hasMany(ClassRoom::class);
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class);
    }

    public function attendents()
    {
        return $this->morphMany(Attendance::class, 'attentable');
    }

    public function salary()
    {
        return $this->morphMany(Salary::class, 'salariable');
    }
}
