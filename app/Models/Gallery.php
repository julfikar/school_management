<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasFactory;

    protected $fillable=['name','gallery_folder_id','status'];


    public function galleryFolder(){
        return $this->belongsTo(GalleryFolder::class,'gallery_folder_id','id');
    }

}
