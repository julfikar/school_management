<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TutionFee extends Model
{
    use HasFactory;

    protected $fillable=['class_id','student_id','month','payed_amount','discount','due'];

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }

}
