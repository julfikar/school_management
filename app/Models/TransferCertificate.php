<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransferCertificate extends Model
{
    use HasFactory;

    protected $fillable =['student_id','name','father_name','mother_name','village','post_office','upazila', 'district','date_of_birth','roll','academic_year','class_name','paid_month','paid_year','by_guardian','by_complete','by_home_change','by_school_wish','status'];

    public function student(){
        return $this->belongsTo(Student::class,'student_id');
    }

}
