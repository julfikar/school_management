<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = ['class_room_id', 'category_id', 'subject_id', 'name', 'date', 'start_at', 'end_at', 'min_mark', 'marks', 'question', 'answer'];

    public function category()
    {
        return $this->belongsTo(ExamCategory::class, 'category_id','id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class,'subject_id', 'id');
    }

    public function classRoom()
    {
        return $this->belongsTo(ClassRoom::class,'class_room_id', 'id');
    }

    public function studentExam()
    {
        return $this->hasMany(StudentExam::class);
    }
}
