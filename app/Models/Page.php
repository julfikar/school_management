<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'title', 'body_type', 'body', 'top_bar', 'left_bar', 'right_bar', 'bottom_bar', 'slug', 'deletable', 'status'];
}
