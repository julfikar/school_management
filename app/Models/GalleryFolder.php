<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryFolder extends Model
{
    use HasFactory;

    protected $fillable=['name','status','slug'];

    public function galleries(){
        return $this->hasMany(Gallery::class,'gallery_folder_id','id');
    }


}
