<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibraryFee extends Model
{
    use HasFactory;

    protected $fillable=['member_id','month','payed_amount','discount','due'];


    public function libraryMember(){
        return $this->belongsTo(LibraryMember::class,'member_id','id');
    }

}
