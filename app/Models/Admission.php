<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admission extends Model
{
    use HasFactory;

    protected $fillable =['user_id','unique_id','date_of_birth','gender','old_class','new_class','mark_sheet'];


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getClassName($id){
        $classname = ClassRoom::where('id',$id)->first()->name;
        return $classname;
    }

    public function applyedCalss(){
        return $this->belongsTo(ClassRoom::class, 'new_class','id');
    }

}
