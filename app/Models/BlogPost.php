<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    use HasFactory;

    protected $fillable = ['folder_id', 'user_id','title', 'thumbnail', 'body', 'tags', 'top_bar', 'left_bar', 'right_bar', 'bottom_bar', 'slug', 'status'];

    public function folder(){
        return $this->belongsTo(BlogFolder::class, 'folder_id', 'id');
    }


    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
