<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
    use HasFactory;

    protected $fillable =['transportation_id','user_id','unique_id'];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function transportation(){
        return $this->belongsTo(Transportation::class,'transportation_id');
    }

    public function transportationFees(){
        return $this->hasMany(TransportationFee::class,'passenger_id','id');
    }

}
