<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','unique_id','designation_id', 'nid', 'join_date', 'cv_file', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }


    public function attendents()
    {
        return $this->morphMany(Attendance::class, 'attentable');
    }

    public function salary()
    {
        return $this->morphMany(Salary::class, 'salariable');
    }

}
