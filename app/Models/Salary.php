<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasFactory;

    protected $fillable=['basic', 'increment', 'hospital_fee', 'accommodation', 'transportation_fee', 'amount','date','salariable_id','salariable_type', 'by'];

    public function salariable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'by','id');
    }

    public function accounts()
    {
        return $this->hasMany(SalaryAccount::class,'salary_id','id');
    }
}
