<?php

namespace App\Http\Controllers\GuestViewControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdmissionRequest;
use App\Models\Admission;
use App\Models\BlogFolder;
use App\Models\BlogPost;
use App\Models\ClassRoom;
use App\Models\Complain;
use App\Models\Ebooks;
use App\Models\EmergencyServices;
use App\Models\Employee;
use App\Models\ExamCategory;
use App\Models\GalleryFolder;
use App\Models\ImportanLinks;
use App\Models\KeyPerson;
use App\Models\NoticeBoard;
use App\Models\Profile;
use App\Models\Student;
use App\Models\StudentExam;
use App\Models\Subject;
use App\Models\SubSlider;
use App\Models\Subscribe;
use App\Models\Teacher;
use App\Models\Testimonial;
use App\Models\TransferCertificate;
use App\Models\User;
use App\Models\VideoSlider;
use App\Models\Page;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class GuestViewController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = __('Home');

            $video_sliders = VideoSlider::where('main_video','!=',1)->get();
            $main_videos = VideoSlider::where('main_video',1)->first();
            $ebooks = Ebooks::inRandomOrder()->take(6)->get();
            $subjects = Subject::inRandomOrder()->take(6)->get();
            $services = EmergencyServices::all();
            $sub_slider = SubSlider::all();
            $links = ImportanLinks::all();
            $notice_board = NoticeBoard::orderBy('id','desc')->take(5)->get();

            $left_persons = KeyPerson::where('left',true)->orderBy('position','asc')->get();
            $right_persons = KeyPerson::where('left',false)->orderBy('position','asc')->get();
            $keyPersons = KeyPerson::inRandomOrder()->get();
            $videos = VideoSlider::all();

            return view('welcome', compact('title','video_sliders','ebooks','subjects','services','sub_slider','links','main_videos','notice_board','left_persons','right_persons', 'keyPersons','videos'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function redirectPage($slug)
    {
        try {
            $page = Page::where(['slug' => $slug, 'status' => true])->first();
            if (!$page){
                $notification = array(
                    'message' => __('Page not found').'.......',
                    'alert-type' => 'error'
                );
                return $this->backWithError($notification);
            }
            $title = $page->title;

            if ($page->name == 'library'){
                return $this->library();
            }elseif ($page->name == 'photo_gallery'){
                return $this->photoGallery();
            }elseif ($page->name == 'admission_form'){
                return $this->admissionForm();
            }elseif ($page->name == 'notice_board'){
                return $this->notice();
            }elseif ($page->name == 'teachers_page'){
                return $this->allTeachers();
            }elseif ($page->name == 'employee_pages'){
                return $this->allEmployees();
            }elseif ($page->name == 'video_gallery'){
                return $this->videoGallery();
            }elseif ($page->name == 'statement_page'){
                return $this->personContent();
            }elseif ($page->name == 'blog_post'){
                return $this->blog();
            }elseif ($page->name == 'school_result'){
                return $this->examResult();
            }
            return view('frontend.theme1.pages.basic-pages.basic-page', compact('page', 'title'));
        }catch (\Throwable $e){
            return $this->backWithError($e->getMessage());
        }
    }

    public function library()
    {
        try {
            $page = Page::where(['name' => 'library', 'status' => true])->first();
            $title = $page->title?$page->title:__('Library');

            $allBooks = [];
            $cb = 9;
            foreach (Ebooks::inRandomOrder()->take(3)->get() as $row){
                $row->e_book = 'upload/e_books/'.$row->e_book;
                $row->thumbnail = 'upload/e_books/thumbnails/'.$row->thumbnail;
                $allBooks [] = $row;
            }
            $cb = $cb - count($allBooks);
            foreach (Subject::inRandomOrder()->take($cb)->get() as $row){
                $row->e_book = 'upload/subjects/e_books/'.$row->e_book;
                $row->thumbnail = 'upload/subjects/thumbnails/'.$row->thumbnail;
                $allBooks [] = $row;
            }

            return view('frontend.theme1.pages.library.index', compact('page', 'title','allBooks'));

        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function textBooks()
    {
        try {
            $page = Page::where(['name' => 'library', 'status' => true])->first();
            $title = $page->title?$page->title:__('Library');
            $textBook = Subject::paginate(9);
            return view('frontend.theme1.pages.library.text-books', compact('page', 'title', 'textBook'));
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function textBooksByClass(ClassRoom $classRoom)
    {
        try {
            $page = Page::where(['name' => 'library', 'status' => true])->first();
            $title = $page->title?$page->title:__('Library');
            $textBook = $classRoom->subjects()->paginate(9);
            return view('frontend.theme1.pages.library.text-books-by-class', compact('page', 'title', 'textBook'));
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function storyBooks()
    {
        try {
            $page = Page::where(['name' => 'library', 'status' => true])->first();
            $title = $page->title?$page->title:__('Library');
            $storryBooks = Ebooks::paginate(9);
            return view('frontend.theme1.pages.library.story-books', compact('page', 'title', 'storryBooks'));
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function photoGallery()
    {
        try {
            $page = Page::where(['name' => 'photo_gallery', 'status' => true])->first();
            $title = $page->title?$page->title:__('Photo Gallery');
            $galleryFolders = GalleryFolder::all();

            return view('frontend.theme1.pages.gallery.photo-gallery.index', compact('page', 'title','galleryFolders'));

        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function admissionForm(){
        try {
            $page = Page::where(['name' => 'admission_form', 'status' => true])->first();
            $title = $page->title?$page->title:__('Admission Form');

            return view('frontend.theme1.pages.admission.form', compact('page', 'title'));

        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function storeAdmissionForm(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => ['required', 'string', 'max:255'],
                'name_utf8' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'father_name_utf8' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],
                'mother_name_utf8' => ['required', 'string', 'max:255'],
                'date_of_birth' => ['required', 'string', 'max:255'],
                'gender' => ['required', 'string', 'max:255'],
                'new_class' => ['required'],
                'age' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:14', 'unique:users'],
                'present_district' => 'required|string|max:255',
                'permanent_district' => 'required|string|max:255',
                'present_thana' => 'required|string|max:255',
                'permanent_thana' => 'required|string|max:255',
                'present_city' => 'required|string|max:255',
                'permanent_city' => 'required|string|max:255',
                'present_post_office' => 'required|string|max:255',
                'permanent_post_office' => 'required|string|max:255',
                'present_road' => 'required|string|max:255',
                'permanent_road' => 'required|string|max:255',
                'profile_photo' => ['required'],
            ]);
            // Validation for image
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('profile_photo')) {
                if (!in_array($request->profile_photo->getClientOriginalExtension(), $acceptable)) {
                    return $this->backWithError('Only jpeg, png, jpg and gif file is supported.');
                }
            }

            // Validation for pdf file
            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('mark_sheet')) {
                if (!in_array($request->mark_sheet->getClientOriginalExtension(), $pdf_acceptable)) {
                    return $this->backWithError('Only pdf file is supported.');
                }
            }

//            dd($request->all());
            $user = new User();
            $user->name = $request->name;
            $user->user_type_id = 5;
            $user->phone = $request->phone;

            if($request->input('email')!=null){
                $request->validate([
                    'email' => ['string', 'email', 'max:255', 'unique:users'],
                ]);
                $user->email = $request->email;
            }

            $user->password = Hash::make($request->phone);
            $user->gender = strtolower($request->gender);
            $user->save();

            if ($request->hasFile('profile_photo')){
                $user->updateProfilePhoto($request->profile_photo);
            }


            // Present and parmanebt Address
            $presentAddress = $request->present_road . ', ' . $request->present_city . ', ' . $request->present_thana . ', ' . $request->present_district . '-' . $request->present_post_office;
            $permanentAddress = $request->permanent_road . ', ' . $request->permanent_city . ', ' . $request->permanent_thana . ', ' . $request->permanent_district . '-' . $request->permanent_post_office;


            Profile::create([
                'user_id' => $user->id,
                'name_utf8' => $request->name_utf8,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8,
                'present_address' => $presentAddress,
                'permanent_address' => $permanentAddress,
            ]);

            // for admission information
            $admission = new Admission();

            if ($request->hasFile('mark_sheet')) {
                // insert new file
                $new_mark_sheet = $request->mark_sheet;
                //file name
                $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                $x = str_shuffle($x);
                $x = substr($x, 0, 6) . 'DAC.';
                $mark_sheet = time() . $x . $new_mark_sheet->getClientOriginalExtension();
                // Upload image
                $new_mark_sheet->move(public_path('/upload/mark-sheets/'), $mark_sheet);
                $admission->mark_sheet = $mark_sheet;
            }

            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            $admission->user_id = $user->id;
            $admission->unique_id = $uniqueID;
            $admission->date_of_birth = $request->date_of_birth;
            $admission->new_class = $request->new_class;
            $admission->old_class = $request->old_class;
            $admission->age = $request->age;
            $admission->blood_group = $request->blood_group;
            $admission->save();

            $notification = [
                'message' => __('Your application has been submitted & now you can login...'),
                'alert-type' => 'success'
            ];

            return redirect()->route('home')->with($notification);
        } catch (\Throwable $th) {
//            DB::rollBack();
            return $this->backWithError($th->getMessage());
        }
    }

    public function notice(){
        try {
            $page = Page::where(['name' => 'notice_board', 'status' => true])->first();
            $title = $page->title?$page->title:__('All Notice');
            $notice = NoticeBoard::all();
            return view('frontend.theme1.pages.notice-board.index', compact('page', 'title','notice'));
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function singleNoticeView($slug)
    {
        try {
            $page = Page::where(['name' => 'notice_board', 'status' => true])->first();
            $title = $page->title?$page->title:__('Single Notice');
            $notice = NoticeBoard::where('slug',$slug)->first();
            return view('frontend.theme1.pages.notice-board.single-notice', compact('page', 'title','notice'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    //    Blog section
    public function blog(){
        try{
            $page = Page::where(['name' => 'blog_post', 'status' => true])->first();
            $title = $page->title?$page->title:__('All Post');
            $blogs = BlogPost::all();
            return view('frontend.theme1.pages.blogs.index', compact( 'page','title','blogs'));
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }

    }

    public function singleBlogView($slug)
    {
        try {
            $page = Page::where(['name' => 'blog_post', 'status' => true])->first();
            $blog = BlogPost::where('slug',$slug)->first();
            $title = $blog->title;
            return view('frontend.theme1.pages.blogs.single-blog', compact( 'page','title','blog'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }


    public function singleBlogFolderView($id){

        try {
            $blogfolder = BlogFolder::where('id',$id)->first();
            $page = Page::where(['name' => 'blog_post', 'status' => true])->first();
            $title = __($blogfolder->name);
            $blogs = $blogfolder->posts;
            return view('frontend.theme1.pages.blogs.single-blog-folder', compact( 'page','title','blogs'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }


    public function personContent()
    {
        try {
            $title = __('Person Content');
            $data = KeyPerson::all();
            return view('frontend.theme1.pages.person-content.index', compact('title','data'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function singlePersonContent($request)
    {
        try {
            $title = __('Single Person Content');
            $data = KeyPerson::find($request);
            return view('frontend.theme1.pages.single-person-content.index', compact('title','data'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function allTeachers()
    {
        try {
            $page = Page::where(['name' => 'teachers_page', 'status' => true])->first();
            $title = $page->title?$page->title:__('All Teachers');
            $teachers = Teacher::where('status',true)->get();
            return view('frontend.theme1.pages.all-teacher.index', compact('page','title','teachers'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function complain(Request $request){
        try{
            $data = new Complain();
            $data->name = $request->name;
            $data->phone = $request->phone;
            $data->massage = $request->message;
            $data->save();
            $notification = array(
                'message' => 'Complain placed successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function teacherExplore($id)
    {
        try {
            $page = Page::where(['name' => 'teachers_page', 'status' => true])->first();
            $title = __('Teacher');
            $teacher = Teacher::find($id);
            return view('frontend.theme1.pages.teacher-explore.index', compact('page','title','teacher'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function Subscribe(Request $request){
        try{
            if ($request->email) {
                $this->validate($request, [
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:subscribes'],
                ]);
            }
            $data = new Subscribe();
            $data->email = $request->email;
            $data->save();
            $notification = array(
                'message' => 'Subscribed successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function allEmployees(){
        try {
            $page = Page::where(['name' => 'employee_pages', 'status' => true])->first();
            $title = $page->title?$page->title:__('All Employees');
            $employees = Employee::all();
            return view('frontend.theme1.pages.all-employee.index', compact('page','title','employees'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function employeeExplore($id){
        try {
            $page = Page::where(['name' => 'employee_pages', 'status' => true])->first();
            $title = __('Employee');
            $employee = Employee::find($id);
            return view('frontend.theme1.pages.employee-explore.index', compact('page','title','employee'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function videoGallery()
    {
        try {
            $page = Page::where(['name' => 'video_gallery', 'status' => true])->first();
            $title = $page->title?$page->title:__('All Videos');
            $videos = VideoSlider::all();
            return view('frontend.theme1.pages.gallery.video-gallery.index', compact('page', 'title','videos'));
        }catch (\Throwable $e){
            return $this->backWithError($e->getMessage());
        }
    }

    public function examResult()
    {
        try {
            $title = __('Result');
            $classes = ClassRoom::all();
            $exams = ExamCategory::where('parent_id', '!=', null)->get();
            $page = Page::where(['name' => 'school_result', 'status' => true])->first();
            return view('frontend.theme1.pages.exam-result.result-form', compact('title','page','classes','exams'));

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function examResultStore(Request $request ){
        try {
            $title = __('Your result');
            $page = Page::where(['name' => 'school_result', 'status' => true])->first();

            $student = Student::where('unique_id',$request->unique_id)->first();
            $class = ClassRoom::where('id',$request->class_id)->first();
            $exam = ExamCategory::where('id',$request->exam_id)->first();
            $year = date('Y', strtotime($request->year));

            $results = StudentExam::where(['student_id' => $student->unique_id, 'exam_category_id' => $exam->id, 'class_id' => $class->id])->whereYear('created_at',$year)->get();
            if ($results->count() != 0){
                return view('frontend.theme1.pages.exam-result.show-result', compact('page', 'title', 'results', 'student', 'exam', 'year'));
            }
            else{
                $notification = array(
                    'message' => 'Data not find',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function examResultDownload(Request $request ){
        try {
            $allUsers = User::where('user_type_id', 2)->get();
            $headMaster = '';
            foreach ($allUsers as $person){
                if ($person->hasRole('headmaster')){
                    $headMaster = config('app.locale') == 'en' ? $person->name:$person->profile->name_utf8;
                }
            }

            $student = Student::where('id',$request->student)->first();
            $class = ClassRoom::where('id',$request->class)->first();
            $exam = ExamCategory::where('id',$request->exam)->first();
            $year = date('Y', strtotime($request->year));

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';


            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $mogib100 = asset('forntend/theme1/images/100.jpg');
//            $mogib100 = 'https://gendagps.com/forntend/theme1/images/100.jpg';

            $results = StudentExam::where(['student_id' => $student->unique_id, 'exam_category_id' => $exam->id, 'class_id' => $class->id])->whereYear('created_at',$year)->get();
            if ($results->count() != 0){
                $view = 'frontend.theme1.pages.exam-result.download-result';
                $qrcode = QrCode::size(90)->generate(route('result.download').'?_token='.csrf_token().'&student='.$student->id.'&class='.$class->id.'&exam='.$exam->id.'&year='.$year);
                $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);

                $pdf = PDF::loadView($view, compact( 'logo', 'groupLogo', 'avatar', 'bg', 'mogib100', 'results', 'student', 'headMaster', 'exam', 'year', 'qrcode'), [], [
                    'default_font_size' => 14,
                    'default_font' => 'kalpurush',
                    'orientation' => 'p',
                    'margin_left'          => 15,
                    'margin_right'         => 10,
                    'margin_top'           => 5,
                    'margin_bottom'           => 5,
                    'title'                => 'Exam Result'
                ]);



                return $pdf->stream('student-result.pdf');
            } else{
                $notification = array(
                    'message' => 'Data not find',
                    'alert-type' => 'error'
                );
                return redirect()->back()->with($notification);
            }

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function downloadTransferCertificateForm($id=null){

        try {
            $allUsers = User::where('user_type_id', 2)->get();
            $headMaster = '';
            foreach ($allUsers as $person){
                if ($person->hasRole('headmaster')){
                    $headMaster = config('app.locale') == 'en' ? $person->name:$person->profile->name_utf8;
                }
            }

            $transfer_certificate = TransferCertificate::where('id',$id)->where('status',true)->first();
            $student = Student::find($transfer_certificate->student_id);

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';


            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $mogib100 = asset('forntend/theme1/images/100.jpg');
//            $mogib100 = 'https://gendagps.com/forntend/theme1/images/100.jpg';

            $view = 'frontend.theme1.auth-clients.pages.transfer-certificates.tc-download';
            $qrcode = QrCode::size(90)->generate(route('download-transfer-certificate-form',$id));
            $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);

            $pdf = PDF::loadView($view, compact( 'transfer_certificate','qrcode', 'logo', 'groupLogo', 'bg', 'mogib100', 'avatar', 'headMaster', 'student'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 5,
                'margin_bottom'           => 5,
                'title'                => 'Application Form',
            ]);


            return $pdf->stream('transfer-certificate.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function downloadTestimonial($id=null){

        try {
            $testimonial = Testimonial::where('id',$id)->where('status',true)->first();

            $allUsers = User::where('user_type_id', 2)->get();
            $headMaster = '';
            foreach ($allUsers as $person){
                if ($person->hasRole('headmaster')){
                    $headMaster = config('app.locale') == 'en' ? $person->name:$person->profile->name_utf8;
                }
            }

            $student = Student::find($testimonial->student_id);

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';


            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $mogib100 = asset('forntend/theme1/images/100.jpg');
//            $mogib100 = 'https://gendagps.com/forntend/theme1/images/100.jpg';

            $qrcode = QrCode::size(90)->generate(route('download-testimonial',$id));
            $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);


            $view = 'frontend.theme1.auth-clients.pages.testimonials.testimonial-download';

            $pdf = PDF::loadView($view, compact('logo', 'avatar', 'groupLogo', 'bg', 'mogib100', 'headMaster', 'student', 'testimonial','qrcode'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 10,
                'title'                => 'Application Form',
            ]);

            $tname= 'testimonial'.rand(2,50);
            return $pdf->stream($tname.'.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }
}
