<?php

namespace App\Http\Controllers;

use App\Models\AccountSetting;
use App\Models\ClassRoom;
use App\Models\Complain;
use App\Models\DistrictList;
use App\Models\EmergencyServices;
use App\Models\Hostel;

use App\Models\ImportanLinks;
use App\Models\KeyPerson;
use App\Models\MenuCategory;
use App\Models\Page;
use App\Models\Slider;
use App\Models\Transportation;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $menus = MenuCategory::all();
        $headerMenu = $menus->count() > 0 ? $menus[0]:null;
        $bottomMenu = $menus->count() > 1 ? $menus[1]:null;
        $districts = DistrictList::all();
        $complains = Complain::where('status',false)->get();
        $keyPersons = KeyPerson::all();
        $classRooms = ClassRoom::all();
        $pages = Page::where('status', true)->get();
        $hostels = Hostel::all();
        $sliders = Slider::where('status',true)->get();
        $transportations = Transportation::all();

        $important_links = ImportanLinks::all();
        $services = EmergencyServices::all();

        $accountSettings = AccountSetting::where('status',true)->get();

        View::share('headerMenu', $headerMenu);
        View::share('bottomMenu', $bottomMenu);
        View::share('classRooms', $classRooms);
        View::share('complains', $complains);
        View::share('keyPersons', $keyPersons);
        View::share('districts', $districts);
        View::share('pages', $pages);
        View::share('hostels', $hostels);
        View::share('sliders', $sliders);
        View::share('transportations', $transportations);
        View::share('services', $services);
        View::share('important_links', $important_links);
        View::share('accountSettings', $accountSettings);
    }


    public function backWithError($message)
    {
        $notification = [
            'message' => $message,
            'alert-type' => 'error'
        ];
        return back()->with($notification);
    }

    public function backWithSuccess($message)
    {
        $notification = [
            'message' => $message,
            'alert-type' => 'success'
        ];
        return back()->with($notification);
    }




}
