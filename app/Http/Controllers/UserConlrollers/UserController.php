<?php

namespace App\Http\Controllers\UserConlrollers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateApplicationRequest;
use App\Http\Requests\CreateTransferCertificateRequest;
use App\Models\Admission;
use App\Models\Application;
use App\Models\ClassRoom;
use App\Models\Ebooks;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Testimonial;
use App\Models\TransferCertificate;
use App\Models\TutionFee;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Mpdf\Mpdf;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class UserController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->middleware(['auth', 'user']);
    }

    public function index()
    {
        try {
            $title = 'Dashboard';
            $user = Auth::user();

            $storryBooks=null;
            $textBook=null;
            if ($user->userType->id == 3){
                $class_id = $user->student->class_id;
                $textBook = Subject::where('class_id',$class_id)->paginate(9);
            }else{
                $storryBooks = Ebooks::paginate(9);
            }

            return view('frontend.theme1.auth-clients.pages.dashboard',compact('textBook','storryBooks'));
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function profile()
    {
        try {
            $title = 'My profile';
            return view('frontend.theme1.auth-clients.pages.profile.show', compact('title'));
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function profileUpdate(Request $request)
    {
        try {
            $input = $request->all();
            if ($request->hasFile('photo')){
                $input['photo'] = $request->photo[0];
            }
            $user = Auth::user();

            Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:14', Rule::unique('users')->ignore($user->id)],
                'photo' => ['nullable', 'mimes:jpg,jpeg,png', 'max:1024'],
            ])->validateWithBag('updateProfileInformation');

            if ($request->email != null){
                $this->validate($request,[
                    'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
                ]);
            }

            if (isset($input['photo'])) {
                $user->updateProfilePhoto($input['photo']);
            }

            if ($input['email'] !== $user->email &&
                $user instanceof MustVerifyEmail) {
                $this->updateVerifiedUser($user, $input);
            } else {
                $user->forceFill([
                    'name' => $input['name'],
                    'phone' => $input['phone'],
                    'email' => $input['email'],
                ])->save();
            }

            $notification = array(
                'message' => $user->name.'\'s personal information has been updated successfully',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function profileInfoUpdate(Request $request)
    {
        try {
            $user = Auth::user();
            if (!$user->profile){
                $this->validate($request, [
                    'present_district' => 'required|string|max:255',
                    'permanent_district' => 'required|string|max:255',
                    'present_thana' => 'required|string|max:255',
                    'permanent_thana' => 'required|string|max:255',
                    'present_city' => 'required|string|max:255',
                    'permanent_city' => 'required|string|max:255',
                    'present_post_office' => 'required|string|max:255',
                    'permanent_post_office' => 'required|string|max:255',
                    'present_road' => 'required|string|max:255',
                    'permanent_road' => 'required|string|max:255',
                ]);

                $profile = new Profile();
                $presentAddress = $request->present_road.', '.$request->present_city.', '.$request->present_thana.', '.$request->present_district.'-'.$request->present_post_office;
                $permanentAddress = $request->permanent_road.', '.$request->permanent_city.', '.$request->permanent_thana.', '.$request->permanent_district.'-'.$request->permanent_post_office;
            }else{
                $this->validate($request, [
                    'present_address' => 'required|string|max:255',
                    'permanent_address' => 'required|string|max:255',
                ]);

                $profile = $user->profile;
                $presentAddress = $request->present_address;
                $permanentAddress = $request->permanent_address;
            }

            $this->validate($request, [
                'name_utf8' => 'required|string|max:255',
                'father_name' => 'required|string|max:255',
                'father_name_utf8' => 'required|string|max:255',
                'mother_name' => 'required|string|max:255',
                'mother_name_utf8' => 'required|string|max:255',
            ]);

            $profile->user_id = $user->id;
            $profile->name_utf8 = $request->name_utf8;
            $profile->father_name = $request->father_name;
            $profile->father_name_utf8 = $request->father_name_utf8;
            $profile->mother_name = $request->mother_name;
            $profile->mother_name_utf8 = $request->mother_name_utf8;
            $profile->present_address = $presentAddress;
            $profile->permanent_address = $permanentAddress;

            $profile->save();


            $notification = array(
                'message' => $user->name.'\'s personal information has been updated successfully',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function downloadAdmitCard()
    {
        try {
            $user = Auth::user();
            $classID = $user->applicant->old_class?$user->applicant->old_class:2;
            $class = is_int($classID)?ClassRoom::find($classID):ClassRoom::find(2);

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';

            $avatar = $user->profile_photo_path?asset('storage/'.$user->profile_photo_path):($user->gender?($user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $pdf = PDF::loadView('frontend.theme1.auth-clients.pages.admid-card', compact('user', 'logo', 'groupLogo', 'avatar','class', 'bg'), [], [
                'default_font_size' => 13,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 5,
                'title'                => 'Admit Card',
            ]);
            return $pdf->stream('admit-card.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function myAllApplications()
    {

        try {
            $title = 'All Application';
            $student_id = auth()->user()->student->id;
            $testimonials= Testimonial::where('student_id',$student_id)->where('status',true)->get();
            $applications= Application::where('student_id',$student_id)->get();
            $transfer_certificates= TransferCertificate::where('student_id',$student_id)->get();

            return view('frontend.theme1.auth-clients.pages.my-all-applications', compact('title','applications','testimonials','transfer_certificates'));
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * For Testimonial
     */

    public function downloadTestimonial($id=null){

        try {
            $testimonial = Testimonial::where('id',$id)->where('status',true)->first();

            $allUsers = User::where('user_type_id', 2)->get();
            $headMaster = '';
            foreach ($allUsers as $person){
                if ($person->hasRole('headmaster')){
                    $headMaster = config('app.locale') == 'en' ? $person->name:$person->profile->name_utf8;
                }
            }

            $student = Student::find($testimonial->student_id);

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';


            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $mogib100 = asset('forntend/theme1/images/100.jpg');
//            $mogib100 = 'https://gendagps.com/forntend/theme1/images/100.jpg';

            $qrcode = QrCode::size(90)->generate(route('download-testimonial',$id));
            $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);


            $view = 'frontend.theme1.auth-clients.pages.testimonials.testimonial-download';

            $pdf = PDF::loadView($view, compact('logo', 'avatar', 'groupLogo', 'bg', 'mogib100', 'headMaster', 'student', 'testimonial','qrcode'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 10,
                'title'                => 'Application Form',
            ]);

            $tname= 'testimonial'.rand(2,50);
            return $pdf->stream($tname.'.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }



    /**
     * For Applications
     */
    public function Application()
    {
        try {
            $title = 'Application';
            $teachers= Teacher::where('status',1)->get();
            return view('frontend.theme1.auth-clients.pages.application-form.form', compact('title','teachers'));
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }



    public function applicationStore(CreateApplicationRequest $request)
    {

        try{

            $student = Auth::user()->student;
            $application = new Application();

            $application->teacher_id = $request->teacher_id;
            $application->student_id = $student->id;
            $application->to = $request->to;
            $application->subject = $request->subject;
            $application->date = $request->date;
            $application->request_body = $request->request_body;
            $application->request_summary = $request->request_summary;
            $application->name = Auth::user()->name;
            $application->roll = $student->class_rol;
            $application->class_name = $student->classRoom->name;

            $application->english = true;

            $application->save();


            $notification = array(
                'message' => 'Applied successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function downloadApplicationForm($id=null){

        try {

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';
            $user = Auth::user();
            $application = Application::where('id',$id)->first();
            $student = Student::find($application->student_id);

           if($application->english== true){
               $view = 'frontend.theme1.auth-clients.pages.application-form.english-form';
           }else{
               $view = 'frontend.theme1.auth-clients.pages.application-form.bangla-form';
           }

            $pdf = PDF::loadView($view, compact('user', 'application', 'logo', 'student'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 15,
                'margin_right'         => 10,
                'margin_top'           => 20,
                'margin_bottom'           => 5,
                'title'                => 'Application Form',
            ]);


            return $pdf->stream('application-form.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    /**
     * For Transfer Certificate
     * @param CreateTransferCertificateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function transferCertificateStore(CreateTransferCertificateRequest $request)
    {

        try{

            $sid= auth()->user()->student->id;
            $payed_month = TutionFee::where('student_id',$sid)->latest()->first();

            $new_payed_month = $payed_month?date('F',$payed_month->month):'';
            $new_payed_year = $payed_month?date('Y',strtotime($payed_month->created_at)):'';

            $transfer_certificate = new TransferCertificate();

            $transfer_certificate->name = config('app.locale') == 'en'?auth()->user()->name:auth()->user()->profile->name_utf8;
            $transfer_certificate->father_name = config('app.locale') == 'en'?auth()->user()->profile->father_name:auth()->user()->profile->father_name_utf8;
            $transfer_certificate->mother_name = config('app.locale') == 'en'?auth()->user()->profile->mother_name:auth()->user()->profile->mother_name_utf8;
            $transfer_certificate->village = $request->village;
            $transfer_certificate->post_office = $request->post_office;
            $transfer_certificate->upazila = $request->upazila;
            $transfer_certificate->district = $request->district;
            $transfer_certificate->roll = auth()->user()->student->class_rol;
            $transfer_certificate->date_of_birth = $request->date_of_birth;
            $transfer_certificate->class_name = auth()->user()->student->classRoom->name;
            $transfer_certificate->academic_year = $request->academic_year;
            $transfer_certificate->paid_month = $new_payed_month;
            $transfer_certificate->paid_year = $new_payed_year;

            $transfer_certificate->student_id = $request->student_id;

            $transfer_certificate->by_guardian = true;

            if($request->has('by_home_change')){
                $transfer_certificate->by_home_change = true;
            }else{
                $transfer_certificate->by_home_change = false;
            }

            $transfer_certificate->save();


            $notification = array(
                'message' => 'Transfer Certificate Applied successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function downloadTransferCertificateForm($id=null){
        try {
            $allUsers = User::where('user_type_id', 2)->get();
            $headMaster = '';
            foreach ($allUsers as $person){
                if ($person->hasRole('headmaster')){
                    $headMaster = config('app.locale') == 'en' ? $person->name:$person->profile->name_utf8;
                }
            }

            $transfer_certificate = TransferCertificate::where('id',$id)->where('status',true)->first();
            $student = Student::find($transfer_certificate->student_id);

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';

            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $mogib100 = asset('forntend/theme1/images/100.jpg');
//            $mogib100 = 'https://gendagps.com/forntend/theme1/images/100.jpg';

            $view = 'frontend.theme1.auth-clients.pages.transfer-certificates.tc-download';
            $qrcode = QrCode::size(90)->generate(route('download-transfer-certificate-form',$id));
            $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);

            $pdf = PDF::loadView($view, compact( 'transfer_certificate','qrcode', 'logo', 'groupLogo', 'bg', 'mogib100', 'avatar', 'headMaster', 'student'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 5,
                'margin_bottom'           => 5,
                'title'                => 'Application Form',
            ]);


            return $pdf->stream('transfer-certificate.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }
}
