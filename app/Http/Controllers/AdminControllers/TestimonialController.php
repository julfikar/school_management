<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTestimonialRequest;
use App\Models\Student;
use App\Models\Testimonial;
use App\Models\User;
use Illuminate\Http\Request;
use PHPUnit\Util\Test;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TestimonialController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'Testimonial';
            $testmonials = Testimonial::all();
            return view('backend.pages.testimonial.index', compact('title','testmonials'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }


    public function create()
    {
        try {
            $title = 'Testimonial Create';

            $students = Student::all();
            return view('backend.pages.testimonial.from', compact('title','students'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        try{
            $request->validate([
                'student_id' => 'required',
                'date_of_birth' => 'required|string|max:255',
                'village' => 'required|string|max:255',
                'post_office' => 'required|string|max:255',
                'upazila' => 'required|string|max:255',
                'district' => 'required|string|max:255',
                'academic_year' => 'required|string|max:255',
            ],[
                'student_id' => 'Student field is required',
                'date_of_birth' => 'Date of birth field is required',
                'village' => 'Village field is required',
                'post_office' => 'Post office field is required',
                'upazila' => 'Upozila field is required',
                'district' => 'District field is required',
                'academic_year' => 'Academic year field is required',
            ]);
            $student = Student::find($request->student_id);
//            dd($student);

            $data = new Testimonial();

            $data->student_id = $student->id;

            $data->name = $student->user->name;
            $data->father_name = $student->user->profile->father_name;
            $data->mother_name = $student->user->profile->mother_name;
            $data->village = $request->village;
            $data->post_office = $request->post_office;
            $data->upazila = $request->upazila;
            $data->district = $request->district;
            $data->date_of_birth = $request->date_of_birth;
            $data->roll = $student->class_rol;
            $data->education_year = $request->academic_year;
            $data->class = $student->classRoom->name;
            $data->headmaster_name = '$request->headmaster_name';
            $data->headmaster_phone = '$request->headmaster_phone';
            $data->headmaster_mobile = '$request->headmaster_mobile';
            $data->status = true;

            $data->save();


            $notification = array(
                'message' => 'Applied successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function download(Testimonial $testimonial){
        try {
            $allUsers = User::where('user_type_id', 2)->get();
            $headMaster = '';
            foreach ($allUsers as $person){
                if ($person->hasRole('headmaster')){
                    $headMaster = config('app.locale') == 'en' ? $person->name:$person->profile->name_utf8;
                }
            }

            $student = Student::find($testimonial->student_id);

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';


            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $mogib100 = asset('forntend/theme1/images/100.jpg');
//            $mogib100 = 'https://gendagps.com/forntend/theme1/images/100.jpg';

//            dd($student);
            $qrcode = QrCode::size(90)->generate(route('download-testimonial',$testimonial->id));
            $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);

            $view = 'backend.pages.testimonial.testimonial-download';

            $pdf = PDF::loadView($view, compact('logo', 'groupLogo', 'bg', 'mogib100', 'avatar', 'testimonial','qrcode', 'student', 'headMaster'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 5,
                'margin_bottom'           => 5,
                'title'                => 'Application Form',
            ]);

            $tname= 'testimonial'.rand(2,50);
            return $pdf->stream($tname.'.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function destroy(Testimonial $testimonial)
    {
        try {

            $testimonial->delete();

            $notification = [
                'message' =>  'Testimonial has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }



}
