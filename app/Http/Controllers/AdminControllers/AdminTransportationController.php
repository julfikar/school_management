<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Transportation;
use Illuminate\Http\Request;

class AdminTransportationController extends Controller
{


    public function __construct()
    {
        Parent::__construct();
    }


    public  function index(){
        try {
            $title = 'All Transportations';
            $transportations = Transportation::all();

            return view('backend.pages.transportations.index', compact('title', 'transportations'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function create(){
        try {
            $title = 'Add New Transportation';
            $employees = Employee::all();
            return view('backend.pages.transportations.create-edit', [
                'title' => $title,
                'employees' => $employees,
                'transportation'=>null
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'vehicle_reg_no' => 'required',
            'employee_id' => 'required',
            'vehicle_no' => 'required',
            'vehicle_type' => 'required',
        ], [
            'vehicle_reg_no' => 'Vehicle reg no is required.',
            'employee_id' => 'Employee is required.',
            'vehicle_no' => 'Vehicle no is required.',
            'vehicle_type' => 'Vehicle type is required.',

        ]);

        try {

            $transportation = new Transportation();

            $transportation->vehicle_reg_no = $request->input('vehicle_reg_no');
            $transportation->employee_id = $request->input('employee_id');
            $transportation->vehicle_no = $request->input('vehicle_no');
            $transportation->vehicle_type = $request->input('vehicle_type');

            $transportation->save();

            $notification = array(
                'message' => 'Transportation has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.transportations.index')->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show(Transportation $transportation)
    {
        try{
            $title = $transportation->name;
            $employees = Employee::all();
            return view('backend.pages.transportations.create-edit', [
                'title' => $title,
                'employees' => $employees,
                'transportation' => $transportation,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function update(Request $request,$id=null){

        $request->validate([
            'vehicle_reg_no' => 'required',
            'employee_id' => 'required',
            'vehicle_no' => 'required',
            'vehicle_type' => 'required',
        ], [
            'vehicle_reg_no' => 'Vehicle reg no is required.',
            'employee_id' => 'Employee is required.',
            'vehicle_no' => 'Vehicle no is required.',
            'vehicle_type' => 'Vehicle type is required.',

        ]);

        try {

            $transportation = Transportation::find($id);

            $transportation->vehicle_reg_no = $request->input('vehicle_reg_no');
            $transportation->employee_id = $request->input('employee_id');
            $transportation->vehicle_no = $request->input('vehicle_no');
            $transportation->vehicle_type = $request->input('vehicle_type');

            $transportation->save();

            $notification = array(
                'message' => 'Transportation has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.transportations.index')->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function destroy(Transportation $transportation){

        try {

            foreach ($transportation->passengers as $passenger){
                $passenger->delete();
            }

            $transportation->delete();

            $notification = [
                'message' =>  'Transportation has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.transportations.index')->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


}
