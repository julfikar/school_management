<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\VideoSlider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class WizardVideoSliderController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $title = 'Video Sliders';
            $video_sliders = VideoSlider::all();

            return view('backend.pages.wizards.video-sliders.index', compact('title','video_sliders'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(VideoSlider $videoSlider=null)
    {
        try {
            $title = $videoSlider?'Video Slider':'Add New Slider';
            return view('backend.pages.wizards.video-sliders.form', [
                'title' => $title,
                'videoSlider'=>$videoSlider
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, VideoSlider $videoSlider=null)
    {

        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }

            if(empty($videoSlider)){
                $videoSlider = new VideoSlider();
            }


            if ($request->hasFile('thumbnail')) {

                if ($videoSlider->thumbnail != null) {
                    // delete existing image
                    $file = 'upload/video-sliders/' . $videoSlider->thumbnail;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(1024, 760)->save(public_path('/upload/video-sliders/' . $filename));
                }
                $videoSlider->thumbnail = $filename;
            }

            $video_url=null;
            if (strpos($request->url, 'youtu.be/')) {
                $video_url = str_replace('youtu.be/', 'www.youtube.com/embed/', $request->url);
            }else if (strpos($request->url, '/vimeo.com/')){
                $video_url = str_replace('/vimeo.com/', '/player.vimeo.com/video/', $request->url);
            }elseif(strpos($request->url,'facebook.com')){
                $prefix = 'https://www.facebook.com/plugins/video.php?height=314&href=';
                $safix = '&show_text=false&width=560&t=0';
                $request_video_url = $request->url;
                $input_video = str_replace(':','%3A',$request_video_url);
                $input_video = str_replace('/','%2F',$input_video);
                $input_video = str_replace('?sfnsn=wa','',$input_video);
                $video_url = $prefix.$input_video.$safix;
            }else{
                $video_url = $request->url;
            }


            $videoSlider->url = $video_url;

            $videoSlider->save();

            $notification = array(
                'message' => 'Video has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function active_main_slider(VideoSlider $videoSlider)
    {
        try{

            $vsid = $videoSlider->id;

            DB::table('video_sliders')
                ->chunkById(100, function ($video_sliders) use($vsid) {
                    foreach ($video_sliders as $video_slider) {
                        if($video_slider->id==$vsid){
                            DB::table('video_sliders')
                                ->where('id', $video_slider->id)
                                ->update(['main_video' => true]);
                        }else{
                            DB::table('video_sliders')
                                ->where('id', $video_slider->id)
                                ->update(['main_video' => false]);
                        }
                    }
                });

            return response()->json($videoSlider);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(VideoSlider $videoSlider)
    {

        try {

            if ($videoSlider->thumbnail != null) {
                // delete existing image
                $file = 'upload/video-sliders/' . $videoSlider->thumbnail;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $videoSlider->delete();

            $notification = [
                'message' =>  'Video has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }




}
