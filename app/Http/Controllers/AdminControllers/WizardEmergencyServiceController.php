<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\EmergencyServices;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class WizardEmergencyServiceController extends Controller
{
    public function index()
    {
        try {
            $title = 'All Emergency Services';
            $services = EmergencyServices::all();
            return view('backend.pages.wizards.emergency-services.index', compact('title','services'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function create()
    {
        try {
            $title = 'Add Emergency Service';
            return view('backend.pages.wizards.emergency-services.form', [
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required|max:255',
            'phone' => 'required|unique:emergency_services',
        ]);

        try{
            $data = new EmergencyServices();
            $data->name = $request->name;
            $data->phone = $request->phone;
            $data->save();
            $notification = array(
                'message' => 'Service has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function edit($request){
        try {
            $title = 'Edit Emergency Services';
            $service = EmergencyServices::find($request);
            return view('backend.pages.wizards.emergency-services.update', compact('title','service'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }

    public function update(Request $request, $id){

        $data = EmergencyServices::find($id);
        $request->validate([
            'name' => 'required|max:255',
            'phone' => ['required', 'string', 'max:14', Rule::unique('emergency_services')->ignore($data->id)],
        ]);
        try{
            $data->name = $request->name;
            $data->phone = $request->phone;
            $data->save();
            $notification = array(
                'message' => 'Service has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function destroy($id){
        try {

            $services= EmergencyServices::find($id);
            $services->delete();
            $notification = [
                'message' =>  'Service has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
}
