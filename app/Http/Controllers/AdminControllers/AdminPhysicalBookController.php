<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\LibraryMember;
use App\Models\PhysicalBook;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class AdminPhysicalBookController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public  function index(){
        try {
            $title = 'All Physical Books';
            $physicalbooks = PhysicalBook::all();
            $members = LibraryMember::all();
            return view('backend.pages.physical-books.index', compact('title', 'physicalbooks', 'members'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function create(PhysicalBook $physicalBook = null){
        try {
            $title = 'Add New Physical Book';
            return view('backend.pages.physical-books.create-edit', [
                'title' => $title,
                'physicalBook'=>$physicalBook
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required|max:255',
            'author' => 'required|max:255',
        ], [
            'name' => 'Book name is required.',
            'author' => 'Book author name is required.',
        ]);

        try {
            $physical_book = new PhysicalBook();

            $physical_book->name = $request->input('name');
            $physical_book->author = $request->input('author');
            $physical_book->insert_date = $request->input('insert_date')?$request->input('insert_date'):date('Y-m-d',time()).'T'.date('H:i',time());
            $physical_book->in_date = $request->input('insert_date')?$request->input('insert_date'):date('Y-m-d',time()).'T'.date('H:i',time());
            $physical_book->save();

            $notification = array(
                'message' => 'Physical Book has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.physicalbooks.index')->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function update(Request $request, PhysicalBook $physicalBook){

        $request->validate([
            'name' => 'required|max:255',
            'author' => 'required|max:255',
        ], [
            'name' => 'Book name is required.',
            'author' => 'Book author name is required.',
        ]);

        try {
            $physicalBook->name = $request->input('name');
            $physicalBook->author = $request->input('author');
            $physicalBook->insert_date = $request->input('insert_date')?$request->input('insert_date'):date('Y-m-d',time()).'T'.date('H:i',time());
            $physicalBook->save();

            $notification = array(
                'message' => 'Physical Book has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.physicalbooks.index')->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function destroy(PhysicalBook $physicalBook)
    {
        try {
            $physicalBook->delete();

            $notification = array(
                'message' => 'Physical Book has been removed successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.physicalbooks.index')->with($notification);
        }catch (\Throwable $e) {
            $notification = (object)[
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function assignToMember(Request $request)
    {
        $request->validate([
            'out_date' => 'required',
            'member' => 'required',
        ], [
            'out_date' => 'When did this book go out?',
            'member' => 'Who took the book?'
        ]);

        $book = PhysicalBook::find($request->row);
        $member = LibraryMember::find($request->member);

        if (!$member){
            $notification = (object)[
                'message' => 'This member is invalid....',
            ];
            return response()->json($notification);
        }

        if (!$book){
            $notification = (object)[
                'message' => 'An Error occur....',
            ];
            return response()->json($notification);
        }

        if($member->book){
            $notification = (object)[
                'message' => $member->user->name. ' had already taken a book Name: '.$member->book->name.' Author: '.$member->book->author.' please return that before borrowing new one.',
            ];
            return response()->json($notification);
        }

        if (strtotime($request->out_date) > time()){
            $notification = (object)[
                'message' => 'You cannot borrow a book in advance. We accept only current date and previous date.',
            ];
            return response()->json($notification);
        }

        try {
            $book->in_date = null;
            $book->out_date = $request->out_date;
            $book->library_member_id = $member->id;
            $book->save();
//            return response()->json($book);
        }catch (\Throwable $e){

        }
    }

    public function revokeToMember(Request $request){
        $request->validate([
            'in_date' => 'required',
        ], [
            'in_date' => 'When the book is back in the library?',
        ]);

        $book = PhysicalBook::find($request->row);

        if (!$book){
            $notification = (object)[
                'message' => 'An Error occur....',
            ];
            return response()->json($notification);
        }

        if (strtotime($request->in_date) > time()){
            $notification = (object)[
                'message' => 'You can not return a book in advance. We accept only current date and previous date.',
            ];
            return response()->json($notification);
        }
        try {
            $book->in_date = $request->in_date;
            $book->library_member_id = null;
            $book->out_date = null;
            $book->save();
//            return response()->json($book);
        }catch (\Throwable $e){

        }
    }
}
