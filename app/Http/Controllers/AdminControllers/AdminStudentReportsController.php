<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Exam;
use App\Models\ExamCategory;
use App\Models\Student;
use App\Models\StudentExam;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AdminStudentReportsController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'Student Reports';

            return view('backend.pages.reports.student-reports.index', compact('title'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function attendanceCalculate(Request $request)
    {
        try {
            $title = 'Student Reports';
            $student = Student::where('unique_id',$request->unique_id)->first();
            if($student==null){
                $notification = [
                    'message' => 'Student not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }

            $student_name = $student->user->name;

            $sdate = $request->start_date;
            $edate = $request->end_date;
            $datetime1 = strtotime($sdate); // convert to timestamps
            $datetime2 = strtotime($edate); // convert to timestamps
            $total_days = (int)(($datetime2 - $datetime1) / 86400);// will give the difference in days , 86400 is the timestamp difference of a day

            $fridays = count($this->countFridays($sdate,$edate));
            $working_days =$total_days - $fridays;

            $total_attendece =0;
            $days = [];
            // Loop between timestamps, 24 hours at a time
            for ( $i = $datetime1; $i <= $datetime2; $i = $i + 86400 ) {
                $thisDate = date( 'Y-m-d', $i ); // 2010-05-01, 2010-05-02, etc

                $attend = $student->attendents()->where('date', $thisDate)->first();
                if($attend){
                    $total_attendece++;
                    $days[] = (object)[
                        'date' => $thisDate,
                        'status' => true,
                        'attend_time' => date('h:i:s a', strtotime($attend->date))
                    ];
                }else{
                    $days[] = (object)[
                        'date' => $thisDate,
                        'status' => false,
                        'attend_time' => null
                    ];
                }
            }

            $total_absent =$working_days-$total_attendece;

            if($working_days==$total_absent){
                $notification = [
                    'message' => ucwords($student_name).' attendance report not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }

            return view('backend.pages.reports.student-reports.show-attendance', compact('title','student_name','total_attendece','total_absent','days'));

        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }

    public function countFridays($dateFromString,$dateToString){
        $dateTo = Carbon::parse($dateToString);
        $friday = Carbon::parse($dateFromString . ' next friday');
        $fridays = [];

        while($friday->lt($dateTo)) {
            $fridays[] = $friday->toDateString();
            $friday->addWeek();
        }

        return $fridays;
    }


    public function examResultCalculate(Request $request)
    {
        try {
            $title = 'Student Exam Result';
            $student = Student::where('unique_id',$request->unique_id)->first();
            if($student==null){
                $notification = [
                    'message' => 'Student not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $student_name = $student->user->name;

            $sdate = $request->start_date;
            $edate = $request->end_date;
            $datetime1 = strtotime($sdate); // convert to timestamps
            $datetime2 = strtotime($edate); // convert to timestamps

            $exams = Exam::whereBetween('created_at', [$sdate,$edate])->get();

            $results = [];
            foreach ($exams as $key => $exam){
               $exists = StudentExam::where(['exam_id'=>$exam->id, 'student_id'=>$student->unique_id])->get();
               if($exists==true){
                   $results[]=$exists;
               }
            }


            return view('backend.pages.reports.student-reports.show-exams', compact('title','student_name','results'));

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }

    }


    /**
     * For Total accounts report
     */
    public function accountsCalculate(Request $request){

        try{

            $data['title'] = 'Student Accounts Report';
            $student = Student::where('unique_id',$request->unique_id)->first();
            if($student==null){
                $notification = [
                    'message' => 'Student not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $student_name = $student->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            //for month count between two date
            $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
            $month_count = $period->count();

            // for tution fee
            if($student->tutionFees) {
                $class_tution_fee = (int)$this->getClassWiseFee('tution', $student->classRoom->name);
                $total_get_tution_fee = $month_count * $class_tution_fee;
                $payed_tution=(int)$student->tutionFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_tution=(int)$student->tutionFees()->whereBetween('created_at', [$start_date, $end_date])->sum('discount');
                $total_paid_tution_fee =$payed_tution+$discount_tution ;
            }else{
                $total_get_tution_fee=0;
                $total_paid_tution_fee=0;
            }

            // for exam fee
            if($student->examFees) {
                $class_exam_fee = (int)$this->getClassWiseFee('exam', $student->classRoom->name);
                $how_many_exams = ExamCategory::whereBetween('created_at', [$start_date,$end_date])->count();
                $total_get_exam_fee = $how_many_exams * $class_exam_fee;
                $payed_exam = (int)$student->examFees()->where('student_id', $student->id)->sum('payed_amount');

                $discount_exam = (int)$student->examFees()->whereBetween('created_at', [$start_date, $end_date])->sum('discount');
                $total_paid_exam_fee = $payed_exam+$discount_exam;
            }else{
                $total_get_exam_fee=0;
                $total_paid_exam_fee=0;
            }

            // for hostel fee
            if($student->user->dweller) {

                $school_hostel_fee = setting('school_fees.hostel_fee.hostel_fee');
                $total_get_hostel_fee = $month_count * $school_hostel_fee;
                $payed_hostel = (int)$student->user->dweller->hostelFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_hostel = (int)$student->user->dweller->hostelFees()->whereBetween('created_at', [$start_date, $end_date])->sum('discount');
                $total_paid_hostel_fee = $payed_hostel+$discount_hostel;
            }else{

                $total_get_hostel_fee=0;
                 $total_paid_hostel_fee=0;
            }

            // for transportation fee
            if($student->user->passenger) {
                $school_transportation_fee = setting('school_fees.transportation_fee.transportation_fee');
                $total_get_transportation_fee = $month_count * $school_transportation_fee;
                $payed_passenger = (int)$student->user->passenger->transportationFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_passenger = (int)$student->user->passenger->transportationFees()->whereBetween('created_at', [$start_date, $end_date])->sum('discount');
                $total_paid_transportation_fee = $payed_passenger+$discount_passenger;
            }else{
                $total_get_transportation_fee=0;
                $total_paid_transportation_fee=0;
            }

            // For library Fee
            if($student->user->libraryMember) {
                $school_library_fee = setting('school_fees.library_fee.library_fee');
                $total_get_library_fee = $month_count * $school_library_fee;
                $payed_member = (int)$student->user->libraryMember->libraryFee()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_member = (int)$student->user->libraryMember->libraryFee()->whereBetween('created_at', [$start_date, $end_date])->sum('discount');
                $total_paid_library_fee = $payed_member + $discount_member;
            }else{
                $total_get_library_fee=0;
                $total_paid_library_fee=0;
            }

            $all_fees_paid_amount =$total_paid_tution_fee+$total_paid_exam_fee+$total_paid_hostel_fee+$total_paid_transportation_fee+$total_paid_library_fee;
            $all_fees_get_amount = $total_get_tution_fee+$total_get_exam_fee+$total_get_hostel_fee+$total_get_transportation_fee+$total_get_library_fee;


            $data['student_name'] = $student_name;
            $data['unique_id'] = $request->unique_id;
            $data['start_date'] = $request->start_date;
            $data['end_date'] = $request->end_date;
            $data['total_get_tution_fee'] =  $total_get_tution_fee;
            $data['total_paid_tution_fee'] =  $total_paid_tution_fee;
            $data['total_get_exam_fee'] =  $total_get_exam_fee;
            $data['total_paid_exam_fee'] =  $total_paid_exam_fee;
            $data['total_get_hostel_fee'] =  $total_get_hostel_fee;
            $data['total_paid_hostel_fee'] =  $total_paid_hostel_fee;
            $data['total_get_transportation_fee'] =  $total_get_transportation_fee;
            $data['total_paid_transportation_fee'] =  $total_paid_transportation_fee;
            $data['total_get_library_fee'] =  $total_get_library_fee;
            $data['total_paid_library_fee'] =  $total_paid_library_fee;

            $data['all_fees_paid_amount'] =  $all_fees_paid_amount;
            $data['all_fees_get_amount'] =  $all_fees_get_amount;

            $tutionfees = $this->singleTutionFeeReport($request);
            $examfees = $this->singleExamFeeReport($request);
            $hostelFees = $this->singleHostelFeeReport($request);
            $libraryFees = $this->singleLibraryFeeReport($request);
            $transportationFees = $this->singleTransportationFeeReport($request);

            $data2 = [
                $tutionfees,
                $hostelFees,
                $examfees,
                $libraryFees,
                $transportationFees
            ];
            $data['data2']=$data2;

            return view('backend.pages.reports.student-reports.show-main-account',$data);

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    /**
     * For Exam fees month wise report
     */
    public function singleTutionFeeReport(Request $request){
        try{

            $student = Student::where('unique_id',$request->unique_id)->first();

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));
            $class_tution_fee = (int)$this->getClassWiseFee('tution',$student->classRoom->name);

            $new_reports=[];
            $i=0;

            $total_fees_payed=0;
            $total_payable_fees=0;

            //for month count between two date
            $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
            $total_payable_fees = (int)$class_tution_fee *  $period->count();

            //  for each month list
            foreach ($period as $dt) {
                $discount_amount = null;
                $payment_date=null;
                $dueamount =null;

                $tution_fee_find = $student->tutionFees()->where('month',$dt->format('m'))->whereBetween('created_at', [$start_date,$end_date])->first();

                if($tution_fee_find!=null) {
                    if ($tution_fee_find->payed_amount == $class_tution_fee) {
                        $payedamount = $tution_fee_find->payed_amount;
                    } else {
                        $payedamount = $tution_fee_find->payed_amount;
                        $discount_amount = $tution_fee_find->discount;
                    }
                    $dueamount = $tution_fee_find->due?$tution_fee_find->due:null;

                    $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                    $payment_date=date('d-m-Y',strtotime($tution_fee_find->created_at));
                }else{
                    $payedamount=0;
                    $dueamount=$class_tution_fee;
                }

                $new_reports[$i]=[
                    'name'=>'Tution Fee',
                    'date'=>$payment_date?$payment_date:$dt->format('d-m-Y'),
                    'paid'=>(int)$payedamount,
                    'discount'=>(int)$discount_amount,
                    'due'=>(int)$dueamount,
                ];

                $i++;

            }

            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed
            ];

            return $data;

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * For Exam fees month wise report
     */
    public function singleExamFeeReport(Request $request){

        try{

            $student = Student::where('unique_id',$request->unique_id)->first();

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));
            $class_exam_fee = (int)$this->getClassWiseFee('exam',$student->classRoom->name);

            $new_reports=[];
            $i=0;


            $how_many_exams = ExamCategory::whereBetween('created_at', [$start_date,$end_date])->get();

            $payable_total_exam_fee =0;
            $payment_date=null;
            $total_fees_payed=0;

            foreach ($how_many_exams as $how_many_exam){

                $payable_total_exam_fee += $class_exam_fee;

                $n_date = date('d-m-Y',strtotime($how_many_exam->created_at));

                $exam_fee_find = $student->examFees()->where('exam_category_id',$how_many_exam->id)->first();

                if($exam_fee_find!=null) {
                    if ($exam_fee_find->payed_amount == $class_exam_fee) {
                        $payedamount = $exam_fee_find->payed_amount;
                        $dueamount = $exam_fee_find->due;
                    } else {
                        $payedamount = $exam_fee_find->discount?$exam_fee_find->payed_amount+$exam_fee_find->discount:$exam_fee_find->payed_amount;
                        $dueamount = $exam_fee_find->due;
                    }
                    $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                    $payment_date=date('d-m-Y',strtotime($exam_fee_find->created_at));
                }else{
                    $payedamount=0;
                    $dueamount=$class_exam_fee;
                }

                $new_reports[$i]=[
                    'name'=>'Exam '.$how_many_exam->name,
                    'date'=>$payment_date?$payment_date:$n_date,
                    'paid'=>(int)$payedamount,
                    'discount'=>0,
                    'due'=>(int)$dueamount,
                ];

                $i++;
                $payment_date=null;

            }

            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $payable_total_exam_fee,
                'total_fees_payed' => $total_fees_payed
            ];

            return $data;

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * For hostel fees month wise report
     */
    public function singleHostelFeeReport(Request $request){

        try{

            $student = Student::where('unique_id',$request->unique_id)->first();


            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            $school_hostel_fee = setting('school_fees.hostel_fee.hostel_fee');

            $new_reports=[];
            $i=0;
            $payment_date =null;
            $total_fees_payed=0;
            $total_payable_fees=0;

            // for hostel fee
            if($student->user->dweller) {

                //for month count between two date
                $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
                $total_payable_fees = (int)$school_hostel_fee *  $period->count();

                //  for each month list
                foreach ($period as $dt) {
                    $discount_amount =null;
                    $payment_date=null;
                    $dueamount =null;

                    $hostel_fee_find = $student->user->dweller->hostelFees()->where('month', $dt->format('m'))->whereBetween('created_at', [$start_date, $end_date])->first();

                    if ($hostel_fee_find != null) {
                        if ($hostel_fee_find->payed_amount == $school_hostel_fee) {
                            $payedamount = $hostel_fee_find->payed_amount;
                        } else {
                            $payedamount = $hostel_fee_find->payed_amount;
                            $discount_amount = $hostel_fee_find->discount;
                        }

                        $dueamount = $hostel_fee_find->due?$hostel_fee_find->due:null;
                        $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                        $payment_date=date('d-m-Y',strtotime($hostel_fee_find->created_at));
                    } else {
                        $payedamount = 0;
                        $dueamount = $school_hostel_fee;
                    }

                    $new_reports[$i] = [
                        'name'=>'Hostel Fees',
                        'date' => $payment_date?$payment_date:$dt->format('d-m-Y'),
                        'paid' => (int)$payedamount,
                        'discount' => (int) $discount_amount,
                        'due' => (int)$dueamount,
                    ];

                    $i++;
                }


            }

            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed
            ];

            return $data;

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    /**
     * For Library fees month wise report
     */
    public function singleLibraryFeeReport(Request $request){

        try{

            $student = Student::where('unique_id',$request->unique_id)->first();

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            $school_library_fee = setting('school_fees.library_fee.library_fee');

            $new_reports=[];
            $i=0;
            $payment_date =null;
            $total_fees_payed=0;
            $total_payable_fees=0;

            // for hostel fee
            if($student->user->libraryMember) {

                //for month count between two date
                $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);

                $total_payable_fees = (int)$school_library_fee *  $period->count();

                //  for each month list
                foreach ($period as $dt) {
                    $discount_amount =null;
                    $payment_date=null;
                    $dueamount =null;

                    $library_fee_find = $student->user->libraryMember->libraryFee()->where('month', $dt->format('m'))->whereBetween('created_at', [$start_date, $end_date])->first();

                    if ($library_fee_find != null) {
                        if ($library_fee_find->payed_amount == $school_library_fee) {
                            $payedamount = $library_fee_find->payed_amount;
                        } else {
                            $payedamount = $library_fee_find->payed_amount;
                            $discount_amount = $library_fee_find->discount;
                        }

                        $dueamount = $library_fee_find->due?$library_fee_find->due:null;
                        $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                        $payment_date=date('d-m-Y',strtotime($library_fee_find->created_at));
                    } else {
                        $payedamount = 0;
                        $dueamount = $school_library_fee;
                    }

                    $new_reports[$i] = [
                        'name'=>'Library Fee',
                        'date' => $payment_date?$payment_date:$dt->format('d-m-Y'),
                        'paid' => (int)$payedamount,
                        'discount' => 0,
                        'due' => (int)$dueamount,
                    ];
                    $payment_date =null;
                    $i++;
                }

            }

            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed
            ];

            return $data;

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * For Transportation fees month wise report
     */
    public function singleTransportationFeeReport(Request $request){

        try{
            $title = 'Student Transportation Accounts Report';
            $student = Student::where('unique_id',$request->unique_id)->first();
            $student_name = $student->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            $school_transportation_fee = setting('school_fees.transportation_fee.transportation_fee');

            $new_reports=[];
            $i=0;
            $payment_date =null;
            $total_fees_payed=0;
            $total_payable_fees=0;

            // for hostel fee
            if($student->user->passenger) {

                //for month count between two date
                $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
                $total_payable_fees = (int)$school_transportation_fee *  $period->count();

                //  for each month list
                foreach ($period as $dt) {
                    $discount_amount =null;
                    $payment_date=null;
                    $dueamount =null;

                    $transportation_fee_find = $student->user->passenger->transportationFees()->where('month', $dt->format('m'))->whereBetween('created_at', [$start_date, $end_date])->first();

                    if ($transportation_fee_find != null) {
                        if ($transportation_fee_find->payed_amount == $school_library_fee) {
                            $payedamount = $transportation_fee_find->payed_amount;
                        } else {
                            $payedamount = $transportation_fee_find->payed_amount;
                            $discount_amount = $transportation_fee_find->discount;
                        }

                        $dueamount = $transportation_fee_find->due?$transportation_fee_find->due:null;
                        $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                        $payment_date=date('d-m-Y',strtotime($transportation_fee_find->created_at));
                    } else {
                        $payedamount = 0;
                        $dueamount = $school_transportation_fee;
                    }

                    $new_reports[$i] = [
                        'name'=>'Transportation Fee',
                        'date' => $payment_date?$payment_date:$dt->format('d-m-Y'),
                        'paid' => (int)$payedamount,
                        'discount' => 0,
                        'due' => (int)$dueamount,
                    ];
                    $payment_date =null;
                    $i++;
                }


            }

            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed,
            ];

            return $data;

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    // Return tution fees and exam fees by class
    public function getClassWiseFee($type,$className){
        try{
            switch ($className) {
                case 'pre-primary':
                    $classFee = setting('school_fees.'.$type.'_fee.pre_primary_fee');
                    break;
                case 'class one':
                    $classFee =setting('school_fees.'.$type.'_fee.class_one_fee');
                    break;
                case 'class two':
                    $classFee = setting('school_fees.'.$type.'_fee.class_two_fee');
                    break;
                case 'class three':
                    $classFee = setting('school_fees.'.$type.'_fee.class_three_fee');
                    break;
                case 'class four':
                    $classFee = setting('school_fees.'.$type.'_fee.class_four_fee');
                    break;
                case 'class five':
                    $classFee = setting('school_fees.'.$type.'_fee.class_five_fee');
                    break;
            }
            return $classFee;

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


}
