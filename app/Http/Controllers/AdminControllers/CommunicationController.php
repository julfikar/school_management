<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\Employee;
use App\Models\SendMail;
use App\Models\SendSMS;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;

class CommunicationController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['auth', 'admin']);
    }

    public function phoneCall()
    {
        try {
            return view('backend.pages.communicatino.phone-call',[
                'title' => 'Call on phone',
                'data' => (object)[
                    'teachers' => Teacher::where('status', true)->get(),
                    'employees' => Employee::where('status', true)->get(),
                    'classRooms' => ClassRoom::all(),
                    'students' => Student::where('status', true)->get()
                ]
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function smsIndex()
    {
        try {
            return view('backend.pages.communicatino.sms-index',[
                'title' => 'Send sms',
                'data' => (object)[
                    'teachers' => Teacher::where('status', true)->get(),
                    'employees' => Employee::where('status', true)->get(),
                    'classRooms' => ClassRoom::all(),
                    'students' => Student::where('status', true)->get()
                ]
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function sendSms(Request $request)
    {
        $request->validate([
            'massage' => 'required',
            'user' => 'required','min:1',
            'user_type' => 'required'
        ],[
            'massage' => 'This massage field is required',
            'user' => 'You don\'t select any user',
            'user_type' => 'An invalid entry'
        ]);

        if ($request->user_type == 'teachers_content'){
            $userType = 'all'.ucwords(str_replace('s_content','',$request->user_type));
            if (in_array($userType,$request->user)){
                $teacher = Teacher::where('status',true)->get();
            }else{
                $teacher = Teacher::findOrFail($request->user);
            }
            $sender = new SendSMS($teacher,$request->massage);
        }elseif ($request->user_type == 'employees_content'){
            $userType = 'all'.ucwords(str_replace('s_content','',$request->user_type));
            if (in_array($userType,$request->user)){
                $employee = Employee::where('status',true)->get();
            }else{
                $employee = Employee::findOrFail($request->user);
            }
            $sender = new SendSMS($employee,$request->massage);
        }elseif ($request->user_type == 'students_content'){
            $userType = 'all'.ucwords(str_replace('s_content','',$request->user_type));
//            dd($userType);
            $student = [];
            if (in_array($userType,$request->user)){
                $student = Student::where('status',true)->get();
            }

            foreach (ClassRoom::all() as $room) {
                $content = 'all_'. str_replace(' ','',$room->name) .'_student';
                if (in_array($content, $request->user)){
                    $student = $room->student()->where('status', true)->get();
                    break;
                }
            }

            if (count($student) == 0){
                $student = Student::findOrFail($request->user);
            }

            $sender = new SendSMS($student,$request->massage);
        }


        $action = $sender->sent();
        if ($action['alert-type'] == 'success'){
            return $this->backWithSuccess($action['message']);
        }else{
            return $this->backWithError($action['message']);
        }
    }

    public function emailIndex()
    {
        try {
            return view('backend.pages.communicatino.email-index',[
                'title' => 'Send email',
                'data' => (object)[
                    'teachers' => Teacher::where('status', true)->get(),
                    'employees' => Employee::where('status', true)->get(),
                    'classRooms' => ClassRoom::all(),
                    'students' => Student::where('status', true)->get()
                ]
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function sendEmail(Request $request)
    {
        $request->validate([
            'massage' => 'required',
            'subject' => 'required',
            'user' => 'required','min:1',
            'user_type' => 'required'
        ],[
            'massage' => 'This massage field is required',
            'subject' => 'This subject field is required',
            'user' => 'You don\'t select any user',
            'user_type' => 'An invalid entry'
        ]);

        if ($request->user_type == 'teachers_content'){
            $userType = 'all'.ucwords(str_replace('s_content','',$request->user_type));
            if (in_array($userType,$request->user)){
                $teacher = Teacher::where('status',true)->get();
            }else{
                $teacher = Teacher::findOrFail($request->user);
            }
            $sender = new SendMail($teacher, $request->subject, $request->massage);
        }elseif ($request->user_type == 'employees_content'){
            $userType = 'all'.ucwords(str_replace('s_content','',$request->user_type));
            if (in_array($userType,$request->user)){
                $employee = Employee::where('status',true)->get();
            }else{
                $employee = Employee::findOrFail($request->user);
            }
            $sender = new SendMail($employee, $request->subject, $request->massage);
        }elseif ($request->user_type == 'students_content'){
            $userType = 'all'.ucwords(str_replace('s_content','',$request->user_type));
//            dd($userType);
            $student = [];
            if (in_array($userType,$request->user)){
                $student = Student::where('status',true)->get();
            }

            foreach (ClassRoom::all() as $room) {
                $content = 'all_'. str_replace(' ','',$room->name) .'_student';
                if (in_array($content, $request->user)){
                    $student = $room->student()->where('status', true)->get();
                    break;
                }
            }

            if (count($student) == 0){
                $student = Student::findOrFail($request->user);
            }

            $sender = new SendMail($student, $request->subject, $request->massage);
        }


        $action = $sender->sent();
        if ($action['alert-type'] == 'success'){
            return $this->backWithSuccess($action['message']);
        }else{
            return $this->backWithError($action['message']);
        }
    }
}
