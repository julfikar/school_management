<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateExamRequest;
use App\Http\Requests\UpdateExamRequest;
use App\Models\ClassRoom;
use App\Models\Exam;
use App\Models\ExamCategory;
use App\Models\Student;
use App\Models\Subject;
//use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Mpdf\Mpdf;
use PDF;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return view('backend.pages.exams.index', [
                'title' => 'Exams',
                'exams' => ExamCategory::where('parent_id', null)->get(),
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            return view('backend.pages.exams.create-edit', [
                'title' => 'Create Exam',
                'classRoom' => ClassRoom::find($request->class_room),
                'category' => ExamCategory::find($request->exam_category),
                'exam' => null
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function createBulk(Request $request)
    {
        try {
            return view('backend.pages.exams.create-bulk', [
                'title' => 'Create Bulk Exam',
                'classRoom' => ClassRoom::find($request->class_room),
                'category' => ExamCategory::find($request->exam_category),
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateExamRequest $request)
    {
        $inputs = $request->validated();
        try {
            if (Exam::where(['class_room_id' => $inputs['class_room_id'], 'category_id' => $inputs['category_id'], 'subject_id' => $inputs['subject_id']])->whereYear('created_at', '=', date('Y', time()))->count() > 0){
                return $this->backWithError('This exam is already created');
            }

            $totalSubjectMark = Subject::findOrFail($inputs['subject_id'])->total_mark;
            $previousSubjectExams = Exam::where('subject_id', $inputs['subject_id'])->get();
            $totalUsedMark = 0;
            foreach ($previousSubjectExams as $previousSubjectExam) {
                $totalUsedMark += $previousSubjectExam->marks;
            }
            $totalUsedMark += $inputs['marks'];
            if ($totalSubjectMark < $totalUsedMark) {
                return $this->backWithError('Use less than ' . ($totalSubjectMark - ($totalUsedMark - $inputs['marks'])) . ' marks.');
            }


            $questioPath = $request->file('question')->store('upload/exam/question');
            $answerPath = null;
            if ($request->hasFile('answer')) {
                $answerPath = $request->file('answer')->store('upload/exam/answer');
            }

            $inputs['question'] = $questioPath;
            $inputs['answer'] = $answerPath;

            $exam = Exam::create($inputs);

            return redirect()->route('admin.exam.category.by-class', $inputs['category_id'])->with([
                'message' => 'Exam Created Successfully',
                'alert-type' => 'success'
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function storeBulk(Request $request)
    {
        try {
            $subjects = $request->subject_id;

            foreach ($subjects as $key => $subject) {
                Exam::create([
                    'class_room_id' => $request->class_room_id,
                    'name' => $request->name,
                    'category_id' => $request->category_id,
                    'subject_id' => $request->subject_id[$key],
                    'date' => $request->date[$key],
                    'start_at' => $request->start_at[$key],
                    'end_at' => $request->end_at[$key],
                    'min_mark' => $request->marks[$key] > 51 ?'33':'17',
                    'marks' => $request->marks[$key],
                ]);
            }

            return $this->backWithSuccess('Exam created successfully');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showExams(Request $request)
    {
        try {
            return view('backend.pages.exams.exam-index', [
                'title' => 'Exams',
                'category' => ExamCategory::find($request->exam_category),
                'classRoom' => ClassRoom::find($request->class_room),
                'exams' => Exam::where(['class_room_id' => $request->class_room, 'category_id' => $request->exam_category])->whereYear('created_at', '=', date('Y', time()))->get(),
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $exam = Exam::findOrFail($id);
            return view('backend.pages.exams.create-edit', [
                'title' => 'Create Exam',
                'classRoom' => ClassRoom::find($exam->class_room_id),
                'category' => ExamCategory::find($exam->category_id),
                'exam' => $exam,
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateExamRequest $request, $id)
    {
        $inputs = $request->validated();

        try {
            $exam = Exam::findOrFail($id);

            $totalSubjectMark = Subject::findOrFail($inputs['subject_id'])->total_mark;
            $previousSubjectExams = Exam::where('subject_id', $inputs['subject_id'])->get();
            $totalUsedMark = 0;
            foreach ($previousSubjectExams as $previousSubjectExam) {
                $totalUsedMark += $previousSubjectExam->marks;
            }

            $totalUsedMark -= $exam->marks;

            $totalUsedMark += $inputs['marks'];
            if ($totalSubjectMark < $totalUsedMark) {
                // $this->backWithError('Use less than ' . ($totalSubjectMark - ($totalUsedMark - $inputs['marks'])) . ' marks.');
                return back()->with([
                    'message' => 'Use less than or equal to ' . ($totalSubjectMark - ($totalUsedMark - $inputs['marks'])) . ' marks.',
                    'alert_type' => 'error'
                ]);
            }

            if ($request->hasFile('question')) {
                Storage::delete($exam->question);
                $questioPath = $request->file('question')->store('upload/exam/question');
                $exam->update(['question' => $questioPath]);
            }

            if ($request->hasFile('answer')) {
                Storage::delete($exam->answer);
                $answerPath = $request->file('answer')->store('upload/exam/answer');
                $exam->update(['answer' => $answerPath]);
            }


            $exam->update(Arr::except($inputs, ['question', 'answer']));

            return $this->backWithSuccess('Updated Successfully');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam $exam)
    {
        try {
            if ($exam->question != null) {
                // delete existing image
                $file =  $exam->question;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            if ($exam->answer != null) {
                // delete existing image
                $file1 = $exam->answer;
                if (file_exists(public_path($file1))) {
                    unlink(public_path($file1));
                }
            }

            $exam->delete();

            return $this->backWithSuccess('Deleted Successfully.');
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function filterByClass($classId)
    {
        $filterdExam = Exam::where('class_room_id', $classId)->get();

        return response()->json(['exams' => $filterdExam]);
    }

    public function selectExamView()
    {
        try {
            return view('backend.pages.exams.routine', [
                'title' => 'Select Exam to get Routine',
                'classes' => ClassRoom::all(),
            ]);
        } catch (\Throwable $th) {
        }
    }

    public function routineDownload(Request $request)
    {
        $pdf = App::make('dompdf.weapper');
        $routine = $pdf->loadView('backend.pages.exams.routine');
        return $routine->download('routine.pdf');
    }

    public function getDistinctValueByClass($classId)
    {
        $exams = Exam::where('class_room_id', $classId)->get()->unique('name');


        return response()->json(['exams' => $exams]);
    }

    public function filter(Request $request)
    {
        $filterdExams = Exam::with('subject')
            ->where('class_room_id', $request->class_id)
            ->where('name', $request->exam_name)
            ->get();

        return response()->json(['exams' => $filterdExams]);
    }

    public function routineShow()
    {
        return view('backend.pages.exams.routine-show');
    }

    public function downloadRoutine(Request $request)
    {
        $request->validate([
            'class_room_id' => 'required',
            'exam_id' => 'required',
        ]);

        $mpdf = new Mpdf([
            'default_font_size' => 14,
            'default_font' => 'kalpurush',
            'orientation' => 'L',
            'margin_left'          => 10,
            'margin_right'         => 10,
            'margin_top'           => 10,
            'title'                => 'Class Routine',
        ]);
        $mpdf->writeHTML($this->output($request->exam_id, $request->class_room_id));
        $mpdf->Output();
    }

    protected function outPut($examName, $class){
        try {
            $class = ClassRoom::findOrFail($class);
            $exams = Exam::where(['class_room_id' => $class->id, 'name' => $examName])
                ->get();

            $row = [];
            foreach ($exams as $exam) {
                $date = '<td style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . date('d-m-Y', strtotime($exam->date)) . '</td>';
                if ($exam->subject) {
                    $subject = '<td style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . $exam->subject->name . '</td>';
                } else {
                    $subject = '<td style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;"> </td>';
                }
                $startAt = '<td style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . date('h:i a', strtotime($exam->start_at)) . '</td>';
                $endAt = '<td style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . date('h:i a', strtotime($exam->end_at)) . '</td>';
                $row[] = '<tr>' . $date . $subject . $startAt . $endAt . '</tr>';
            }

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://savarmanikchandra-gps.com/upload/settings/1627575990gew34vDAC.PNG';
            $heading = '<table style="width: 100%;">
                            <tr>
                                <td style="text-align:right; width: 35%;">
                                    <img src="'.$logo.'" alt="" width="80">
                                </td>
                                <td style="text-align: left; width: 65%;">
                                    <p style="text-align: left; font-size: 18pt; font-weight: bold;">'.setting('backend.general.site_name').'</p>
                                </td>
                            </tr>
                        </table>';
            $output = $heading.'<p style="font-size: 22px; text-align: center">' . __($examName) . '<br>' . __($class->name) . '</p>  <table  style="width: 100%; border: 1px dotted #0a0302;">
            <thead>
                <tr>
                    <th style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . __('Date') . '</th>
                    <th style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . __('Subject') . '</th>
                    <th style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . __('Start') . '</th>
                    <th style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . __('End') . '</th>
                </tr>
            </thead>
            <tbody><tbody>' . implode(" ", $row) . '</tbody></table>';
            return $output;
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function admitCardIndex()
    {
        try {
            return view('backend.pages.exams.admit-card.index', [
                'title' => 'Admit Card',
                'exams' => ExamCategory::where('parent_id', '!=', null)->get(),
                'students' => Student::where('status', true)->get()
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function getAdmitCard(Request $request)
    {
        try {
//            dd($request->all());

            $student = Student::where('unique_id',$request->student)->first();
            $user = $student->user;
            $class = $student->classRoom;
            $category = ExamCategory::find($request->exam);
            $exams = Exam::where(['class_room_id' => $class->id, 'category_id' => $category->id])->get();

            $logo = setting('backend.general.og_meta_image');
            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $groupLogo = asset('backend.logo_favicon.logo');
            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';

            $avatar = $user->profile_photo_path?asset('storage/'.$user->profile_photo_path):($user->gender?($user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $bg = asset('forntend/image/certificate-bg.jpeg');
            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

//            $qrcode = QrCode::size(90)->generate('Name:'.$user->name.', Phone:'.$user->phone.', Applied Class:'.$user->applicant->applyedCalss->name);
            /*            $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);*/
//            return view('frontend.theme1.auth-clients.pages.admid-card', compact('user'));
            $pdf = PDF::loadView('backend.pages.exams.admit-card.admid-card', compact('user', 'logo', 'groupLogo', 'bg', 'avatar','class', 'category', 'exams'), [], [
                'default_font_size' => 13,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 5,
                'title'                => 'Admit Card',
            ]);
            return $pdf->stream('admit-card.pdf');
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }
}
