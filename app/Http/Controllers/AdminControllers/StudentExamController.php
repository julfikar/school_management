<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStudentExamRequest;
use App\Http\Requests\UpdateStudentExamRequest;
use App\Models\ClassRoom;
use App\Models\Exam;
use App\Models\ExamCategory;
use App\Models\Student;
use App\Models\StudentExam;
use App\Models\Subject;
use Illuminate\Http\Request;

class StudentExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return view('backend.pages.student-exams.index', [
                'title' => 'Exams Result',
                'classes' => ClassRoom::all(),
                'exams' => ExamCategory::where('parent_id', '!=', null)->get()
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $request->validate([
                'exam' => 'required',
                'class' => 'required',
                'student' => 'required',
            ]);
            $classRoom = ClassRoom::find($request->class);
            $student = Student::find($request->student);
//            dd($request->all());
            return view('backend.pages.student-exams.create-edit', [
                'title' => __('Create').' '.$student->user->name.'\'s '.__('result'),
                'examCategory' => ExamCategory::find($request->exam),
                'classRoom' => $classRoom,
                'student' => $student
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "class_id" => "required",
                "student_id" => "required",
                "subject" => 'required',
                "subject_id" => 'required',
                "exam_id" => 'required'
            ]);
            $class = ClassRoom::find($request->class_id);
            $student = Student::where('unique_id', $request->student_id)->first();

            if (count($request->subject_id) == count($request->exam_id)){
                foreach ($request->subject_id as $key => $subject_id){
                    $subject = Subject::find($subject_id);
                    $exam = Exam::find($request->exam_id[$key]);
                    $results = StudentExam::where(['student_id' => $student->unique_id, 'exam_id' => $exam->id, 'class_id' => $class->id, 'subject_id' => $subject->id])->whereYear('created_at',date('Y',time()))->first();

                    $cgpa = str_replace(' ', '',$subject->name).'_cgpa';
                    $remark = str_replace(' ', '',$subject->name).'_remark';
                    $got_mark = str_replace(' ', '',$subject->name).'_got_mark';
                    if ($results){
                        $results->cgpa = $request->$cgpa;
                        $results->remark = $request->$remark;
                        $results->got_mark = $request->$got_mark;
                        $results->save();

                    }else{
                        StudentExam::create([
                            'cgpa' => $request->$cgpa,
                            'remark' => $request->$remark,
                            'student_id' => $student->unique_id,
                            'subject_id' => $subject->id,
                            'exam_category_id' => $exam->category->id,
                            'exam_id' => $exam->id,
                            'class_id' => $class->id,
                            'got_mark' => $request->$got_mark
                        ]);
                    }
                }
            }
            return $this->backWithSuccess($student->name.'\'s result created successfully');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        try {
            $request->validate([
               'exam' => 'required',
               'class' => 'required',
            ]);
            $classRoom = ClassRoom::find($request->class);
            return view('backend.pages.student-exams.show', [
                'title' => ucwords($classRoom->name).'\'s Student Exams Result',
                'examCategory' => ExamCategory::find($request->exam),
                'classRoom' => $classRoom,
            ]);

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            return view('backend.pages.student-exam.create-edit', [
                'title' => 'Edit Student Exam',
                'studentExam' => StudentExam::findOrFail($id),
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentExamRequest $request, $id)
    {
        try {
            $inputs = $request->validated();

            $studentExam = StudentExam::findOrFail($id)->update($inputs);

            return $this->backWithSuccess('Updated Successfully');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            StudentExam::findOrFail($id)->delete();
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function preCreateForm()
    {
        return view('backend.pages.student-exams.pre-create', [
            'title' => 'Create Student Result',
            'classes' => ClassRoom::all(),
        ]);
    }
}
