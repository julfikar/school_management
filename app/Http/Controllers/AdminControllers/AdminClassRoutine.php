<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\ClassRoutine;
use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Mpdf\Mpdf;
use PDF;

class AdminClassRoutine extends Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClassRoom $classRoom)
    {
        try {
            $title = __('Class Routine');
            $dayNames = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
            $weekDays = [];
            foreach ($dayNames as $dayName){
                $day = (object)[];
                $day->name = $dayName;
                $weekDays[] = $day;
            }
            $saturdayClass = $classRoom->classRoutines()->where('name_of_day', 'saturday')->count();
            $sundayClass = $classRoom->classRoutines()->where('name_of_day', 'sunday')->count();
            $mondayClass = $classRoom->classRoutines()->where('name_of_day', 'monday')->count();
            $tuesdayClass = $classRoom->classRoutines()->where('name_of_day', 'tuesday')->count();
            $wednesdayClass = $classRoom->classRoutines()->where('name_of_day', 'wednesday')->count();
            $thursdayClass = $classRoom->classRoutines()->where('name_of_day', 'thursday')->count();
            $fridayClass = $classRoom->classRoutines()->where('name_of_day', 'thursday')->count();
            $hihestClass = max($saturdayClass, $sundayClass, $mondayClass, $tuesdayClass, $wednesdayClass, $thursdayClass, $fridayClass);
            return view('backend.pages.class-routine.index', compact('title','classRoom', 'weekDays', 'hihestClass'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, ClassRoom $classRoom)
    {
        try {
            $dayNames = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
            if (!in_array($request->week_day ,$dayNames)){
                return $this->backWithError('Sorry! An error occurred.');
            }
            $weekDay = $request->week_day;
            $periods = $classRoom->classRoutines()->where('name_of_day', $weekDay)->get();
            $title = __($weekDay) . ' '. __('Make New Class Routine');

            return view('backend.pages.class-routine.form', compact('title','classRoom', 'weekDay', 'periods'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, ClassRoom $classRoom)
    {
        $request->validate([
            'subject' => 'required',
            'start_at' => 'required',
            'end_at' => 'required',
        ], [
            'subject' => __('Subject name is required.'),
            'start_at' => __('Class start time is required.'),
            'end_at' => __('Class end time is required.'),
        ]);

        $dayNames = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
        if (!in_array($request->week_day ,$dayNames)){
            return $this->backWithError('Sorry! An error occurred.');
        }
        $weekDay = $request->week_day;
        $subject = Subject::find($request->subject);

        if (!$subject){
            $subject = $request->subject == 'tiffeen-period' ? 'tiffeen-period':($request->subject == 'off-period' ? 'off-period': null);
        }

        if (!$subject){
            return $this->backWithError('Sorry! An error occurred.');
        }

        try {
//            'class_room_id', 'subject_id', 'name_of_day', 'start_at', 'end_at'
            ClassRoutine::create([
                'class_room_id' => $classRoom->id,
                'subject_id' => $subject == 'tiffeen-period' ? null:($request->subject == 'off-period' ? null: $subject->id),
                'off_period' => $request->subject == 'off-period' ? true: false,
                'tiffeen_period' => $request->subject == 'tiffeen-period' ? true: false,
                'name_of_day' => $weekDay,
                'start_at' => $request->start_at,
                'end_at' => $request->end_at,
            ]);

            return $this->backWithSuccess('Success');
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClassRoutine $period)
    {
        $subject = Subject::find($request->subject);
//        if (!$subject){
//            return $this->backWithError('Sorry! An error occurred.');
//        }
        try {
            $period->subject_id = $subject?$subject->id:null;
            $period->start_at = $request->start_at;
            $period->end_at = $request->end_at;
            $period->off_period = $request->subject == 'off-period' ? true: false;
            $period->tiffeen_period = $request->subject == 'tiffeen-period' ? true: false;
            $period->save();
            return $this->backWithSuccess('Success');
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClassRoutine $period)
    {
        try {
            dd($period);
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    public function download(ClassRoom $classRoom)
    {
        try {
            $dayNames = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday'];
            $weekDays = [];
            foreach ($dayNames as $dayName){
                $day = (object)[];
                $day->name = $dayName;
                $weekDays[] = $day;
            }
            $saturdayClass = $classRoom->classRoutines()->where('name_of_day', 'saturday')->count();
            $sundayClass = $classRoom->classRoutines()->where('name_of_day', 'sunday')->count();
            $mondayClass = $classRoom->classRoutines()->where('name_of_day', 'monday')->count();
            $tuesdayClass = $classRoom->classRoutines()->where('name_of_day', 'tuesday')->count();
            $wednesdayClass = $classRoom->classRoutines()->where('name_of_day', 'wednesday')->count();
            $thursdayClass = $classRoom->classRoutines()->where('name_of_day', 'thursday')->count();
            $fridayClass = $classRoom->classRoutines()->where('name_of_day', 'thursday')->count();
            $highestClass = max($saturdayClass, $sundayClass, $mondayClass, $tuesdayClass, $wednesdayClass, $thursdayClass, $fridayClass);

            $mpdf = new Mpdf([
                'default_font_size' => 12.5,
                'default_font' => 'kalpurush',
                'orientation' => 'L',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 10,
                'title'                => 'Class Routine',
            ]);
            $mpdf->writeHTML($this->output($classRoom->id, $weekDays, $highestClass));
            $mpdf->Output();
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    private function output($classRoom, $weekDays, $highestClass)
    {
        try {
            $classRoom = ClassRoom::find($classRoom);
            $row = [];
            foreach ($weekDays as $weekDay) {
                $td = [];
                if ($classRoom->classRoutines()->where('name_of_day', $weekDay->name)->count() > 0) {
                    foreach ($classRoom->classRoutines()->where('name_of_day', $weekDay->name)->get() as $period) {
                        if ($period->subject) {
                            $tds = __($period->subject->name);
                            $tdt = "<br> (" . ($period->subject->teacher?$period->subject->teacher->user->name:'') . ")";
                        } else {
                            if ($period->off_period) {
                                $tds = __('Off-period');
                            } else {
                                $tds = __('Tiffeen-period');
                            }
                            $tdt = '';
                        }
                        $td[] = '<td style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . $tds . $tdt . '
                                <br> ' . date('h:i a', strtotime($period->start_at)) . __(' to ') . date('h:i a', strtotime($period->end_at)) . '
                            </td>';
                    }
                } else {
                    $td[] = '<td colspan="' . $highestClass . '" style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">
                            <div>
                                <h6>' . __('Weekend.') . '</h6>
                            </div>
                        </td>';
                }
                $row[] = '<tr><th style="text-align: center; padding: 10px 0; border: 1px dotted #0a0302; ext-transform: capitalize;">' . __($weekDay->name) . '</th>' . implode(" ", $td) . '</tr>';
            }

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://savarmanikchandra-gps.com/upload/settings/1627575990gew34vDAC.PNG';
            $heading = '<table style="width: 100%;">
                            <tr>
                                <td style="text-align:right; width: 35%;">
                                    <img src="'.$logo.'" alt="" width="80">
                                </td>
                                <td style="text-align: left; width: 65%;">
                                    <p style="text-align: left; font-size: 18pt; font-weight: bold;">'.setting('backend.general.site_name').'</p>
                                </td>
                            </tr>
                        </table>';
            $output = $heading.'<p style="text-align: center;">' . __('Class Routine') . ' <br> ' . __($classRoom->name) . '</p>' .
                '<table style="width: 100%; border: 1px dotted #0a0302;">' . implode(" ", $row) . '</table>';
            return $output;
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }
}
