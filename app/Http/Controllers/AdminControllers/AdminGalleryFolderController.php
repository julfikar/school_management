<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\GalleryFolder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminGalleryFolderController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $title = 'Gallery Folder';
            $galleries_folders = GalleryFolder::all();

            return view('backend.pages.galleries.gallery-folders.index', compact('title','galleries_folders'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(GalleryFolder $galleryFolder=null)
    {
        try {
            $title = 'Add New Gallery Folder';
            return view('backend.pages.galleries.gallery-folders.form', [
                'title' => $title,
                'galleryFolder'=>$galleryFolder
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show(GalleryFolder $galleryFolder=null)
    {
        try {
            $title = $galleryFolder?$galleryFolder->name:'Gallery Folder';
            return view('backend.pages.wizards.sliders.update-form', [
                'title' => $title,
                'slider'=>$slider
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, GalleryFolder $galleryFolder=null)
    {

        $request->validate([
            'name' => 'required',

        ], [
            'name' => 'Gallery Folder name is required.',

        ]);
        try{

            if(empty($galleryFolder)){
                $galleryFolder = new GalleryFolder();
            }

            $galleryFolder->name = $request->name;
            $galleryFolder->slug = Str::slug($request->name);
            $galleryFolder->save();


            $notification = array(
                'message' => 'Gallery folder has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return redirect()->route('admin.galleries.galleryfolder.index')->with($notification);
        }
    }



    public function galleryFolderActivation(GalleryFolder $galleryFolder)
    {
        try{
            if($galleryFolder->status){
                $galleryFolder->status = false;
                $galleryFolder->save();
            }else{
                $galleryFolder->status = true;
                $galleryFolder->save();
            }
            return response()->json($galleryFolder);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GalleryFolder $galleryFolder)
    {
        try {

            $galleryFolder->delete();

            $notification = [
                'message' =>  'Gallery Folder has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }


}
