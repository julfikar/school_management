<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Complain;
use Illuminate\Http\Request;

class WizardComplainController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->middleware(['auth', 'admin']);
    }

    public function index(){
        try {
            $title = 'All Massage';
            $values = Complain::all();
            return view('backend.pages.wizards.complain.index', compact('title','values'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }

    }

    public function view($id){
        try {
            $values = Complain::find($id);
            $values->status = true;
            $values->save();
            $title = $values->phone;
            return view('backend.pages.wizards.complain.view', compact('title','values'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function delete($id){
        try {

            $services= Complain::find($id);
            $services->delete();
            $notification = [
                'message' =>  'Complain has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }
}
