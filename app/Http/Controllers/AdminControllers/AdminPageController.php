<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminPageController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = 'Add new page';
            $page = null;
            return view('backend.pages.settings.page-settings.form', compact('title', 'page'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255', 'unique:pages'],
            'page_content' => 'required'
        ]);
        if ($request->page_title != null){
            $this->validate($request, [
                'page_title' => ['required', 'string', 'max:255'],
            ]);
        }

        try {
            $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $x = str_shuffle($x);
            $x = substr($x, 0, 6) . 'DAC';
            $slug = $x.time();

            $page = Page::create([
                'name' => $request->name,
                'title' => $request->page_title != null ?$request->page_title:null,
                'body' => $request->page_content,
                'top_bar' => false,
                'left_bar' => false,
                'right_bar' => $request->page_right?true:false,
                'bottom_bar' => false,
                'slug' => $slug,
                'deletable' => true,
                'status' => $request->page_status == 'on'?true:false
            ]);

            $notification = array(
                'message' => $page->name.' has been created successfully...',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.pages.show',$page->id)->with($notification);
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        try {
            $title = $page->title?$page->title:$page->name;
            return view('backend.pages.settings.page-settings.form', compact('title', 'page'));
        }catch (\Throwable $e){
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {

        if ($request->page_title != null){
            $this->validate($request, [
                'page_title' => ['required', 'string', 'max:255'],
            ]);
        }

        try {
            $page->forceFill([
                'title' => $request->page_title != null ?$request->page_title:null,
                'body' => $request->page_content,
                'top_bar' => false,
                'left_bar' => false,
                'right_bar' => $request->page_right?true:false,
                'bottom_bar' => false,
                'status' => $request->page_status == 'on'?true:false
            ])->save();

            return $this->backWithSuccess($page->name.' has been Updated successfully...');
        }catch (\Throwable $e){
            return $this->backWithError($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        try {
            $user = Auth::user();
            $page->delete();

            $notification = array(
                'message' => $page->name.' has been deleted successfully...',
                'alert-type' => 'success'
            );
            if ($user->user_type_id > 2){
                return redirect()->route('dashboard')->with($notification);
            }
            return redirect()->route('index.dashboard')->with($notification);

        }catch (\Throwable $e){
            return $this->backWithError($e->getMessage());
        }
    }
}
