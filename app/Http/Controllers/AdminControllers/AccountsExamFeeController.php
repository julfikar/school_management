<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\ExamCategory;
use App\Models\ExamFee;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccountsExamFeeController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * For exams fees
     */
    public function index(){

        try {
            $title = 'exam fee';
            $exam_fees= json_decode(json_encode(setting('school_fees.exam_fee')));
            $exam_categories = ExamCategory::where('parent_id', null)->get();
            $allStudents = Student::all();
            return view('backend.pages.accounts.exam-fees.form', compact('title','exam_fees','exam_categories', 'allStudents'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }


    public function filterStudentByClass($classId)
    {
        $students = Student::with('user')->where('class_id', $classId)->get();

        return response()->json($students);
    }


    public function store(Request $request)
    {

        try {
            $this->validate($request, [
                'class_id' => 'required',
                'exam_category_id' => 'required',
                'student_id' => ['required'],
                'payed_amount' => ['required'],
            ]);
            $student = Student::find($request->student_id);
            if ($student->classRoom->id != $request->class_id){
                return $this->backWithError('Please enter a valid student id or name');
            }

            $exam_fee = new ExamFee();
            $exam_fee->class_id = $request->class_id;
            $exam_fee->student_id = $request->student_id;
            $exam_fee->exam_category_id = $request->exam_category_id;
            $exam_fee->payed_amount = $request->payed_amount;

            if($request->discount){
                $exam_fee->discount = $request->discount;
            }

            if($request->due){
                $exam_fee->due = $request->due;
            }

            $exam_fee->save();

            $notification = [
                'message' => 'Exam fee paid successfully',
                'alert-type' => 'success'
            ];

            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

}
