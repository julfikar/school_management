<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AdminEmployeeReportsController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'Employee Reports';

            return view('backend.pages.reports.employee-reports.index', compact('title'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


    public function attendanceCalculate(Request $request)
    {

        try {

            $title = 'Employee Reports';
            $employee = Employee::where('unique_id',$request->unique_id)->first();
            if($employee==null){
                $notification = [
                    'message' => 'Employee not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $employee_name = $employee->user->name;

            $sdate = $request->start_date;
            $edate = $request->end_date;
            $datetime1 = strtotime($sdate); // convert to timestamps
            $datetime2 = strtotime($edate); // convert to timestamps
            $total_days = (int)(($datetime2 - $datetime1) / 86400);// will give the difference in days , 86400 is the timestamp difference of a day

            $fridays = count($this->countFridays($sdate,$edate));
            $working_days =$total_days - $fridays;

            $total_attendece =0;
            $days = [];
            // Loop between timestamps, 24 hours at a time
            for ( $i = $datetime1; $i <= $datetime2; $i = $i + 86400 ) {
                $thisDate = date( 'Y-m-d', $i ); // 2010-05-01, 2010-05-02, etc

                $attend = $employee->attendents()->where('date', $thisDate)->first();

                if($attend){
                    $total_attendece++;
                    $days[] = (object)[
                        'date' => $thisDate,
                        'status' => true,
                        'attend_time' => date('h:i:s a', strtotime($attend->date))
                    ];
                }else{
                    $days[] = (object)[
                        'date' => $thisDate,
                        'status' => false,
                        'attend_time' => null
                    ];
                }
            }

            $total_absent =$working_days-$total_attendece;

            if($working_days==$total_absent){
                $notification = [
                    'message' => __($employee_name).' attendance report not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }

            return view('backend.pages.reports.employee-reports.show-attendance', compact('title','employee_name','total_attendece','total_absent','days'));

        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

    }

    /**
     * For Total accounts report
     */
    public function accountsCalculate(Request $request){

        try{

            $data['title'] = 'Employee Accounts Report';
            $employee = Employee::where('unique_id',$request->unique_id)->first();
            if($employee==null){
                $notification = [
                    'message' => 'Employee not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $employee_name = $employee->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            //for month count between two date
            $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
            $month_count = $period->count();

            // for salary calculate
            $salary=0;
            // salary paid calculate
            foreach ( $employee->salary as $item){
                if($item->accounts){
                    $salary=$salary+$item->accounts()->sum('amount');
                }

            }
            $total_paid_salary = $salary;

            $total_get_salary=0;
            //  for each month list

            if( $employee->salary){
                foreach ($period as $dt) {
                    $last_day = strtotime($dt->endOfMonth()->toDateString());

                    $amount_of_this_month = $employee->salary()->where('date', '<', $last_day)->latest()->first();

                    if($amount_of_this_month!=null){
                        $amount_of_this_month = $amount_of_this_month->amount;
                    }

                    $total_get_salary = $total_get_salary+$amount_of_this_month;
                }
            }


            // for hostel fee
            if($employee->user->dweller) {

                $school_hostel_fee = setting('school_fees.hostel_fee.hostel_fee');
                $total_get_hostel_fee = $month_count * $school_hostel_fee;
                $payed_hostel = (int)$employee->user->dweller->hostelFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_hostel = (int)$employee->user->dweller->hostelFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $total_paid_hostel_fee = $payed_hostel+$discount_hostel;
            }else{

                $total_get_hostel_fee=0;
                $total_paid_hostel_fee=0;
            }

            // for transportation fee
            if($employee->user->passenger) {
                $school_transportation_fee = setting('school_fees.transportation_fee.transportation_fee');
                $total_get_transportation_fee = $month_count * $school_transportation_fee;
                $payed_passenger = (int)$employee->user->passenger->transportationFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_passenger = (int)$employee->user->passenger->transportationFees()->whereBetween('created_at', [$start_date, $end_date])->sum('discount');
                $total_paid_transportation_fee = $payed_passenger+$discount_passenger;
            }else{
                $total_get_transportation_fee=0;
                $total_paid_transportation_fee=0;
            }


            $all_fees_paid_amount = $total_paid_salary+$total_paid_hostel_fee+$total_paid_transportation_fee;
            $all_fees_get_amount = $total_get_salary+$total_get_hostel_fee+$total_get_transportation_fee;


            $data['employee_name'] = $employee_name;
            $data['unique_id'] = $request->unique_id;
            $data['start_date'] = $request->start_date;
            $data['end_date'] = $request->end_date;

            $data['total_paid_salary'] =  $total_paid_salary;
            $data['total_get_salary'] =  $total_get_salary;
            $data['total_get_hostel_fee'] =  $total_get_hostel_fee;
            $data['total_paid_hostel_fee'] =  $total_paid_hostel_fee;
            $data['total_get_transportation_fee'] =  $total_get_transportation_fee;
            $data['total_paid_transportation_fee'] =  $total_paid_transportation_fee;


            $data['all_fees_paid_amount'] =  $all_fees_paid_amount;
            $data['all_fees_get_amount'] =  $all_fees_get_amount;

            $hostelFees = $this->singleHostelFeeReport($request);
            $transportationFees = $this->singleTransportationFeeReport($request);
            $data2= [
                $hostelFees,
                $transportationFees
            ];

            $data['data2']= $data2;


            return view('backend.pages.reports.employee-reports.accounts-report',$data);

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * For Salary report
     */
    public function salaryCalculate(Request $request){

        try {
            $title = 'Salary Reports';
            $employee = Employee::where('unique_id',$request->unique_id)->first();
            if($employee==null){
                $notification = [
                    'message' => 'Teacher not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $employee_name = $employee->user->name;

            $sd =$request->start_date;
            $ed= $request->end_date;

            $employee_salary = $employee->salary->first();

            $given_salary_accounts = $employee_salary->accounts()->whereBetween('payed_date', [$sd, $ed])->get();


            // for salary calculate
            $salary=0;
            // salary paid calculate
            foreach ( $employee->salary as $item){
                if($item->accounts){
                    $salary=$salary+$item->accounts()->sum('amount');
                }

            }
            $total_paid_salary = (int)$salary;
            //for month count between two date
            $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
            $total_get_salary=0;

            //  for each month list
            if( $employee->salary){
                foreach ($period as $dt) {
                    $last_day = strtotime($dt->endOfMonth()->toDateString());

                    $amount_of_this_month = $employee->salary()->where('date', '<', $last_day)->latest()->first();
                    if($amount_of_this_month!=null){
                        $amount_of_this_month = $amount_of_this_month->amount;
                    }
                    $total_get_salary = $total_get_salary+$amount_of_this_month;
                }
            }



            return view('backend.pages.reports.employee-reports.single-salary-pay',compact('title','employee_name','given_salary_accounts','total_get_salary','total_paid_salary'));

        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }


    /**
     * For hostel fees month wise report
     */
    public function singleHostelFeeReport(Request $request){

        try{

            $employee = Employee::where('unique_id',$request->unique_id)->first();
            $employee_name = $employee->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            $school_hostel_fee = setting('school_fees.hostel_fee.hostel_fee');

            $new_reports=[];
            $i=0;
            $payment_date =null;
            $total_fees_payed=0;
            $total_payable_fees=0;

            // for hostel fee
            if($employee->user->dweller) {

                //for month count between two date
                $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
                $total_payable_fees = (int)$school_hostel_fee *  $period->count();

                //  for each month list
                foreach ($period as $dt) {
                    $discount_amount =null;
                    $payment_date=null;
                    $dueamount =null;

                    $hostel_fee_find = $employee->user->dweller->hostelFees()->where('month', $dt->format('m'))->whereBetween('created_at', [$start_date, $end_date])->first();

                    if ($hostel_fee_find != null) {
                        if ($hostel_fee_find->payed_amount == $school_hostel_fee) {
                            $payedamount = $hostel_fee_find->payed_amount;
                        } else {
                            $payedamount = $hostel_fee_find->payed_amount;
                            $discount_amount = $hostel_fee_find->discount;
                        }

                        $dueamount = $hostel_fee_find->due?$hostel_fee_find->due:null;
                        $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                        $payment_date=date('d-m-Y',strtotime($hostel_fee_find->created_at));
                    } else {
                        $payedamount = $school_hostel_fee;
                        $dueamount = $school_hostel_fee;
                    }

                    $new_reports[$i] = [
                        'name'=>'Hostel Fee',
                        'date' => $payment_date?$payment_date:$dt->format('d-m-Y'),
                        'paid' => (int)$payedamount,
                        'discount' => (int) $discount_amount,
                        'due' => (int)$dueamount,
                    ];

                    $i++;
                }


            }
            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed
            ];

            return $data;

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    /**
     * For Transportation fees month wise report
     */
    public function singleTransportationFeeReport(Request $request){

        try{

            $employee = Employee::where('unique_id',$request->unique_id)->first();
            $employee_name = $employee->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            $school_transportation_fee = setting('school_fees.transportation_fee.transportation_fee');

            $new_reports=[];
            $i=0;
            $payment_date =null;
            $total_fees_payed=0;
            $total_payable_fees=0;

            // for hostel fee
            if($employee->user->passenger) {

                //for month count between two date
                $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
                $total_payable_fees = (int)$school_transportation_fee *  $period->count();

                //  for each month list
                foreach ($period as $dt) {

                    $discount_amount =null;
                    $payment_date=null;
                    $dueamount =null;

                    $transportation_fee_find = $employee->user->passenger->transportationFees()->where('month', $dt->format('m'))->whereBetween('created_at', [$start_date, $end_date])->first();

                    if ($transportation_fee_find != null) {
                        if ($transportation_fee_find->payed_amount == $school_hostel_fee) {
                            $payedamount = $transportation_fee_find->payed_amount;
                        } else {
                            $payedamount = $transportation_fee_find->payed_amount;
                            $discount_amount = $transportation_fee_find->discount;
                        }

                        $dueamount = $transportation_fee_find->due?$transportation_fee_find->due:null;
                        $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                        $payment_date=date('d-m-Y',strtotime($transportation_fee_find->created_at));
                    } else {
                        $payedamount = $school_transportation_fee;
                        $dueamount = $school_transportation_fee;
                    }

                    $new_reports[$i] = [
                        'name'=>'Transportation Fee',
                        'date' => $payment_date?$payment_date:$dt->format('d-m-Y'),
                        'paid' => (int)$payedamount,
                        'discount' => (int) $discount_amount,
                        'due' => (int)$dueamount,
                    ];

                    $i++;
                }


            }/*else{

                $notification = [
                    'message' => 'Employee is not a passenger.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }*/
            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed
            ];

            return $data;

//            return view('backend.pages.reports.employee-reports.transportation-fees',compact('title','employee_name','new_reports','total_payable_fees','total_fees_payed'));
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    public function countFridays($dateFromString,$dateToString){
        $dateTo = Carbon::parse($dateToString);
        $friday = Carbon::parse($dateFromString . ' next friday');
        $fridays = [];

        while($friday->lt($dateTo)) {
            $fridays[] = $friday->toDateString();
            $friday->addWeek();
        }

        return $fridays;
    }



}
