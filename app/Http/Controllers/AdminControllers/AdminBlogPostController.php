<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\BlogFolder;
use App\Models\BlogPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class AdminBlogPostController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BlogFolder $blogFolder = null)
    {
        try {
            $title = $blogFolder?$blogFolder->name:'Blog Post';
            $folders = BlogFolder::all();
            $posts = [];
            if ($blogFolder){
                if (Auth::user()->hasRole('super admin')){
                    $posts = $blogFolder->posts;
                }else{
                    $posts = $blogFolder->posts()->where('user_id',Auth::user()->id)->get();
                }
            }

//            dd($blogFolder->posts);


            return view('backend.pages.blogs.blog-post.index', compact('title','blogFolder', 'folders', 'posts'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(BlogFolder $blogFolder = null)
    {
        try {
            $title = 'Add New Post';
            return view('backend.pages.blogs.blog-post.form', [
                'title' => $title,
                'blogFolder'=>$blogFolder
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show(BlogPost $blogpost)
    {
        try {
            $title = 'Post';
            return view('backend.pages.blogs.blog-post.update-form', [
                'title' => $title,
                'blogpost'=>$blogpost
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'title' => 'required',
                'body' => 'required',
                'status' => 'required',
                'thumbnail' => 'required',
            ], [
                'title' => 'Subject name is required.',
                'body' => 'Post body is needed..',
                'thumbnail' => 'Post thumbnail is required.',

            ]);

        $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }

            $bpost= new BlogPost();

            if ($request->hasFile('thumbnail')) {

                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
                        ->resize(1536, 1024)
                        ->save(public_path('/upload/blogs/post-thumbnails/' . $filename));
                    $bpost->thumbnail = $filename;
                }
            }

            $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $x = str_shuffle($x);
            $x = substr($x, 0, 6) . 'DAC';
            $slug = $x.time();

            $bpost->title = $request->title;
            $bpost->slug = $slug;
            $bpost->user_id = Auth::user()->id;
            $bpost->tags = $request->post_tags;
            $bpost->status = $request->status;
            $bpost->right_bar = $request->post_right == 'on'?true:false;
            $bpost->folder_id = $request->folder_id;
            $bpost->body = $request->body;

            $bpost->save();

            return $this->backWithSuccess('Post has been added successfully');
        } catch (\Throwable $e) {
            return $this->backWithError($e->getMessage());
        }
    }


    public function update(Request $request, BlogPost $blogpost)
    {

        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }


            if ($request->hasFile('thumbnail')) {

                if ($blogpost->thumbnail != null) {
                    // delete existing image
                    $file = 'upload/blogs/post-thumbnails/' . $blogpost->thumbnail;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
                        ->resize(1536, 1024)
                        ->save(public_path('/upload/blogs/post-thumbnails/' . $filename));
                    $blogpost->thumbnail = $filename;
                }
            }

            $blogpost->title = $request->title;
            $blogpost->tags = $request->post_tags;
            $blogpost->status = $request->status;
            $blogpost->right_bar = $request->post_right == 'on'?true:false;
            $blogpost->body = $request->body;

            $blogpost->save();

            $notification = array(
                'message' => 'Post has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }



    public function blogpostActivation(BlogPost $blogpost)
    {
        try{
            if($blogpost->status){
                $blogpost->status = false;
                $blogpost->save();
            }else{
                $blogpost->status = true;
                $blogpost->save();
            }
            return response()->json($blogpost);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogPost $blogpost)
    {
        try {

            if ($blogpost->thumbnail != null) {
                // delete existing image
                $file = 'upload/blogs/post-thumbnails/' . $blogpost->thumbnail;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $blogpost->delete();

            $notification = [
                'message' =>  'Post has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }
}
