<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Passenger;
use App\Models\Transportation;
use App\Models\User;
use Illuminate\Http\Request;
use Mockery\Generator\StringManipulation\Pass\Pass;

class AdminPassengerController extends Controller
{


    public function index(Transportation $transportation){

        try {
            $title = $transportation->vehicle_no.' No Vehicle';
            $users = User::all();

            return view('backend.pages.passengers.index', compact('title', 'transportation','users'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function create(Transportation $transportation)
    {

        try {
            $title = $transportation->name.' New Passenger';
            $student = User::all();

            return view('backend.pages.passengers.create-edit', [
                'transportation' => $transportation,
                'student' => $student,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'user_id' => 'required',
        ], [
            'user_id' => 'Passenger name is required.',

        ]);

        try {

            $find_passenger = Passenger::where('user_id',$request->user_id)->first();

            if($find_passenger !=null){

                $notification = array(
                    'message' => 'Passenger already exists.',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            $passenger = new Passenger();
            $passenger->user_id = $request->input('user_id');
            $passenger->transportation_id = $request->input('transportation_id');
            $passenger->unique_id = $uniqueID;

            $passenger->save();

            $notification = array(
                'message' => 'New Passenger has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);

        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function destroy(Passenger $passenger){
        try{

            $passenger->delete();

            $notification = array(
                'message' => 'Passenger has been deleted successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }



}
