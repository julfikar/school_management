<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\BlogFolder;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminBlogFolderController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
        $this->middleware(['auth', 'admin']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        try {
            $title = 'Blog Folder';
            $blog_folders = BlogFolder::all();

            return view('backend.pages.blogs.blog-folders.index', compact('title','blog_folders'));
        }catch (\Throwable $e){
            $notification = [
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = 'Add New Blog Folder';
            return view('backend.pages.blogs.blog-folders.form', [
                'title' => $title,
                'blogFolder'=>null
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show(BlogFolder $blogfolder)
    {

        try {
            $title = $blogfolder->name;
            return view('backend.pages.blogs.blog-folders.form', [
                'title' => $title,
                'blogFolder'=>$blogfolder
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',

        ], [
            'name' => 'Blog Folder name is required.',

        ]);
        try{

            // for validation check
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }

            $blogFolder = new BlogFolder();

            if ($request->hasFile('thumbnail')) {

                // insert new file
                $images = $request->thumbnail;
                foreach ($images as $image) {
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
                        ->resize(1536, 480)
                        ->save(public_path('/upload/blogs/folder-thumnails/' . $filename));
                }
                $blogFolder->thumbnail = $filename;
            }

            $blogFolder->name = $request->name;

            $blogFolder->save();


            $notification = array(
                'message' => 'Blog folder has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
       } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return redirect()->route('admin.blogfolders.index')->with($notification);
        }
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogFolder $blogfolder)
    {

        $request->validate([
            'name' => 'required',

        ], [
            'name' => 'Blog Folder name is required.',

        ]);

        try {

            if ($request->hasFile('thumbnail')) {

                if ($blogfolder->thumbnail != null) {
                    // delete existing image
                    $file = 'upload/blogs/folder-thumnails/' . $blogfolder->thumbnail;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
                        ->resize(1536, 480)
                        ->save(public_path('/upload/blogs/folder-thumnails/' . $filename));
                    $blogfolder->thumbnail = $filename;
                }
            }

            $blogfolder->name = $request->name;
            $blogfolder->save();

            return $this->backWithSuccess($blogfolder->name.' has been Updated successfully...');
        }catch (\Throwable $e){
            return $this->backWithError($e->getMessage());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlogFolder $blogfolder)
    {

        try {

            if ($blogfolder->thumbnail != null) {
                // delete existing image
                $file = 'upload/blogs/folder-thumnails/' . $blogfolder->thumbnail;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }

            $blogfolder->delete();

            $notification = [
                'message' =>  'Blog Folder has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }


}
