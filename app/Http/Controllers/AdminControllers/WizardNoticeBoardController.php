<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\NoticeBoard;
use App\Models\OnesignalNotification;
use Illuminate\Http\Request;

class WizardNoticeBoardController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        try {
            $title = 'All Notice';
            $allnotice = NoticeBoard::all();
            return view('backend.pages.wizards.notice-board.index', compact('title','allnotice'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function create()
    {
        try {
            $title = 'Add Notice';
            return view('backend.pages.wizards.notice-board.form', [
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'heading' => 'required|max:255',
            'description' => 'required',
        ]);

        $acceptable = ['pdf'];
        if ($request->has('notice_file')){
            if (!in_array($request->notice_file->getClientOriginalExtension(), $acceptable)) {
                $notification = array(
                    'message' => 'Only pdf file is supported.',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $x = str_shuffle($x);
            $x = substr($x, 0, 6) . 'DAC.';
            $file = time().$x.$request->notice_file->getClientOriginalExtension();
            $request->notice_file->move(public_path('/upload/notice-files/'), $file);
        }else{
            $file = null;
        }

        try{
            $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $x = str_shuffle($x);
            $x = substr($x, 0, 6) . 'DAC';
            $slug = $x.time();

            $data = new NoticeBoard();
            $data->heading = $request->heading;
            $data->description = $request->description;
            $data->file = $file;
            $data->slug = $slug;
            $data->save();

            $pushNotification = new OnesignalNotification();
            $pushNotification = $pushNotification->sendNotificationToAll($request->heading,route('notice.single',$data->slug));

//            $notification = array(
//                'message' => 'Service has been added successfully',
//                'alert-type' => 'success'
//            );
            return redirect()->back()->with($pushNotification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function edit($request){
        try {
            $title = 'Edit Notice';
            $niticeEdit = NoticeBoard::find($request);
            return view('backend.pages.wizards.notice-board.update', compact('title','niticeEdit'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }

    public function update(Request $request, $id){

        $data = NoticeBoard::find($id);

//        dd($request->notice_file);
        $acceptable = ['pdf'];
        if ($request->has('notice_file')){
            if (!in_array($request->notice_file->getClientOriginalExtension(), $acceptable)) {
                $notification = array(
                    'message' => 'Only pdf file is supported.',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            if ($data->file){
                $path = 'upload/notice-files/'.$data->file;
                if (file_exists(public_path($path))){
                    unlink(public_path($path));
                }
            }

            $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $x = str_shuffle($x);
            $x = substr($x, 0, 6) . 'DAC.';
            $file = time().$x.$request->notice_file->getClientOriginalExtension();
            $request->notice_file->move(public_path('/upload/notice-files/'), $file);
        }

        try{
            $data->heading = $request->heading;
            $data->description = $request->description;
            if ($request->has('notice_file')){
                $data->file = $file;
            }
            $data->save();
            $notification = array(
                'message' => 'Service has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }
    public function delete($id){
        try {

            $services= NoticeBoard::find($id);
            $services->delete();
            $notification = [
                'message' =>  'Service has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
}
