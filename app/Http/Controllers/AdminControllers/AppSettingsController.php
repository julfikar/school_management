<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AppSettingsController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function getGeneralSettings()
    {
        try {
            $title = 'General Settings';
            return view('backend.pages.settings.general', compact('title'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function saveGeneralSettings(Request $request)
    {
        try {

            $group = 'backend.general.';

            setting([$group . 'site_name' => $request->site_name]);
            setting([$group . 'site_emis_code' => $request->site_emis_code]);
            setting([$group . 'established' => $request->established]);
            setting([$group . 'site_tag_line' => $request->site_tag_line]);
            setting([$group . 'site_sub_tag_line' => $request->site_sub_tag_line]);
            setting([$group . 'author_name' => $request->author_name]);
            setting([$group . 'footer_copy_right' => $request->footer_copy_right]);
            setting([$group . 'location_map' => $request->location_map]);
            setting([$group . 'og_meta_title' => $request->og_meta_title]);
            setting([$group . 'og_meta_description' => $request->og_meta_description]);
            if ($request->hasFile('og_meta_image')) {
                if (strlen(setting($group.'og_meta_image')) > 17){
                    $path = setting($group.'og_meta_image');
                    if (file_exists(public_path($path))){
                        unlink(public_path($path));
                    }
                }
                $image = $request->og_meta_image;
                $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                $x = str_shuffle($x);
                $x = substr($x, 0, 6) . 'DAC.';
                $filename = time() . $x . $image->getClientOriginalExtension();
                Image::make($image->getRealPath())->resize(128, 128)->save(public_path('/upload/settings/' . $filename));
                $path = "/upload/settings/".$filename;
                setting([$group . 'og_meta_image' => $path]);
//                $this->replaceImage($group . 'og_meta_image', $request->og_meta_image);
            }
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
        return $this->backWithSuccess('Successfull');
    }

    public function getLogoFaviconSettings()
    {
        try {
            $title = 'Logo Settings';
            return view('backend.pages.settings.logo-favicon', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function saveLogoFaviconSettings(Request $request)
    {
//        $group = 'backend.logo_favicon.';
        try {
            if ($request->hasFile('logo')) {
                if (strlen(setting('backend.logo_favicon.logo')) > 17){
                    $path = setting('backend.logo_favicon.logo');
                    if (file_exists(public_path($path))){
                        unlink(public_path($path));
                    }
                }

                $images = $request->logo;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                }
                Image::make($image->getRealPath())->resize(128, 128)->save(public_path('/upload/settings/' . $filename));
                $path = "/upload/settings/".$filename;
                setting(['backend.logo_favicon.logo' => $path]);
//                $this->replaceImage($group . 'logo', $path);
            }

            if ($request->hasFile('favicon')) {
                if (strlen(setting('backend.logo_favicon.favicon')) > 17){
                    $path = setting('backend.logo_favicon.favicon');
                    if (file_exists(public_path($path))){
                        unlink(public_path($path));
                    }
                }

                $images = $request->favicon;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                }
                Image::make($image->getRealPath())->resize(32, 32)->save(public_path('/upload/settings/' . $filename));
                $path = '/upload/settings/'.$filename;
                setting(['backend.logo_favicon.favicon' => $path]);
//                $this->replaceImage($group . 'favicon', public_path($path));
            }

            if ($request->hasFile('site_tag_image')) {
                if (strlen(setting('backend.logo_favicon.site_tag_image')) > 17){
                    $path = setting('backend.logo_favicon.site_tag_image');
                    if (file_exists(public_path($path))){
                        unlink(public_path($path));
                    }
                }

                $images = $request->site_tag_image;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                }
                Image::make($image->getRealPath())->resize(128, 128)->save(public_path('/upload/settings/' . $filename));
                $path = '/upload/settings/'.$filename;
                setting(['backend.logo_favicon.site_tag_image' => $path]);
//                $this->replaceImage($group . 'site_tag_image', $filename);
            }

            $notification = array(
                'message' => 'Logo sections are updated successfully....',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function getSeoSettings()
    {
        try {
            $title = 'Seo Settings';
            return view('backend.pages.settings.seo', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function saveSeoSettings(Request $request)
    {
        try {
            setting(['backend.seo.meta_keyword' => $request->meta_keyword]);
            setting(['backend.seo.meta_description' => $request->meta_description]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('Success');
    }

    public function getSmtpSettings()
    {
        try {
            $title = 'Smtp Settings';
            return view('backend.pages.settings.smtp', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function saveSmtpSettings(Request $request)
    {
        $group = 'backend.smtp.';

        try {
            setting([$group . 'mail_driver' => $request->mail_driver]);
            setting([$group . 'mail_host' => $request->mail_host]);
            setting([$group . 'mail_port' => $request->mail_port]);
            setting([$group . 'mail_username' => $request->mail_username]);
            setting([$group . 'mail_password' => $request->mail_password]);
            setting([$group . 'mail_encryption' => $request->mail_encryption]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('Success');
    }

    public function getSmsSettings()
    {
        try {
            $title = 'SMS Settings';
            return view('backend.pages.settings.sms', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function saveSmsSettings(Request $request)
    {
        $group = 'backend.sms.';

        try {
            setting([$group . 'url' => $request->url]);
            setting([$group . 'authkey' => $request->authkey]);
            setting([$group . 'sender' => $request->sender]);
            setting([$group . 'route' => $request->route]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('Success');
    }

    public function getCustomCssSettings()
    {
        try {
            $title = 'Custom Css';
            return view('backend.pages.settings.custom-css', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function saveCustomCssSettings(Request $request)
    {
        try {
            setting(['backend.custom_css.custom_css' => $request->custom_css]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('Success');
    }

    public function getCustomJsSettings()
    {
        try {
            $title = 'Custom Js';
            return view('backend.pages.settings.custom-js', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function saveCustomJsSettings(Request $request)
    {
        try {
            setting(['backend.custom_js.custom_js' => $request->custom_js]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('Success');
    }

    public function getInsertHeaderFooterSettings()
    {
        try {
            $title = 'Insert Header Footer';
            return view('backend.pages.settings.insert-header-footer', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function saveInsertHeaderFooterSettings(Request $request)
    {
        try {
            setting(['backend.header_footer.header' => $request->input('header')]);
            setting(['backend.header_footer.footer' => $request->input('footer')]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('Success');
    }

    private function replaceImage($imagePath, $newImage)
    {
        $oldImage = setting($imagePath);
        if ($oldImage) {
            Storage::delete($oldImage);
        }
        $newImage = $newImage->store('upload/settings');
        setting([$imagePath => '/' . $newImage]);
    }

    public function languageSettings($locale)
    {
        if (!in_array($locale, ['en', 'bn'])) {
            abort(400);
        }
        try {
            $path = base_path('.env');
            if (file_exists($path)) {
                file_put_contents($path, str_replace(
                    'APP_LANGUAGE=' . env("APP_LANGUAGE"), 'APP_LANGUAGE=' . $locale, file_get_contents($path)
                ));
            }
            return $this->backWithSuccess('App language is chenged successfully....');
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function socialLinkSettings()
    {
        try {
            $title = 'Social-link Settings';
            return view('backend.pages.settings.social-link', compact('title'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function socialLinkSettingsStore(Request $request)
    {
        try {
            $group = 'social-media.links.';
            setting([$group . 'twitter' => $request->twitter]);
            setting([$group . 'facebook' => $request->facebook]);
            setting([$group . 'whatsapp' => $request->whatsapp]);
            setting([$group . 'linkedin' => $request->linkedin]);
            setting([$group . 'skype' => $request->skype]);
            return $this->backWithSuccess('Successfull');
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
}
