<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Teacher;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AdminTeacherReportsController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'Teacher Reports';

            return view('backend.pages.reports.teacher-reports.index', compact('title'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * For Total Attendants report
     */
    public function attendanceCalculate(Request $request)
    {

        try {
            $title = 'Teacher Reports';
            $teacher = Teacher::where('unique_id',$request->unique_id)->first();
            if($teacher==null){
                $notification = [
                    'message' => 'Teacher not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $teacher_name = $teacher->user->name;

            $sdate = $request->start_date;
            $edate = $request->end_date;
            $datetime1 = strtotime($sdate); // convert to timestamps
            $datetime2 = strtotime($edate); // convert to timestamps
            $total_days = (int)(($datetime2 - $datetime1) / 86400);// will give the difference in days , 86400 is the timestamp difference of a day

            $fridays = count($this->countFridays($sdate,$edate));
            $working_days =$total_days - $fridays;

            $total_attendece =0;
            $days = [];
            // Loop between timestamps, 24 hours at a time
            for ( $i = $datetime1; $i <= $datetime2; $i = $i + 86400 ) {
                $thisDate = date( 'Y-m-d', $i ); // 2010-05-01, 2010-05-02, etc

                $attend = $teacher->attendents()->where('date', $thisDate)->first();
                if($attend){
                    $total_attendece++;
                    $days[] = (object)[
                        'date' => $thisDate,
                        'status' => true,
                        'attend_time' => date('h:i:s a', strtotime($attend->date))
                    ];
                }else{
                    $days[] = (object)[
                        'date' => $thisDate,
                        'status' => false,
                        'attend_time' => null
                    ];
                }
            }

            $total_absent =$working_days-$total_attendece;

            if($working_days==$total_absent){
                $notification = [
                    'message' => ucwords($teacher_name).' attendance report not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }

            return view('backend.pages.reports.teacher-reports.show-attendance', compact('title','teacher_name','total_attendece','total_absent','days'));

        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }


    /**
     * For Total accounts report
     */
    public function accountsCalculate(Request $request){

        try{

            $data['title'] = 'Teacher Accounts Report';
            $teacher = Teacher::where('unique_id',$request->unique_id)->first();
            if($teacher==null){
                $notification = [
                    'message' => 'Teacher not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $teacher_name = $teacher->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            //for month count between two date
            $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
            $month_count = $period->count();

            // for salary calculate
            $salary=0;
                // salary paid calculate
            foreach ( $teacher->salary as $item){
                $salary=$salary+$item->accounts()->sum('amount');
            }
            $total_paid_salary = $salary;

            $total_get_salary=0;
            //  for each month list
            foreach ($period as $dt) {
                $last_day = strtotime($dt->endOfMonth()->toDateString());

                $amount_of_this_month = $teacher->salary()->where('date', '<', $last_day)->latest()->first();
                if($amount_of_this_month!=null){
                    $amount_of_this_month = $amount_of_this_month->amount;
                }

                $total_get_salary = $total_get_salary+$amount_of_this_month;
            }


            // for hostel fee
            if($teacher->user->dweller) {

                $school_hostel_fee = setting('school_fees.hostel_fee.hostel_fee');
                $total_get_hostel_fee = $month_count * $school_hostel_fee;
                $payed_hostel = (int)$teacher->user->dweller->hostelFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_hostel = (int)$teacher->user->dweller->hostelFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $total_paid_hostel_fee = $payed_hostel+$discount_hostel;
            }else{

                $total_get_hostel_fee=0;
                $total_paid_hostel_fee=0;
            }

            // for transportation fee
            if($teacher->user->passenger) {
                $school_transportation_fee = setting('school_fees.transportation_fee.transportation_fee');
                $total_get_transportation_fee = $month_count * $school_transportation_fee;
                $payed_passenger = (int)$teacher->user->passenger->transportationFees()->whereBetween('created_at', [$start_date, $end_date])->sum('payed_amount');
                $discount_passenger = (int)$teacher->user->passenger->transportationFees()->whereBetween('created_at', [$start_date, $end_date])->sum('discount');
                $total_paid_transportation_fee = $payed_passenger+$discount_passenger;
            }else{
                $total_get_transportation_fee=0;
                $total_paid_transportation_fee=0;
            }


            $all_fees_paid_amount = $total_paid_salary+$total_paid_hostel_fee+$total_paid_transportation_fee;
            $all_fees_get_amount = $total_get_salary+$total_get_hostel_fee+$total_get_transportation_fee;


            $data['teacher_name'] = $teacher_name;
            $data['unique_id'] = $request->unique_id;
            $data['start_date'] = $request->start_date;
            $data['end_date'] = $request->end_date;

            $data['total_paid_salary'] =  $total_paid_salary;
            $data['total_get_salary'] =  $total_get_salary;
            $data['total_get_hostel_fee'] =  $total_get_hostel_fee;
            $data['total_paid_hostel_fee'] =  $total_paid_hostel_fee;
            $data['total_get_transportation_fee'] =  $total_get_transportation_fee;
            $data['total_paid_transportation_fee'] =  $total_paid_transportation_fee;


            $data['all_fees_paid_amount'] =  $all_fees_paid_amount;
            $data['all_fees_get_amount'] =  $all_fees_get_amount;

            $hostelFees = $this->singleHostelFeeReport($request);
            $transportationFees = $this->singleTransportationFeeReport($request);
            $data2= [
                $hostelFees,
                $transportationFees
            ];

            $data['data2']= $data2;

            return view('backend.pages.reports.teacher-reports.accounts-report',$data);

        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    /**
     * For Salary report
     */
    public function salaryCalculate(Request $request){


        try {
            $title = 'Salary Reports';
            $teacher = Teacher::where('unique_id',$request->unique_id)->first();
            if($teacher==null){
                $notification = [
                    'message' => 'Teacher not found.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }
            $teacher_name = $teacher->user->name;

            $sd =$request->start_date;
            $ed= $request->end_date;

            $teacher_salary = $teacher->salary->first();

            $salary_accounts = $teacher_salary->accounts()->whereBetween('payed_date', [$sd, $ed])->get();

            // for salary calculate
            $salary=0;
            // salary paid calculate
            foreach ( $teacher->salary as $item){
                if($item->accounts){
                    $salary=$salary+$item->accounts()->sum('amount');
                }

            }
            $total_paid_salary = (int)$salary;
            //for month count between two date
            $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
            $total_get_salary=0;

            //  for each month list
            if( $teacher->salary){
                foreach ($period as $dt) {
                    $last_day = strtotime($dt->endOfMonth()->toDateString());

                    $amount_of_this_month = $teacher->salary()->where('date', '<', $last_day)->latest()->first();
                    if($amount_of_this_month!=null){
                        $amount_of_this_month = $amount_of_this_month->amount;
                    }
                    $total_get_salary = $total_get_salary+$amount_of_this_month;
                }
            }

            return view('backend.pages.reports.teacher-reports.single-salary-pay',compact('title','teacher_name','salary_accounts','total_paid_salary','total_get_salary'));

        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * For hostel fees month wise report
     */
    public function singleHostelFeeReport(Request $request){

        try{
            $title = 'Teacher Accounts Report';
            $teacher = Teacher::where('unique_id',$request->unique_id)->first();
            $teacher_name = $teacher->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            $school_hostel_fee = setting('school_fees.hostel_fee.hostel_fee');

            $new_reports=[];
            $i=0;
            $payment_date =null;
            $total_fees_payed=0;
            $total_payable_fees=0;

            // for hostel fee
            if($teacher->user->dweller) {

                //for month count between two date
                $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
                $total_payable_fees = (int)$school_hostel_fee *  $period->count();

                //  for each month list
                foreach ($period as $dt) {
                    $discount_amount = null;
                    $payment_date=null;
                    $dueamount =null;

                    $hostel_fee_find = $teacher->user->dweller->hostelFees()->where('month', $dt->format('m'))->whereBetween('created_at', [$start_date, $end_date])->first();

                    if ($hostel_fee_find != null) {
                        if ($hostel_fee_find->payed_amount == $school_hostel_fee) {
                            $payedamount = $hostel_fee_find->payed_amount;
                        } else {
                            $payedamount = $hostel_fee_find->payed_amount;
                            $discount_amount = $hostel_fee_find->discount;
                        }

                        $dueamount = $hostel_fee_find->due?$hostel_fee_find->due:null;
                        $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                        $payment_date=date('d-m-Y',strtotime($hostel_fee_find->created_at));
                    } else {
                        $payedamount = $school_hostel_fee;
                        $dueamount = $school_hostel_fee;
                    }

                    $new_reports[$i] = [
                        'name' => 'Hostel Fees',
                        'date' => $payment_date?$payment_date:$dt->format('d-m-Y'),
                        'paid' => (int)$payedamount,
                        'discount' => (int)$discount_amount,
                        'due' => (int)$dueamount,
                    ];

                    $i++;
                }


            }/*else{

                $notification = [
                    'message' => 'Teacher is not a dweller.',
                    'alert-type' => 'error'
                ];
                return redirect()->route('admin.teacher-report.accounts-calculate')->with($notification);
            }*/

//            return view('backend.pages.reports.teacher-reports.hostel-fees',compact('title','teacher_name','new_reports','total_fees_payed','total_payable_fees'));
            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed
            ];

            return $data;
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    /**
     * For Transportation fees month wise report
     */
    public function singleTransportationFeeReport(Request $request){

        try{

            $teacher = Teacher::where('unique_id',$request->unique_id)->first();
            $teacher_name = $teacher->user->name;

            $start_date= date('Y-m-d H:s:i',strtotime($request->start_date));
            $end_date= date('Y-m-d H:s:i',strtotime($request->end_date));

            $school_transportation_fee = setting('school_fees.transportation_fee.transportation_fee');

            $new_reports=[];
            $i=0;
            $payment_date =null;
            $total_fees_payed=0;
            $total_payable_fees=0;

            // for hostel fee
            if($teacher->user->passenger) {

                //for month count between two date
                $period = CarbonPeriod::create($request->start_date, '1 month', $request->end_date);
                $total_payable_fees = (int)$school_transportation_fee *  $period->count();

                //  for each month list
                foreach ($period as $dt) {
                    $discount_amount =null;
                    $payment_date=null;
                    $dueamount =null;

                    $transportation_fee_find = $teacher->user->passenger->transportationFees()->where('month', $dt->format('m'))->whereBetween('created_at', [$start_date, $end_date])->first();

                    if ($transportation_fee_find != null) {
                        if ($transportation_fee_find->payed_amount == $school_transportation_fee) {
                            $payedamount = $transportation_fee_find->payed_amount;
                        } else {
                            $payedamount = $transportation_fee_find->payed_amount;
                            $discount_amount = $transportation_fee_find->discount;
                        }

                        $dueamount = $transportation_fee_find->due?$transportation_fee_find->due:null;
                        $total_fees_payed = (int)$total_fees_payed+ (int)$payedamount;
                        $payment_date=date('d-m-Y',strtotime($transportation_fee_find->created_at));
                    } else {
                        $payedamount = $school_transportation_fee;
                        $dueamount = $school_transportation_fee;
                    }

                    $new_reports[$i] = [
                        'name'=>'Transportation Fee',
                        'date' => $payment_date?$payment_date:$dt->format('d-m-Y'),
                        'paid' => (int)$payedamount,
                        'discount' => (int) $discount_amount,
                        'due' => (int)$dueamount,
                    ];

                    $i++;
                }


            }/*else{

                $notification = [
                    'message' => 'Teacher is not a passenger.',
                    'alert-type' => 'error'
                ];
                return back()->with($notification);
            }*/
            $data = (object)[
                'new_reports' => $new_reports,
                'total_payable_fees' => $total_payable_fees,
                'total_fees_payed' => $total_fees_payed,
            ];

            return $data;
//            return view('backend.pages.reports.teacher-reports.transportation-fees',compact('title','teacher_name','new_reports','total_payable_fees','total_fees_payed'));
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }


    public function countFridays($dateFromString,$dateToString){
        $dateTo = Carbon::parse($dateToString);
        $friday = Carbon::parse($dateFromString . ' next friday');
        $fridays = [];

        while($friday->lt($dateTo)) {
            $fridays[] = $friday->toDateString();
            $friday->addWeek();
        }

        return $fridays;
    }


}
