<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\KeyPerson;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class WizardKeyPersonController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $title = 'Key Persons';
            $key_persons = KeyPerson::all();

            return view('backend.pages.wizards.key-persons.index', compact('title','key_persons'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(KeyPerson $keyPerson=null)
    {
        try {
            $title = $keyPerson?'Key Person':'Add New Key Person';
            return view('backend.pages.wizards.key-persons.form', [
                'title' => $title,
                'keyPerson'=>$keyPerson
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, KeyPerson $keyPerson=null)
    {


        $request->validate([
            'name' => 'required|string|max:255',
            'designation' => 'required|string|max:255',
            'position' => 'required',
        ], [
            'name' => __('Person name is required.'),
            'designation' => __('Person designation start time is required.'),
            'position' => __('Position is required.'),
        ]);

        if(!$keyPerson){
            $request->validate([
            'image' => 'required',
            ], [
            'image' => __('Person photo is required.'),
            ]);
        }

        $havePerson = KeyPerson::where(['left' => !$request->has('left')? false:true, 'position'=>$request->position])->first();
//        dd($havePerson);


        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('image')) {
                foreach ($request->image as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }

            if(empty($keyPerson)){
                $keyPerson = new KeyPerson();
            }


            if ($request->hasFile('image')) {

                if ($keyPerson->image != null) {
                    // delete existing image
                    $file = 'upload/key-persons/' . $keyPerson->image;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                $images = $request->image;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(713, 656)->save(public_path('/upload/key-persons/' . $filename));
                }
                $keyPerson->image = $filename;
            }

            $keyPerson->name = $request->name;
            $keyPerson->designation = $request->designation;
            $keyPerson->person_content = $request->person_content;
            $keyPerson->position = $request->position;

           if($request->has('left')){
               $keyPerson->left = true;
           }else{
               $keyPerson->left = false;
           }
            $keyPerson->save();

            if ($havePerson != null && $havePerson->id != $keyPerson->id){
                for($kp = $request->position; $kp < KeyPerson::where('left',$havePerson->left)->count(); $kp++){
                   $ukp = KeyPerson::whereNotIn('id',[$keyPerson->id])->where(['left'=>$havePerson->left, 'position'=>$kp])->first();
                    $ukp->position = $kp+1;
                    $ukp->save();
                }
            }

            $notification = array(
                'message' => 'Key person has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }





    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(KeyPerson $keyPerson)
    {

        try {

            if ($keyPerson->image != null) {
                // delete existing image
                $file = 'upload/key-persons/' . $keyPerson->image;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $keyPerson->delete();

            $notification = [
                'message' =>  'Key Person has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }


}
