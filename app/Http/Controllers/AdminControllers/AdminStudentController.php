<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStudentRequest;
use App\Imports\StudentsImport;
use App\Models\ClassRoom;
use App\Models\Profile;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class AdminStudentController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClassRoom $classRoom)
    {
        try {
            $title = 'Students of '.$classRoom->name;
            $class_name = $classRoom->name;
            $students = Student::where('class_id',$classRoom->id)->get();

            return view('backend.pages.students.index', compact('title', 'students','class_name'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Student $student = null)
    {
        try {
            $title = 'Add New Student';
            return view('backend.pages.students.form', [
                'teacher' => null,
                'title' => $title,
                'student' => $student
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function createBulk()
    {
        try {
            return view('backend.pages.students.create-bulk', [
                'title' => 'Insert Bulk Studetns',
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function downloadDemoFile()
    {
        return Storage::download('backend/files/students_demo.xlsx', 'Students Demo.xlsx');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStudentRequest $request)
    {
        $ganderArray = ['male','female','other'];
        if (!in_array($request->gender,$ganderArray)){
            return $this->backWithError('An invalid gander you selected');
        }
        try {

            if ($request->email) {
                $this->validate($request, [
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                ]);
            }
            if ($request->password != $request->confirm_password) {
                $notification = array(
                    'message' => 'Password and confirm password don\'t match',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            if (!ClassRoom::find($request->class_id)) {
                $notification = array(
                    'message' => 'Sorry! You selected an invalid class room',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            // Present and parmanebt Address
            $presentAddress = $request->present_road . ', ' . $request->present_city . ', ' . $request->present_thana . ', ' . $request->present_district . '-' . $request->present_post_office;
            $permanentAddress = $request->permanent_road . ', ' . $request->permanent_city . ', ' . $request->permanent_thana . ', ' . $request->permanent_district . '-' . $request->permanent_post_office;

            $user = new User();
            $user->name = $request->name;
            $user->user_type_id = 3;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->gender = $request->gender;
            $user->password = Hash::make($request->password);
            $user->save();

            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            $student = Student::create([
                'user_id' => $user->id,
                'class_id' => $request->class_id,
                'class_rol' => Student::count() + 1,
                'unique_id' => $uniqueID,
                'height' => $request->height,
                'age' => $request->age,
                'admission_date' => $request->admission_date,
                'blood_group' => $request->blood_group,
            ]);


            $st_profile = Profile::create([
                'user_id' => $user->id,
                'name_utf8' => $request->name_utf8,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8,
                'present_address' => $presentAddress,
                'permanent_address' => $permanentAddress,
            ]);


            $notification = [
                'message' => 'New Student added successfully',
                'alert-type' => 'success'
            ];

            return redirect()->route('admin.get-class-students',$student->class_id)->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function storeBulk(Request $request)
    {
        try {

            Excel::import(new StudentsImport, $request->file('file'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('ok');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student = null)
    {
        try {
            $title = 'Add New Student';
            return view('backend.pages.students.form', [
                'title' => $title,
                'student' => $student
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function studentActivation(Student $student)
    {
        try {
            if ($student->status) {
                $student->status = false;
                $student->save();
            } else {
                $student->status = true;
                $student->save();
            }
            return response()->json($student);
        } catch (\Exception $th) {
            return $this->backWithError($th->getMessage());
        }
    }



    /**
     * Promotion Student nex class
     */

    public function promotedToStudent($student){
        try{
            $students = [];
            foreach (explode(',',$student) as $key => $item){
                $itemStudent = Student::findOrFail($item!='allStudent'?$item:explode(',',$student)[$key-1]);
                if ($item != 'allStudent'){
                    $students[] = $itemStudent;
                }else{
                    $class = $itemStudent->classRoom;
                    $students = $class->student;
                }
            }
            foreach ($students as $student){
                $new_class = ClassRoom::find($student->class_id+1);
                if ($new_class == null){
                    break;
                }

                $class_roll = 1+ Student::where('class_id',$new_class->id)->whereYear('updated_at',date('Y'))->count();

                $student->class_id = $new_class->id;
                $student->class_rol = $class_roll;

                $student->save();
            }

            if (!$new_class){
                return response()->json([
                    'message' => 'Sorry! In promotion, there were no class',
                    'alert-type' => 'error'
                ]);
            }else{
                return response()->json([
                    'message' => 'Student has been Promoted successfully',
                    'alert-type' => 'success'
                ]);
            }

        } catch (\Exception $th) {
             return $this->backWithError($th->getMessage());
        }

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $ganderArray = ['male','female','other'];
        if (!in_array($request->gender,$ganderArray)){
            return $this->backWithError('An invalid gander you selected');
        }
        try {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'name_utf8' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'father_name_utf8' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],
                'mother_name_utf8' => ['required', 'string', 'max:255'],
                'class_id' => ['required', 'string', 'max:255'],
                'blood_group' => ['required', 'string', 'max:255'],
                'admission_date' => ['required', 'string', 'max:255'],
                'height' => ['required', 'string', 'max:255'],
                'age' => ['required', 'string', 'max:255'],
            ],[
                'name' => 'Name field is required',
                'name_utf8' => 'Name in Bangla field is required',
                'father_name' => 'Fathers name field is required',
                'father_name_utf8' => 'Fathers name in Bangla field is required',
                'mother_name' => 'Mothers name field is required',
                'mother_name_utf8' => 'Mothers name in Bangla field is required',
                'class_id' => 'Class field is required',
                'blood_group' => 'Blood group field is required',
                'admission_date' => 'Admission date field is required',
                'height' => 'Height field is required',
                'age' => 'Age field is required',
            ]);

            $user = $student->user;
            $profile = $student->user->profile;


            $this->validate($request, [
                'name' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],

            ]);


            $user->forceFill([
                'name' => $request->name,
                'gender' => $request->gender
            ])->save();


            $profile->update([
                'name_utf8' => $request->name_utf8,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8,
            ]);


            $student->forceFill([
                'class_id' => $request->class_id,
                'height' => $request->height,
                'age' => $request->age,
                'admission_date' => $request->admission_date,
                'blood_group' => $request->blood_group,
            ])->save();

            return $this->backWithSuccess('Student has been updated successfully');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        try {
            $student->user->profile->delete();
            $student->user->deleteProfilePhoto();
            $student->user->tokens->each->delete();
            $student->user()->delete();
            $student->delete();
            $notification = [
                'message' =>  $student->user->name . ' has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }


    public function download(Student $student){
        try {
            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';
            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';
            $bg = asset('forntend/image/card-bg.jpg');
//            $bg = 'https://gendagps.com/forntend/image/card-bg.jpg';

            $pdf = PDF::loadView('backend.pages.students.card-download', compact( 'student','avatar', 'logo','bg'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 80,
                'title'                => 'ID Card',
            ]);

            $tname= 'id_card'.rand(2,50);
            return $pdf->stream($tname.'.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


}
