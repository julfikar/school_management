<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\VideoSlider;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class WizardSliderController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $title = 'Sliders';
            $sliders = Slider::all();

            return view('backend.pages.wizards.sliders.index', compact('title','sliders'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = 'Add New Slider';
            return view('backend.pages.wizards.sliders.form', [
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show(Slider $slider)
    {
        try {
            $title = 'Slider';
            return view('backend.pages.wizards.sliders.update-form', [
                'title' => $title,
                'slider'=>$slider
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('slider_name')) {
                foreach ($request->slider_name as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }


            if ($request->hasFile('slider_name')) {

                $images = $request->slider_name;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
                        ->resize(1536, 480)
                        ->save(public_path('/upload/sliders/' . $filename));

                    $slider = new Slider();
                    $slider->name = $filename;
                    $slider->status = true;
                    $slider->save();
                }
            }



            $notification = array(
                'message' => 'Slider has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
            'message' => $e->getMessage(),
            'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function update(Request $request, Slider $slider)
    {

        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('slider_name')) {
                foreach ($request->slider_name as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }


            if ($request->hasFile('slider_name')) {

                if ($slider->name != null) {
                    // delete existing image
                    $file = 'upload/sliders/' . $slider->name;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                $images = $request->slider_name;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
                        ->resize(1536, 480)
                        ->save(public_path('/upload/sliders/' . $filename));
                    $slider->name = $filename;
                }
            }

            $slider->save();

            $notification = array(
                'message' => 'Slider has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function sliderActivation(Slider $slider)
    {
        try{
            if($slider->status){
                $slider->status = false;
                $slider->save();
            }else{
                $slider->status = true;
                $slider->save();
            }
            return response()->json($slider);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider)
    {
        try {

            if ($slider->name != null) {
                // delete existing image
                $file = 'upload/sliders/' . $slider->name;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $slider->delete();

            $notification = [
                'message' =>  'Slider has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }
}
