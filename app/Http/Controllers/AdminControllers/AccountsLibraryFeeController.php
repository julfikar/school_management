<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\LibraryFee;
use App\Models\LibraryMember;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccountsLibraryFeeController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * For Library fees
     */
    public function index(){

        try {
            $title = 'library fee';
            $library_members = LibraryMember::all();
            $library_fees = json_decode(json_encode(setting('school_fees.library_fee')));
            return view('backend.pages.accounts.library-fees.form', compact('title','library_members','library_fees'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }



    public function storeLibraryFee(Request $request)
    {


        try {
            $this->validate($request, [
                'member_id' => 'required',
                'month' => ['required'],
                'payed_amount' => ['required'],
            ]);

            $now = Carbon::now();
            $payed_a_month = LibraryFee::where('member_id',$request->member_id)->where('month',$request->month)->whereYear('created_at', '=', $now->year)->first();

            if($payed_a_month!=null){

                $notification = [
                    'message' => 'Library fee already paid in this month',
                    'alert-type' => 'error'
                ];

                return back()->with($notification);
            }

            $tution_fee = new LibraryFee();
            $tution_fee->member_id = $request->member_id;
            $tution_fee->month = $request->month;
            $tution_fee->payed_amount = $request->payed_amount;

            if($request->discount){
                $tution_fee->discount = $request->discount;
            }
            if($request->due){
                $tution_fee->due = $request->due;
            }
            // Get todays date
            $today = Carbon::now();
            $t_day = date('d', strtotime($today));
            $t_year = date('Y', strtotime($today));

            // convert request month to created at month
            $input  = $t_day.'/'.$request->month.'/'.$t_year;
            $format = 'd/m/Y';
            $month_date = Carbon::createFromFormat($format, $input);
            $tution_fee->created_at = $month_date;

            $tution_fee->save();

            $notification = [
                'message' => 'Library fee paid successfully',
                'alert-type' => 'success'
            ];

            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }


}
