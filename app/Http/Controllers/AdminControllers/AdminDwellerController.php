<?php

namespace App\Http\Controllers\AdminControllers;

use App\Models\Dweller;
use App\Models\Hostel;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminDwellerController extends Controller
{


    public function index(Hostel $hostel){
        try {
            $title = $hostel->name;
            $users = User::all();

            return view('backend.pages.dwellers.index', compact('title', 'hostel','users'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function create(Hostel $hostel)
    {

        try {
            $title = $hostel->name.' New Dweller';
            $student = Student::all();

            return view('backend.pages.dwellers.create-edit', [
                'hostel' => $hostel,
                'student' => $student,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'user_id' => 'required',
        ], [
            'user_id' => 'Dweller name is required.',

        ]);

        try {

            $find_dweller = Dweller::where('user_id',$request->user_id)->first();

            if($find_dweller !=null){

                $notification = array(
                    'message' => 'Dweller already exists.',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }
            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            $dweller = new Dweller();
            $dweller->user_id = $request->input('user_id');
            $dweller->hostel_id = $request->input('hostel_id');
            $dweller->unique_id = $uniqueID;

            $dweller->save();

            $notification = array(
                'message' => 'New Dweller has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);

        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function destroy(Dweller $dweller){
        try{

            $dweller->delete();

            $notification = array(
                'message' => 'Dweller has been deleted successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


}
