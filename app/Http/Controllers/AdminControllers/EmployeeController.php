<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;
use App\Imports\EmployeesImport;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'All Employees';
            $employees = Employee::all();
            return view('backend.pages.employees.index', compact('title', 'employees'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Show the form for creating a new employee.
     */
    public function create(Employee $employee = null)
    {
        try {
            $title = 'Add Employee';
            $designations = Designation::all();
            $roles = Role::whereNotIn('name', ['super admin', 'headmaster', 'teacher'])->get();
            return view('backend.pages.employees.form', compact('title', 'designations', 'roles', 'employee'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }


    public function createBulk()
    {
        try {
            return view('backend.pages.employees.create-bulk', [
                'title' => 'Insert All Employee',
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }


    public function downloadDemoFile()
    {
        return Storage::download('backend/files/Employees_demo.xlsx', 'Employee Demo.xlsx');
    }

    public function store(Request $request)
    {


        try {

            $this->validate($request, [
                'name' => 'required|string|max:25|unique:roles',
                'name_utf8' => 'required|string',
                'father_name_utf8' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],
                'mother_name_utf8' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:14', 'unique:users'],
                'nid' => 'required',
                'join_date' => 'required',

                'present_district' => 'required|string|max:255',
                'permanent_district' => 'required|string|max:255',
                'present_thana' => 'required|string|max:255',
                'permanent_thana' => 'required|string|max:255',
                'present_city' => 'required|string|max:255',
                'permanent_city' => 'required|string|max:255',
                'present_post_office' => 'required|string|max:255',
                'permanent_post_office' => 'required|string|max:255',
                'present_road' => 'required|string|max:255',
                'permanent_road' => 'required|string|max:255',

            ]);


            if (isset($request->email)) {
                $this->validate($request, [
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users']
                ]);
            }
            if ($request->password != $request->confirm_password) {
                $notification = array(
                    'message' => 'Password and confirm password don\'t match',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('cv_file')) {
                if (!in_array($request->cv_file->getClientOriginalExtension(), $pdf_acceptable)) {
                    $notification = array(
                        'message' => 'Only pdf file is supported.',
                        'alert-type' => 'error'
                    );
                    return back()->with($notification);
                }
            }


            DB::beginTransaction();

            $user = new User();
            $user->name = $request->name;
            $user->user_type_id = 2;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            //assign the user to a role
            $user->assignRole($request->role_name);



            if ($request->hasFile('cv_file')) {

                // insert new file
                $new_cv = $request->cv_file;
                //file name
                $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                $x = str_shuffle($x);
                $x = substr($x, 0, 6) . 'DAC.';
                $cvfile = time() . $x . $new_cv->getClientOriginalExtension();
                // Upload file
                $new_cv->move(public_path('/upload/employee/cv/'), $cvfile);
            } else {
                $cvfile = null;
            }

            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            $employee = new Employee();
            $employee->user_id = $user->id;
            $employee->unique_id = $uniqueID;
            $employee->designation_id = $request->designation_id;
            $employee->nid = $request->nid;
            $employee->join_date = $request->join_date;
            $employee->cv_file = $cvfile;

            $employee->save();


            $presentAddress = $request->present_road . ', ' . $request->present_city . ', ' . $request->present_thana . ', ' . $request->present_district . '-' . $request->present_post_office;
            $permanentAddress = $request->permanent_road . ', ' . $request->permanent_city . ', ' . $request->permanent_thana . ', ' . $request->permanent_district . '-' . $request->permanent_post_office;

            Profile::create([
                'user_id' => $user->id,
                'name_utf8' => $request->name_utf8,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8,
                'present_address' => $presentAddress,
                'permanent_address' => $permanentAddress
            ]);

            DB::commit();

            $notification = [
                'message' => 'New Employee added successfully',
                'alert-type' => 'success'
            ];

            return redirect()->route('admin.employee.index')->with($notification);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->backWithError($th->getMessage());
        }
    }

    public function storeBulk(Request $request)
    {
        try {

            Excel::import(new EmployeesImport, $request->file('file'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('ok');
    }


    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Employee $employee)
    {

        try {

            $this->validate($request, [
                'name' => 'required|string|max:25|unique:roles',
                'name_utf8' => 'required|string|max:25|unique:roles',
                'father_name_utf8' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],
                'mother_name_utf8' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:14', 'unique:users'],
                'nid' => 'required',
                'join_date' => 'required',

                'present_district' => 'required|string|max:255',
                'permanent_district' => 'required|string|max:255',
                'present_thana' => 'required|string|max:255',
                'permanent_thana' => 'required|string|max:255',
                'present_city' => 'required|string|max:255',
                'permanent_city' => 'required|string|max:255',
                'present_post_office' => 'required|string|max:255',
                'permanent_post_office' => 'required|string|max:255',
                'present_road' => 'required|string|max:255',
                'permanent_road' => 'required|string|max:255',

            ]);

            $user = $employee->user;
            $profile = $employee->user->profile;

            $this->validate($request, [
                'name' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],

                'join_date' => 'required',
                'designation_id' => 'required',
                'nid' => 'required',
                'role_name' => 'required',
            ]);

            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('cv_file')) {
                if (!in_array($request->cv_file->getClientOriginalExtension(), $pdf_acceptable)) {
                    $notification = array(
                        'message' => 'Only pdf file is supported.',
                        'alert-type' => 'error'
                    );
                    return back()->with($notification);
                }
            }

            DB::beginTransaction();

            $user->forceFill([
                'name' => $request->name,
            ])->save();

            //assign the user to a role
            $user->syncRoles([]);
            $user->assignRole($request->role_name);

            if ($request->hasFile('cv_file')) {

                if ($employee->cv_file != null) {
                    // delete existing file
                    $file = 'upload/employee/cv/' . $employee->cv_file;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                // insert new file
                $new_cv = $request->cv_file;
                //file name
                $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                $x = str_shuffle($x);
                $x = substr($x, 0, 6) . 'DAC.';
                $cvfile = time() . $x . $new_cv->getClientOriginalExtension();
                // Upload image
                $new_cv->move(public_path('/upload/employee/cv/'), $cvfile);
            } else {
                $cvfile = $employee->cv_file;
            }

            $profile->update([
                'name_utf8' => $request->name_utf8,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8
            ]);


            $employee->forceFill([
                'join_date' => $request->join_date,
                'designation_id' => $request->designation_id,
                'nid' => $request->nid,
                'cv_file' => $cvfile,
            ])->save();
            DB::commit();

            return redirect()->route('admin.employee.index', $employee->id)->with([
                'message' => 'Employee has been updated successfully',
                'alert-type' => 'success',
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function employeeActivation(Employee $employee)
    {
        try {
            if ($employee->status) {
                $employee->status = false;
                $employee->save();
            } else {
                $employee->status = true;
                $employee->save();
            }
            return response()->json($employee);
        } catch (\Exception $th) {
            return $this->backWithError($th->getMessage());
        }
    }


    /*
   *  Destroy a Employee
   */
    public function destroy(Employee $employee)
    {
        try {

            if ($employee->cv_file != null) {
                // delete existing image
                $file = 'upload/employee/cv/' . $employee->cv_file;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }

            $employee->user->profile->delete();
            $employee->user->deleteProfilePhoto();
            $employee->user->tokens->each->delete();
            $employee->user->syncRoles([]);
            $employee->user->delete();
            $employee->delete();

            $notification = [
                'message' =>  $employee->user->name . ' has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }
}
