<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Admission;
use App\Models\Student;
use App\Models\User;
use Illuminate\Http\Request;

class AdminAdmissionController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $title = 'New Students';
            $students = Admission::orderBy('new_class','asc')->get();

            return view('backend.pages.admissions.index', compact('title', 'students'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


    public function admittedToStudent(Admission $admission)
    {
        try {
            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

             Student::create([
                'user_id' => $admission->user_id,
                'class_id' => $admission->new_class,
                'class_rol' => Student::count() + 1,
                'unique_id' => $uniqueID,
                'admission_date' => today(),
                'age' => $admission->age,
                'blood_group' => $admission->blood_group,
            ]);

            $user = User::find($admission->user_id);
            $user->user_type_id = 3;
            $user->save();

            if ($admission->mark_sheet != null) {
                // delete existing image
                $file = 'upload/mark-sheets/' . $admission->mark_sheet;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $admission->delete();

            return response()->json( ['message' => 'Student has been admitted successfully']);

        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function destroy(Admission $admission)
    {
        try {
            $admission->user->profile->delete();
            $admission->user->deleteProfilePhoto();
            $admission->user->tokens->each->delete();
            $admission->user()->delete();
            if ($admission->mark_sheet != null) {
                // delete existing image
                $file = 'upload/mark-sheets/' . $admission->mark_sheet;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $admission->delete();
            $notification = [
                'message' =>  $admission->user->name . ' has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
}
