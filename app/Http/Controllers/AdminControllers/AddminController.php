<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Admission;
use App\Models\ClassRoom;
use App\Models\Profile;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AddminController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        try {
            $title = 'My Dashboard';
//            return view('dashboard');
            $allUsers = User::count();
            $allTeachers = Teacher::count();
            $allStudents = Student::count();
            $allWantToBeAdmitted = Admission::count();
            return view('backend.pages.dashboard', compact('title','allUsers', 'allTeachers', 'allStudents', 'allWantToBeAdmitted'));
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function profile()
    {
        try {
            $title = 'My profile';
            return view('backend.pages.profile.show', compact('title'));
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function profileUpdate(Request $request)
    {
        try {
            $input = $request->all();
            if ($request->hasFile('photo')){
                $input['photo'] = $request->photo[0];
            }
            $user = Auth::user();

            Validator::make($input, [
                'name' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:14', Rule::unique('users')->ignore($user->id)],
                'photo' => ['nullable', 'mimes:jpg,jpeg,png', 'max:1024'],
            ])->validateWithBag('updateProfileInformation');

            if (isset($input['photo'])) {
                $user->updateProfilePhoto($input['photo']);
            }

            if($input['email'] != null){
                Validator::make($input,[
                    'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
                ]);
            }


            if ($input['email'] !== $user->email &&
                $user instanceof MustVerifyEmail) {
                $this->updateVerifiedUser($user, $input);
            } else {
                $user->forceFill([
                    'name' => $input['name'],
                    'phone' => $input['phone'],
                    'email' => $input['email'],
                ])->save();
            }


            if ($request->has('name_utf8')){
                $profile = $user->profile;
                $profile->name_utf8 = $request->name_utf8;
                $profile->save();
            }

            $notification = array(
                'message' => $user->name.'\'s personal information has been updated successfully',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function profileInfoUpdate(Request $request)
    {
        try {
            $user = Auth::user();
            if (!$user->profile){
                $this->validate($request, [
                    'present_district' => 'required|string|max:255',
                    'permanent_district' => 'required|string|max:255',
                    'present_thana' => 'required|string|max:255',
                    'permanent_thana' => 'required|string|max:255',
                    'present_city' => 'required|string|max:255',
                    'permanent_city' => 'required|string|max:255',
                    'present_post_office' => 'required|string|max:255',
                    'permanent_post_office' => 'required|string|max:255',
                    'present_road' => 'required|string|max:255',
                    'permanent_road' => 'required|string|max:255',
                ]);

                $profile = new Profile();
                $presentAddress = $request->present_road.', '.$request->present_city.', '.$request->present_thana.', '.$request->present_district.'-'.$request->present_post_office;
                $permanentAddress = $request->permanent_road.', '.$request->permanent_city.', '.$request->permanent_thana.', '.$request->permanent_district.'-'.$request->permanent_post_office;
            }else{
                $this->validate($request, [
                    'present_address' => 'required|string|max:255',
                    'permanent_address' => 'required|string|max:255',
                ]);

                $profile = $user->profile;
                $presentAddress = $request->present_address;
                $permanentAddress = $request->permanent_address;
            }

            $this->validate($request, [
                'father_name' => 'required|string|max:255',
                'father_name_utf8' => 'required|string|max:255',
                'mother_name' => 'required|string|max:255',
                'mother_name_utf8' => 'required|string|max:255',
                'about' => 'required',
            ]);


            $profile->user_id = $user->id;
            $profile->father_name = $request->father_name;
            $profile->father_name_utf8 = $request->father_name_utf8;
            $profile->mother_name = $request->mother_name;
            $profile->mother_name_utf8 = $request->mother_name_utf8;
            $profile->present_address = $presentAddress;
            $profile->permanent_address = $permanentAddress;
            $profile->about = $request->about;
            $profile->facebook_link = $request->facebook_link;
            $profile->whatsapp_link = $request->whatsapp_link?'https://wa.me/'.$request->whatsapp_link:null;
//            $profile->imo_link = $request->imo_link;
            $profile->linked_in = $request->linked_in;
            $profile->save();


            $notification = array(
                'message' => $user->name.'\'s personal information has been updated successfully',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    protected function updateVerifiedUser($user, array $input)
    {
        try {
            $user->forceFill([
                'name' => $input['name'],
                'phone' => $input['phone'],
                'email' => $input['email'],
                'email_verified_at' => null,
            ])->save();

//            $user->sendEmailVerificationNotification();
            $notification = array(
                'message' => $user->name.'\'s personal information has been updated successfully',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function adminDelete(Request $request)
    {
        try {
            $user = Auth::user();
            if (! isset($request->password) || ! Hash::check($request->password, $user->password)) {
                $notification = array(
                    'message' => 'The provided password does not match your current password.',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }
            $user->deleteProfilePhoto();
            $user->tokens->each->delete();
            $user->delete();
            return redirect()->back();
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function getAllClassRoom(){

        try{
            $title = 'All Class';
            return view('backend.pages.class-rooms.index', compact('title'));
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function showClassRoom(ClassRoom $classroom){

        try{
            $title = 'Class';
            $teachers = Teacher::all();
            return view('backend.pages.class-rooms.edit-class-room', compact('title','classroom','teachers'));
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function classRoomUpdate(Request $request,ClassRoom $classroom=null){
        try {
            $this->validate($request,[
                'name'=>['required','max:255'],
                'room_no'=>['required','max:255'],
                'teacher_id'=>'required'
            ]);
            $classroom->forceFill([
                'name' => strtolower($request->name),
                'room_no' => $request->room_no,
                'teacher_id' => $request->teacher_id,
            ])->save();

            $notification = array(
                'message' => $classroom->name.' has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.get-classrooms')->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }
}
