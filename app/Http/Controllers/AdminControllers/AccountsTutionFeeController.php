<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\TutionFee;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccountsTutionFeeController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * For tutions fees
     */
    public function index(){

        try {
            $title = 'tuition fee';
            $tution_fees= json_decode(json_encode(setting('school_fees.tuition_fee')));
            $allStudents = Student::all();
            return view('backend.pages.accounts.tution-fees.form', compact('title','tution_fees', 'allStudents'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }


    public function filterStudentByClass($classId)
    {
        $students = Student::with('user')->where('class_id', $classId)->get();
        return response()->json($students);
    }


    public function storeTutionFee(Request $request)
    {

        try {
            $this->validate($request, [
                'class_id' => 'required',
                'student_id' => ['required'],
                'month' => ['required'],
                'payed_amount' => ['required'],
            ]);

            $student = Student::find($request->student_id);
            $now = Carbon::now();
            $payed_a_month = TutionFee::where('student_id',$request->student_id)->where('month',$request->month)->whereYear('created_at', '=', $now->year)->first();

            if($payed_a_month!=null){
                return $this->backWithError('Tution fee already paid in this month');
            }

            if ($student->classRoom->id != $request->class_id){
                return $this->backWithError('Please enter a valid student id or name');
            }

            $tution_fee = new TutionFee();
            $tution_fee->class_id = $request->class_id;
            $tution_fee->student_id = $student->id;
            $tution_fee->month = $request->month;
            $tution_fee->payed_amount = $request->payed_amount;

            if($request->discount){
                $tution_fee->discount = $request->discount;
            }
            if($request->due){
                $tution_fee->due = $request->due;
            }
            // Get todays date
            $today = Carbon::now();
            $t_day = date('d', strtotime($today));
            $t_year = date('Y', strtotime($today));

            // convert request month to created at month
            $input  = $t_day.'/'.$request->month.'/'.$t_year;
            $format = 'd/m/Y';
            $month_date = Carbon::createFromFormat($format, $input);
            $tution_fee->created_at = $month_date;

            $tution_fee->save();

            return $this->backWithSuccess('Tution fee paid successfully');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }
}
