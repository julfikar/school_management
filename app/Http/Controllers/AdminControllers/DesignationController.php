<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Designation;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'All Employees';
            $designations = Designation::all();
            return view('backend.pages.designations.index', compact('title', 'designations'));
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function createForm(Designation $designation=null){
        try {
            $title = $designation?$designation->name:'Add Designations';
            return view('backend.pages.designations.form', compact('title', 'designation'));

        }catch (\Throwable $e){
            $notification = array(
            'message' => $e->getMessage(),
            'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request, Designation $designation=null){
        try {

            $this->validate($request, [
                'name' => 'required|string|max:100',
                'salary'=>'required',
            ]);

            if(empty($designation)){
                $designation = new Designation();
            }

            $designation->name = $request->name;
            $designation->salary = $request->salary;
            $designation->note = $request->note;
            $designation->save();


            $notification = [
                'message' => 'New designation added successfully',
                'alert-type' => 'success'
            ];

            return redirect()->route('admin.designation.index')->with($notification);
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


    public function destroy(Designation $designation)
    {
        try {

            foreach ($designation->employee as $employee){
                $employee->delete();
            }
            $designation->delete();

            $notification = [
                'message' => 'Designation has been deleted successfully',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.designation.index')->with($notification);
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }



}
