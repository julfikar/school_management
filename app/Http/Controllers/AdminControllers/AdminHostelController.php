<?php

namespace App\Http\Controllers\AdminControllers;

use App\Models\Employee;
use App\Models\Hostel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminHostelController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }


    public  function index(){
        try {
            $title = 'All Hostels';
            $hostels = Hostel::all();

            return view('backend.pages.hostels.index', compact('title', 'hostels'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function create(){
        try {
            $title = 'Add New Hostel';
            $employees = Employee::all();
            return view('backend.pages.hostels.create-edit', [
                'title' => $title,
                'employees' => $employees,
                'hostel'=>null
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'employee_id' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ], [
            'name' => 'Admin name is required.',
            'employee_id' => 'Employee is required.',
            'phone' => 'Phone is required.',
            'address' => 'Address is required.',

        ]);

        try {

            $hostel = new Hostel();

            $hostel->name = $request->input('name');
            $hostel->employee_id = $request->input('employee_id');
            $hostel->phone = $request->input('phone');
            $hostel->address = $request->input('address');

            $hostel->save();

            $notification = array(
                'message' => 'Hostel has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.hostels.index')->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show(Hostel $hostel)
    {
        try{
            $title = $hostel->name;
            $employees = Employee::all();
            return view('backend.pages.hostels.create-edit', [
                'title' => $title,
                'employees' => $employees,
                'hostel' => $hostel,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
            'message' => $e->getMessage(),
            'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function update(Request $request,$id=null){
        $request->validate([
            'name' => 'required',
            'employee_id' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ], [
            'name' => 'Admin name is required.',
            'employee_id' => 'Employee is required.',
            'phone' => 'Phone is required.',
            'address' => 'Address is required.',

        ]);

        try {

            $hostel = Hostel::find($id);

            $hostel->name = $request->input('name');
            $hostel->employee_id = $request->input('employee_id');
            $hostel->phone = $request->input('phone');
            $hostel->address = $request->input('address');

            $hostel->save();

            $notification = array(
                'message' => 'Hostel has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('admin.hostels.index')->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function destroy(Hostel $hostel){

        try {

            foreach ($hostel->dwellers as $dweller){
                $dweller->delete();
            }
            $hostel->delete();

            $notification = [
                'message' =>  'Hostel has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.hostels.index')->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


}
