<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Ebooks;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminEbookController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }


    public  function index(){
        try {
            $title = 'All E-Books';
            $ebooks = Ebooks::all();

            return view('backend.pages.e-books.index', compact('title', 'ebooks'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function create(Ebooks $ebook=null ){
        try {
            $title = 'Add New E-book';
            return view('backend.pages.e-books.create-edit', [
                'ebook' => $ebook,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'e_book' => 'required',
            'thumbnail' => 'required',
        ], [
            'name' => 'Admin name is required.',
            'e_book' => 'Ebook is required.',
            'thumbnail' => 'Thumbnail is required.',

        ]);

        try {

            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        return back()->with('error', 'Only jpeg, png, jpg and gif file is supported.');
                    }
                }
            }

            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('e_book')) {
                if (!in_array($request->e_book->getClientOriginalExtension(), $pdf_acceptable)) {
                    $notification = array(
                        'message' => 'Only pdf file is supported.',
                        'alert-type' => 'error'
                    );
                    return back()->with($notification);
                }
            }

            $ebook = new Ebooks();

            if ($request->hasFile('thumbnail')) {

                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(270, 500)->save(public_path('/upload/e_books/thumbnails/' . $filename));
                }
                $ebook->thumbnail = $filename;
            }


            if ($request->hasFile('e_book')) {

                // insert new image
                $new_ebook = $request->e_book;
                //image name
                $nebook = time() . '.' . $new_ebook->getClientOriginalName();
                // Upload image
                $new_ebook->move(public_path('/upload/e_books/'), $nebook);
                $ebook->e_book = $nebook;
            }

            $ebook->name = $request->input('name');

            $ebook->save();

            $notification = array(
                'message' => 'E-Book has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show($id){
        try {
            $ebook = Ebooks::find($id);
            $title = $ebook->name;
            return view('backend.pages.e-books.create-edit', [
                'ebook' => $ebook,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function update(Request $request,$id=null){

        try {
            $request->validate([
                'name' => 'required',

            ], [
                'name' => 'Admin name is required.',

            ]);

            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        return back()->with('error', 'Only jpeg, png, jpg and gif file is supported.');
                    }
                }
            }

            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('e_book')) {
                if (!in_array($request->e_book->getClientOriginalExtension(), $pdf_acceptable)) {
                    $notification = array(
                        'message' => 'Only pdf file is supported.',
                        'alert-type' => 'error'
                    );
                    return back()->with($notification);
                }
            }

            $ebook = Ebooks::find($id);


            if ($request->hasFile('thumbnail')) {

                if ($ebook->thumbnail != null) {
                    // delete existing image
                    $file = 'upload/e_books/thumbnails/' . $ebook->thumbnail;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }
                // insert new image
                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(270, 500)->save(public_path('/upload/e_books/thumbnails/' . $filename));
                }
                $ebook->thumbnail = $filename;
            }



            if ($request->hasFile('e_book')) {

                if ($ebook->e_book != null) {
                    // delete existing image
                    $file = 'upload/e_books/' . $ebook->e_book;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                // insert new file
                $new_ebook = $request->e_book;
                //image name
                $nebook = time() . '.' . $new_ebook->getClientOriginalName();
                // Upload image
                $new_ebook->move(public_path('/upload/e_books/'), $nebook);
                $ebook->e_book = $nebook;
            }

            $ebook->name = $request->input('name');
            $ebook->save();

            $notification = array(
                'message' => 'Ebooks has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function destroy(Ebooks $ebook){
        try {
            if ($ebook->thumbnail != null) {

                // delete existing image
                $file = 'upload/e_books/thumbnails/' . $ebook->thumbnail;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            if ($ebook->e_book != null) {
                // delete existing image
                $file = 'upload/e_books/' . $ebook->e_book;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $ebook->delete();

            $notification = [
                'message' =>  'Ebook has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.ebooks.index')->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


}
