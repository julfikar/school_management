<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Resource;
use App\Models\Subject;
use Illuminate\Http\Request;

class AdminResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Subject $subject, Resource $resource = null)
    {
        try {
            $title = 'All Resource';

            return view('backend.pages.resources.index', compact('title', 'subject', 'resource'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function create(Subject $subject)
    {

        try {
            $title = 'Add New Resource';

            return view('backend.pages.resources.create-edit', [
                'subject' => $subject,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store the specified resource in storage.
     */
    public function store(Request $request)
    {
        try {

            $request->validate([
                'name' => 'required',
                'file' => 'required',
            ], [
                'name' => 'Admin name is required.',
                'file' => 'Resource file  is required.',

            ]);

            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('file')) {
                foreach ($request->file as $file) {
                    if (!in_array($file->getClientOriginalExtension(), $pdf_acceptable)) {
                        return back()->with('error', 'Only pdf file is supported.');
                    }
                }
            }

            $resource = new Resource();

            if ($request->hasFile('file')) {

                // insert new image
                $new_file = $request->file;
                //image name
                $file = time() . '.' . $new_file->getClientOriginalName();
                // Upload image
                $new_file->move(public_path('/upload/resources/'), $file);
                $resource->file = $file;
            }

            $resource->name = $request->input('name');
            $resource->subject_id = $request->input('subject_id');

            $resource->save();

            $notification = array(
                'message' => 'Resource has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function showUpdateForm(Resource $resource)
    {
        try {
            $title = $resource->name;

            return view('backend.pages.resources.update-form', compact('title', 'resource'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {

            $request->validate([
                'name' => 'required',
            ], [
                'name' => 'Admin name is required.',
            ]);

            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('file')) {
                foreach ($request->file as $file) {
                    if (!in_array($file->getClientOriginalExtension(), $pdf_acceptable)) {
                        return back()->with('error', 'Only pdf file is supported.');
                    }
                }
            }

            $resource = Resource::find($request->rid);

            if ($request->hasFile('file')) {

                if ($resource->file != null) {
                    // delete existing image
                    $file = 'upload/resources/' . $resource->file;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }
                // insert new image
                $new_file = $request->file;
                //image name
                $file = time() . '.' . $new_file->getClientOriginalName();
                // Upload image
                $new_file->move(public_path('/upload/resources/'), $file);
                $resource->file = $file;
            }

            $resource->name = $request->input('name');

            $resource->save();

            $notification = array(
                'message' => 'Resource has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $resource = Resource::find($request->rid);

            if ($resource->file != null) {
                // delete existing image
                $file = 'upload/resources/' . $resource->file;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }

            $resource->delete();

            $notification = [
                'message' =>  'Resource has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return redirect()->route('admin.get-subject-resources',$request->subject_id)->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
}
