<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\AccountSetting;
use App\Models\ClassRoom;
use App\Models\Student;
use App\Models\TutionFee;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminAccountSettingsController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'Accounts';
            $account_settings = AccountSetting::all();
            return view('backend.pages.accounts.account-settings.index', compact('title','account_settings'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function accountSettingActivation($id)
    {
        try{
            $accountSetting = AccountSetting::where('id',$id)->first();
            if($accountSetting->status){
                $group = 'school_fees.'.$accountSetting->name;
                setting([$group => []]);
                $accountSetting->status = false;
                $accountSetting->save();
            }
            return response()->json($accountSetting);
        } catch (\Exception $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function getForm(AccountSetting $accountSetting)
    {
        try{
            $row = [];
            foreach (ClassRoom::all() as $classRoom){
                $row[] = '<div class="form-group">
            <label for="'.str_replace(' ','_',$classRoom->name).'"><span class="card-title">'.__($classRoom->name).'</span></label>
            <input type="number" name="'.Str::slug ($classRoom->name,'_').'_fee" id="'.str_replace(' ','_',$classRoom->name).'" step="0.01" placeholder=" '.__('How much will be the fees for').' '.__($classRoom->name).'" class="form-control" required>
        </div>';
            }
            $output = '';
            $part1 = '<div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">'.__(str_replace('_',' ',$accountSetting->name)).'</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="'.route('admin.account-settings.store-fees').'" method="post">
                    <input type="hidden" name="_token" value="'.csrf_token().'">';
            $part2 = '<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Save Fees</button>
                    </div>
                </form>';

            if ($accountSetting->name == 'tuition_fee' || $accountSetting->name == 'exam_fee'){
                $output = $part1.'<input type="hidden" name="fee" value="'.$accountSetting->name.'">
                    <div class="modal-body">' . implode(" ", $row) . '</div>'.$part2;
            } else {
                $output = $part1.'<input type="hidden" name="fee" value="'.$accountSetting->name.'">
                    <div class="modal-body">
                    <div class="form-group">
        <label for="'.$accountSetting->name.'"><span class="card-title text-capitalize">'.__(str_replace('_', ' ', $accountSetting->name)).'</span></label>
        <input type="number" name="'.$accountSetting->name.'" id="'.$accountSetting->name.'" step="0.01" placeholder=" '.__('How much will be the fees for').' '.__(str_replace('_fee', ' ', $accountSetting->name)).'" class="form-control" required>
    </div></div>'.$part2;
            }
                return response()->json($output);
        } catch (\Exception $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function storeFee(Request $request)
    {
        try{
            $group = 'school_fees.'.$request->fee.'.';
            $fees = $request->all();
            foreach ($fees as $key => $fee){
                if ($fee == null){
                    $this->validate($request, [
                        $key => 'required|string|max:100'
                    ]);
                }
                unset($fees['_token']);
                unset($fees['fee']);
            }
            foreach ($fees as $key => $value){
                setting([$group.$key => $value]);
            }

            $accountSetting = AccountSetting::where('name',$request->fee)->first();
            $accountSetting->status = true;
            $accountSetting->save();

            return $this->backWithSuccess(ucwords(str_replace('_',' ',$accountSetting->name)).' has been saved successfully');
        } catch (\Exception $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }
}
