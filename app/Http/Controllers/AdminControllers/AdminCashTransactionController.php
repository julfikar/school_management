<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Cash;
use App\Models\Employee;
use App\Models\Expense;
use App\Models\Income;
use App\Models\Payable;
use App\Models\Receivable;
use App\Models\Salary;
use Illuminate\Http\Request;

class AdminCashTransactionController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function index()
    {
        try {
            $title = 'Cash Transaction';
            $cash_transactions = Cash::latest()->get();
            return view('backend.pages.accounts.cash-transactions.index', compact('title','cash_transactions'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


    public function store(Request $request)
    {
        try{
            $transactionType = $request->transaction_type;
            if($transactionType == 'Expense'){
    //            'date', 'description', 'debit', 'credit'
                if($request->credit_amount != null){
                    $cash = new Cash();
                    $cash->date = date('Y-m-d',strtotime($request->date));
                    $cash->description = $request->input_description;
                    $cash->credit = $request->credit_amount;
                    $cash->save();

                    $expanse = new Expense();
                    $expanse->date = date('Y-m-d',strtotime($request->date));
                    $expanse->description = $request->input_description;
                    $expanse->debit = $request->credit_amount;
                    $expanse->save();
                }else{
                    $cash = new Cash();
                    $cash->date = date('Y-m-d',strtotime($request->date));
                    $cash->description = $request->input_description;
                    $cash->debit = $request->debit_amount;
                    $cash->save();

                    $expanse = new Expense();
                    $expanse->date = date('Y-m-d',strtotime($request->date));
                    $expanse->description = $request->input_description;
                    $expanse->credit = $request->debit_amount;
                    $expanse->save();
                }
                $notification = array(
                'message' =>'Cash Transaction successfully.',
                'alert-type' => 'success'
                );
            return back()->with($notification);
            }elseif ($transactionType == 'Income'){
                if($request->debit_amount != null){
                    $cash = new Cash();
                    $cash->date = date('Y-m-d',strtotime($request->date));
                    $cash->description = $request->input_description;
                    $cash->debit = $request->debit_amount;
                    $cash->save();

                    $income = new Income();
                    $income->date = date('Y-m-d',strtotime($request->date));
                    $income->description = $request->input_description;
                    $income->credit = $request->debit_amount;
                    $income->save();
                }else{
                    $cash = new Cash();
                    $cash->date = date('Y-m-d',strtotime($request->date));
                    $cash->description = $request->input_description;
                    $cash->credit = $request->credit_amount;
                    $cash->save();

                    $income = new Income();
                    $income->date = date('Y-m-d',strtotime($request->date));
                    $income->description = $request->input_description;
                    $income->debit = $request->credit_amount;
                    $income->save();
                }
                $notification = array(
                'message' => 'Cash Transaction successfully.',
                'alert-type' => 'success'
                );
                 return back()->with($notification);
            }
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }


    }


    public function filterUserByType($type)
    {
        if($type == 'Payable'){
            $employees = Payable::with('user')->get();
        }else{
            $employees = Receivable::with('user')->get();
        }

        return response()->json(['employees' => $employees]);
    }



}
