<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\ImportanLinks;
use Illuminate\Http\Request;

class WizardImportantLinksController extends Controller
{
    public function index()
    {
        try {
            $title = 'All Important Links';
            $links = ImportanLinks::all();
            return view('backend.pages.wizards.important-links.index', compact('title','links'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function create()
    {
        try {
            $title = 'Add Important Links';
            return view('backend.pages.wizards.important-links.form', [
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required|max:255',
            'link' => 'required|unique:importan_links',
        ]);

        try{
            $inportant = new ImportanLinks();
            $inportant->name = $request->name;
            $inportant->link = $request->link;
            $inportant->save();
            $notification = array(
                'message' => 'Link has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function edit($request){
        try {
            $title = 'Edit Important Links';
            $service = ImportanLinks::find($request);
            return view('backend.pages.wizards.important-links.update', compact('title','service'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }

    public function update(Request $request, $id){

        $inportant = ImportanLinks::find($id);
        try{
            $inportant->name = $request->name;
            $inportant->link = $request->link;
            $inportant->save();
            $notification = array(
                'message' => 'Link has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

    }

    public function delete($id){
        try {

            $services= ImportanLinks::find($id);
            $services->delete();
            $notification = [
                'message' =>  'Service has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

}
