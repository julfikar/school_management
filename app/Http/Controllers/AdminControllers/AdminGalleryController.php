<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\GalleryFolder;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminGalleryController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GalleryFolder $galleryFolder)
    {
        try {
            $title = $galleryFolder->name;

            return view('backend.pages.galleries.gallery.index', compact('title','galleryFolder'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(GalleryFolder $galleryFolder)
    {

        try {
            $title = 'Add New Gallery';
            return view('backend.pages.galleries.gallery.form', [
                'title' => $title,
                'galleryFolder'=>$galleryFolder
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function show(Gallery $gallery)
    {
        try {
            $title = 'Gallery';
            return view('backend.pages.galleries.gallery.update-form', [
                'title' => $title,
                'gallery'=>$gallery
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('gallery_name')) {
                foreach ($request->gallery_name as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }


            if ($request->hasFile('gallery_name')) {

                $images = $request->gallery_name;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
//                        ->resize(1536, 480)
                        ->save(public_path('/upload/galleries/' . $filename));

                    $gallery = new Gallery();
                    $gallery->name = $filename;
                    $gallery->gallery_folder_id = $request->gallery_folder_id;
                    $gallery->status = true;
                    $gallery->save();
                }
            }



            $notification = array(
                'message' => 'Gallery has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function update(Request $request, Gallery $gallery)
    {

        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('gallery_name')) {
                foreach ($request->gallery_name as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }


            if ($request->hasFile('gallery_name')) {

                if ($gallery->name != null) {
                    // delete existing image
                    $file = 'upload/galleries/' . $gallery->name;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                $images = $request->gallery_name;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())
//                        ->resize(1536, 480)
                        ->save(public_path('/upload/galleries/' . $filename));
                    $gallery->name = $filename;
                }
            }

            $gallery->save();

            $notification = array(
                'message' => 'Gallery has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function galleryActivation(Gallery $gallery)
    {
        try{
            if($gallery->status){
                $gallery->status = false;
                $gallery->save();
            }else{
                $gallery->status = true;
                $gallery->save();
            }
            return response()->json($gallery);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Gallery $gallery)
    {
        try {

            if ($gallery->name != null) {
                // delete existing image
                $file = 'upload/galleries/' . $gallery->name;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $gallery->delete();

            $notification = [
                'message' =>  'Gallery has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }
}
