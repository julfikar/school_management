<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTeacherRequest;
use App\Imports\TeachersImport;
use App\Models\Profile;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class AdminTeacherController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $title = 'All Teacher';
            $teachers = Teacher::all();
            return view('backend.pages.teachers.index', compact('title', 'teachers'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $title = 'Add New Teacher';
            return view('backend.pages.teachers.create-edit', [
                'teacher' => null,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function createBulk()
    {
        try {
            return view('backend.pages.teachers.create-bulk', [
                'title' => 'Insert Bulk Teacher',
            ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function downloadDemoFile()
    {
        return Storage::download('backend/files/teachers_demo.xlsx', 'Teachers Demo.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTeacherRequest $request)
    {
        $ganderArray = ['male','female','other'];
        if (!in_array($request->gender,$ganderArray)){
            return $this->backWithError('An invalid gander you selected');
        }

        $pdf_acceptable = ['pdf'];
        if ($request->hasFile('cv_file')) {
            if (!in_array($request->cv_file->getClientOriginalExtension(), $pdf_acceptable)) {
                $notification = array(
                    'message' => 'Only pdf file is supported.',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }
        }

        $presentAddress = $request->present_road . ', ' . $request->present_city . ', ' . $request->present_thana . ', ' . $request->present_district . '-' . $request->present_post_office;
        $permanentAddress = $request->permanent_road . ', ' . $request->permanent_city . ', ' . $request->permanent_thana . ', ' . $request->permanent_district . '-' . $request->permanent_post_office;
        try {

            DB::beginTransaction();
            $user = new User();
            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->gender = $request->gender;
            $user->password = Hash::make($request->password);
            $user->user_type_id = 2;
            $user->save();

            Profile::create([
                'user_id' => $user->id,
                'name_utf8' => $request->name_utf8,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8,
                'present_address' => $presentAddress,
                'permanent_address' => $permanentAddress,
                'about' => $request->about,
            ]);

            $inputs = $request->validated();

            $inputs['user_id'] = $user->id;
            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            $teacher = new Teacher();
            $teacher->join_date = $inputs['join_date'];
            $teacher->user_id = $inputs['user_id'];
            $teacher->unique_id = $uniqueID;
            $teacher->expert_in = $inputs['expert_in'];
            $teacher->n_id = $inputs['n_id'];
            $teacher->status = true;
            if ($request->hasFile('cv_file')) {

                // insert new file
                $new_cv = $request->cv_file;
                //file name
                $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                $x = str_shuffle($x);
                $x = substr($x, 0, 6) . 'DAC.';
                $cvfile = time() . $x . $new_cv->getClientOriginalExtension();
                // Upload file
                $new_cv->move(public_path('/upload/employee/cv/'), $cvfile);
            } else {
                $cvfile = null;
            }
            $teacher->cv_file = $cvfile;
            $teacher->save();

            //assign the user to a role
            $user->assignRole('teacher');
            DB::commit();

            return redirect()->route('admin.teachers.teacher.index')->with([
                'message' => 'Teacher has been created successfully',
                'alert-type' => 'success',
            ]);
        } catch (\Throwable $e) {
            DB::rollBack();
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function storeBulk(Request $request)
    {
        try {
            Excel::import(new TeachersImport, $request->file('file'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }

        return $this->backWithSuccess('ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        try {
            $title = 'Add New Teacher';
            return view('backend.pages.teachers.create-edit', [
                'teacher' => $teacher,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $ganderArray = ['male','female','other'];
        if (!in_array($request->gender,$ganderArray)){
            return $this->backWithError('An invalid gander you selected');
        }
//dd($request->all());
        $pdf_acceptable = ['pdf'];
        if ($request->hasFile('cv_file')) {
            if (!in_array($request->cv_file->getClientOriginalExtension(), $pdf_acceptable)) {
                return $this->backWithError('Only pdf file is supported.');
            }
            if ($teacher->cv_file){
                $path = 'upload/employee/cv/'.$teacher->cv_file;
                if (file_exists(public_path($path))){
                    unlink(public_path($path));
                }
            }
            $new_cv = $request->cv_file;
            //file name
            $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $x = str_shuffle($x);
            $x = substr($x, 0, 6) . 'DAC.';
            $cvfile = time() . $x . $new_cv->getClientOriginalExtension();
            // Upload file
            $new_cv->move(public_path('/upload/employee/cv/'), $cvfile);
        }else{
            $cvfile = null;
        }
        try {
            DB::beginTransaction();
            $user = $teacher->user;
            $profile = $teacher->user->profile;
            $this->validate($request, [
                'name' => ['required', 'string', 'max:255'],
                'name_utf8' => ['required', 'string', 'max:255'],
                'father_name' => ['required', 'string', 'max:255'],
                'father_name_utf8' => ['required', 'string', 'max:255'],
                'mother_name' => ['required', 'string', 'max:255'],
                'mother_name_utf8' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:14', Rule::unique('users')->ignore($user->id)],
                'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
                'join_date' => 'required',
                'expert_in' => 'required',
                'n_id' => 'required',
            ]);

            $user->forceFill([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'gender' => $request->gender
            ])->save();


            $profile->forceFill([
                'name_utf8' => $request->name_utf8,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8,
            ])->save();

            $teacher->forceFill([
                'join_date' => $request->join_date,
                'expert_in' => $request->expert_in,
                'status' => true,
                'n_id' => $request->n_id,
                'cv_file' => $cvfile
            ])->save();

            DB::commit();

            return $this->backWithSuccess('Teacher has been updated successfully');
        } catch (\Throwable $e) {
            DB::rollBack();
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }



    public function teacherActivation(Teacher $teacher)
    {
        try {
            if ($teacher->status) {
                $teacher->status = false;
                $teacher->save();
            } else {
                $teacher->status = true;
                $teacher->save();
            }
            return response()->json($teacher);
        } catch (\Exception $th) {
            return $this->backWithError($th->getMessage());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        try {
            if ($teacher->cv_file != null) {
                // delete existing image
                $file = 'upload/employee/cv/' . $teacher->cv_file;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }

            $teacher->classRoom->each->update(['teacher_id' => null]);
            $teacher->subjects->each->update(['teacher_id' => null]);
            $teacher->user->profile->delete();
            $teacher->user->deleteProfilePhoto();
            $teacher->user->tokens->each->delete();
            $teacher->user->syncRoles([]);
            $teacher->user()->delete();
            $teacher->delete();
            $notification = [
                'message' =>  $teacher->user->name . ' has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
}
