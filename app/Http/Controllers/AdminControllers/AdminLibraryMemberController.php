<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\LibraryMember;
use App\Models\User;
use Illuminate\Http\Request;

class AdminLibraryMemberController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $title = 'Library Members';
            $libraryMembers = LibraryMember::all();
            $users = User::all();
            return view('backend.pages.library-member.index', compact('title', 'libraryMembers', 'users'));
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
        ], [
            'user_id' => 'Dweller name is required.',

        ]);
        try {
            $member = LibraryMember::where('user_id',$request->user_id)->first();

            if($member !=null){

                $notification = array(
                    'message' => 'Member already exists.',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $x = str_shuffle($x);
            $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

            $member = new LibraryMember();
            $member->user_id = $request->input('user_id');
            $member->unique_id = $uniqueID;

            $member->save();

            $notification = array(
                'message' => 'New Member has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(LibraryMember $library_member)
    {
        try {
            if ($library_member->book){
                $notification = array(
                    'message' => $library_member->user->name.' holding a library book Name: '.$library_member->book->name.' Author: '.$library_member->book->author.'. Please take this book first. Then Try again.',
                    'alert-type' => 'warning'
                );
                return back()->with($notification);
            }
            $library_member->delete();

            $notification = array(
                'message' => $library_member->user->name.' has been deleted successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }
}
