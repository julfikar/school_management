<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AdminSubjectController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClassRoom $classRoom, Subject $subject = null)
    {
        try {
            $title = 'All Subject';

            return view('backend.pages.subjects.index', compact('title', 'classRoom', 'subject'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function addClassSubject(ClassRoom $classRoom)
    {

        try {
            $title = 'Add New Subject';
            $teachers = Teacher::all();

            return view('backend.pages.subjects.create-edit', [
                'classRoom' => $classRoom,
                'teachers' => $teachers,
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function storeClassSubject(Request $request)
    {


        try {

            $request->validate([
                'name' => 'required',
                'total_mark' => 'required',
            ], [
                'name' => 'Admin name is required.',
                'total_mark' => 'Total Mark is required.',

            ]);

            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }


            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('e_book')) {
                if (!in_array($request->e_book->getClientOriginalExtension(), $pdf_acceptable)) {
                    $notification = array(
                        'message' => 'Only pdf file is supported.',
                        'alert-type' => 'error'
                    );
                    return back()->with($notification);
                }
            }

            $subject = new Subject();



            if ($request->hasFile('thumbnail')) {

                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(270, 500)->save(public_path('/upload/subjects/thumbnails/' . $filename));
                }
                $subject->thumbnail = $filename;
            }


            if ($request->hasFile('e_book')) {

                // insert new image
                $new_ebook = $request->e_book;
                //image name
                $ebook = time() . '.' . $new_ebook->getClientOriginalName();
                // Upload image
                $new_ebook->move(public_path('/upload/subjects/e_books'), $ebook);
                $subject->e_book = $ebook;
            }

            $subject->name = $request->input('name');
            $subject->class_id = $request->input('class_id');
            $subject->teacher_id = $request->input('teacher_id');
            $subject->total_mark = $request->input('total_mark');

            $subject->save();

            $notification = array(
                'message' => 'Subject has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function showUpdateForm(Subject $subject)
    {
        try {
            $title = $subject->name;
            $teachers = Teacher::all();
            return view('backend.pages.subjects.update-form', compact('title', 'subject', 'teachers'));
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }


    public function subjectUpdate(Request $request)
    {
        try {


            $request->validate([
                'name' => 'required',
                'total_mark' => 'required',
            ], [
                'name' => 'Subject name is required.',
                'total_mark' => 'Total Mark is required.',

            ]);

            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('thumbnail')) {
                foreach ($request->thumbnail as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }


            $pdf_acceptable = ['pdf'];
            if ($request->hasFile('e_book')) {
                if (!in_array($request->e_book->getClientOriginalExtension(), $pdf_acceptable)) {
                    $notification = array(
                        'message' => 'Only pdf file is supported.',
                        'alert-type' => 'error'
                    );
                    return back()->with($notification);
                }
            }


            $subject = Subject::find($request->sid);


            if ($request->hasFile('thumbnail')) {

                if ($subject->thumbnail != null) {
                    // delete existing image
                    $file = 'upload/subjects/thumbnails/' . $subject->thumbnail;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }
                // insert new image
                $images = $request->thumbnail;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(270, 500)->save(public_path('/upload/subjects/thumbnails/' . $filename));
                }
                $subject->thumbnail = $filename;
            }



            if ($request->hasFile('e_book')) {

                if ($subject->e_book != null) {
                    // delete existing image
                    $file = 'upload/subjects/e_books/' . $subject->e_book;
                    if (file_exists(public_path($file))) {
                        unlink(public_path($file));
                    }
                }

                // insert new file
                $new_ebook = $request->e_book;
                //image name
                $ebook = time() . '.' . $new_ebook->getClientOriginalName();
                // Upload image
                $new_ebook->move(public_path('/upload/subjects/e_books'), $ebook);
                $subject->e_book = $ebook;
            }

            $subject->name = $request->input('name');
            $subject->teacher_id = $request->input('teacher_id');
            $subject->total_mark = $request->input('total_mark');

            $subject->save();

            $notification = array(
                'message' => 'Subject has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }



    public function destroySubject(Subject $subject)
    {
        try {
            if ($subject->thumbnail != null) {

                // delete existing image
                $file = 'upload/subjects/thumbnails/' . $subject->thumbnail;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            if ($subject->e_book != null) {
                // delete existing image
                $file = 'upload/subjects/e_books/' . $subject->e_book;
                if (file_exists(public_path($file))) {
                    unlink(public_path($file));
                }
            }
            $subject->delete();

            $notification = [
                'message' =>  'Subject has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }


    public function filterByClass($classId)
    {
        $subjects = Subject::where('class_id', $classId)->get();

        return response()->json(['subjects' => $subjects]);
    }
}
