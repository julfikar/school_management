<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\ExamCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ExamCategoryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => ['required', 'string', 'max:255', 'unique:exam_categories'],
                'mark_at_percentage' => ['required', 'string', 'max:3'],
            ]);

            $exam = ExamCategory::create([
                'parent_id' => $request->has('parent')?$request->parent:null,
                'name' => $request->name,
                'mark_at_percentage' => $request->mark_at_percentage
            ]);
            return $this->backWithSuccess($exam->name. ' has been created successfully');
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ExamCategory $category)
    {
        try {
            return view('backend.pages.exams.show', [
                'title' => 'Exams',
                'category' => $category,
                'exams' => $category->childrens,
                'parents' => ExamCategory::where('parent_id', null)->get()
                ]);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamCategory $category)
    {
        try {
            if(ExamCategory::where('parent_id', null)->count() > 0){
                $row = [];
                foreach(ExamCategory::where('parent_id', null)->get() as $exam){
                    if ($exam->id == $category->parent_id){
                        $row[] = '<option selected value="'.$exam->id.'">'.$exam->name.'</option>';
                    }else{
                        $row[] = '<option value="'.$exam->id.'">'.$exam->name.'</option>';
                    }
                }
                $parent = '<div class="form-group">
                            <label for="examParent">'. __('Main Term Exam').'</label>
                            <select name="parent" id="examParent" class="form-control">
                                <option value="'.null.'">'.__('Select one if you need').'...</option>'. implode(' ', $row).'</select>
                        </div>';
            }

            $output = '<div class="modal-header">
                <h5 class="modal-title text-capitalize" id="examCategoryModalLongTitle">'.__($category->name).' '. __('edit').'</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="'.route('admin.exam.category.update',$category->id).'" method="post">
                <input type="hidden" name="_token" value="'.csrf_token().'">'.
                method_field('PUT')
                .'<div class="modal-body">
                    <div class="form-group">
                        <label for="examName">'.__('Exam Name').'</label>
                        <input type="text" name="name" id="examName" class="form-control" required placeholder="'.__('Exam Name').'" value="'.$category->name.'">
                    </div>
                    <div class="form-group">
                        <label for="examMark">'.__('Exam Mark At (%)').'</label>
                        <input type="number" step="0.01" name="mark_at_percentage" id="examName" class="form-control" required placeholder="'.__('Exam Mark At (%)').'" min="5" max="100" value="'.$category->mark_at_percentage.'">
                    </div>'.$parent.'</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Save</button>
                </div>
            </form>';
            return response()->json($output);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamCategory $category)
    {
        try {
            $this->validate($request, [
                'name' => ['required', 'string', 'max:255',  Rule::unique('exam_categories')->ignore($category->id)],
                'mark_at_percentage' => ['required', 'string', 'max:3'],
            ]);
            if ($request->has('parent')){
                $category->parent_id = $request->parent;
            }
            $category->name = $request->name;
            $category->mark_at_percentage = $request->mark_at_percentage;
            $category->save();
            return $this->backWithSuccess($category->name. ' has been updated successfully');
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamCategory $category)
    {
        try {
            $category->exam->each->delete();
            $category->childrens->each->delete();
            $category->delete();
            return $this->backWithSuccess($category->name.' has been deleted successfully');
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function showExamByClass(ExamCategory $category)
    {
        try {
            return view('backend.pages.exams.show-by-class', [
                'title' => 'Every class\'s exam',
                'category' => $category,
                'classRooms' => ClassRoom::all(),
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }
}
