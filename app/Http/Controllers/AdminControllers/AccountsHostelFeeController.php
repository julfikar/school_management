<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Dweller;
use App\Models\HostelFee;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AccountsHostelFeeController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    /**
     * For Hostel fees
     */
    public function index(){

        try {
            $title = 'hostel fee';
            $hostel_fees = json_decode(json_encode(setting('school_fees.hostel_fee')));
            $dwellers = Dweller::all();
            return view('backend.pages.accounts.hostel-fee.form', compact('title','dwellers','hostel_fees'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }



    public function store(Request $request)
    {

        try {
            $this->validate($request, [
                'dweller_id' => 'required',
                'month' => ['required'],
                'payed_amount' => ['required'],
            ]);
            $now = Carbon::now();
            $payed_a_month = HostelFee::where('dweller_id',$request->dweller_id)->where('month',$request->month)->whereYear('created_at', '=', $now->year)->first();

            if($payed_a_month!=null){

                $notification = [
                    'message' => 'Tution fee already paid this month',
                    'alert-type' => 'error'
                ];

                return back()->with($notification);
            }

            $tution_fee = new HostelFee();
            $tution_fee->dweller_id = $request->dweller_id;
            $tution_fee->month = $request->month;
            $tution_fee->payed_amount = $request->payed_amount;

            if($request->discount){
                $tution_fee->discount = $request->discount;
            }
            if($request->due){
                $tution_fee->due = $request->due;
            }

            // Get todays date
            $today = Carbon::now();
            $t_day = date('d', strtotime($today));
            $t_year = date('Y', strtotime($today));

            // convert request month to created at month
            $input  = $t_day.'/'.$request->month.'/'.$t_year;
            $format = 'd/m/Y';
            $month_date = Carbon::createFromFormat($format, $input);
            $tution_fee->created_at = $month_date;

            $tution_fee->save();

            $notification = [
                'message' => 'Tution fee paid successfully',
                'alert-type' => 'success'
            ];

            return back()->with($notification);

        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

}
