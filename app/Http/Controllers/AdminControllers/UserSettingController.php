<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\ClassRoom;
use App\Models\Profile;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSettingController extends Controller
{
    public function __construct()
    {
        Parent::__construct();
    }

    public function getAdmin()
    {
        try {
            $title = 'Admin Users';
            $adminUsers = User::Where('user_type_id','2')->get();
            $roles = Role::all('name');
            return view('backend.pages.users.all-admin', compact('title', 'adminUsers', 'roles'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /*
     *  Get All Guardians
     */
    public function getGuardian()
    {
        try {
            $title = 'Guardians';
            return view('backend.pages.users.all-guardians', compact('title'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /*
     *  Create admin form
     */
    public function createAdmin()
    {
        try {
            $title = 'Create New Admin';
            return view('backend.pages.users.create-admin', compact('title'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function show(User $user)
    {
        try {
            $title = 'Admin';
            $roles = Role::where('name', '!=', 'super admin')->get();
            return view('backend.pages.users.show-admin', compact('title', 'roles', 'user'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function updateAdmin(Request $request, User $user)
    {
        try {
            $role = Role::where('id', $request->role)->first();
//            dd($role);
            if ($role->name == 'teacher'){
                if (!$user->teacher){
                    $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $x = str_shuffle($x);
                    $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

                    $teacher = new Teacher();
                    $teacher->join_date = date('Y-m-d', time());
                    $teacher->user_id = $user->id;
                    $teacher->unique_id = $uniqueID;
                    $teacher->n_id = 'need to provide';
                    $teacher->status = true;
                    $teacher->save();

                }
            }elseif ($role->name == 'headmaster'){
                if (!$user->teacher){
                    $x = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    $x = str_shuffle($x);
                    $uniqueID = substr(str_shuffle(time()),0,3).'-'.substr($x,0,3).'-'. rand(3,999999);

                    $teacher = new Teacher();
                    $teacher->join_date = date('Y-m-d', time());
                    $teacher->user_id = $user->id;
                    $teacher->unique_id = $uniqueID;
                    $teacher->n_id = 'need to provide';
                    $teacher->status = true;
                    $teacher->save();
                }
            }else{
                if ($user->teacher){
                    $teacher = $user->teacher;
                    if ($teacher->cv_file != null) {
                        // delete existing cv
                        $file = 'upload/employee/cv/' . $teacher->cv_file;
                        if (file_exists(public_path($file))) {
                            unlink(public_path($file));
                        }
                    }
                    $teacher->classRoom->each->update(['teacher_id' => null]);
                    $teacher->subjects->each->update(['teacher_id' => null]);
                    $teacher->delete();
                }
            }
            //assign the user to a role
            $user->syncRoles($role->name);

            $notification = [
                'message' => 'Admin has been updated successfully',
                'alert-type' => 'success'
            ];

            return redirect()->route('admin.user.get-admin')->with($notification);
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    /*
    *  Get All students
    */
    public function getStudent()
    {
        try {
            $title = 'Students';
            $students = Student::all();

            return view('backend.pages.users.all-students', compact('title', 'students'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function getStudentByClass($classId)
    {
        $filterdStudents = Student::where('class_id', $classId)->with('user')->get();

        return response()->json(['students' => $filterdStudents]);
    }


    /*
     *  Create Student form
     */
    public function createStudent(Student $student = null)
    {
        try {
            $title = 'Create New Student';
            return view('backend.pages.users.create-students', compact('title', 'student'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
    /*
    *  Create Student form
    */
    public function storeStudent(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string',
                'name_utf8' => 'required',
                'phone' => ['required', 'string', 'min:11', 'max:14', 'unique:users'],
                'class_id' => ['required'],
                'height' => 'required',
                'age' => 'required',
                'admission_date' => 'required',
                'blood_group' => 'required',

                'father_name_utf8' => 'required|string|max:255',
                'father_name' => 'required|string|max:255',
                'mother_name' => 'required|string|max:255',
                'mother_name_utf8' => 'required|string|max:255',
                'present_district' => 'required|string|max:255',
                'permanent_district' => 'required|string|max:255',
                'present_thana' => 'required|string|max:255',
                'permanent_thana' => 'required|string|max:255',
                'present_city' => 'required|string|max:255',
                'permanent_city' => 'required|string|max:255',
                'present_post_office' => 'required|string|max:255',
                'permanent_post_office' => 'required|string|max:255',
                'present_road' => 'required|string|max:255',
                'permanent_road' => 'required|string|max:255',
            ]);

            if ($request->email) {
                $this->validate($request, [
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                ]);
            }
            if ($request->password != $request->confirm_password) {
                $notification = array(
                    'message' => 'Password and confirm password don\'t match',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            if (!ClassRoom::find($request->class_id)) {
                $notification = array(
                    'message' => 'Sorry! You selected an invalid class room',
                    'alert-type' => 'error'
                );
                return back()->with($notification);
            }

            DB::beginTransaction();
            // Present and parmanebt Address
            $presentAddress = $request->present_road . ', ' . $request->present_city . ', ' . $request->present_thana . ', ' . $request->present_district . '-' . $request->present_post_office;
            $permanentAddress = $request->permanent_road . ', ' . $request->permanent_city . ', ' . $request->permanent_thana . ', ' . $request->permanent_district . '-' . $request->permanent_post_office;

            $user = new User();
            $user->name = $request->name;
            $user->user_type_id = 3;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();

            $student = Student::create([
                'user_id' => $user->id,
                'class_id' => $request->class_id,
                'class_rol' => Student::count() + 1,
                'height' => $request->height,
                'age' => $request->age,
                'admission_date' => $request->admission_date,
                'blood_group' => $request->blood_group,
            ]);


            $st_profile = Profile::create([
                'user_id' => $user->id,
                'name_utf8' => $request->father_name,
                'father_name' => $request->father_name,
                'father_name_utf8' => $request->father_name_utf8,
                'mother_name' => $request->mother_name,
                'mother_name_utf8' => $request->mother_name_utf8,
                'present_address' => $presentAddress,
                'permanent_address' => $permanentAddress,
            ]);

            DB::commit();

            $notification = [
                'message' => 'New Student added successfully',
                'alert-type' => 'success'
            ];

            return redirect()->route('admin.user.get-student')->with($notification);
        } catch (\Throwable $th) {
            DB::rollBack();
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function studentActivation(Student $student)
    {
        try {
            if ($student->status) {
                $student->status = false;
                $student->save();
            } else {
                $student->status = true;
                $student->save();
            }
            return response()->json($student);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }

    /*
   *  Destroy a Student
   */
    public function destroyStudent(Student $student)
    {
        try {
            $student->user->profile->delete();
            $student->user->deleteProfilePhoto();
            $student->user->tokens->each->delete();
            $student->user()->delete();
            $student->delete();
            $notification = [
                'message' =>  $student->user->name . ' has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    /*
    *  Get All Roles
    */
    public function allRole()
    {
        try {
            $title = 'User Roles';
            $roles = Role::where('id', '!=', 1)->get();
            return view('backend.pages.users.all-roles', compact('title', 'roles'));
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
    /*
    *  Create new role form
    */
    public function createRole($role = null)
    {
        try {
            $title = 'Create New Role';
            $role = Role::where('id',$role)->first();
            $permission = Permission::all();
            return view('backend.pages.users.create-role', compact('title', 'role', 'permission'));
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }

    public function updateRole(Request $request, $role)
    {
        try {
            $role = Role::where('id',$role)->first();
            $this->validate($request, [
                'name' => ['required', 'string', 'max:255', Rule::unique('roles')->ignore($role->id)]
            ]);

            $role->name = $request->name;
            $role->save();

            $permissions = [];
            foreach ($request->permission as $item){
                if (Permission::where('id',$item)->first()){
                    $permissions[] = Permission::where('id',$item)->first()->name;
                }
            }
            $role->syncPermissions(array_unique($permissions));

            return $this->backWithSuccess('Role updated successfully');
        } catch (\Throwable $th) {
            return $this->backWithError($th->getMessage());
        }
    }
    /*
    *  Store new role form
    */
    public function storeRole(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string|max:255|unique:roles'
            ]);

            $permissions = [];
            foreach ($request->permission as $item){
                if (Permission::where('id',$item)->first()){
                    $permissions[] = Permission::where('id',$item)->first()->name;
                }
            }

            $role = Role::create([
                'name' => $request->name,
                'guard_name' => 'web'
            ]);

            $role->givePermissionTo(array_unique($permissions));

            $notification = [
                'message' => 'New Role added successfully',
                'alert-type' => 'success'
            ];

            return redirect()->route('admin.user.all-role')->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
    /*
   *  Destroy a role form
   */
    public function destroyRole(Role $role)
    {
        try {
            try {
                $users = User::role($role->name)->get();
                if (count($users) > 0) {
                    $notification = [
                        'message' => '',
                        'alert-type' => 'error'
                    ];
                    return $this->backWithError('Sorry! You can\'t delete this ' . $role->name . ' role');
                }
                $role->syncPermissions([]);
                $role->delete();
                return $this->backWithSuccess($role->name . ' role has been deleted successfully');
            } catch (\Exception $e) {
                $role->delete();
                $notification = [
                    'message' => $role->name . ' role has been deleted successfully..',
                    'alert-type' => 'success'
                ];
                return back()->with($notification);
            }
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }
}
