<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTransferCertificateRequest;
use App\Models\Student;
use App\Models\TransferCertificate;
use App\Models\TutionFee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TransferCertificateController extends Controller
{

    public function __construct()
    {
        Parent::__construct();
    }


    public function index()
    {
        try {
            $title = 'Transfer Certificate';
            $transfer_certificates = TransferCertificate::latest()->get();
            return view('backend.pages.transfer-certificates.index', compact('title','transfer_certificates'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function create()
    {
        try {
            $title = 'Transfer Certificate Create';

            $students = Student::all();
            return view('backend.pages.transfer-certificates.from', compact('title','students'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }

    public function store(CreateTransferCertificateRequest $request){

        try{

            $sid= Student::where('id',$request->student_id)->first();
            $payed_month = TutionFee::where('student_id',$sid)->latest()->first();

            $new_payed_month = $payed_month?date('F',$payed_month->month):'';
            $new_payed_year = $payed_month?date('Y',strtotime($payed_month->created_at)):'';


            $transfer_certificate = new TransferCertificate();

            $transfer_certificate->name = config('app.locale') == 'en'?$sid->user->name:$sid->user->profile->name_utf8;
            $transfer_certificate->father_name = config('app.locale') == 'en'?$sid->user->profile->father_name:$sid->user->profile->father_name_utf8;
            $transfer_certificate->mother_name = config('app.locale') == 'en'?$sid->user->profile->mother_name:$sid->user->profile->mother_name_utf8;
            $transfer_certificate->village = $request->village;
            $transfer_certificate->post_office = $request->post_office;
            $transfer_certificate->upazila = $request->upazila;
            $transfer_certificate->district = $request->district;
            $transfer_certificate->roll = $sid->class_rol;
            $transfer_certificate->date_of_birth = $request->date_of_birth;
            $transfer_certificate->class_name = $sid->classRoom->name;
            $transfer_certificate->academic_year = $request->academic_year;
            $transfer_certificate->paid_month = $new_payed_month;
            $transfer_certificate->paid_year = $new_payed_year;

            $transfer_certificate->student_id = $request->student_id;


            if($request->reasone == 'by_home_change'){
                $transfer_certificate->by_home_change = true;
            }else{
                $transfer_certificate->by_home_change = false;
            }

            if($request->reasone == 'by_complete'){
                $transfer_certificate->by_complete = true;
            }else{
                $transfer_certificate->by_complete = false;
            }

            if($request->reasone == 'by_school_wish'){
                $transfer_certificate->by_school_wish = true;
            }else{
                $transfer_certificate->by_school_wish = false;
            }

            if($request->reasone == 'by_guardian'){
                $transfer_certificate->by_guardian = true;
            }else{
                $transfer_certificate->by_guardian = false;
            }

            $transfer_certificate->save();


            $notification = array(
                'message' => 'Transfer Certificate Created successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function download($id=null){
        try {
            $allUsers = User::where('user_type_id', 2)->get();
            $headMaster = '';
            foreach ($allUsers as $person){
                if ($person->hasRole('headmaster')){
                    $headMaster = config('app.locale') == 'en' ? $person->name:$person->profile->name_utf8;
                }
            }

            $transfer_certificate = TransferCertificate::where('id',$id)->where('status',true)->first();
            $student = Student::find($transfer_certificate->student_id);

            $logo = setting('backend.general.og_meta_image');
//            $logo = 'https://gendagps.com/upload/settings/1629039006itaegvDAC.png';

            $avatar = $student->user->profile_photo_path?asset('storage/'.$student->user->profile_photo_path):($student->user->gender?($student->user->gender=='male'?asset('backend/assets/img/profile/male.jpg'):asset('backend/assets/img/profile/female.jpg')):'N/A');
//            $avatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/User-avatar.svg/1024px-User-avatar.svg.png';

            $groupLogo = setting('backend.logo_favicon.logo');
//            $groupLogo = 'https://gendagps.com/upload/settings/16288652527f59eqDAC.png';


            $bg = asset('forntend/image/certificate-bg.jpeg');
//            $bg = 'https://gendagps.com/forntend/image/certificate-bg.jpeg';

            $mogib100 = asset('forntend/theme1/images/100.jpg');
//            $mogib100 = 'https://gendagps.com/forntend/theme1/images/100.jpg';

            $view = 'backend.pages.transfer-certificates.tc-download';


            $qrcode = QrCode::size(90)->generate(route('download-transfer-certificate-form',$id));
            $qrcode = str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$qrcode);

            $pdf = PDF::loadView($view, compact( 'transfer_certificate', 'qrcode', 'logo', 'groupLogo', 'bg', 'mogib100', 'avatar', 'student', 'headMaster'), [], [
                'default_font_size' => 14,
                'default_font' => 'kalpurush',
                'orientation' => 'P',
                'margin_left'          => 10,
                'margin_right'         => 10,
                'margin_top'           => 5,
                'margin_bottom'           => 5,
                'title'                => 'Application Form'
            ]);


            return $pdf->stream('transfer-certificate.pdf');

        }catch (\Throwable $e){
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function approve(TransferCertificate $transfercertificate)
    {
        try{
            if($transfercertificate->status){
                $transfercertificate->status = false;
                $transfercertificate->save();
            }else{
                $transfercertificate->status = true;
                $transfercertificate->save();
            }
            return response()->json($transfercertificate);
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e);
        }
    }



    public function destroy(TransferCertificate $transfercertificate)
    {
        try {

            $transfercertificate->delete();

            $notification = [
                'message' =>  'Transfer certificate has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }

    }


}
