<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\SubSlider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class WizardSudSliderController extends Controller
{
    public function index()
    {
        try {
            $title = 'All Sub Slider Images';
            $images = SubSlider::all();
            return view('backend.pages.wizards.sub-slider.index', compact('title','images'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }
    public function create()
    {
        try {
            $title = 'Add Sub Slider Image';
            return view('backend.pages.wizards.sub-slider.form', [
                'title' => $title,
            ]);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }
    public function store(Request $request){
        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('image')) {
                foreach ($request->image as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }
            if ($request->hasFile('image')) {
                $count = DB::table('sub_sliders')->count();
                    if ($count==4) {
                        $notification = array(
                            'message' => 'you can not add any image',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }

            }

            if ($request->hasFile('image')) {
                $images = $request->image;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(300, 300)->save(public_path('/upload/sub_slider/' . $filename));

                    $subject = new SubSlider();
                    $subject->image = $filename;
                    $subject->save();
                }

            }

            $notification = array(
                'message' => 'Image has been added successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }

    public function edit($request){
        try {
            $title = 'Edit Sub Slider Image';
            $service = SubSlider::find($request);
            return view('backend.pages.wizards.sub-slider.update', compact('title','service'));
        }catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];

            return back()->with($notification);
        }
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'image' => 'required|max:1'
        ]);
        try{
            $acceptable = ['jpeg', 'png', 'jpg', 'gif'];
            if ($request->hasFile('image')) {
                foreach ($request->image as $img) {
                    if (!in_array($img->getClientOriginalExtension(), $acceptable)) {
                        $notification = array(
                            'message' => 'Only jpeg, png, jpg and gif file is supported.',
                            'alert-type' => 'error'
                        );
                        return back()->with($notification);
                    }
                }
            }
            $subject = SubSlider::find($id);
            if ($request->hasFile('image')) {
                $images = $request->image;
                foreach ($images as $img) {
                    $image = $img;
                    $x = 'abcdefghijklmnopqrstuvwxyz0123456789';
                    $x = str_shuffle($x);
                    $x = substr($x, 0, 6) . 'DAC.';
                    $filename = time() . $x . $image->getClientOriginalExtension();
                    Image::make($image->getRealPath())->resize(300, 300)->save(public_path('/upload/sub_slider/' . $filename));
                }
                $subject->image = $filename;
            }
            $subject->save();
            $notification = array(
                'message' => 'Image has been updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->back()->with($notification);
        } catch (\Throwable $e) {
            $notification = array(
                'message' => $e->getMessage(),
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }
    }
    public function delete($id){
        try {

            $services= SubSlider::find($id);
            $services->delete();
            $notification = [
                'message' =>  'Image has been deleted successfully..',
                'alert-type' => 'success'
            ];
            return back()->with($notification);
        } catch (\Throwable $th) {
            $notification = [
                'message' => $th->getMessage(),
                'alert-type' => 'error'
            ];
            return back()->with($notification);
        }
    }
}
