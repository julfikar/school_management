<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\Salary;
use App\Models\SalaryAccount;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class AdminSalaryController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware(['auth','admin']);
    }

    public function index()
    {
        try {
            $data = Teacher::where('status', true)->get();
            foreach (Employee::where('status', true)->get() as $row){
                $data[] = $row;
            }

            return view('backend.pages.accounts.salary.index',[
                'title' => 'Salary Register',
                'roles' => Role::all(),
                'data' => $data
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }

    public function pay(Request $request)
    {
        $employee = null;
        $basicSalary = null;
        $balance = null;
        $payedAmount = null;
        $thisCharge = null;
        $thisGift = null;
        $thisBonus = null;
        if ($request->has('employee') > 0){
            $teacher = Teacher::where('unique_id', $request->employee)->first();
            $staff = Employee::where('unique_id', $request->employee)->first();
            $employee = $teacher?$teacher:($staff?$staff:null);
        }

        if ($employee){
            $salary = $employee->salary()->orderBy('id','DESC')->first();
            $basicSalary = $salary?$salary->amount:0.00;
            $payedAmount = $salary->accounts()->whereMonth('payed_date', date('m'))->sum('amount');
            $thisCharge = $salary->accounts()->whereMonth('payed_date', date('m'))->sum('charge');
            $thisGift = $salary->accounts()->whereMonth('payed_date', date('m'))->sum('gift');
            $thisBonus = $salary->accounts()->whereMonth('payed_date', date('m'))->sum('bonus');
            $lastDate = $salary->accounts()->whereMonth('payed_date', '!=', date('m'))->orderBy('payed_date','Desc')->first();
            if ($lastDate) {
                $balance = $salary->accounts()->where('payed_date', $lastDate->payed_date)->orderBy('id', 'Desc')->first();
            }
        }


        try {
            return view('backend.pages.accounts.salary.form',[
                'title' => 'Employee Salary',
                'employee' => $employee,
                'basicSalary' => $basicSalary,
                'balance' => $balance,
                'payedAmount' => $payedAmount,
                'thisCharge' => $thisCharge,
                'thisGift' => $thisGift,
                'thisBonus' => $thisBonus,
                'roles' => Role::all()
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($this);
        }
    }

    public function storePayment(Request $request)
    {
        try{
            $request->validate([
                'total_salary' => ['required'],
                'previous_due' => ['required'],
                'previous_advance' => ['required'],
                'employee_type' => ['required'],
                'employee_id' => ['required'],
                'date' => ['required'],
                'amount' => ['required'],
            ],[
                'total_salary' => 'An invalid entry',
                'previous_due' => 'An invalid entry',
                'previous_advance' => 'An invalid entry',
                'employee_type' => 'An invalid entry',
                'employee_id' => 'An invalid entry',
                'date' => 'Payment date field is required',
                'amount' => 'Payment amount field is required'
            ]);

            if ($request->has('charge')){
                $this->validate($request,[
                   'charge_amount' => ['required']
                ]);
            }

            if ($request->has('due')){
                $this->validate($request,[
                   'due_amount' => ['required']
                ]);
            }

            if ($request->has('gift')){
                $this->validate($request,[
                    'gift_amount' => ['required']
                ]);
            }

            if ($request->has('advance')){
                $this->validate($request,[
                    'advance_amount' => ['required']
                ]);
            }

            if ($request->has('bonus')){
                $this->validate($request,[
                    'bonus_amount' => ['required']
                ]);
            }

//            dd($request->all());
            if ($request->employee_type == 'Employee'){
                $staff = Employee::find($request->employee_id);
            }elseif ($request->employee_type == 'Teacher'){
                $staff = Teacher::find($request->employee_id);
            }
            $salary = $staff->salary()->orderBy('id','DESC')->first();
            $balance = null;

            $alreadyPayedAmount = $salary->accounts()->whereMonth('payed_date', date('m'))->sum('amount');
            $lastDate = $salary->accounts()->whereMonth('payed_date', '!=', date('m'))->orderBy('payed_date','Desc')->first();
            if ($lastDate) {
                $balance = $salary->accounts()->where('payed_date', $lastDate->payed_date)->orderBy('id', 'Desc')->first();
            }

            $thisMonthSalary = ($salary->amount + ($balance?$balance->balance :0.00)) - $alreadyPayedAmount;


            $payedAmount = $request->amount - ($request->has('gift_amount')?$request->gift_amount:0) - ($request->has('bonus_amount')?$request->bonus_amount:0) + ($request->has('charge_amount')?$request->charge_amount:0);

//            'salary_id', 'payed_date', 'amount', 'gift', 'bonus', 'charge', 'balance'
            SalaryAccount::create([
                'salary_id' => $salary->id,
                'payed_date' => date('Y-m-d', strtotime($request->date)),
                'amount' => $payedAmount,
                'gift' => $request->has('gift_amount')?$request->gift_amount:0.00,
                'bonus' => $request->has('bonus_amount')?$request->bonus_amount:0.00,
                'charge' => $request->has('charge_amount')?$request->charge_amount:0.00,
                'balance' => $thisMonthSalary - $payedAmount
            ]);

            return $this->backWithSuccess('Salary has been saved successfully');
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }

    }

    public function create(User $user)
    {
        try {
            if ($user->employee){
                $salary = $user->employee->salary()->orderBy('id', 'DESC')->first();
                $userType = $user->employee;
            }else{
                $salary = $user->teacher->salary()->orderBy('id', 'DESC')->first();
                $userType = $user->teacher;
            }
            return view('backend.pages.accounts.salary.create',[
                'title' => 'Salary info',
                'salary' => $salary,
                'employeeUser' => $user,
                'roles' => Role::all(),
                'userType' => $userType,
            ]);
        }catch (\Throwable $th){
            return $this->backWithError($th);
        }
    }

    public function store(Request $request, User $user)
    {
        try {
            $request->validate([
                'basic' => ['required']
            ],[
                'basic' => __('Basic Salary field is required')
            ]);
            if ($user->employee){
                $userType = $user->employee;
            }else{
                $userType = $user->teacher;
            }
            $totalAmount = $request->basic + $request->increment + $request->hospital_fee + $request->accommodation + $request->transportation_fee;
//            'basic', 'increment', 'hospital_fee', 'accommodation', 'transportation_fee', 'amount','date','salariable_id','salariable_type', 'by'
            $salary = $userType->salary()->create([
                'basic' => $request->basic,
                'increment' => $request->increment?$request->increment:0.00,
                'hospital_fee' => $request->hospital_fee?$request->hospital_fee:0.00,
                'accommodation' => $request->accommodation?$request->accommodation:0.00,
                'transportation_fee' => $request->transportation_fee?$request->transportation_fee:0.00,
                'amount' => $totalAmount,
                'date' => time(),
                'by' => Auth::user()->id
            ]);

            return $this->backWithSuccess('Salary has been registered successfully');
        }catch (\Throwable $th){
            return $this->backWithError($th->getMessage());
        }
    }
}
