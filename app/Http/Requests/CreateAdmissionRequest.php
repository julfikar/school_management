<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdmissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_utf8' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'new_class' => 'required',
            'age' => 'required',
            'profile_photo' => 'required',

            'name' => ['required', 'string', 'max:255'],
            'father_name' => ['required', 'string', 'max:255'],
            'father_name_utf8' => ['required', 'string', 'max:255'],
            'mother_name' => ['required', 'string', 'max:255'],
            'mother_name_utf8' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:14', 'unique:users'],
            'present_district' => 'required|string|max:255',
            'permanent_district' => 'required|string|max:255',
            'present_thana' => 'required|string|max:255',
            'permanent_thana' => 'required|string|max:255',
            'present_city' => 'required|string|max:255',
            'permanent_city' => 'required|string|max:255',
            'present_post_office' => 'required|string|max:255',
            'permanent_post_office' => 'required|string|max:255',
            'present_road' => 'required|string|max:255',
            'permanent_road' => 'required|string|max:255',
        ];
    }
}
