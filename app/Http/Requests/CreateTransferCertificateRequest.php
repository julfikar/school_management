<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTransferCertificateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'village' => ['required', 'string', 'max:255'],
            'post_office' => ['required', 'string', 'max:255'],
            'upazila' => ['required', 'string', 'max:255'],
            'district' => ['required', 'string', 'max:255'],
            'date_of_birth' => 'required',
            'academic_year' => 'required',
            'reasone'=> 'required'

        ];
    }
}
