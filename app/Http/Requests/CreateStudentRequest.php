<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:25|unique:roles',
            'phone' => ['required', 'string', 'min:11', 'max:14', 'unique:users'],
            'class_id'=>['required'],
            'height'=>'required',
            'age'=>'required',
            'admission_date'=>'required',
            'blood_group'=>'required',

            'father_name'=>'required|string|max:255',
            'mother_name'=>'required|string|max:255',
            'present_district' => 'required|string|max:255',
            'permanent_district' => 'required|string|max:255',
            'present_thana' => 'required|string|max:255',
            'permanent_thana' => 'required|string|max:255',
            'present_city' => 'required|string|max:255',
            'permanent_city' => 'required|string|max:255',
            'present_post_office' => 'required|string|max:255',
            'permanent_post_office' => 'required|string|max:255',
            'present_road' => 'required|string|max:255',
            'permanent_road' => 'required|string|max:255',
        ];
    }
}
