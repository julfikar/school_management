<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTestimonialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required',
            'name' => ['required', 'string', 'max:255'],
            'father_name' => ['required', 'string', 'max:255'],
            'mother_name' => ['required', 'string', 'max:255'],
            'village' => ['required', 'string', 'max:255'],
            'post_office' => ['required', 'string', 'max:255'],
            'upazila' => ['required', 'string', 'max:255'],
            'district' => ['required', 'string', 'max:255'],
            'date_of_birth' => ['required', 'string', 'max:255'],
            'roll' => ['required', 'string', 'max:255'],
            'education_year' => ['required', 'string', 'max:255'],
            'class' => ['required', 'string', 'max:255'],
            'headmaster_name' => ['required', 'string', 'max:255'],
            'headmaster_phone' => ['required', 'string', 'max:255'],
            'headmaster_mobile' => ['required', 'string', 'max:255'],
        ];
    }
}
