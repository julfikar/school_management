<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailNotify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $maleUserName;
    private $siteName;
    private $mailSubject;
    private $massage;

    public function __construct($maleUserName, $siteNamem, $mailSubject, $massage)
    {
        $this->mail_username = $maleUserName;
        $this->site_name = $siteNamem;
        $this->mail_subject = $mailSubject;
        $this->message = $massage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->from($this->mail_username,$this->site_name)
           ->subject($this->mail_subject)
            ->view('email-tamplate.general-email',[
                'message' => $this->message
            ]);
        return $mail;
    }
}
