<head>
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- ENCODING -->
    <meta charset="UTF-8" />
    <!-- AUTHOR -->
    <meta name="author" content="snazzysheet" />
    <!-- DESCRIPTION -->
    <meta name="description" content="Modern Bootstrap 4 Admin Template - Fully Responsive" />
    <!-- IE EDGE COMPATIBILITY -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- RESPONSIVE BROWSER TO SCREEN WIDTH -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no, minimal-ui" />
    <!------------------------------------------------------------------------------------------------>
    <!-- FAVICON  -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{setting('backend.logo_favicon.favicon') }}" />
    <!-- BOOTSTRAP - V 4.0.0 -->
    <link rel="stylesheet" href="/backend/assets/dist/css/bootstrap.min.css" />
    <!-- MATERIAL ICONS -->
    <link rel="stylesheet" href="/backend/assets/icons/material-icons/material-icons.css" />
    <!-- FONT AWESOME -->
{{--    <link rel="stylesheet" href="/backend/assets/icons/font-awesome/font-awesome.min.css" />--}}
    <link rel="stylesheet" href="{{ asset('forntend/css/all.min.css') }}" />
    <!-- WEATHER ICONS -->
    <link rel="stylesheet" href="/backend/assets/icons/weather-icons/css/weather-icons.min.css" />
    <!-- FLAG ICON CSS -->
    <link rel="stylesheet" href="/backend/assets/icons/flag-icon-css/css/flag-icon.min.css" />
    <!-- OVERLAYSCROLLBARS -->
    <link type="text/css" href="/backend/assets/plugin/OverlayScrollbars/css/OverlayScrollbars.min.css" rel="stylesheet"/>
    <!-- JVECTORMAP -->
    <link rel="stylesheet" href="/backend/assets/plugin/jVectormap/jquery-jvectormap-2.0.3.css" />
    <!-- Circliful Master -->
    <link rel="stylesheet" href="/backend/assets/plugin/circliful/css/jquery.circliful.css" />
    <!-- DATA TABLES -->
    <link rel="stylesheet" href="/backend/assets/plugin/DataTables/1.10.16/css/dataTables.bootstrap4.min.css" />
    <!-- SUMMERNOTE -->
    <link rel="stylesheet" href="/backend/assets/plugin/summernote/summernote-bs4.css" />
    <!-- JQUERY NOTIFY -->
    <link rel="stylesheet" href="/backend/assets/plugin/notify/css/notify.css" />
    <!-- BOOTSTRAP SLIDER -->
    <link rel="stylesheet" href="/backend/assets/plugin/bootstrap-slider/bootstrap-slider.min.css" />
    <!-- SUMOSELECT -->
    <link rel="stylesheet" href="/backend/assets/plugin/sumoselect/sumoselect.min.css" />
    <!-- IMAGE UPLOADER -->
    <link rel="stylesheet" href="{{asset('backend/assets/css/image-uploader.min.css')}}" />
    <!-- toastr alert -->
    <link rel="stylesheet" href="{{asset('notification_assets/css/toastr.min.css')}}" />
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Julius+Sans+One" rel="stylesheet">
    <!-- Font family CSS -->
    <link rel="stylesheet" href="{{ asset('forntend/css/custom-font-family.css') }}">
    <!-- STYLE -->
    <link rel="stylesheet" href="{{ asset('backend/assets/css/style.css') }}" />

    @yield('page-css')

</head>
