<!-- WRAPPER SLIDE ------------------------------------------------------------------------------->
<div id="wrapper-slide">
    <button data-toggle="slideUp" data-target="body" class="btn btn-circle btn-danger btn-flash-dark ">
        <i class="material-icons">keyboard_arrow_up</i>
    </button>
</div>
<!-- END WRAPPER SLIDE --------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------>
<!-- WRAPPER FOOTER ------------------------------------------------------------------------------>
<div id="wrapper-footer">
    <div class="copyright bg-dark text-center text-sm-left">
        <span class="text-grey">Copyright © {{ date('Y', time()) }} </span>
        <span class="px-1 text-white">{{ config('app.name', 'laravel') }}</span>
        <span class="text-grey">All rights reserved.</span>
        <span class="d-block d-sm-inline-block  float-sm-right mt-1 mt-sm-0"><a href="#" class="text-light mx-2">Terms of use</a> <span class="text-muted">|</span> <a href="#" class="text-light ml-2">Privacy Policy</a></span>
    </div>
</div>
<!-- END WRAPPER FOOTER ------------------------------------------------------------------------>
<!------------------------------------------------------------------------------------------------>
