@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ route('admin.get-class-subjects',$classRoom->id) }}">{{__($classRoom->name) }}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-6">
                            <h6 class="card-title">{{__($classRoom->name) }} / {{__($title)}}</h6>
                        </div>

                        @if($hihestClass > 0)
                            <div class="col-md-6 col-sm-12 text-right">
                                <a href="{{ route('admin.class-routine.download',$classRoom->id) }}" class="btn btn-success" target="_blank"> {{__('Download Routine')}}</a>
                            </div>
                        @endif

                    </div>
                    <div class="card-body ">
                        <table class="table table-bordered">
                            <tbody>
                            @foreach($weekDays as $weekDay)
                                <tr>
                                    <th class="text-center text-capitalize">{{ __($weekDay->name) }}</th>

                                    @if($classRoom->classRoutines()->where('name_of_day', $weekDay->name)->count() > 0)
                                        @foreach($classRoom->classRoutines()->where('name_of_day', $weekDay->name)->get() as $period)
                                            <td class="text-center">{{ $period->subject?__($period->subject->name):($period->off_period?__('Off-period'):__('Tiffeen-period'))  }}
                                                {!! $period->subject?'</br> ('.($period->subject->teacher?$period->subject->teacher->user->name:'').')':'' !!}
                                                <br> {{ date('h:i a',strtotime($period->start_at)) .__(' to '). date('h:i a',strtotime($period->end_at)) }}
                                            </td>
                                        @endforeach
                                            <td>
                                                <a href="javascript:void(0)" class="btn btn-sm btn-circle btn-warning addRoutineBtn">
                                                    <i class="material-icons">edit</i>
                                                    <form action="{{ route('admin.class-routine.create',$classRoom->id) }}" method="get">
                                                        @csrf
                                                        <input type="hidden" name="week_day" value="{{ $weekDay->name }}">
                                                    </form>
                                                </a>
                                            </td>
                                    @else
                                        <td colspan="{{ $hihestClass }}">
                                            <div class="text-warning p-3" >
                                                <h6 class="text-center h6">{{__('No class added.')}}</h6>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-sm btn-circle btn-success addRoutineBtn">
                                                <i class="material-icons">add</i>
                                                <form action="{{ route('admin.class-routine.create',$classRoom->id) }}" method="get">
                                                    @csrf
                                                    <input type="hidden" name="week_day" value="{{ $weekDay->name }}">
                                                </form>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('page-script')
    @include('backend.pages.class-routine.internal-assets.class-routine-js')
@endsection
