<script>
    $(function (){
        $('.addRoutineBtn').on('click', addClassRoutine)
        $('.deleteRowBtn').on('click', deleteRow)
    });

    function addClassRoutine(){
        $(this).find('form').submit();
    }

    function deleteRow(){
        swal({
            title: "{{__('Are you sure?')}}",
            text: "{{__('Once you delete, You can\'t recover this data')}}",
            icon: "warning",
            dangerMode: true,
            buttons: {
                cancel: true,
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    closeModal: true
                },
            },
        }).then((value) => {
            if(value){
                $(this).parent().parent().parent().parent().find('.deleteRowForm').submit();
            }
        });
    }
</script>
