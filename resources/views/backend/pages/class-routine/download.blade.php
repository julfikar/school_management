<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta charset="ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Class Routine</title>
{{--    <link href="https://fonts.maateen.me/kalpurush/font.css" rel="stylesheet">--}}
    <style>
        /*@import url('https://fonts.maateen.me/kalpurush/font.css');*/
        @font-face {
            font-family: 'Kalpurush';
            src: url({{ asset('forntend/Kalpurush/Kalpurush.eot') }});
            src: url({{ asset('forntend/Kalpurush/Kalpurush.woff') }}) format('woff'),
            url({{ asset('forntend/Kalpurush/Kalpurush.ttf') }}) format('truetype'),
            url({{ asset('forntend/Kalpurush/Kalpurush.svg#Kalpurush') }}) format('svg'),
            url({{ asset('forntend/Kalpurush/Kalpurush.eot?#iefix') }}) format('embedded-opentype');
            font-weight: normal;
            font-style: normal;
        }
        {{--@font-face {--}}
        {{--    font-family: 'Galada';--}}
        {{--    src: url({{ asset('forntend/google-fonts/Galada-Regular.ttf') }}) format('truetype');--}}
        {{--}--}}
        {{--@font-face {--}}
        {{--    font-family: 'Baloo Da 2';--}}
        {{--    src: url({{ asset('forntend/google-fonts/BalooDa2-Medium.ttf') }}) format('truetype');--}}
        {{--}--}}
        body {
            font-family: 'Kalpurush', Arial, sans-serif !important;
        }
        .wrapper{
            width: 90vw;
            height: auto;
            overflow-x: hidden;
            display: block;
            margin: 0 auto;
            margin-top: 60px;
        }
        .wrapper .title{
            width: 100%;
            height: auto;
            text-align: center;
            font-size: 22px;
            font-weight: bold;
            margin-bottom: 30px;
            /*font-family: 'Galada', cursive;*/
        }
        .wrapper table{
            width: 100%;
            border: 1px dotted #0a0302;
        }
        .wrapper table tr th , .wrapper table tr td{
            text-align: center;
            padding: 10px 0;
            border: 1px dotted #0a0302;
            text-transform: capitalize;
            /*font-family: 'Baloo Da 2', cursive;*/
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="title">
            <p>{{ __('Class Routine') }} <br> {{ $classRoom->name }}</p>
        </div>
        <table>
            @foreach($weekDays as $weekDay)
                <tr>
                    <th>{{ __($weekDay->name) }}</th>

                    @if($classRoom->classRoutines()->where('name_of_day', $weekDay->name)->count() > 0)
                        @foreach($classRoom->classRoutines()->where('name_of_day', $weekDay->name)->get() as $period)
                            <td class="text-center">{{ $period->subject?__($period->subject->name):($period->off_period?__('Off-period'):__('Tiffeen-period'))  }}
                                {!! $period->subject?'</br> ('.$period->subject->teacher->user->name.')':'' !!}
                                <br> {{ date('h:i a',strtotime($period->start_at)) .__(' to '). date('h:i a',strtotime($period->end_at)) }}
                            </td>
                        @endforeach
                    @else
                        <td colspan="{{ $highestClass }}">
                            <div>
                                <h6>{{__('Weekend.')}}</h6>
                            </div>
                        </td>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
</body>

</html>
