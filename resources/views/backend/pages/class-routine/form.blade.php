@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
    <style>
        .bootstrap-select .dropdown-toggle {
            padding: 0 0 0  8px !important;
            background: #ffffff !important;
            color: #000 !important;
            box-shadow: none !important;
            margin-top: 8px;
            left: -8px;
        }
        .bootstrap-select.form-control .dropdown-toggle:focus{
            outline: none !important;
        }
    </style>
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ route('admin.class-routine',$classRoom->id) }}">{{__($classRoom->name) }}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-6">
                            <h6 class="card-title">{{__($classRoom->name) }} / {{__($title)}}</h6>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-12 px-5">
                                <p class="h5 font-weight-bold text-white">{{ __('Add new class or tiffeen period') }}</p>
                                <form action="{{ route('admin.class-routine.store', $classRoom->id) }}" method="post">
                                    @csrf
                                    <input type="hidden" name="week_day" value="{{ $weekDay }}">
                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="form-group">
                                                <select name="subject" class="form-control subject font-weight-bold selectpicker" data-style="btn-primary" data-live-search="true" data-size="5" tabindex="-98" required>
                                                    <option selected disabled value="{{ null }}">{{ __('Select one') }}</option>
                                                    @foreach($classRoom->subjects as $subject)
                                                        <option value="{{ $subject->id }}">{{ $subject->name }}</option>
                                                    @endforeach
                                                    <option value="{{ 'off-period' }}">{{ __('Off-period') }}</option>
                                                    <option value="{{ 'tiffeen-period' }}">{{ __('Tiffeen-period') }}</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="time" name="start_at" class="form-control subject startAt" value="{{ old('start_at') }}" required>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <input type="time" name="end_at" class="form-control endAt" value="{{ old('end_at') }}" required>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <button type="submit" class="btn btn-sm btn-circle btn-success"><i class="material-icons">checkmark</i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($periods->count() > 0)
            <hr>

            <div class="row">
                <div class="col-12">
                    <div class="card card-dark bg-dark">
                        <div class="card-header">
                            <p class="h5 font-weight-bold text-white">{{ __('Edit below class or tiffeen periods') }}</p>
                        </div>
                        <div class="card-body">
                            @foreach($periods as $period)
                                <div class="row">
                                    <div class="col-12 px-5">
                                        <form action="{{ route('admin.class-routine.update', $period->id) }}" method="post">
                                            @csrf
                                            <input type="hidden" name="week_day" value="{{ $weekDay }}">
                                            <div class="form-row">
                                                <div class="col-4">
                                                    <div class="form-group">
                                                        <select name="subject" class="form-control subject font-weight-bold selectpicker" data-live-search="true" data-size="5" tabindex="-98" required>
                                                            <option selected disabled value="{{ null }}">{{ __('Select one') }}</option>
                                                            @foreach($classRoom->subjects as $subject)
                                                                <option {{ $period->subject_id == $subject->id ? 'selected':'' }}  value="{{ $subject->id }}">{{ $subject->name }}</option>
                                                            @endforeach
                                                            <option {{ !$period->subject_id ? ($period->off_period?'selected':''):'' }} value="{{ 'off-period' }}">{{ __('Off-period') }}</option>
                                                            <option {{ !$period->subject_id ? ($period->tiffeen_period?'selected':''):'' }} value="{{ 'tiffeen-period' }}">{{ __('Tiffeen-period') }}</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="time" name="start_at" class="form-control subject startAt" value="{{ $period->start_at }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="time" name="end_at" class="form-control endAt" value="{{ $period->end_at }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-2">
                                                    <button type="submit" class="btn btn-sm btn-circle btn-success my-1 m-lg-0"><i class="material-icons">checkmark</i></button>
                                                    <button type="button" class="btn btn-sm btn-circle btn-danger my-1 m-lg-0 deleteRowBtn">
                                                        <i class="material-icons">delete</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <form action="{{ route('admin.class-routine.destroy', $period->id) }}" method="post" class="deleteRowForm">
                                            @csrf
                                            @method('delete')
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @endif
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
    @include('backend.pages.class-routine.internal-assets.class-routine-js')
@endsection
