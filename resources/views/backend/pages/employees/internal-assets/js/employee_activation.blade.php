
<script>
    var drivePath = window.location.pathname.replace('/','');
    console.log(drivePath);
    $('.employeeActivationBtn').on('click', function () {
        var selected = $(this).attr('id');
        $.ajax({
            type:'get',
            url: '/'+drivePath + '/activation/'+selected,
            success:function (data) {
                // do nothing;
                toastr.success("Status has been changed successfully.");
            }
        });
    });
</script>
