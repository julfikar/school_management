@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.students.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-md-6 col-sm-12">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right">
                        <a href="{{ route('admin.employee.create') }}" class="btn btn-success">{{__('Add New Employee')}}</a>
                        <a href="{{ route('admin.employee.create-bulk') }}" class="btn btn-primary">{{__('Add All Employee')}}</a>
                    </div>
                </div>
                <div class="card-body ">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Designation') }}</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('NID') }}</th>
                                <th scope="col">{{ __('Join Date') }}</th>
                                <th scope="col">{{ __('CV / Resume') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                                <th scope="col">{{ __('Option') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <th>{{ $loop->index+1 }}</th>
                                <td>{{ __($employee->designation->name) }}</td>
                                <td>{{ __($employee->user->name) }}</td>
                                <td>{{ __($employee->nid) }}</td>
                                <td>{{ __(date('D, d M Y',strtotime($employee->join_date))) }}</td>
                                <td>
                                    @if($employee->cv_file)
                                    <a href="{{ asset('upload/employee/cv/'.$employee->cv_file) }}" target="_blank">
                                        <img src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="" class="img-fluid w-25">
                                    </a>
                                    @endif
                                </td>
                                <td>
                                    <label class="switch float-right">
                                        <input type="checkbox" {{ $employee->status?'checked':'' }} id="{{ $employee->id }}" class="employeeActivationBtn">
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td>
                                    <a href="{{ route('admin.employee.create',$employee->id) }}" class="btn btn-info  btn-circle">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="javascript:void(0)" title="Delete" class="btn btn-danger btn-circle deleteRow">
                                        <i class="material-icons">delete</i>
                                        <form action="{{ route('admin.employee.destroy',$employee->id) }}" method="get">
                                            @csrf
                                        </form>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@include('backend.pages.employees.internal-assets.js.employee_activation')
@include('backend.pages.employees.internal-assets.js.employee-js')
@endsection
