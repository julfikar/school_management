@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="javascript:history.go(-1)">{{__('Employees')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <form action="{{ $employee?route('admin.employee.update',$employee->id):route('admin.employee.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                               placeholder="{{__('Name')}}" value="{{ $employee ? $employee->user->name : old('name') }}">
                                        <br>
                                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <p class="mb-1"><label for="name_utf8" class="card-title font-weight-bold">{{__('বাংলায় নাম:')}}</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="name_utf8" id="name_utf8" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                               placeholder="{{__('বাংলায় নাম')}}" value="{{ $employee ? $employee->user->name_utf8 : old('name_utf8') }}">
                                        <br>
                                        @if ($errors->has('name_utf8'))
                                            <span class="text-danger">{{ $errors->first('name_utf8') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="father_name" class="card-title font-weight-bold">{{__('Father Name')}}:</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="father_name" id="father_name" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Father Name')}}"
                                               value="{{ $employee ? $employee->father_name : old('father_name') }}">
                                        <br>
                                        @if ($errors->has('father_name'))
                                            <span class="text-danger">{{ $errors->first('father_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <p class="mb-1"><label for="father_name_utf8" class="card-title font-weight-bold">{{__('পিতার নাম')}}:</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="father_name_utf8" id="father_name_utf8" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="{{__('বাংলায় পিতার নাম')}}"
                                               value="{{ $employee ? $employee->father_name_utf8 : old('father_name_utf8') }}">
                                        <br>
                                        @if ($errors->has('father_name_utf8'))
                                            <span class="text-danger">{{ $errors->first('father_name_utf8') }}</span>
                                        @endif
                                    </div>

                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="mother_name" class="card-title font-weight-bold">{{__('Mother Name')}}:</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="mother_name" id="mother_name" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Mother Name')}}"
                                               value="{{ $employee ? $employee->mother_name : old('mother_name') }}">
                                        <br>
                                        @if ($errors->has('mother_name'))
                                            <span class="text-danger">{{ $errors->first('mother_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <p class="mb-1"><label for="mother_name_utf8" class="card-title font-weight-bold">{{__('মাতার নাম')}}:</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="mother_name_utf8" id="mother_name_utf8" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="{{__('বাংলায় মাতার নাম')}}"
                                               value="{{ $employee ? $employee->mother_name_utf8 : old('mother_name_utf8') }}">
                                        <br>
                                        @if ($errors->has('mother_name_utf8'))
                                            <span class="text-danger">{{ $errors->first('mother_name_utf8') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <p class="mb-1"><label for="gender" class="card-title font-weight-bold">{{__('Gender')}}:</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <select class="form-control form-control-lg" name="gender">
                                    <option value="1" >{{__('male')}}</option>
                                    <option value="2">{{__('female')}}</option>
                                    <option value="2">{{__('other')}}</option>
                                </select>

                                <br>
                                @if ($errors->has('gender'))
                                    <span class="text-danger">{{ $errors->first('gender') }}</span>
                                @endif
                            </div>

                            @if(!$employee)
                            <div class="form-row">
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="phone" class="card-title font-weight-bold">{{__('Phone')}}:</label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="tel" id="phone" name="phone" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                               placeholder="{{__('Phone')}}" value="{{ $employee ? $employee->user->phone : old('phone') }}">
                                        <br>
                                        @if ($errors->has('phone'))
                                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="email" class="card-title font-weight-bold">{{__('Email')}}:</label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="email" id="email" name="email" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                               placeholder="{{__('Email')}}" value="{{ $employee ? $employee->user->email : old('email') }}">
                                        <br>
                                        @if ($errors->has('email'))
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="form-row">
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="designation_id" class="card-title font-weight-bold">{{__('Designation')}}</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <select class="form-control form-control-lg" name="designation_id">
                                            <option value="" selected disabled>{{__('Select Designation')}}</option>
                                            @foreach($designations as $designation)
                                                <option class="text-capitalize" value="{{$designation->id}}" {{$employee?($designation->id==$employee->designation_id?'selected':''):''}}>{{$designation->name}}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                        @if ($errors->has('designation_id'))
                                            <span class="text-danger">{{ $errors->first('designation_id') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="nId" class="card-title font-weight-bold">{{__('NID')}}:</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" id="nId" name="nid" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                               placeholder="{{__('NID')}}" value="{{ $employee ? $employee->nid : old('nid') }}">
                                    </div>
                                </div>
                            </div>

                            <p class="mb-1"><label for="role_name" class="card-title font-weight-bold">{{__('Employee Role')}}: </label></p>
                            <div class="input-group input-group-lg mb-3">
                                <select class="form-control form-control-lg" name="role_name">
                                    <option value="" selected disabled>{{__('Select Designation')}}</option>
                                    @foreach($roles as $role)
                                        <option class="text-capitalize" value="{{$role->name}}" {{ $employee ? ($employee->user->hasRole($role->name) ? 'selected':'') : '' }} >{{ ucwords($role->name) }}</option>
                                    @endforeach
                                </select>
                                <br>
                                @if ($errors->has('role_name'))
                                    <span class="text-danger">{{ $errors->first('role_name') }}</span>
                                @endif
                            </div>

                            @if(!$employee)
                            <div class="form-row">
                                <div class="col-md-6">
                                    <h6 class="h6 text-center text-left card-title">{{__('present address')}}</h6>
                                    <div class="form-group">
                                        <label for="presentDistrict" class="card-title font-weight-bold">{{__('District')}}</label>
                                        <input type="text" name="present_district" id="district" list="districtList" class="form-control rounded" required
                                               value="{{ old('present_district') }}" placeholder="{{ __('Type your district name') }}">
                                        <datalist id="districtList">
                                            @foreach($districts as $district)
                                                <option>{{ $district->name }}</option>
                                            @endforeach
                                        </datalist>
                                    </div>

                                    <div class="form-group">
                                        <label for="presentThana" class="card-title font-weight-bold">{{__('Thana')}}</label>
                                        <input type="text" name="present_thana" id="presentThana" list="thanaList" class="form-control rounded" required
                                               value="{{ old('present_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                        <datalist id="thanaList">
                                            @foreach($districts as $district)
                                                @foreach($district->thana as $thana)
                                                    <option value="{{ $thana->name }}">{{ $district->name }}</option>
                                                @endforeach
                                            @endforeach
                                        </datalist>
                                    </div>

                                    <div class="form-group">
                                        <label for="presentCity" class="card-title font-weight-bold">{{__('City / Village')}}</label>
                                        <input type="text" name="present_city" id="presentCity" class="form-control rounded" required
                                               value="{{ old('present_city') }}" placeholder="{{ __('Type your city') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="presentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}</label>
                                        <input type="text" name="present_post_office" id="presentPostOffice" list="postOfficeList" class="form-control rounded"
                                               required value="{{ old('present_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                        <datalist id="postOfficeList">
                                            @foreach($districts as $district)
                                                @foreach($district->postOffice as $postOffice)
                                                    <optgroup label="{{ $district->name }}">
                                                        <option value="{{ $postOffice->post_code }}">{{ $postOffice->name }}</option>
                                                    </optgroup>
                                                @endforeach
                                            @endforeach
                                        </datalist>
                                    </div>

                                    <div class="form-group">
                                        <label for="presentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}</label>
                                        <input type="text" name="present_road" id="presentRoad" class="form-control rounded" required
                                               value="{{ old('present_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h6 class="h6 text-center text-left card-title">{{__('permanent address')}}</h6>
                                    <div class="form-group">
                                        <label for="permanentDistrict" class="card-title font-weight-bold">{{__('District')}}</label>
                                        <input type="text" name="permanent_district" id="permanentDistrict" list="districtList" class="form-control rounded"
                                               required value="{{ old('permanent_district') }}" placeholder="{{ __('Type your district name') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentThana" class="card-title font-weight-bold">{{__('Thana')}}</label>
                                        <input type="text" name="permanent_thana" id="permanentThana" list="thanaList" class="form-control rounded" required
                                               value="{{ old('permanent_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentCity" class="card-title font-weight-bold">{{__('City / Village')}}</label>
                                        <input type="text" name="permanent_city" id="permanentCity" class="form-control rounded" required
                                               value="{{ old('permanent_city') }}" placeholder="{{ __('Type your city') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}</label>
                                        <input type="text" name="permanent_post_office" id="permanentPostOffice" list="postOfficeList" class="form-control rounded"
                                               required value="{{ old('permanent_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}</label>
                                        <input type="text" name="permanent_road" id="permanentRoad" class="form-control rounded" required
                                               value="{{ old('permanent_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="password" class="card-title font-weight-bold">{{__('Password')}}</label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="password" id="password" name="password" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Password')}}">
                                        <br>
                                        @if ($errors->has('password'))
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="passwordConfirmation" class="card-title font-weight-bold">{{__('Confirm Password')}}</label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="password" id="passwordConfirmation" name="confirm_password" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Password')}}">
                                        <br>
                                        @if ($errors->has('password'))
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="form-row">
                                <div class="col-md-6">
                                    @if($employee)
                                        @if($employee->cv_file)
                                            <a href="{{ asset('upload/employee/cv/'.$employee->cv_file) }}" target="_blank">
                                                <img src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="" class="img-fluid my-1">
                                            </a>
                                        @endif
                                    @endif
                                    <p class="mb-1"><label for="cvFile" class="card-title font-weight-bold">{{__('CV')}}:</label>
                                        <code>{{ __('please upload CV in PDF format') }}</code></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="file" name="cv_file" id="cv_file" class="form-control-file" value="{{ old('cv_file') }}">
                                        <br>
                                        @if ($errors->has('cv_file'))
                                            <span class="text-danger">{{ $errors->first('cv_file') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-1"><label for="joinDate" class="card-title font-weight-bold">{{__('Join Date')}}:</label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="date" name="join_date" id="joinDate" class="form-control"
                                               value="{{ $employee ? $employee->join_date : old('join_date') }}">
                                        <br>
                                        @if ($errors->has('join_date'))
                                            <span class="text-danger">{{ $errors->first('join_date') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
