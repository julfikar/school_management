@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                       <a class="breadcrumb-item text-white text-capitalize" href="{{route('admin.wizards.complain.index') }}">{{__('complains')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>

            <div class="col-12">
                <div class="card card-dark bg-dark px-0">
                    <div class="cord-body p-2">
                        <form action="" method="post" >
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <input type="text" name="name" class="form-control font-weight-bold"
                                           readonly value="{{$values->name}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" name="phone" class="form-control font-weight-bold"
                                          readonly value="{{$values->phone}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="contact-textarea">
                                        <textarea class="form-control font-weight-bold" rows="4" placeholder="Wright Message" readonly name="message">{{$values->massage}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.wizards.complain.internal-assets.js.notice')
@endsection
