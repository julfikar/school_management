@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.wizards.sliders.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header d-block">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h6 class="card-title">{{__($title)}}</h6>
                            </div>
                            <div class="col-md-6 col-sm-12 text-right">
                                <a href="{{ route('admin.wizards.video_sliders.create') }}" class="btn btn-success"> {{__('Add New Slider')}}</a>
                            </div>
                        </div>

                    </div>
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="15%">{{__('SL No.')}}</th>
                                    <th>{{__('Thumbnail')}}</th>
                                    <th>{{__('Video Url')}}</th>
                                    <th class="text-center">{{__('Main Video')}}</th>
                                    <th class="text-center">{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($video_sliders as $key => $data)
                                    <tr>
                                        <th>{{__($loop->index+1)}}</th>
                                        <th>
                                            <img src="{{asset('upload/video-sliders/'.$data->thumbnail)}}" class="img-fluid"  height="15" width="100" alt="">
                                        </th>
                                        <td><a href="{{ $data->url }}" target="_blank" class="">{{ Str::limit($data->url,60,'...') }}</a></td>
                                        <td class="text-center">
                                            <label class="switch">
                                                <input type="checkbox" {{ $data->main_video?'checked':'' }} id="{{ $data->id }}" class="sliderActivationBtn">
                                                <span class="slider round"></span>
                                            </label>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.wizards.video_sliders.create',$data->id) }}" class="btn btn-info  btn-circle" >
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                                <i class="material-icons">delete</i>
                                                <form action="{{ route('admin.wizards.video_sliders.destroy',$data->id) }}" method="get">
                                                    @csrf
                                                </form>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.wizards.video-sliders.internal-assets.js.video_slider_activation')
    @include('backend.pages.wizards.video-sliders.internal-assets.js.delete-warning')

@endsection
