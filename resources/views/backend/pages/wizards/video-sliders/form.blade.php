@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.wizards.video_sliders.index'):route('dashboard') }}">{{__('Video Sliders')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">

                        <form action="{{ route('admin.wizards.video_sliders.store',$videoSlider?$videoSlider->id:'') }}" method="POST" enctype="multipart/form-data" class="wma-form">
                            @csrf
                            <p class="mb-1">{{__('Video Thumbnail')}}: <code>{{__('expected size is 1024 x 760px')}}</code></p>
                            <div class="form-row p-5">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <div class="px-5">
                                            <div class="video_slider_img" id="video_slider_img">
                                                <div class="input-images"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6  d-md-block  d-sm-none">
                                    <div class="img-favicon">
                                        @if($videoSlider)
                                          <img src="{{ asset('upload/video-sliders/'.$videoSlider->thumbnail) }}" alt="Og Meta Image" class="img-thumbnail img-fluid">
                                        @endif
                                    </div>

                                </div>
                            </div>

                            <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Video Url')}}:</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="url" id="url" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('Video Url')}}" value="{{$videoSlider?$videoSlider->url:''}}">
                                <br>
                                @if ($errors->has('url'))
                                    <span class="text-danger">{{ $errors->first('url') }}</span>
                                @endif
                            </div>


                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit')}}</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    @include('backend.pages.wizards.video-sliders.internal-assets.js.slider-form')
@endsection
