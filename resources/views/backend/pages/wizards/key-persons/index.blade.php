@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header d-block">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h6 class="card-title">{{__($title)}}</h6>
                            </div>
                            <div class="col-md-6 col-sm-12 text-right">
                                <a href="{{ route('admin.wizards.key_persons.create') }}" class="btn btn-success"> {{__('Add New Key Person')}}</a>
                            </div>
                        </div>

                    </div>
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="5%">{{__('SL No.')}}</th>
                                    <th>{{__('Image')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Designation')}}</th>
                                    <th>{{__('Side')}}</th>
                                    <th>{{__('Position')}}</th>
                                    <th width="15%">{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($key_persons as $key => $data)
                                    <tr>
                                        <th>{{__($loop->index+1)}}</th>
                                        <th>
                                            <img src="{{asset('upload/key-persons/'.$data->image)}}" class="img-fluid"  height="15" width="100" alt="">
                                        </th>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->designation}}</td>
                                        <td class="text-center">
                                            <label class="switch">
                                               <span class="badge {{$data->left?'badge-success':'badge-primary'}}">{{$data->left?'Left':'Right'}}</span>
                                            </label>
                                        </td>
                                        <td>{{$data->position}}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.wizards.key_persons.create',$data->id) }}" class="btn btn-info  btn-circle" >
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                                <i class="material-icons">delete</i>
                                                <form action="{{ route('admin.wizards.key_persons.destroy',$data->id) }}" method="get">
                                                    @csrf
                                                </form>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.wizards.key-persons.internal-assets.js.delete-warning')

@endsection
