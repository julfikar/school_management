@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.wizards.sliders.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.wizards.key_persons.index'):route('dashboard') }}">{{__('Key Persons')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">

                        <form action="{{ route('admin.wizards.key_persons.store',$keyPerson?$keyPerson->id:'') }}" method="POST" enctype="multipart/form-data" class="wma-form">
                            @csrf


                            <div class="row">
                                <div class="col-md-7">
                                    <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Person Name :')}}</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="name" value="{{__($keyPerson?$keyPerson->name:old('name')) }}" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="Full Name" required >

                                    </div>

                                    <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Person Designation :')}}</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="designation" value="{{__($keyPerson?$keyPerson->designation:old('designation')) }}" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="Full designation" >


                                    </div>

                                    <p class="mb-1"><label for="class_id" class="card-title font-weight-bold">{{__('Position:')}}</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="position" value="{{ $keyPerson?$keyPerson->position:old('position') }}" class="form-control" aria-label="Large"
                                               aria-describedby="inputGroup-sizing-sm" placeholder="Position" >


                                    </div>

                                    <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Person Content:')}}</label> </p>
                                    <div class="input-group form-group mb-3">
                                        <textarea rows="10" name="person_content" id="person_content" class="form-control rounded" placeholder="{{__('Personal Content here')}}">{!! $keyPerson?html_entity_decode($keyPerson->person_content):old('person_content') !!}</textarea>

                                    </div>

                                </div>


                                <div class="col-md-5">

                                    <p class="mb-1"><label for="person_image" class="card-title font-weight-bold">{{__('Left Side:')}}</label></p>
                                    <div class="form-row pb-3">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <label class="switch float-right">
                                                <input type="checkbox" name="left" {{ $keyPerson ?($keyPerson->left?'checked':''):'' }} >
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>


                                    <p class="mb-1 "><label for="person_image" class="card-title font-weight-bold">{{__('Person Image:')}}</label> <code>{{__('expected size is 713 x 656px')}}</code></p>
                                    <div class="form-row ">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="person_image px-4" id="person_image">
                                                    <div class="input-images"></div>
                                                </div>

                                            </div>
                                        </div>
                                        @if($keyPerson)
                                        <div class="col-md-12  d-md-block  d-sm-none">
                                            <div class="img-favicon">
                                                <img src="{{ asset('upload/key-persons/'.$keyPerson->image) }}" alt="Og Meta Image" class="img-fluid h-100">
                                            </div>
                                        </div>
                                        @endif
                                    </div>


                                </div>


                            </div>
                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit')}}</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/js/form-summerNote.js') }}"></script>
    @include('backend.pages.wizards.key-persons.internal-assets.js.key-person-form')
@endsection
