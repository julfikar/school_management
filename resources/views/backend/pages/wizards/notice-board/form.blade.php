@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <form action="{{ route('admin.wizards.notice_board.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Heading')}}:</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="heading" id="heading" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('Heading')}}" value="">
                                <br>
                                @if ($errors->has('heading'))
                                    <span class="text-danger">{{ $errors->first('heading') }}</span>
                                @endif
                            </div>

                            <p class="mb-1"><label for="description" class="card-title font-weight-bold">{{__('Description')}}:</label> </p>
                            <div class="from group">
                                <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="7"></textarea>
                                <br>
                                @if ($errors->has('description'))
                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                @endif
                            </div>

                            <p class="mb-1"><label for="description" class="card-title font-weight-bold">{{__('File')}}:</label> </p>
                            <div class="from group">
                                <input type="file" name="notice_file" id="cvFile" class="form-control-file" value="">
                                <br>
                                @if ($errors->has('notice_file'))
                                    <span class="text-danger">{{ $errors->first('notice_file') }}</span>
                                @endif
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
