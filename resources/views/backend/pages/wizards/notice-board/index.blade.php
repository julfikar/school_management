@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-12">
                            <h6 class="card-title">{{__($title)}}</h6>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right">
                            <a href="{{ route('admin.wizards.notice_board.create') }}" class="btn btn-success">{{__('Add Notice')}}</a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Description')}}</th>
                                <th scope="col">{{ __('File')}}</th>
                                <th scope="col">{{ __('Slug')}}</th>
                                <th scope="col">{{ __('Option')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                          @foreach($allnotice as $notice)
                                <tr>
                                    <th>{{ $loop->index+1 }}</th>
                                    <td>{{ __($notice->heading) }}</td>
                                    <td>{{ __($notice->description) }}</td>
                                    <td>{{ __($notice->file) }}</td>
                                    <td>{{ __($notice->slug) }}</td>

                                    <td>
                                        <a href="{{ route('admin.wizards.notice_board.edit',$notice->id)}}" class="btn btn-info  btn-circle" >
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                            <i class="material-icons">delete</i>
                                            <form action="{{ route('admin.wizards.notice_board.delete',$notice->id)}}" method="get">
                                                @csrf
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.wizards.notice-board.internal-assets.js.notice')
@endsection
