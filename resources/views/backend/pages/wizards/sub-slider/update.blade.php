@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ route('admin.wizards.sub-slider.index') }}">{{__('Sub Slider')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">

                        <form action="{{route('admin.wizards.sub-slider.update',$service->id)}}" method="POST" enctype="multipart/form-data" class="wma-form">
                            @csrf
                            <p class="mb-1">{{__('Sub Slider Image')}}: <code>{{__('expected size is 300px x 300px')}} & {{ __('Maximum 4 image is granted') }}</code></p>
                            <div class="form-row p-5">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <div class="px-5">
                                            <div class="sub_slider_img_update" id="sub_slider_img">
                                                <div class="input-images"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6  d-md-block  d-sm-none">
                                    <div class="img-favicon">

                                        <img src="{{asset('/upload/sub_slider/'.$service->image)}}" alt="Image" class="img-thumbnail img-fluid">

                                    </div>

                                </div>
                            </div>



                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Update Form')}}</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    @include('backend.pages.wizards.sub-slider.internal-assets.js.slider-form')
@endsection
