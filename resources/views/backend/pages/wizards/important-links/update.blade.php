@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <form action="{{ route('admin.wizards.important_links.update',$service->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('Name')}}" value="{{$service->name}}">
                                <br>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <p class="mb-1"><label for="father_name" class="card-title font-weight-bold">{{__('Link')}}:</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="link" id="link" class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Link')}}"
                                       value="{{$service->link}}">
                                <br>
                                @if ($errors->has('link'))
                                    <span class="text-danger">{{ $errors->first('link') }}</span>
                                @endif
                            </div>

                            <div class="card-footer">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Update Form')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
