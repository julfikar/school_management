@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-12">
                            <h6 class="card-title">{{__($title)}}</h6>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right">
                            <a href="{{ route('admin.wizards.important_links.create')}}" class="btn btn-success">{{__('Add Important Links')}}</a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Links') }}</th>
                                <th scope="col">{{ __('Option') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                        @foreach($links as $link)
                                <tr>
                                    <th>{{ $loop->index+1 }}</th>
                                    <td>{{ __($link->name) }}</td>
                                    <td>{{ __($link->link) }}</td>

                                    <td>
                                        <a href="{{route('admin.wizards.important_links.edit',$link->id)}}" class="btn btn-info  btn-circle" >
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                            <i class="material-icons">delete</i>
                                            <form action="{{route('admin.wizards.important_links.delete',$link->id)}}" method="get">
                                                @csrf
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.wizards.important-links.internal-assets.js.important-links')
@endsection
