@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="javascript:history.go(-1)">{{__('Designations')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <form action="{{route('admin.designation.store',$designation?$designation->id:'')}}" method="POST">
                            @csrf
                            <p class="mb-1">{{__('Name:')}} </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="name" value="{{ $designation?$designation->name:'' }}" class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Name')}}" >
                                <br>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Salary')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="salary" value="{{ $designation?$designation->salary:'' }}" class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Designation salary')}}">
                                <br>
                                @if ($errors->has('salary'))
                                    <span class="text-danger">{{ $errors->first('salary') }}</span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="about" class="">{{__('Note')}}:</label>
                                <textarea rows="10" name="note"  id="about" class="form-control  rounded"
                                         placeholder="{{__('Write note here..')}}" required>{{ $designation?$designation->note:'' }}</textarea>
                            </div>


                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Create Designation')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
