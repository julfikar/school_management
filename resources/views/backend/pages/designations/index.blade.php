@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-md-6 col-sm-12">
                            <h6 class="card-title">{{__($title)}}</h6>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right">
                            <a href="{{ route('admin.designation.create') }}" class="btn btn-success"> {{__('Add New Designation')}}</a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('Salary') }}</th>
                                <th scope="col">{{ __('Note') }}</th>
                                <th scope="col">{{ __('Employees') }}</th>
                                <th scope="col">{{ __('Option') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($designations as $designation)
                                <tr>
                                    <th>{{ $loop->index+1 }}</th>
                                    <td>{{ __(strtoupper($designation->name)) }}</td>
                                    <td>{{ $designation->salary }}</td>
                                    <td>{!! html_entity_decode($designation->note) !!}</td>
                                    <td>{{ $designation->employee->where('status', true)->count() }}</td>
                                    <td>
                                        <a href="{{route('admin.designation.create',$designation->id)}}" class="btn btn-info  btn-circle" >
                                            <i class="material-icons">visibility</i>
                                        </a>
                                        <a href="{{route('admin.designation.destroy',$designation->id)}}" title="Delete" onclick="return confirm('Are you sure, would you like to delete tha user?');">
                                            <button class="btn btn-danger  btn-circle">
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@endsection
