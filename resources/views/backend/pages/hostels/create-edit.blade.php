@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white" href="{{ route('admin.hostels.index') }}">{{__('Hostels')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-12 pl-0">
                        <h6 class="card-title ">{{__($title)}}</h6>

                    </div>
                </div>
                <form class="" action="{{ $hostel?route('admin.hostel.update',$hostel->id):route('admin.hostels.store') }}" method="POST">
                        @csrf
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="name" id="name"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Name')}}" value="{{ $hostel ? $hostel->name : old('name') }}">
                                    <br>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Employee:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <select class="form-control form-control-lg" name="employee_id">
                                        <option value="">{{__('Select an Employee')}}</option>
                                        @foreach($employees as $key => $data)
                                            <option value="{{$data->id}}" {{ $hostel ? ($hostel->employee_id==$data->id?'selected':'') : '' }}>{{$data->user->name}}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                    @if ($errors->has('employee_id'))
                                        <span class="text-danger">{{ $errors->first('employee_id') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Phone:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="phone" id="phone"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Phone')}}" value="{{ $hostel ?$hostel->phone:old('phone') }}">
                                    <br>
                                    @if ($errors->has('phone'))
                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Address:')}}</label> </p>
                                <div class="input-group form-group mb-3">
                                    <textarea rows="5" name="address" id="address" class="form-control rounded" placeholder="{{__('Hostel address here')}}" required>{!! $hostel ? html_entity_decode($hostel->address):old('address') !!}</textarea>
                                </div>

                            </div>
                        </div>


                        {{--<input type="hidden" name="class_id" value="{{$ebook->id}}">--}}
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

@endsection
