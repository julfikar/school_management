<script>
    $(function (){
        $('.teacherDestroyBtn').on('click', deleteTeacher)
    });

    function deleteTeacher(){
        swal({
            title: "{{__('Are you sure?')}}",
            text: "{{__('Once you delete, You can\'t recover this data')}}",
            icon: "warning",
            dangerMode: true,
            buttons: {
                cancel: true,
                confirm: {
                    text: "{{__('Delete')}}",
                    value: true,
                    visible: true,
                    closeModal: true
                },
            },
        }).then((value) => {
            if(value){
                $(this).find('.deleteForm').submit();
            }
        });
    }
</script>
