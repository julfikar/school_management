@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.students.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <div class="card-body ">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Name') }}</th>
                                <th scope="col">{{ __('NID') }}</th>
                                <th scope="col">{{ __('Join Date') }}</th>
                                <th scope="col">{{ __('Expert in') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                                <th scope="col">{{ __('Option') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($teachers as $teacher)
                        <tr>
                            <th>{{ $loop->index+1 }}</th>
                            <td>{{ $teacher->user->name }}</td>
                            <td>{{ $teacher->n_id }}</td>
                            <td>{{ date('d-M-y', strtotime($teacher->join_date)) }}</td>
                            <td>{{ $teacher->expert_in }}</td>
                            <td>
                                <label class="switch float-right">
                                    <input type="checkbox" {{ $teacher->status?'checked':'' }} id="{{ $teacher->id }}" class="teacherActivationBtn">
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                <a href="{{ route('admin.teachers.teacher.show',$teacher->id) }}" title="{{__('Edit')}}" class="btn btn-info  btn-circle" >
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="javascript:void(0)" title="{{__('Delete')}}" class="btn btn-danger btn-circle teacherDestroyBtn">
                                    <i class="material-icons">delete</i>
                                    <form action="{{ route('admin.teachers.destroy',$teacher->id) }}" method="get" class="deleteForm">
                                        @csrf
                                    </form>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.teachers.internal-assets.js.teacher_activation')
    @include('backend.pages.teachers.internal-assets.js.all-teacher-js')
@endsection
