@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white" href="{{ route('admin.teachers.teacher.index') }}">{{__('All Teachers')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header d-flex justify-content-between">
                    <h6 class="card-title">{{__($title)}}</h6>
                    <a href="{{ route('admin.teachers.create-bulk') }}" class="btn btn-primary">{{ __('Input All Teacher Once') }}</a>
                </div>
                <form action="{{ $teacher?route('admin.teachers.update',$teacher->id):route('admin.teachers.teacher.store') }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="card-body ">

                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                        placeholder="{{__('Name')}}" value="{{ $teacher ? $teacher->user->name : old('name') }}">
                                    <br>
                                    @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <p class="mb-1"><label for="name_utf8" class="card-title font-weight-bold">{{__('Name In Bangla')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="name_utf8" id="name_utf8" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Name In Bangla')}}" value="{{ $teacher ? $teacher->user->profile->name_utf8 : old('name_utf8') }}">
                                    <br>
                                    @if ($errors->has('name_utf8'))
                                        <span class="text-danger">{{ $errors->first('name_utf8') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="father_name" class="card-title font-weight-bold">{{__('Father Name')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="father_name" id="father_name" class="form-control" aria-label="Large"
                                        aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Father Name')}}"
                                        value="{{ $teacher ? $teacher->user->profile->father_name : old('father_name') }}">
                                    <br>
                                    @if ($errors->has('father_name'))
                                    <span class="text-danger">{{ $errors->first('father_name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <p class="mb-1"><label for="father_name_utf8" class="card-title font-weight-bold">{{__('Father\'s Name In Bangla')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="father_name_utf8" id="father_name_utf8" class="form-control" aria-label="Large"
                                           aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Father\'s Name In Bangla')}}"
                                           value="{{ $teacher ? $teacher->user->profile->father_name_utf8 : old('father_name_utf8') }}">
                                    <br>
                                    @if ($errors->has('father_name_utf8'))
                                        <span class="text-danger">{{ $errors->first('father_name_utf8') }}</span>
                                    @endif
                                </div>

                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="mother_name" class="card-title font-weight-bold">{{__('Mother Name')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="mother_name" id="mother_name" class="form-control" aria-label="Large"
                                        aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Mother Name')}}"
                                        value="{{ $teacher ? $teacher->user->profile->mother_name : old('mother_name') }}">
                                    <br>
                                    @if ($errors->has('mother_name'))
                                    <span class="text-danger">{{ $errors->first('mother_name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <p class="mb-1"><label for="mother_name_utf8" class="card-title font-weight-bold">{{__('Mother\'s Name In Bangla')}}:</label>  <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="mother_name_utf8" id="mother_name_utf8" class="form-control" aria-label="Large"
                                           aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Mother\'s Name In Bangla')}}"
                                           value="{{ $teacher ? $teacher->user->profile->mother_name_utf8 : old('mother_name_utf8') }}">
                                    <br>
                                    @if ($errors->has('mother_name_utf8'))
                                        <span class="text-danger">{{ $errors->first('mother_name_utf8') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <p class="mb-1"><label for="gender" class="card-title font-weight-bold">{{__('Gender')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                        <div class="input-group input-group-lg mb-3">
                                <select class="form-control form-control-lg" name="gender">
                                    <option disabled selected class="text-capitalize" value="{{ null }}" >{{__('Select one')}}</option>
                                    <option {{ $teacher?($teacher->user->gender == 'male'?'selected':''):'' }} class="text-capitalize" value="{{ 'male' }}" >{{__('Male')}}</option>
                                    <option {{ $teacher?($teacher->user->gender == 'female'?'selected':''):'' }} class="text-capitalize" value="{{ 'female' }}">{{__('Female')}}</option>
                                    <option {{ $teacher?($teacher->user->gender != 'male'?($teacher->user->gender != 'female'?'selected':''):''):'' }} class="text-capitalize" value="{{ 'other' }}">{{__('Other')}}</option>
                                </select>

                            <br>
                            @if ($errors->has('gender'))
                                <span class="text-danger">{{ $errors->first('gender') }}</span>
                            @endif
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="phone" class="card-title font-weight-bold">{{__('Phone')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="tel" id="phone" name="phone" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                        placeholder="{{__('Phone')}}" value="{{ $teacher ? $teacher->user->phone : old('phone') }}">
                                    <br>
                                    @if ($errors->has('phone'))
                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p class="mb-1"><label for="email" class="card-title font-weight-bold">{{__('Email')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="email" id="email" name="email" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                        placeholder="{{__('Email')}}" value="{{ $teacher ? $teacher->user->email : old('email') }}">
                                    <br>
                                    @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="expertIn" class="card-title font-weight-bold">{{__('Expert in')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="expert_in" id="expertIn" class="form-control" aria-label="Large"
                                        aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Expert in')}}"
                                        value="{{ $teacher ? $teacher->expert_in : '' }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p class="mb-1"><label for="nId" class="card-title font-weight-bold">{{__('NID')}}:</label> <i class="text-danger fas fa-star-of-life"></i></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" id="nId" name="n_id" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                        placeholder="{{__('N_id')}}" value="{{ $teacher ? $teacher->n_id : '' }}">
                                </div>
                            </div>
                        </div>

                        @if(!$teacher)
                        {{-- address --}}
                        <div class="form-row">
                            <div class="col-md-6">
                                <h6 class="h6 text-center text-left card-title">{{__('present address')}}</h6>
                                <div class="form-group">
                                    <label for="presentDistrict" class="card-title font-weight-bold">{{__('District')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="present_district" id="district" list="districtList" class="form-control rounded" required
                                        value="{{ old('present_district') }}" placeholder="{{ __('Type your district name') }}">
                                    <datalist id="districtList">
                                        @foreach($districts as $district)
                                        <option>{{ $district->name }}</option>
                                        @endforeach
                                    </datalist>
                                </div>

                                <div class="form-group">
                                    <label for="presentThana" class="card-title font-weight-bold">{{__('Thana')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="present_thana" id="presentThana" list="thanaList" class="form-control rounded" required
                                        value="{{ old('present_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                    <datalist id="thanaList">
                                        @foreach($districts as $district)
                                        @foreach($district->thana as $thana)
                                        <option value="{{ $thana->name }}">{{ $district->name }}</option>
                                        @endforeach
                                        @endforeach
                                    </datalist>
                                </div>

                                <div class="form-group">
                                    <label for="presentCity" class="card-title font-weight-bold">{{__('City / Village')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="present_city" id="presentCity" class="form-control rounded" required
                                        value="{{ old('present_city') }}" placeholder="{{ __('Type your City') }}">
                                </div>

                                <div class="form-group">
                                    <label for="presentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="present_post_office" id="presentPostOffice" list="postOfficeList" class="form-control rounded"
                                        required value="{{ old('present_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                    <datalist id="postOfficeList">
                                        @foreach($districts as $district)
                                        @foreach($district->postOffice as $postOffice)
                                        <optgroup label="{{ $district->name }}">
                                            <option value="{{ $postOffice->post_code }}">{{ $postOffice->name }}</option>
                                        </optgroup>
                                        @endforeach
                                        @endforeach
                                    </datalist>
                                </div>

                                <div class="form-group">
                                    <label for="presentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="present_road" id="presentRoad" class="form-control rounded" required
                                        value="{{ old('present_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h6 class="h6 text-center text-left card-title">{{__('permanent address')}}</h6>
                                <div class="form-group">
                                    <label for="permanentDistrict" class="card-title font-weight-bold">{{__('District')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="permanent_district" id="permanentDistrict" list="districtList" class="form-control rounded"
                                        required value="{{ old('permanent_district') }}" placeholder="{{ __('Type your district name') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentThana" class="card-title font-weight-bold">{{__('Thana')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="permanent_thana" id="permanentThana" list="thanaList" class="form-control rounded" required
                                        value="{{ old('permanent_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentCity" class="card-title font-weight-bold">{{__('City / Village')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="permanent_city" id="permanentCity" class="form-control rounded" required
                                        value="{{ old('permanent_city') }}" placeholder="{{ __('Type your city') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="permanent_post_office" id="permanentPostOffice" list="postOfficeList" class="form-control rounded"
                                        required value="{{ old('permanent_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <input type="text" name="permanent_road" id="permanentRoad" class="form-control rounded" required
                                        value="{{ old('permanent_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="about" class="card-title font-weight-bold">{{__('About')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                            <textarea rows="10" name="about" id="about" class="form-control rounded" required>{!! old('about') !!}</textarea>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="password" class="card-title font-weight-bold">{{__('Password')}}: <i class="text-danger fas fa-star-of-life"></i></label></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="password" id="password" name="password" class="form-control" aria-label="Large"
                                        aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Password')}}">
                                    <br>
                                    @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p class="mb-1"><label for="passwordConfirmation" class="card-title font-weight-bold">{{__('Confirm Password')}}: <i class="text-danger fas fa-star-of-life"></i></label></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="password" id="passwordConfirmation" name="password_confirmation" class="form-control" aria-label="Large"
                                        aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Password')}}">
                                    <br>
                                    @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="cvFile" class="card-title font-weight-bold">{{__('CV')}}: <i class="text-danger fas fa-star-of-life"></i></label>
                                    <code>{{ __('please upload CV in PDF format') }}</code></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="file" name="cv_file" id="cvFile" class="form-control-file" value="{{ old('cv_file') }}">
                                    <br>
                                    @if ($errors->has('cv_file'))
                                    <span class="text-danger">{{ $errors->first('cv_file') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <p class="mb-1"><label for="joinDate" class="card-title font-weight-bold">{{__('Join Date')}}: <i class="text-danger fas fa-star-of-life"></i></label></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="date" name="join_date" id="joinDate" class="form-control"
                                        value="{{ $teacher ? $teacher->join_date : old('join_date') }}">
                                    <br>
                                    @if ($errors->has('join_date'))
                                    <span class="text-danger">{{ $errors->first('join_date') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


</div>
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/js/form-summerNote.js') }}"></script>
@endsection
