@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <form action="{{ route('admin.teachers.store-bulk') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <label>{{ __('Insert Excel File:') }}</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="customFile" name="file" required>
                            <label class="custom-file-label" for="customFile">{{ __('Choose file') }}</label>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                        <div class="ml-2">
                            <a href="{{ route('admin.teachers.create-bulk.demo-file') }}" class="btn btn-success">{{__('Download')}}</a><span>{{__(' a demo file to follow column name and data')}}</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('page-script')
<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@include('backend.pages.teachers.internal-assets.js.all-teacher-js')
@endsection
