@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header d-block">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <h6 class="card-title">{{__($title)}}</h6>
                        </div>
                        <div class="col-md-6 col-sm-12 text-right">

                        </div>
                    </div>

                </div>
                <div class="card-body ">
                    <div class="table-responsive style-scroll">

                        <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%">{{__('SL No.')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Bangla Name')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Old Class')}}</th>
                                    <th>{{__('New Class')}}</th>
                                    <th>{{__('Address')}}</th>
                                    <th>{{__('Gender')}}</th>
                                    <th>{{__('Marksheet')}}</th>
                                    <th>{{__('Birth Date')}}</th>
                                    <th width="104">{{__('Option')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $key => $data)
                                <tr>
                                    <th>{{__($loop->index+1)}}</th>
                                    <th>{{__($data->user->name)}}</th>
                                    <td>{{__($data->user->profile->name_utf8)}}</td>
                                    <td>{{__($data->user->phone)}}</td>
                                    <td>{{__($data->old_class?$data->getClassName($data->old_class):'')}}</td>
                                    <td>{{__($data->getClassName($data->new_class))}}</td>
                                    <td>{{__($data->user->profile->present_address)}}</td>
                                    <td>{{__($data->gender)}}</td>
                                    <td>
                                        <a href="{{asset('upload/mark-sheets/'.$data->mark_sheet)}}" target="_blank" class="">
                                            <img src="{{asset('backend/assets/img/file/pdf.png')}}" width="60" height="50" alt="">
                                        </a>
                                    </td>
                                    <td>{{__(date('D, d M Y',strtotime($data->date_of_birth)))}}</td>
                                    <td>
                                        <a href="javascript:void(0)" id="{{ $data->id }}" title="Approved" class="btn btn-success  btn-circle studentAdmittedBtn">
                                            <i class="material-icons">check</i>
                                        </a>
                                        <a href="javascript:void(0)" title="Deny Student" class="btn btn-danger btn-circle deleteRow">
                                            <i class="material-icons">clear</i>
                                            <form action="{{ route('admin.admission.destroy',$data->id) }}" method="get">
                                                @csrf
                                            </form>
                                        </a>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@include('backend.pages.admissions.internal-assets.js.student_activation')
@include('backend.pages.admissions.internal-assets.js.delete-warning')

@endsection