
<script>

    $('.studentAdmittedBtn').on('click', function () {
        var selected = $(this).attr('id');
        var url = "{{route('admin.admission.activation', '')}}"+"/"+selected;
        $.ajax({
            type:'get',
            url: url,
            success:function (data) {
                // do nothing;
                setInterval('location.reload()', 1000);
                toastr.success(data.message);
            }
        });
    });
</script>
