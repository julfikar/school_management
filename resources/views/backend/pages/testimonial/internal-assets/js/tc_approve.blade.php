
<script>
    var drivePath = window.location.pathname.replace('/','');
    console.log(drivePath);

    $('.tcApprovedBtn').on('click', function () {
        var selected = $(this).attr('id');
        $.ajax({
            type:'get',
            url: '/'+drivePath + '/approve/'+selected,
            success:function (data) {
                toastr.success("TC status has changed successfully.");
            }
        });
    });
</script>
