@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.wizards.sliders.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header d-block">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h6 class="card-title">{{__($title)}}</h6>
                            </div>
                            <div class="col-md-6 col-sm-12 text-right">

                            </div>
                        </div>

                    </div>
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="15%">{{__('SL No.')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Testimonial')}}</th>
                                    {{--<th class="text-center">{{__('Status')}}</th>--}}
                                    <th class="text-center">{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($testmonials as $key => $data)
                                    <tr>
                                        <th>{{__($loop->index+1)}}</th>
                                        <th>{{$data->name}}</th>
                                        <td>{{ date('F d, Y', strtotime($data->created_at)) }}</td>
                                        <td>
                                            <a href="{{ route('admin.testimonial.download',$data->id) }}" class=""> <img src="{{ asset('backend/assets/img/file/pdf.png') }}" class="img-fluid" alt=""></a>
                                        </td>
                                        {{--<td class="text-center">
                                            <label class="switch">
                                                <input type="checkbox" {{ $data->status?'checked':'' }} id="{{ $data->id }}" class="tcApprovedBtn">
                                                <span class="slider round"></span>
                                            </label>
                                        </td>--}}
                                        <td class="text-center">

                                            <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                                <i class="material-icons">delete</i>
                                                <form action="{{ route('admin.testimonial.destroy',$data->id) }}" method="get">
                                                    @csrf
                                                </form>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.testimonial.internal-assets.js.tc_approve')
    @include('backend.pages.testimonial.internal-assets.js.delete-warning')

@endsection
