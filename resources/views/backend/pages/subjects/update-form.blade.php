@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="javascript:history.go(-1)">{{__('Subjects')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-12">
                            <span class="h6 card-title">{{__($title)}}</span>
                            <button type="button" class="btn btn-warning btn-sm float-right" id="subjectDeleteBtn">{{__('Delete')}}</button>
                        </div>
                    </div>
                    <form class="" action="{{ route('admin.update.subject') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-7">
                                    <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                               placeholder="{{__('Name')}}" value="{{$subject->name}}">
                                        <br>
                                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>

                                    <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Total Mark')}}:</label> </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="number" name="total_mark" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                               placeholder="{{__('Total Mark')}}" value="{{$subject->total_mark}}">
                                        <br>
                                        @if ($errors->has('total_mark'))
                                            <span class="text-danger">{{ $errors->first('total_mark') }}</span>
                                        @endif
                                    </div>

                                    <p class="mb-1">{{__('Teacher Name')}}: </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <select class="form-control form-control-lg" name="teacher_id">
                                            <option value="">{{__('Select Teacher')}}</option>
                                            @foreach($teachers as $key => $data)
                                                <option value="{{$data->id}}" {{ $data->id==$subject->teacher_id?'selected':'' }}>{{$data->user->name}}</option>
                                            @endforeach
                                        </select>
                                        <br>
                                        @if ($errors->has('teacher_id'))
                                            <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                                        @endif
                                    </div>

                                    @if($subject->e_book)
                                        <p class="h6 mb-3">{{ __('Old Ebook') }}</p>
                                        <a href="{{ asset('upload/subjects/e_books/'.$subject->e_book) }}" class="">
                                        <img src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="">
                                        </a>
                                    @endif

                                </div>
                                <div class="col-md-5">
                                    <div class="mb-3">
                                        @if($subject->thumbnail)
                                        <img src="{{ asset('upload/subjects/thumbnails/'.$subject->thumbnail) }}" alt="" class="img-fluid" height="100" width="100">
                                        @endif
                                    </div>
                                    <p class="h6 mb-3">{{ __('Thumbnail') }}</p>
                                    <div class="">
                                        <div class="subject-thumbnail" id="subject-thumbnail">
                                            <div class="input-images"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>



                            <p class="h6 mb-3">{{ __('E-Book') }}</p>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="">
                                        <input type="file" name="e_book">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="sid" value="{{$subject->id}}">
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <form action="{{ route('admin.destroy-subject',$subject->id) }}" method="get" id="deleteForm"></form>
@endsection

@section('page-script')
    @include('backend.pages.subjects.internal-assets.js.subject-page-scripts')
@endsection
