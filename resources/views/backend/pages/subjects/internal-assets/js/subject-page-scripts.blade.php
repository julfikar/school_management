<script !src="">
    $(function () {
        $('.subject-thumbnail').imageUploader({
            imagesInputName: 'thumbnail',
            maxFiles: 1,
        });
        $('#subjectDeleteBtn').on('click', deleteAccountAlert);
    });

    function deleteAccountAlert() {
        swal({
            title: "{{__('Are you sure?')}}",
            text: "{{__('Once you delete, You can\'t recover this file')}}",
            icon: "warning",
            dangerMode: true,
            buttons: {
                cancel: true,
                confirm: {
                    text: "{{__('Delete')}}",
                    value: true,
                    visible: true,
                    closeModal: true
                },
            },
        }).then((value) => {
           if(value){
                document.getElementById('deleteForm').submit();
            }
        });
    }
</script>
