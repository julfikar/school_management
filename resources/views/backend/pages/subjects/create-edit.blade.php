@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white" href="{{ route('admin.get-class-subjects',$classRoom->id) }}">{{__('All Subjects')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <h6 class="card-title">{{__($classRoom->name)}} / {{__($title)}}</h6>
                </div>
                <form class="" action="{{ route('admin.store-class-subject') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-7">
                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Name')}}" value="">
                                    <br>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Total Mark:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="number" name="total_mark" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Total Mark')}}" value="">
                                    <br>
                                    @if ($errors->has('total_mark'))
                                        <span class="text-danger">{{ $errors->first('total_mark') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1">{{__('Teacher Name:')}} </p>
                                <div class="input-group input-group-lg mb-3">
                                    <select class="form-control form-control-lg" name="teacher_id">
                                        <option value="">Select Teacher</option>
                                        @foreach($teachers as $key => $data)
                                            <option value="{{$data->id}}">{{$data->user->name}}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                    @if ($errors->has('teacher_id'))
                                        <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5">
                                <p class="h6 mb-3">{{ __('Thumbnail') }}</p>

                                <div class="">
                                    <div class="subject-thumbnail" id="subject-thumbnail">
                                        <div class="input-images"></div>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <p class="h6 mb-3">{{ __('E-Book') }}</p>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="">
                                    <input type="file" name="e_book">
                                </div>
                            </div>
                        </div>


                        <input type="hidden" name="class_id" value="{{$classRoom->id}}">
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
    @include('backend.pages.subjects.internal-assets.js.subject-page-scripts')
@endsection
