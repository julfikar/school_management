@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item text-white">{{__($classRoom->name) }}</span>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-6">
                        <h6 class="card-title">{{__($classRoom->name) }} / {{__($title)}}</h6>
                    </div>

                    <div class="col-md-6 col-sm-12 text-right">
{{--                        <a href="javascript:void(0)" class="btn btn-danger" id="onlineClassBtn"> <span class="material-icons">videocam</span></a>--}}
                        <a href="{{ route('admin.class-routine',$classRoom->id) }}" class="btn btn-warning"> {{__('Routine')}}</a>
                        <a href="{{ route('admin.add-class-subject',$classRoom->id) }}" class="btn btn-success"> {{__('Add New Subject')}}</a>
                    </div>

                </div>
                <div class="card-body ">
                    <div class="row">
                        @if($classRoom->subjects->count()>0)
                            @foreach($classRoom->subjects as $subject)

                            <div class="col-md-3">
                                <div class="card text-white rounded">
                                    <img class="card-img-top rounded-top" src="{{ asset('upload/subjects/thumbnails/'.$subject->thumbnail) }}" alt="Card image cap" height="180">
                                    <div class="card-body bg-dark">
                                        <h5 class="card-title text-center">{{ __($subject->name) }}</h5>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item d-flex justify-content-between px-0 bg-dark">
                                                <span class="font-weight-bold">{{__('Teacher')}}:</span>
                                                <span>{{__($subject->teacher?$subject->teacher->user->name:'')}}</span>
                                            </li>
                                            <li class="list-group-item d-flex justify-content-between px-0 bg-dark">
                                                <span class="font-weight-bold">{{__('Total Mark')}}:</span>
                                                <span>{{ $subject->total_mark }}</span>
                                            </li>
                                        </ul>
                                        <div class="">
                                            <a href="{{ asset('upload/subjects/e_books/'.$subject->e_book) }}" target="_blank" class="btn btn-primary btn-block">{{__('Read book')}}</a>
                                        </div>

                                        <div class="mt-2">
                                            <a href="{{ route('admin.get-subject-resources',$subject->id) }}"  class="btn btn-info btn-block">{{__('View Resource')}}</a>
                                        </div>
                                        <div class="mt-3">
                                            <a href="{{ route('admin.show-update-subject-form',$subject->id) }}" class="btn btn-success btn-block">{{__('Edit')}}</a>
{{--                                            <div class="">--}}
{{--                                                <a href="{{ route('admin.destroy-subject',$subject->id) }}" class="btn btn-danger btn-sm">Delete</a>--}}
{{--                                            </div>--}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <div class="text-warning p-3" >
                                    <h6 class="text-center h6">{{__('There is no subject found.')}}</h6>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    send video class url--}}
    <div class="modal fade" id="sendVideoClassUrl" tabindex="-1" role="dialog" aria-labelledby="sendVideoClassUrlTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="sendVideoClassUrlTitle">{{ __('Send you online class url to student') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="javascript:void(0)" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="text">{{ __('Content') }}</label>
                            <textarea name="text" id="text" placeholder="{{ __('Write some text') }}" class="form-control" rows="5"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="url">{{ __('Class Url') }}</label>
                            <input type="text" name="text" id="text" placeholder="{{ __('Pest your meeting url') }}" required class="form-control">
                        </div>
{{--                        <div class="form-group">--}}
{{--                            <label for="shedulet">{{ __('Notify at') }}</label>--}}
{{--                            <input type="datetime-local" name="shedulet" id="shedulet" class="form-control">--}}
{{--                        </div>--}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary rounded" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-danger rounded">{{ __('Send') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.subjects.internal-assets.js.online-class')
@endsection
