@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="javascript:history.go(-1)">{{__('All Resource')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-12">
                            <span class="h6 card-title">{{__($title)}}</span>
                            <button type="button" class="btn btn-warning btn-sm float-right" id="resourceDeleteBtn">Delete</button>
                        </div>
                    </div>
                    <form class="" action="{{ route('admin.update.resource') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body ">


                            <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('Name')}}" value="{{ $resource->name }}">
                                <br>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Old Resource:')}}</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <a href="{{asset('upload/resources/'.$resource->file )}}" class="">
                                    <img src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="">
                                </a>
                            </div>

                            <p class="mb-1"><label for="file" class="card-title font-weight-bold">{{__('Resource/Notes')}}:</label>
                                <code>{{ __('please upload Resource/Notes in PDF format') }}</code></p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="file" name="file" id="cvFile" class="form-control-file" value="{{ old('file') }}">
                                <br>
                                @if ($errors->has('file'))
                                    <span class="text-danger">{{ $errors->first('file') }}</span>
                                @endif
                            </div>


                            <input type="hidden" name="rid" value="{{$resource->id}}">
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <form action="{{ route('admin.destroy-resource') }}" method="post" id="deleteForm">
        @csrf
        <input type="hidden" name="subject_id" value="{{$resource->subject->id}}">
        <input type="hidden" name="rid" value="{{$resource->id}}">
    </form>
@endsection

@section('page-script')
    @include('backend.pages.resources.internal-assets.js.resource-page-scripts')
@endsection