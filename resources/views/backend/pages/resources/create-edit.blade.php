@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white" href="javascript:history.go(-1)">{{__('All Resource')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <form class="" action="{{route('admin.store-subject-resource') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="card-body ">


                        <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                   placeholder="{{__('Name')}}" value="{{  old('name') }}">
                            <br>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>


                        <p class="mb-1"><label for="file" class="card-title font-weight-bold">{{__('Resource/Notes')}}:</label>
                            <code>{{ __('please upload Resource/Notes in PDF format') }}</code></p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="file" name="file" id="cvFile" class="form-control-file" value="{{ old('file') }}">
                            <br>
                            @if ($errors->has('file'))
                                <span class="text-danger">{{ $errors->first('file') }}</span>
                            @endif
                        </div>


                        <input type="hidden" name="subject_id" value="{{$subject->id}}">
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
    @include('backend.pages.subjects.internal-assets.js.subject-page-scripts')
@endsection