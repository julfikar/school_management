@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.settings.page-settings.internal-assets.css.page-css')
    <link rel="stylesheet" href="{{asset('backend/assets/plugin/tag-input/tagsinput.css')}}" />
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('admin.blogposts.index',$blogFolder->id):route('dashboard') }}">{{__(ucwords($blogFolder->name))}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>

                    <form action="{{ route('admin.blogposts.store') }}" method="POST" enctype="multipart/form-data"
                          class="">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 order-1">
                                    <div class="card-body bg-dark">
                                        <p class="mb-1 text-uppercase"><label for="pageTitle">{{__('Post Title')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup> </p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="title" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="pageName"
                                                   placeholder="{{__('Post title')}}" value="{{old('title')}}" required>
                                            <br>
                                            @if ($errors->has('title'))
                                                <span class="text-danger">{{ $errors->first('title') }}</span>
                                            @endif
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="body">{{__('Content')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup> </p>
                                        <div class="input-group">
                                            <div class="form-group w-100">
                                                <textarea rows="8" name="body" id="body" class="form-control rounded">{!! old('body') !!}</textarea>
                                            </div>
                                        </div>



                                        <p class="mb-1 text-uppercase"><label for="pageTitle">{{__('Post tag')}}</label>:  </p>
                                        <div class="input-group input-group-lg">
                                            <input type="text" name="post_tags" class="form-control" data-role="tagsinput" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="pageName"
                                                   placeholder="{{__('Post tags')}}" value="{{old('post_tags')}}" >
                                            <br>
                                            @if ($errors->has('post_tags'))
                                                <span class="text-danger">{{ $errors->first('post_tags') }}</span>
                                            @endif
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 order-2">
                                    <div class="card-body bg-dark">

                                        <p class="mb-1 text-uppercase"><label for="postStatus">{{__('Publish status')}}</label>: </p>
                                        <div class="input-group input-group-lg mb-3 text-center">
                                            <select name="status" id="postStatus" class="form-control" required>
                                                <option selected disabled value="{{ null }}">{{__('Select one')}}</option>
                                                <option value="1">{{__('Publish')}}</option>
                                                <option value="0">{{__('Un-publish')}}</option>
                                            </select>
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="pageRightBar">{{__('Right-bar')}}</label>:</p>
                                        <div class="input-group input-group-lg mb-3 text-center">
                                            <label class="switch">
                                                <input type="checkbox" id="pageRightBar" name="post_right">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>

                                        <p class="mb-1">{{__('Post Image')}}: <sup><i class="text-danger fas fa-star-of-life small"></i></sup></p>
                                        <div class="input-group p-3">
                                            <div class="form-group w-100">
                                                <div class="px-2">
                                                    <div class="post_img" id="post_img">
                                                        <div class="input-images"></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <input type="hidden" name="folder_id" value="{{$blogFolder->id}}">

                            <div class="wizard-action card-body py-0 text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg px-4" type="submit">{{__('Submit')}}</button>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    @include('backend.pages.blogs.blog-post.internal-assets.js.post-form')
    <script src="{{asset('backend/assets/plugin/tag-input/tagsinput.js')}}"></script>
    <script src="{{ asset('backend/assets/js/form-summerNote.js') }}"></script>
@endsection
