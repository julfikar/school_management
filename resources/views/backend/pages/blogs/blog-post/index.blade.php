@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
    @include('backend.pages.wizards.sliders.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    @can('blog settings')
                        <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.blogfolders.index'):route('dashboard') }}">{{__('Blog Folder')}}</a>
                    @endcan
                    <span class="breadcrumb-item active text-capitalize">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form action="" method="get" id="folderForm">
                    <div class="form-group">
                        <select class="form-control selectpicker" data-live-search="true" data-size="3" tabindex="-98">

                            <option {{ $blogFolder?"":'selected' }} disabled value="{{ null }}">{{ __('Select one') }}</option>
                            @foreach($folders as $folder)
                                <option {{ $blogFolder?($blogFolder->id == $folder->id?'selected':''):'' }} value="{{ $folder->id }}">{{ __($folder->name) }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header d-block">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h6 class="card-title">{{__($title)}}</h6>
                            </div>
                            <div class="col-md-6 col-sm-12 text-right">
                                <a href="{{ route('admin.blogposts.create',$blogFolder?$blogFolder->id:'') }}" class="btn btn-success"> {{__('Add New Post')}}</a>
                            </div>
                        </div>

                    </div>
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="15%">{{__('SL No.')}}</th>
                                    @role('super admin')<th>{{__('Author')}}</th>@endrole
                                    <th>{{__('Title')}}</th>
                                    <th>{{__('Thumbnail')}}</th>
                                    @role('super admin')<th class="text-center">{{__('Status')}}</th>@endrole
                                    <th class="text-center">{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $key => $data)
                                    <tr>
                                        <th>{{ $loop->index+1 }}</th>
                                        @role('super admin')<td>{{__($data->user->name)}}</td>@endrole
                                        <td>{{__($data->title)}}</td>
                                        <td>
                                            <img src="{{asset('upload/blogs/post-thumbnails/'.$data->thumbnail)}}" class="img-fluid"  height="15" width="100" alt="">
                                        </td>
                                        @role('super admin')<td class="text-center">
                                            <label class="switch">
                                                <input type="checkbox" {{ $data->status?'checked':'' }} id="{{ $data->id }}" class="postActivationBtn">
                                                <span class="slider round"></span>
                                            </label>
                                        </td>@endrole
                                        <td class="text-center">
                                            <a href="{{ route('admin.blogposts.show',$data->id) }}" class="btn btn-info  btn-circle" >
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                                <i class="material-icons">delete</i>
                                                <form action="{{ route('admin.blogposts.destroy',$data->id) }}" method="get">
                                                    @csrf
                                                </form>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
    @include('backend.pages.blogs.blog-post.internal-assets.js.post_activation')
    @include('backend.pages.blogs.blog-post.internal-assets.js.delete-warning')

@endsection
