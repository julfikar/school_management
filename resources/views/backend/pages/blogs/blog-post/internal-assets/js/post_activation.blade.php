
<script>
    $('#folderForm select').on('change', function (){
        if ($(this).val()) {
            let url = "{{ route('admin.blogposts.index','') }}" + '/' + $(this).val();
            $('#folderForm').attr('action', url).submit();
        }
    });

    $('.postActivationBtn').on('click', function () {
        var selected = $(this).attr('id');
        var url = "{{route('admin.blogposts.activation', '')}}"+"/"+selected;

        $.ajax({
            type:'get',
            url: url,
            success:function (data) {
                // do nothing;
                toastr.success("Status has been changed successfully.");
            }
        });
    });


</script>


