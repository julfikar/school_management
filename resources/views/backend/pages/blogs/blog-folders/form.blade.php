@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.blogfolders.index'):route('dashboard') }}">{{__('Blogs')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">

                        <form action="{{ $blogFolder?route('admin.blogfolders.update',$blogFolder->id):route('admin.blogfolders.store') }}" method="POST" class="wma-form" enctype="multipart/form-data">
                            @csrf
                            @if($blogFolder)
                                @method('PATCH')
                            @endif

                            <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="name" id="name"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('Name')}}" value="{{ $blogFolder ? $blogFolder->name : old('name') }}">
                                <br>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <p class="h6 mb-3">{{ __('Thumbnail') }}</p>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="">
                                        <div class="admin-image" id="admin_image">
                                            <div class="input-images"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center">
                                    @if($blogFolder)
                                    <img src="{{ asset('upload/blogs/folder-thumnails/'.$blogFolder->thumbnail)  }}" class="img-thumbnail " width="80%" >
                                    @endif
                                </div>


                            </div>


                            <div class="wizard-action text-left mt-4">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit')}}</button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    @include('backend.pages.blogs.blog-folders.internal-assets.js.blog-folder-scripts')
@endsection
