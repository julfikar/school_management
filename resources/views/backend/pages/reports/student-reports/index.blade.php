@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.wizards.sliders.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-body ">
                        <nav class="my-3">
                            <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active text-danger border-0" id="nav-home-tab" data-toggle="tab" href="#english" role="tab" aria-controls="nav-home" aria-selected="true"><h6 class="mb-0">{{__('Attendance')}}</h6></a>
                                <a class="nav-item nav-link text-danger border-0" id="nav-profile-tab" data-toggle="tab" href="#bangla" role="tab" aria-controls="nav-profile" aria-selected="false"><h6 class="mb-0">{{__('Result')}}</h6></a>
                                <a class="nav-item nav-link text-danger border-0" id="nav-account-tab" data-toggle="tab" href="#account" role="tab" aria-controls="nav-account" aria-selected="false"><h6 class="mb-0">{{__('Accounts')}}</h6></a>
                            </div>
                        </nav>
                        <br>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="english" role="tabpanel" aria-labelledby="nav-home-tab">
                                <form action="{{route('admin.student-report.attendance-calculate')}}" method="post" >
                                    @csrf

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('Student ID')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" class="form-control border-md" value="" id="unique_id" name="unique_id" placeholder="{{__('Enter student ID for attendance..')}}" required>
                                        <div class="invalid-feedback">
                                            @if ($errors->has('unique_id'))
                                                <span class="text-danger">{{ $errors->first('unique_id') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('Start Date')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="date" class="form-control border-md" value="" id="start_date" name="start_date" placeholder="{{__('Select Date')}}">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('start_date'))
                                                <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('End Date')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="date" class="form-control border-md" value="" id="end_date" name="end_date" placeholder="Select Date">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('end_date'))
                                                <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="py-2 mb-4 text-center">
                                        <button type="submit" class="btn btn-theme-green btn-lg bg-danger text-white">{{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade" id="bangla" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <form action="{{route('admin.student-report.exam-result-calculate')}}" method="post" >
                                    @csrf

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('Student ID')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" class="form-control border-md" value="" id="unique_id" name="unique_id" placeholder="{{__('Enter student ID for result..')}}" required>
                                        <div class="invalid-feedback">
                                            @if ($errors->has('unique_id'))
                                                <span class="text-danger">{{ $errors->first('unique_id') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('Start Date')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="date" class="form-control border-md" value="" id="start_date" name="start_date" placeholder="Select Date">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('start_date'))
                                                <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('End Date')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="date" class="form-control border-md" value="" id="end_date" name="end_date" placeholder="Select Date">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('end_date'))
                                                <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="py-2 mb-4 text-center">
                                        <button type="submit" class="btn btn-theme-green btn-lg bg-danger text-white">{{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="account" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <form action="{{route('admin.student-report.accounts-calculate')}}" method="post" >
                                    @csrf

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('Student ID')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" class="form-control border-md" value="" id="unique_id" name="unique_id" placeholder="{{__('Enter student ID for accounts..')}}" required>
                                        <div class="invalid-feedback">
                                            @if ($errors->has('unique_id'))
                                                <span class="text-danger">{{ $errors->first('unique_id') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('Start Date')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="date" class="form-control border-md" value="" id="start_date" name="start_date" placeholder="{{__('Select Date')}}">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('start_date'))
                                                <span class="text-danger">{{ $errors->first('start_date') }}</span>
                                            @endif
                                        </div>
                                    </div>

                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('End Date')}} : <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="date" class="form-control border-md" value="" id="end_date" name="end_date" placeholder="Select Date">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('end_date'))
                                                <span class="text-danger">{{ $errors->first('end_date') }}</span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="py-2 mb-4 text-center">
                                        <button type="submit" class="btn btn-theme-green btn-lg bg-danger text-white">{{__('Submit')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="feeInputModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.accounts.account-settings.internal-assets.js.account_setting_activation')

@endsection
