@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.student-report.index'):route('dashboard') }}">{{__('Search Report')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-body ">

                        <input type="hidden" id="all_fees_paid_amount" value="{{$all_fees_paid_amount}}">
                        <input type="hidden" id="all_fees_get_amount" value="{{$all_fees_get_amount}}">
                        <p class="my-2 text-center h5">{{ucwords($student_name) }} {{__('Accounts Report')}}</p>
                        <div class="d-flex justify-content-center text-center">
                            <div id="piechart"></div>
                        </div>


                            <p class="mb-3 text-center">{{__('Here will see the student tuition fee, exam fee, hostel fee , library fee, transportation fees report.')}}</p>


                        <div class="table-responsive style-scroll">

                           {{-- <table id="" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center">{{__('Tution Fee')}}</th>
                                    <th class="text-center">{{__('Hostel Fee')}}</th>
                                    <th class="text-center">{{__('Transportation Fee')}}</th>
                                    <th class="text-center">{{__('Library Fee')}}</th>
                                    <th class="text-center">{{__('Exam Fee')}}</th>
                                </tr>
                                </thead>


                                    <tr>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="btn btn-primary" onclick="{{$total_get_tution_fee==0?'event.preventDefault()':"event.preventDefault();document.getElementById('tutionFeeReport').submit();"}}">
                                                {{$total_paid_tution_fee}} / {{$total_get_tution_fee}} <i class="fa fa-eye text-right pl-3"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="btn btn-primary" onclick="{{$total_get_hostel_fee==0?'event.preventDefault()':"event.preventDefault();document.getElementById('hostelFeeReport').submit();"}}" >
                                                {{$total_paid_hostel_fee}} / {{$total_get_hostel_fee}}<i class="fa fa-eye text-right pl-3"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="btn btn-primary" onclick="{{$total_get_transportation_fee==0?'event.preventDefault()':"event.preventDefault();document.getElementById('transportationFeeReport').submit();"}}">
                                                {{$total_paid_transportation_fee}} / {{$total_get_transportation_fee}} <i class="fa fa-eye text-right pl-3"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="btn btn-primary" onclick="{{$total_get_library_fee==0?'event.preventDefault()':"event.preventDefault();document.getElementById('libraryFeeReport').submit();"}}">
                                                {{$total_paid_library_fee}} / {{$total_get_library_fee}} <i class="fa fa-eye text-right pl-3"></i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="btn btn-primary" onclick="{{$total_get_exam_fee==0?'event.preventDefault()':"event.preventDefault();document.getElementById('examFeeReport').submit();"}}">
                                                {{$total_paid_exam_fee}} / {{$total_get_exam_fee}}<i class="fa fa-eye text-right pl-3"></i>
                                            </a>
                                        </td>

                                    </tr>

                                <tbody>

                                </tbody>
                            </table>--}}

                           <div class="row">

                               <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                   <thead>
                                   <tr>
                                       <th width="10%">{{__('SL No.')}}</th>
                                       <th class="text-center">{{__('Name')}}</th>
                                       <th class="text-center">{{__('Date')}}</th>
                                       <th class="text-center">{{__('Payment')}}</th>
                                       <th class="text-center">{{__('Discount')}}</th>
                                       <th class="text-center">{{__('Comment')}}</th>


                                   </tr>
                                   </thead>

                                   <tbody>

                                   @php
                                       $payed_amount =0;
                                       $payable_amount =0;
                                   @endphp
                                   @foreach($data2 as $item )
                                       @foreach($item->new_reports as $new_item)
                                           <tr>
                                               <td>{{ $loop->index+1 }}</td>
                                               <td class="text-center">{{ $new_item['name']}}</td>
                                               <td class="text-center">{{ $new_item['date']}}</td>
                                               <td class="text-center">{{$new_item['paid']}}</td>
                                               <td class="text-center">{{$new_item['discount']}}</td>
                                               <td>{{ $new_item['due']?'Due ('.$new_item['due'].')':__('Payed') }}</td>
                                           </tr>
                                       @endforeach
                                       @php
                                           $payed_amount =$payed_amount+$item->total_fees_payed;
                                           $payable_amount =$payable_amount+$item->total_payable_fees;
                                       @endphp
                                   @endforeach
                                   </tbody>
                               </table>


                           </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    <script src="{{asset('backend/assets/js/loader.js')}}"></script>
    @include('backend.pages.reports.student-reports.internal-assets.js.main_account_js')
@endsection
