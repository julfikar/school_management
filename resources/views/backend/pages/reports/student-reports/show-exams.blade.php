@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.wizards.sliders.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="javascript:void(0)" onclick="javascript:history.go(-1)">{{__('All Report')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-body ">


                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="10%">{{__('SL No.')}}</th>
                                    <th class="text-center">{{__('Exam Name')}}</th>
                                    <th class="text-center">{{__('Date')}}</th>
                                    <th class="text-center">{{__('Got Mark')}}</th>
                                    <th class="text-center">{{__('Remark')}}</th>
                                    <th class="text-center">{{__('CGPA')}}</th>
                                </tr>
                                </thead>
                                @foreach ($results as $key=>$result)
                                    @foreach ($result as $data)
                                        <tr>
                                            <td>{{ $loop->index+1 }}</td>
                                            <td>{{$data->exam->name}}</td>
                                            <td class="text-center">{{ date('d-M-Y', strtotime($data->created_at)) }}</td>
                                            <td class="text-center">{{ $data->got_mark }}</td>
                                            <td class="text-center">{{ $data->remark }}</td>
                                            <td class="text-center">{{ $data->cgpa }}</td>
                                        </tr>
                                        @endforeach
                                @endforeach
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="feeInputModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    <script src="{{asset('backend/assets/js/loader.js')}}"></script>




@endsection
