@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.student-report.index'):route('dashboard') }}">{{__('Search Report')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-body ">

                        <input type="hidden" id="total_attendece" value="{{ $total_attendece }}">
                        <input type="hidden" id="total_absent" value="{{ $total_absent }}">
                        <p class="my-2 text-center h5">{{ucwords($student_name) }} Attendance Days Report</p>
                        <div class="d-flex justify-content-center text-center">
                            <div id="piechart"></div>
                        </div>

                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="10%">{{__('SL No.')}}</th>
                                        <th class="text-center">{{__('Date')}}</th>
                                        <th class="text-center">{{__('Status')}}</th>
                                        <th width="15%">{{__('Attendace Time')}}</th>
                                    </tr>
                                </thead>
                                    @foreach($days as $key=> $data)
                                        <tr>
                                            <td>{{ $loop->index+1 }}</td>
                                            <td class="text-center">{{ date('d-M-Y', strtotime($data->date)) }}</td>
                                            <td class="text-center">
                                                @if($data->status)
                                                    <span class="badge badge-success">Present</span>
                                                @else
                                                    <span class="badge badge-danger">Absent</span>
                                                 @endif
                                            </td>
                                            <td>{{ $data->attend_time }}</td>

                                        </tr>
                                    @endforeach
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="feeInputModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    <script src="{{asset('backend/assets/js/loader.js')}}"></script>

    @include('backend.pages.reports.student-reports.internal-assets.js.students_report_js')



@endsection
