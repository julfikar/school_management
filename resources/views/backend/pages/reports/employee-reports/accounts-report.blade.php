@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.employee-report.index'):route('dashboard') }}">{{__('Search Report')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-body ">

                        <input type="hidden" id="all_fees_paid_amount" value="{{$all_fees_paid_amount}}">
                        <input type="hidden" id="all_fees_get_amount" value="{{$all_fees_get_amount}}">
                        <p class="my-2 text-center h5">{{__($employee_name) }} Accounts Report</p>
                        <div class="d-flex justify-content-center text-center">
                            <div id="piechart"></div>
                        </div>

                        <div class="text-center">
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="{{$total_get_salary==0?'event.preventDefault()':"event.preventDefault();document.getElementById('salaryFeeReport').submit();"}}">
                                <span class="pr-3">Salary Report</span> {{$total_paid_salary}} / {{$total_get_salary}} <i class="fa fa-eye text-right pl-3"></i>
                            </a>
                        </div>

                        <div class="table-responsive style-scroll">

                            <div class="row">
                                <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th width="10%">{{__('SL No.')}}</th>
                                        <th class="text-center">{{__('Name')}}</th>
                                        <th class="text-center">{{__('Date')}}</th>
                                        <th class="text-center">{{__('Payment')}}</th>
                                        <th class="text-center">{{__('Discount')}}</th>
                                        <th class="text-center">{{__('Comment')}}</th>


                                    </tr>
                                    </thead>

                                    <tbody>

                                    @php
                                        $payed_amount =0;
                                        $payable_amount =0;
                                    @endphp
                                    @foreach($data2 as $item )
                                        @foreach($item->new_reports as $new_item)
                                            <tr>
                                                <td>{{ $loop->index+1 }}</td>
                                                <td class="text-center">{{ $new_item['name']}}</td>
                                                <td class="text-center">{{ $new_item['date']}}</td>
                                                <td class="text-center">{{$new_item['paid']}}</td>
                                                <td class="text-center">{{$new_item['discount']}}</td>
                                                <td>{{ $new_item['due']?'Due ('.$new_item['due'].')':'Payed' }}</td>
                                            </tr>
                                        @endforeach
                                        @php
                                            $payed_amount =$payed_amount+$item->total_fees_payed;
                                            $payable_amount =$payable_amount+$item->total_payable_fees;
                                        @endphp
                                    @endforeach
                                    </tbody>
                                </table>


                            </div>


                            <form action="{{ route('admin.employee-report.salary-calculate') }}" method="post" id="salaryFeeReport">
                                @csrf
                                <input type="hidden" id="employee_id" name="unique_id" value="{{$unique_id}}" >
                                <input type="hidden" id="start_date" name="start_date" value="{{$start_date}}" >
                                <input type="hidden" id="end_date" name="end_date" value="{{$end_date}}" >
                            </form>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    <script src="{{asset('backend/assets/js/loader.js')}}"></script>
    @include('backend.pages.reports.employee-reports.internal-assets.js.main_account_js')
@endsection
