<script type="text/javascript">
    var total_attendece=  document.getElementById('total_attendece').value;
    var total_absent=  document.getElementById('total_absent').value;

    var total_attendece1= parseInt(total_attendece);
    var total_absent1= parseInt(total_absent);

    // Load google charts
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    // Draw the chart and set the chart values
    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Attendance'],
            ['Attendance', total_attendece1],
            ['Absent', total_absent1],

        ]);

        // Optional; add a title and set the width and height of the chart
        var options = {
            'title':'Attendance Report',
            'backgroundColor': '#464646',
            'width':550,
            'height':400,
            'chartArea': {
                top:20,
                'width': '100%',
                'height': '70%'
            },
            titlePosition: 'none',
          /*  titleTextStyle:{
                color: '#ffffff',
                fontSize: 21,
                bold: true,
            },*/
            legend:{
                textStyle:{
                    color: '#fff',
                    fontSize: 18,
                    bold: true,
                },
                position:'bottom',

            }
            // pieHole: 0.4,
            // is3D: true,
        };

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>