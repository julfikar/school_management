<script type="text/javascript">

    var all_fees_paid_amount=  document.getElementById('total_paid_salary').value;
    var all_fees_get_amount=  document.getElementById('total_get_salary').value;

    var all_fees_paid_amount1= parseInt(all_fees_paid_amount);
    var all_fees_get_amount1= parseInt(all_fees_get_amount);
    var all_fees_get_due= all_fees_get_amount1-all_fees_paid_amount1;

    // Load google charts
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    // Draw the chart and set the chart values
    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Attendance'],
            ['Total Paid', all_fees_paid_amount1],
            ['Total Due', all_fees_get_due],

        ]);

        // Optional; add a title and set the width and height of the chart
        var options = {
            'title':'Attendance Report',
            'backgroundColor': '#464646',
            'width':550,
            'height':400,
            'chartArea': {
                top:20,
                'width': '100%',
                'height': '70%'
            },
            titlePosition: 'none',
            /*  titleTextStyle:{
                  color: '#ffffff',
                  fontSize: 21,
                  bold: true,
              },*/
            legend:{
                textStyle:{
                    color: '#fff',
                    fontSize: 18,
                    bold: true,
                },
                position:'bottom',

            }
            // pieHole: 0.4,
            // is3D: true,
        };

        // Display the chart inside the <div> element with id="piechart"
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
</script>