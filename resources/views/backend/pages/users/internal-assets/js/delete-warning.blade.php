<script !src="">
    $(function () {
        $('.studentDeleteBtn').on('click',function (){
            swal({
                title: "{{__('Are you sure?')}}",
                text: "{{__('Once you delete, You can\'t recover this data')}}",
                icon: "warning",
                dangerMode: true,
                buttons: {
                    cancel: true,
                    confirm: {
                        text: "{{__('Delete')}}",
                        value: true,
                        visible: true,
                        closeModal: true
                    },
                },
            }).then((value) => {
                if(value){
                    $(this).parent().find('.deleteForm').submit();
                }
            });
        });
    });
</script>
