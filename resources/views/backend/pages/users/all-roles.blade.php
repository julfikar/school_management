@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">
                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="10%">{{__('SL No.')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $key => $role)
                                    <tr>
                                        <th>{{ $key+1 }}</th>
                                        <th class="text-capitalize">{{ $role->name }}</th>
                                        <td>
                                            <a href="{{ route('admin.user.create-role',$role->id) }}">
                                                <button class="btn btn-primary  btn-circle">
                                                    <i class="material-icons">edit</i>
                                                </button>
                                            </a>
                                            <a href="{{ route('admin.user.destroy-role',$role->id) }}" title="Delete" onclick="return confirm('Are you sure, would you like to delete this role?');">
                                                <button class="btn btn-danger  btn-circle">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@endsection
