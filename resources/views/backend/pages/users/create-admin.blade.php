@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <form action="#" method="POST">
                            @csrf
                            <p class="mb-1">{{__('Full Name:')}} </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="name"  class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Full Name')}}" >
                                <br>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Phone:')}}</p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="phone"  class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Phone Number')}}">
                                <br>
                                @if ($errors->has('phone'))
                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Email:')}} </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="email"  class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Email Name')}}">
                                <br>
                                @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Admin Type:')}} </p>
                            <div class="input-group input-group-lg mb-3">
                                <select class="form-control form-control-lg" name="type">
                                    <option value="1">ADM</option>
                                </select>
                                <br>
                                @if ($errors->has('type'))
                                    <span class="text-danger">{{ $errors->first('type') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Password:')}} </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="password" name="password"  class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Password')}}" >
                                <br>
                                @if ($errors->has('password'))
                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                            <p class="mb-1">{{__('Confirm Password:')}} </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="password" name="confirm_password"  class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Confirm Password')}}" >
                                <br>
                                @if ($errors->has('confirm_password'))
                                    <span class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                @endif
                            </div>
                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Create Admin')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
