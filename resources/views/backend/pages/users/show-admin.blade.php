@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <form action="{{ route('admin.user.show-admin',$user->id) }}" method="POST">
                            @csrf
                            <p class="mb-1">{{__('Admin\'s Name')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="name"  class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="{{__('Admin Name')}}" readonly required value="{{ $user->name }}">
                                <br>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Admin\'s Role')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <select name="role" id="role" class="selectpicker form-control" data-live-search="true" data-size="5" tabindex="-98" required>
                                    <option selected disabled value="{{ null }}">{{ __('Select one') }}</option>
                                    @foreach($roles as $item)
                                        <option {{ $user->hasRole($item->name)?'selected':'' }} class="text-capitalize" value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role'))
                                    <span class="text-danger">{{ $errors->first('role') }}</span>
                                @endif
                            </div>
                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Edit Admin')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
@endsection
