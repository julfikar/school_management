<script>
    $('#selectUserType').on('change', function (){
        $("input:checkbox").removeAttr('checked');
        $('#allUsers').find('.card-body').removeClass('d-none').addClass('d-none');
        $('#'+$(this).val()+'_content').removeClass('d-none');
    });

    $('#selectByClass').on('change', function (){
        $("input:checkbox").removeAttr('checked');
        let id = $(this).val();
        id = id.replace(' ','')
        $('#students_content').find('.stubentViewContent').removeClass('d-none').addClass('d-none');
        $('#'+id+'_content').removeClass('d-none').find('#checkAll'+ id+'Student').removeAttr('checked');
        // $('#'+id+'_content');
    });
</script>
