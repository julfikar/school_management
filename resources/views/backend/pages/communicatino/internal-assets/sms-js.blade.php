<script>
    $('#checkAllTeacher').on('change', function(){
        $('.teacherCheckBox').removeAttr('checked');
        if ($(this).prop('checked')){
            $('.teacherCheckBox').attr('checked','checked')
        }else {
            $('.teacherCheckBox').removeAttr('checked');
        }
    });
    $('#checkAllEmployee').on('change', function(){
        $('.employeeCheckBox').removeAttr('checked');
        if ($(this).prop('checked')){
            $('.employeeCheckBox').attr('checked','checked')
        }else {
            $('.employeeCheckBox').removeAttr('checked');
        }
    });
    $('.studentAllCheckBox').on('change', function(){
        $('.studentCheckBox').removeAttr('checked');
        if ($(this).prop('checked')){
            $(this).parent().parent().parent().find('.studentCheckBox').attr('checked','checked')
        }else {
            $(this).parent().parent().parent().find('.studentCheckBox').removeAttr('checked');
        }
    });

    $('.smsBtn').on('click', getuser);


    function getuser(){
        let content = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
        if (!content.getAttribute('id')){
            content = this.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
        }


        let id = content.getAttribute('id');
        let checkedUser = [];
        checkedUser[id] = [];
        for (let i = 0; i < content.querySelectorAll('input[type=checkbox]').length; i++){
            if (content.querySelectorAll('input[type=checkbox]')[i].checked){
                // console.log(id)
                checkedUser[id].push(content.querySelectorAll('input[type=checkbox]')[i].value)
            }
        }
        // <div class="form-group" id="sendtoUser">
        //     <input type="hidden" name="user[]" id="sendUserToMassage">
        // </div>
        if (checkedUser[id].length > 0){
            $('#smsModal').modal('show').on('hidden.bs.modal', function (e) {
                window.location.reload();
            });
            document.getElementById('sendtoUser').innerHTML = '';
            let content = document.createElement('input');
            content.setAttribute('type','hidden');
            content.setAttribute('name','user_type');
            content.value = id;
            document.getElementById('sendtoUser').append(content);

            for (let ic = 0; ic < checkedUser[id].length; ic++){
                let content = document.createElement('input');
                content.setAttribute('type','hidden');
                content.setAttribute('name','user[]');
                content.value = checkedUser[id][ic];
                document.getElementById('sendtoUser').append(content);
            }
        }else {
            swal({
                {{--title: "{{__('No user has been selected')}}",--}}
                text: "{{__('No user has been selected')}}",
                icon: "warning",
                buttons: {
                    cancel: true,
                    confirm: false,
                },
            })
        }
    }
</script>
