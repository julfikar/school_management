@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.students.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header d-block">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body">
                        <select class="form-control" id="selectUserType">
                            <option value="{{ null }}">{{ __('Select one') }}</option>
                            @foreach($data as $key => $row)
                                @if($key != 'classRooms')
                                <option class="text-capitalize" value="{{ $key }}">{{ __($key) }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark" id="allUsers">
                    <div class="card-body d-none" id="students_content">
                        <div class="table-responsive style-scroll">

                            <div class="row my-5">
                                <div class="col-10 mx-auto">
                                    <label for="selectByClass" class="card-title">{{ __('See by class') }}</label>
                                    <select class="form-control" id="selectByClass">
                                        <option value="{{ null }}">{{ __('Select one') }}</option>
                                        @foreach($data->classRooms as $row)
                                            <option value="{{ $row->name }}">{{ __($row->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row stubentViewContent">
                                <div class="col-12">
                                    <table class="bdcoder stubentView table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>{{__('Class Roll')}}</th>
                                            <th>{{__('Name')}}</th>
                                            <th>{{__('Phone')}}</th>
                                            <th>{{__('ID')}}</th>
                                            <th>{{__('Class Name')}}</th>
                                            <th>{{__('Option')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data->students as $key => $student)
                                            <tr>
                                                <th>{{ $student->class_rol }}</th>
                                                <th>{{__($student->user->name)}}</th>
                                                <td>{{__($student->user->phone)}}</td>
                                                <td>{{__($student->unique_id)}}</td>
                                                <td>{{__($student->classRoom?$student->classRoom->name:'')}}</td>
                                                <td>
                                                    <a href="{{ 'tel:'.$student->user->phone }}" class="btn btn-info  btn-circle">
                                                        <i class="material-icons">call</i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            @foreach($data->classRooms as $row)
                                <div class="row stubentViewContent d-none" id="{{ $row->name.'_content' }}">
                                    <div class="col-12">
                                        <table class="bdcoder table table-striped table-bordered miw-500" width="100%">
                                            <thead>
                                            <tr>
                                                <th>{{__('Class Roll')}}</th>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Phone')}}</th>
                                                <th>{{__('ID')}}</th>
                                                <th>{{__('Class Name')}}</th>
                                                <th>{{__('Option')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($row->student as $key => $student)
                                                <tr>
                                                    <th>{{ $student->class_rol }}</th>
                                                    <th>{{__($student->user->name)}}</th>
                                                    <td>{{__($student->user->phone)}}</td>
                                                    <td>{{__($student->unique_id)}}</td>
                                                    <td>{{__($student->classRoom?$student->classRoom->name:'')}}</td>
                                                    <td>
                                                        <a href="{{ 'tel:'.$student->user->phone }}" class="btn btn-info  btn-circle">
                                                            <i class="material-icons">call</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-body d-none" id="teachers_content">
                        <div class="table-responsive style-scroll">

                            <table class="bdcoder table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Expart In')}}</th>
                                    <th>{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->teachers as $key => $teacher)
                                    <tr>
                                        <td>{{__($teacher->unique_id)}}</td>
                                        <th>{{__($teacher->user->name)}}</th>
                                        <td>{{__($teacher->user->phone)}}</td>
                                        <td>{{__($teacher->expert_in)}}</td>
                                        <td>
                                            <a href="{{ 'tel:'.$teacher->user->phone }}" class="btn btn-info  btn-circle">
                                                <i class="material-icons">call</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-body d-none" id="employees_content">
                        <div class="table-responsive style-scroll">

                            <table class="bdcoder table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Posting')}}</th>
                                    <th>{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->employees as $key => $employee)
                                    <tr>
                                        <td>{{__($employee->unique_id)}}</td>
                                        <th>{{__($employee->user->name)}}</th>
                                        <td>{{__($employee->user->phone)}}</td>
                                        <td>{{__($employee->designation->name)}}</td>
                                        <td>
                                            <a href="{{ 'tel:'.$employee->user->phone }}" class="btn btn-info  btn-circle">
                                                <i class="material-icons">call</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.communicatino.internal-assets.phone-call-js')
@endsection
