@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.students.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header d-block">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body">
                        <select class="form-control" id="selectUserType">
                            <option value="{{ null }}">{{ __('Select One') }}</option>
                            @foreach($data as $key => $row)
                                @if($key != 'classRooms')
                                    <option class="text-capitalize" value="{{ $key }}">{{ $key }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark" id="allUsers">
                    <div class="card-body d-none" id="students_content">
                        <div class="table-responsive style-scroll">

                            <div class="row my-5">
                                <div class="col-10 mx-auto">
                                    <label for="selectByClass" class="card-title">{{ __('See by class') }}</label>
                                    <select class="form-control" id="selectByClass">
                                        <option value="{{ null }}">{{ __('Select one') }}</option>
                                        @foreach($data->classRooms as $row)
                                            <option value="{{ $row->name }}">{{ __($row->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row stubentViewContent">
                                <div class="col-12">
                                    <div class="row my-3">
                                        <div class="col-11 mx-auto">
                                            <input type="checkbox" class="studentAllCheckBox" name="all_student" id="checkAllStudent" value="all_student">
                                            <label for="checkAllTeacher">All Studebnt</label>
                                        </div>
                                    </div>
                                    <table class="bdcoder stubentView table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>{{__('Class Roll')}}</th>
                                            <th>{{__('Name')}}</th>
                                            <th>{{__('Phone')}}</th>
                                            <th>{{__('ID')}}</th>
                                            <th>{{__('Class Name')}}</th>
                                            <th>{{__('Option')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data->students as $key => $student)
                                            <tr>
                                                <td><input type="checkbox" name="student" class="studentCheckBox" value="{{ $student->id }}"></td>
                                                <th>{{ $student->class_rol }}</th>
                                                <th>{{__($student->user->name)}}</th>
                                                <td>{{__($student->user->phone)}}</td>
                                                <td>{{__($student->unique_id)}}</td>
                                                <td>{{__($student->classRoom?$student->classRoom->name:'')}}</td>
                                                <td>
                                                    <a href="javascript:void(0)" class="btn btn-info  btn-circle smsBtn">
                                                        <i class="material-icons">email</i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            @foreach($data->classRooms as $row)
                                <div class="row stubentViewContent d-none" id="{{ str_replace(' ','',$row->name).'_content' }}">
                                    <div class="col-12">
                                        <div class="row my-3">
                                            <div class="col-11 mx-auto">
                                                <input type="checkbox" name="all_{{ str_replace(' ','',$row->name) }}_student" class="studentAllCheckBox" id="checkAll{{ str_replace(' ','',$row->name) }}Student" value="all_{{ str_replace(' ','',$row->name) }}_student">
                                                <label for="checkAll{{ str_replace(' ','',$row->name) }}Student">{{ __('All Student from ').__($row->name) }}</label>
                                            </div>
                                        </div>
                                        <table class="bdcoder table table-striped table-bordered miw-500" width="100%">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>{{__('Class Roll')}}</th>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Phone')}}</th>
                                                <th>{{__('ID')}}</th>
                                                <th>{{__('Class Name')}}</th>
                                                <th>{{__('Option')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($row->student as $key => $student)
                                                <tr>
                                                    <th>
                                                        <input type="checkbox" name="student" class="studentCheckBox" value="{{ $student->id }}">
                                                    </th>
                                                    <th>{{ $student->class_rol }}</th>
                                                    <th>{{__($student->user->name)}}</th>
                                                    <td>{{__($student->user->phone)}}</td>
                                                    <td>{{__($student->unique_id)}}</td>
                                                    <td>{{__($student->classRoom?$student->classRoom->name:'')}}</td>
                                                    <td>
                                                        <a href="javascript:void(0)" class="btn btn-info  btn-circle smsBtn">
                                                            <i class="material-icons">email</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="card-body d-none" id="teachers_content">

                        <div class="row my-3">
                            <div class="col-11 mx-auto">
                                <input type="checkbox" name="all_teacher" id="checkAllTeacher" value="allTeacher">
                                <label for="checkAllTeacher">All Teacher</label>
                            </div>
                        </div>
                        <div class="table-responsive style-scroll">

                            <table class="bdcoder table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Expart In')}}</th>
                                    <th>{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->teachers as $key => $teacher)
                                    <tr>
                                        <td><input type="checkbox" name="teacher" class="teacherCheckBox" value="{{ $teacher->id }}"></td>
                                        <td>{{__($teacher->unique_id)}}</td>
                                        <th>{{__($teacher->user->name)}}</th>
                                        <td>{{__($teacher->user->phone)}}</td>
                                        <td>{{__($teacher->expert_in)}}</td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-info  btn-circle smsBtn">
                                                <i class="material-icons">email</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-body d-none" id="employees_content">
                        <div class="table-responsive style-scroll">
                            <div class="row my-3">
                                <div class="col-11 mx-auto">
                                    <input type="checkbox" name="all_employee" id="checkAllEmployee" value="allEmployee">
                                    <label for="checkAllEmployee">All Employee</label>
                                </div>
                            </div>
                            <table class="bdcoder table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Posting')}}</th>
                                    <th>{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data->employees as $key => $employee)
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="employee" class="employeeCheckBox" value="{{ $employee->id }}">
                                        </td>
                                        <td>{{__($employee->unique_id)}}</td>
                                        <th>{{__($employee->user->name)}}</th>
                                        <td>{{__($employee->user->phone)}}</td>
                                        <td>{{__($employee->designation->name)}}</td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-info  btn-circle smsBtn">
                                                <i class="material-icons">email</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    email Modal--}}
    <div class="modal fade bg-dark" id="smsModal" tabindex="-1" role="dialog" aria-labelledby="smsModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="smsModalTitle">{{ __('Type your massage') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.communication.email.sent') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group" id="sendtoUser">
                            <input type="hidden" name="user[]" id="sendUserToMassage">
                        </div>

                        <div class="form-group">
                            <label for="textMessage"><span class="card-title">{{ __('Subject') }}</span></label>
                            <input type="text" name="subject" id="messageSubject" class="form-control" required placeholder="{{ __('Write your message subject') }}">
                        </div>
                        <div class="form-group">
                            <label for="textMessage"><span class="card-title">{{ __('Write your message here') }}</span></label>
                            <textarea name="massage" id="textMessage" class="form-control" rows="10"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn btn-danger rounded">{{ __('Send message') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.communicatino.internal-assets.phone-call-js')
    @include('backend.pages.communicatino.internal-assets.sms-js')
@endsection
