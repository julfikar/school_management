
<script>
    var drivePath = window.location.pathname.replace('/','');
    console.log(drivePath);

    $('.galleryFolderActivationBtn').on('click', function () {
        var selected = $(this).attr('id');
        $.ajax({
            type:'get',
            url: '/'+drivePath + '/activation/'+selected,
            success:function (data) {
                toastr.success("Gallery status has changed successfully.");

            }
        });
    });
</script>
