<script>

    let transportation_fees = '<?php echo json_encode($transportation_fees) ?>';
    var transportation_fee = JSON.parse(transportation_fees);

$(function () {
    var cal_due_amount = null;
    var payable_transportation_amount = parseInt(transportation_fee.transportation_fee);

    $("#payed_amount").on('keyup', function() {
        let new_payed_amount = parseInt(this.value);

        if((payable_transportation_amount >= new_payed_amount) && (new_payed_amount > 0)){
            var due_amount = payable_transportation_amount - new_payed_amount;

            $("#due_amount").val(due_amount);
            cal_due_amount = due_amount;

            $("#discount_amount").val(null);
        }else{
            $("#payed_amount").val('');
            toastr.error('Please enter right amount.');
        }

    });


    $("#discount_amount").on('keyup', function() {
        let discount_amount = this.value;

        // if given discount price
        if(discount_amount!=null ) {
            let new_due_amount = cal_due_amount - discount_amount;

            if (new_due_amount >= 0) {
                $("#due_amount").val(new_due_amount);
            } else {
                $("#discount_amount").val('');
                toastr.error('Please give correct value in discount price.');
            }
        }
    });
});



</script>
