@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
    @include('backend.pages.settings.page-settings.internal-assets.css.page-css')
    <link rel="stylesheet" href="{{asset('backend/assets/plugin/tag-input/tagsinput.css')}}"/>
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>

                    <span class="breadcrumb-item active text-capitalize">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-sm-12 order-1">
                            <form action="{{ route('admin.transportation-fee.store') }}" method="POST" class="">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class=" col-sm-12 order-1">
                                    <div class="card-body bg-dark">

                                        <p class="mb-1 text-uppercase"><label for="passenger_id">{{__('Passenger Name')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup></p>
                                        <div class="input-group input-group-lg mb-3 text-center">
                                            <select name="passenger_id" id="passenger_id" class="form-control selectpicker" data-live-search="true" data-size="3" tabindex="-98" required>
                                                <option selected disabled value="{{ null }}">{{__('Select one')}}</option>
                                                @foreach ($passengers as $passenger)
                                                    <option value="{{ $passenger->id }}" >{{ $passenger->user->name }}</option>
                                                @endforeach

                                            </select>
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="postStatus">{{__('month')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup></p>
                                        <div class="input-group input-group-lg mb-3 text-center">
                                            <select name="month" id="postStatus" class="form-control selectpicker"  data-live-search="true" data-size="5" tabindex="-98" required>
                                                <option selected disabled value="{{ null }}">{{__('Select one')}}</option>
                                                <option value="1">{{ 'January' }}</option>
                                                <option value="2">{{ 'February' }}</option>
                                                <option value="3">{{ 'March' }}</option>
                                                <option value="4">{{ 'April' }}</option>
                                                <option value="5">{{ 'May' }}</option>
                                                <option value="6">{{ 'June' }}</option>
                                                <option value="7">{{ 'July' }}</option>
                                                <option value="8">{{ 'August' }}</option>
                                                <option value="9">{{ 'September' }}</option>
                                                <option value="10">{{ 'October' }}</option>
                                                <option value="11">{{ 'November' }}</option>
                                                <option value="12">{{ 'December' }}</option>

                                            </select>
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="payed_amount">{{__('Payed Amount')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup><br><code>{{ __('Please don\'t use Bangla as your input.') }}</code></p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="payed_amount" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm"  id="payed_amount"
                                                   placeholder="{{__('Payed Amount')}}"  value="{{old('payed_amount')}}" required>
                                            <br>
                                            @if ($errors->has('payed_amount'))
                                                <span class="text-danger">{{ $errors->first('payed_amount') }}</span>
                                            @endif
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="discount_amou">{{__('Discount')}}</label>: <br><code>{{ __('Please don\'t use Bangla as your input.') }}</code></p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="discount" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="discount_amount"
                                                   placeholder="{{__('Discount Amount')}}" value="{{old('discount')}}">
                                            <br>
                                            @if ($errors->has('discount'))
                                                <span class="text-danger">{{ $errors->first('discount') }}</span>
                                            @endif
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="due_amount">{{__('Due')}}</label>: <br><code>{{ __('Please don\'t use Bangla as your input.') }}</code></p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="due" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="due_amount"
                                                   value="{{ null }}" readonly>
                                            <br>
                                            @if ($errors->has('due'))
                                                <span class="text-danger">{{ $errors->first('due') }}</span>
                                            @endif
                                        </div>

                                        <input type="hidden" value="1000" id="main_transportation_fee">

                                    </div>
                                </div>

                            </div>

                            <div class="wizard-action card-body py-0 text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg px-4"
                                        type="submit">{{__('Submit')}}</button>
                            </div>

                        </div>

                    </form>
                        </div>
                         <div class="col-md-4 col-sm-12 order-2">
                            <div class="card-body bg-dark ">
                                <table class="table border border-2 border-info p-3">
                                    <thead>
                                    <tr>
                                        <th scope="col">{{ __('Name') }}</th>
                                        <th scope="col">{{ __('BDT') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transportation_fees as $transportation_fee => $value)
                                        <tr>
                                            <td class="text-capitalize">{{ __(str_replace('_', ' ', str_replace('_fee', '',$transportation_fee))) }}</td>
                                            <td>{{ __($value) }}/-</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
    @include('backend.pages.accounts.transportation-fee.internal-assets.js.transportation_fee_script')
@endsection
