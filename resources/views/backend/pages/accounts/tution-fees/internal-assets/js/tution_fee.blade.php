<script>

    let tution_fees = '<?php echo json_encode($tution_fees) ?>';
    var tutions = JSON.parse(tution_fees);

    const classInput = document.getElementById('class_id');

    classInput.addEventListener('change', function () {
        const classId = this.value;
        var url = "{{route('admin.tution-fee.class-student', '')}}"+"/"+classId;
        let myFetch = fetch(url);

        myFetch.then(res => res.json())
            .then(data => {
                // console.log(data);
                let studentInput = document.getElementById('getStudents');
                if (data.length > 0) {
                    $('#getStudents2').addClass('d-none');
                    studentInput.classList.remove('d-none');
                }else {
                    $('#getStudents2').removeClass('d-none');
                    studentInput.classList.add('d-none');
                }

            }).catch(err => {
            // console.log(err);
            toastr.error(err);
        });

    });



    $(function () {
        $("#payed_amount").on('keyup',function() {
            let new_payed_amount = this.value;
            var class_id = $("#class_id").val();

            if(class_id!=null){
                var payable_class_amount = getClassAmount(parseInt(class_id));

                if(payable_class_amount >= new_payed_amount){
                    var due_amount = payable_class_amount - new_payed_amount;

                    $("#due_amount").val(due_amount);
                    $("#cal_due_amount").val(due_amount);
                }else{
                    $("#payed_amount").val('');
                    toastr.error('Please give correct amount.');
                }

            }else{
                $("#payed_amount").val('');
                toastr.error('Please select class first.');
            }

        });


        $("#discount_amount").on('keyup',function() {
            let discount_amount = this.value;

            // if given discount price

            if(discount_amount!=null ) {
                let due_amount = $("#cal_due_amount").val();
                let new_due_amount = due_amount - discount_amount;

                if (new_due_amount >= 0) {
                    $("#due_amount").val(new_due_amount);
                } else {
                    $("#discount_amount").val('');
                    toastr.error('Please give correct value in discount price.');
                }
            }
        });
    });



    function getClassAmount(id) {
        var classAmount;
        switch (id) {
            case 1:
                classAmount = parseInt(tutions.pre_primary_fee);
                break;
            case 2:
                classAmount = parseInt(tutions.class_one_fee);
                break;
            case 3:
                classAmount = parseInt(tutions.class_two_fee);
                break;
            case 4:
                classAmount = parseInt(tutions.class_three_fee);
                break;
            case 5:
                classAmount = parseInt(tutions.class_four_fee);
                break;
            case 6:
                classAmount = parseInt(tutions.class_five_fee);
               break;
        }
        return classAmount;
    }


</script>
