@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>

                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-8">
                            <h6 class="card-title">
                                {{ $employeeUser->name }}
                                &nbsp;
                                @foreach($roles as $role)
                                    @if($userType->user->hasRole($role->name))
                                        @if($role->name == 'employee')
                                            <span class="badge badge-success font-weight-bold rounded text-capitalize m-1">{{ __($userType->designation->name) }}</span>
                                        @else
                                            <span class="badge badge-success font-weight-bold rounded text-capitalize m-1">{{ __($role->name) }}</span>
                                        @endif
                                    @endif
                                @endforeach
                                ({{ __($title) }})
                            </h6>
                        </div>
                        <div class="col-4 text-right">
                            <a href="javascript:history.go(-2)">
                                <button type="button" class="btn btn-danger btn-sm rounded" data-toggle="tooltip" data-placement="top" title="{{ __('Go Back') }}"><i class="material-icons">reply</i></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
           <div class="col-12">
               <div class="card card-dark bg-dark col-md-6 d-inline-block">
                   <div class="card-body bg-dark">
                       <form action="{{ route('admin.salary.store',$employeeUser->id) }}" method="post">
                           @csrf
                           <div class="form-group">
                               <label for="basic" class="card-title">{{ __('Basic Salary') }}</label>
                               <input type="number" name="basic" id="basic" step="0.01" class="form-control" required value="{{ $salary?$salary->basic:'' }}" placeholder="{{ __('EXM: 8000') }}">
                           </div>
                           <div class="form-group">
                               <label for="increment" class="card-title">{{ __('Increment') }}</label>
                               <input type="number" name="increment" id="increment" step="0.01" class="form-control" value="{{ $salary?$salary->increment:'' }}" placeholder="{{ __('EXM: 500') }}">
                           </div>
                           <div class="form-group">
                               <label for="accommodation" class="card-title">{{ __('Accommodation') }}</label>
                               <input type="number" name="accommodation" id="accommodation" step="0.01" class="form-control" value="{{ $salary?$salary->accommodation:'' }}" placeholder="{{ __('EXM: 2500') }}">
                           </div>
                           <div class="form-group">
                               <label for="hospital_fee" class="card-title">{{ __('hospital fee') }}</label>
                               <input type="number" name="hospital_fee" id="hospital_fee" step="0.01" class="form-control" value="{{ $salary?$salary->hospital_fee:'' }}" placeholder="{{ __('EXM: 500') }}">
                           </div>
                           <div class="form-group">
                               <label for="transportation_fee" class="card-title">{{ __('transportation fee') }}</label>
                               <input type="number" name="transportation_fee" id="transportation_fee" step="0.01" class="form-control" value="{{ $salary?$salary->transportation_fee:'' }}" placeholder="{{ __('EXM: 500') }}">
                           </div>
                           <div class="card-footer">
                               <button type="submit" class="btn btn-danger w-50">{{ __('Save') }}</button>
                           </div>
                       </form>
                   </div>
               </div>

               <div class="card card-dark bg-dark col-md-5 float-right d-inline-block">
                   <div class="card-header">
                       <div class="col-6">
                           {{ __('Declared Date') }}: {{ $salary? date('d-M-y', $salary->date):'' }}
                       </div>
                       <div class="col-6">
                           {{ __('Declared By') }}: {{ $salary?$salary->user->name:'' }}
                       </div>
                   </div>
                   <div class="card-body">
                       <table class="table table-bordered table-hover">
                           <tbody>
                           <tr>
                               <th width="60%" class="text-capitalize">{{ __('Basic') }}</th>
                               <td width="40%" class="text-right">{{ $salary?$salary->basic.'/-':'' }}</td>
                           </tr>
                           <tr>
                               <th>{{ __('Increment') }}</th>
                               <td class="text-right">{{ $salary?$salary->increment.'/-':'' }}</td>
                           </tr>
                           <tr>
                               <th class="text-capitalize">{{ __('Accommodation') }}</th>
                               <td class="text-right">{{ $salary?$salary->accommodation.'/-':'' }}</td>
                           </tr>
                           <tr>
                               <th class="text-capitalize">{{ __('hospital fee') }}</th>
                               <td class="text-right">{{ $salary?$salary->hospital_fee.'/-':'' }}</td>
                           </tr>
                           <tr>
                               <th class="text-capitalize">{{ __('transportation fee') }}</th>
                               <td class="text-right">{{ $salary?$salary->transportation_fee.'/-':'' }}</td>
                           </tr>
                           <tr>
                               <th class="text-capitalize">{{ __('Total') }}</th>
                               <th class="text-right">{{ $salary?$salary->amount.'/-':'' }}</th>
                           </tr>
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
        </div>
    </div>
@endsection

@section('page-script')
    {{--    @include('backend.pages.accounts.tution-fees.internal-assets.js.tution_fee')--}}
@endsection
