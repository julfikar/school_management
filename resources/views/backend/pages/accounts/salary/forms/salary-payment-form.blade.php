<div class="col-md-8 mx-auto">
    <form action="{{ route('admin.salary.pay') }}" method="post">
        @csrf

        <input type="hidden" name="previous_due" id="previousDue" value="{{ $balance?($balance->balance > 0?$balance->balance:0.00):0.00 }}">
        <input type="hidden" name="previous_advance" id="previousAdvance" value="{{ $balance?($balance->balance < 0?abs($balance->balance):0.00):0.00 }}">
        <input type="hidden" name="total_salary" id="totalSalary" value="{{ ($basicSalary + ($balance?$balance->balance:0.00) - $payedAmount) > 0 ? $basicSalary + ($balance?$balance->balance:0.00) - $payedAmount:0 }}">
        <input type="hidden" name="employee_type" value="{{ $employee->user->teacher?'Teacher':'Employee' }}">
        <input type="hidden" name="employee_id" value="{{ $employee->id }}">

        <div class="form-row">
            <div class="col-12">
                <div class="form-group">
                    <label for="paymentDate">{{ __('Payment Date') }}</label>
                    <input type="date" name="date" id="paymentDate" required class="form-control" max="{{ date('Y-m-d', time()) }}" value="{{ date('Y-m-d', time()) }}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="paymentAmount">{{ __('Payment Amount') }} <br><code>{{ __('Please don\'t use Bangla as your input.') }}</code></label>
                    <input type="number" step="0.01" name="amount" id="paymentAmount" required class="form-control" placeholder="{{ __('EXM: 8000') }}">
                </div>
            </div>

            <div class="col-12" id="show">

            </div>

            <div class="col-12">
                <button type="submit" class="btn btn-danger w-100">{{ __('Save') }}</button>
            </div>
        </div>
    </form>
</div>
