@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.students.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white"
                       href="{{ route('admin.salary.pay') }}">{{__('Salary')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header d-block">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h6 class="card-title">{{__($title)}}</h6>
                            </div>
                        </div>

                    </div>
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="5%">{{__('SL No.')}}</th>
                                    <th width="20%">{{__('ID')}}</th>
                                    <th width="15%">{{__('Name')}}</th>
                                    <th width="15%">{{__('Phone')}}</th>
                                    <th width="10%">{{__('Position')}}</th>
                                    <th width="10%">{{__('Now Salary')}}</th>
                                    <th width="10%">{{__('Salary Last Update')}}</th>
                                    <th width="10%">{{__('Salary Last Update By')}}</th>
                                    <th width="5%">{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $row)
                                    <tr>
                                        <th>{{ $key+1 }}</th>
                                        <td>{{ $row->unique_id }}</td>
                                        <td>{{ $row->user->name }}</td>
                                        <td>{{ $row->user->phone }}</td>
                                        <td>
                                            @foreach($roles as $role)
                                                @if($row->user->hasRole($role->name))
                                                    @if($role->name == 'employee')
                                                        <span class="badge badge-success font-weight-bold rounded text-capitalize m-1">{{ __($row->designation->name) }}</span>
                                                    @else
                                                        <span class="badge badge-success font-weight-bold rounded text-capitalize m-1">{{ __($role->name) }}</span>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </td>
                                        <td class="text-right">{{ $row->salary()->orderBy('id', 'DESC')->first()?$row->salary()->orderBy('id', 'DESC')->first()->amount:'0.00/-' }}</td>
                                        <td>{{ $row->salary()->orderBy('id', 'DESC')->first()? date('d-M-y', strtotime($row->salary()->orderBy('id', 'DESC')->first()->created_at)):'' }}</td>
                                        <td>{{ $row->salary()->orderBy('id', 'DESC')->first()? $row->salary()->orderBy('id', 'DESC')->first()->user->name:'' }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.salary.create', $row->user->id) }}">
                                                <button type="button" class="btn btn-success btn-sm rounded"><i class="fas fa-folder-open"></i></button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@endsection
