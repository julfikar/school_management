<script>
    const totalPayeAmmount = $('#totalSalary').val();
    let dueAmount = 0;
    let restAmount = 0;
    $('#paymentAmount').on('keyup', function (){
        dueAmount = totalPayeAmmount - parseFloat($(this).val());
        // console.log(dueAmount);
        if (dueAmount > 0){
            showOtherDueFunction(dueAmount);
        }else if (dueAmount < 0){
            showOtherFunction(Math.abs(dueAmount))
        }else {
            $('#show').empty();
        }
    });

    // function showDueAmount(amount){
    //     $('#show').empty().append(
    //         '<div class="form-group">\n'+
    //             '<label for="due">Due</label>\n'+
    //             '<input type="number" step="0.01" name="due" id="due" class="form-control" readonly value="'+amount+'">\n'+
    //         '</div>'
    //     )
    // }

    function showOtherDueFunction(amount){
        $('#show').empty().append(
            '<h5 class="text-center">Balance is <span id="restBlance">'+amount+'</span></h5>\n'+
        '<div class="form-group" id="showDue">\n'+
            '<input type="checkbox" name="due" id="due">\n'+
            '<label for="due">Rest Amount is as due</label>\n'+
        '</div>\n'+
       '<div class="form-group" id="showCharge">\n'+
            '<input type="checkbox" name="charge" id="charge">\n'+
            '<label for="charge">Rest Amount is as charge</label>\n'+
        '</div>'
        );

        $('#due').on('change', function (){
            if ($(this).prop("checked")){
                if ($('#restBlance').html() > 0){
                    createDueInput()
                }else {
                    $(this).removeAttr('checked');
                }
            }else{
                $(this).parent().find('#dueAmount').remove();
            }
        });

        $('#charge').on('change', function (){
            if ($(this).prop("checked")){
                if ($('#restBlance').html() > 0) {
                    createChargeInput()
                }else {
                    $(this).removeAttr('checked');
                }
            }else{
                $(this).parent().find('#chargeAmount').remove();
            }
        });
    }

    function showOtherFunction(amount){
        $('#show').empty().append(
            '<h5 class="text-center">Balance is <span id="restBlance">'+amount+'</span></h5>\n'+
        '<div class="form-group" id="showGift">\n'+
            '<input type="checkbox" name="gift" id="gift">\n'+
            '<label for="gift">Rest Amount is as gift</label>\n'+
        '</div>\n'+
       '<div class="form-group" id="showAdvance">\n'+
            '<input type="checkbox" name="advance" id="advance">\n'+
            '<label for="advance">Rest Amount is as Advance</label>\n'+
        '</div>\n'+
        '<div class="form-group" id="showBonus">\n'+
            '<input type="checkbox" name="bonus" id="bonus">\n'+
            '<label for="bonus">Rest Amount is as Bonus</label>\n'+
        '</div>'
        );

        $('#gift').on('change', function (){
            if ($(this).prop("checked")){
                if ($('#restBlance').html() > 0){
                    createGiftInput()
                }else {
                    $(this).removeAttr('checked');
                }
            }else{
                $(this).parent().find('#giftAmount').remove();
            }
        });

        $('#advance').on('change', function (){
            if ($(this).prop("checked")){
                if ($('#restBlance').html() > 0) {
                    createAdvanceInput()
                }else {
                    $(this).removeAttr('checked');
                }
            }else{
                $(this).parent().find('#advanceAmount').remove();
            }
        });

        $('#bonus').on('change', function (){
            if ($(this).prop("checked")){
                if ($('#restBlance').html() > 0) {
                    createBonusInput()
                }else {
                    $(this).removeAttr('checked');
                }
            }else{
                $(this).parent().find('#bonusAmount').remove();
            }
        });
    }

    function createGiftInput(){
        let element = document.createElement('input')
        element.setAttribute('type','number');
        element.setAttribute('name','gift_amount');
        element.setAttribute('step','0.01');
        element.setAttribute('required','required');
        element.setAttribute('id','giftAmount');
        element.classList.add('form-control');
        document.getElementById('showGift').appendChild(element);
        restAmount = $('#restBlance').html();
        $('#giftAmount').focus().on('keyup', function (){
            let amount = restAmount - $(this).val();
            if (amount >= 0){
                // rest(amount);
                $('#restBlance').html(amount);
            }else{
                $(this).val(Math.abs($('#restBlance').html() - restAmount));
            }
        });
    }

    function createAdvanceInput(){
        let element = document.createElement('input')
        element.setAttribute('type','number');
        element.setAttribute('name','advance_amount');
        element.setAttribute('step','0.01');
        element.setAttribute('required','required');
        element.setAttribute('id','advanceAmount');
        element.classList.add('form-control');
        document.getElementById('showAdvance').appendChild(element);
        restAmount = $('#restBlance').html();
        $('#advanceAmount').focus().on('keyup', function (){
            let amount = restAmount - $(this).val();
            if (amount >= 0){
                // rest(amount);
                $('#restBlance').html(amount);
            }else{
                $(this).val(Math.abs($('#restBlance').html() - restAmount));
            }
        });
    }

    function createBonusInput(){
        let element = document.createElement('input')
        element.setAttribute('type','number');
        element.setAttribute('name','bonus_amount');
        element.setAttribute('step','0.01');
        element.setAttribute('required','required');
        element.setAttribute('id','bonusAmount');
        element.classList.add('form-control');
        document.getElementById('showBonus').appendChild(element);
        restAmount = $('#restBlance').html();
        $('#bonusAmount').focus().on('keyup', function (){
            let amount = restAmount - $(this).val();
            if (amount >= 0){
                // rest(amount);
                $('#restBlance').html(amount);
            }else{
                $(this).val(Math.abs($('#restBlance').html() - restAmount));
            }
        });
    }

    function createDueInput(){
        let element = document.createElement('input')
        element.setAttribute('type','number');
        element.setAttribute('name','due_amount');
        element.setAttribute('step','0.01');
        element.setAttribute('required','required');
        element.setAttribute('id','dueAmount');
        element.classList.add('form-control');
        document.getElementById('showDue').appendChild(element);
        restAmount = $('#restBlance').html();
        $('#dueAmount').focus().on('keyup', function (){
            let amount = restAmount - $(this).val();
            if (amount >= 0){
                // rest(amount);
                $('#restBlance').html(amount);
            }else{
                $(this).val(Math.abs($('#restBlance').html() - restAmount));
            }
        });
    }

    function createChargeInput(){
        let element = document.createElement('input')
        element.setAttribute('type','number');
        element.setAttribute('name','charge_amount');
        element.setAttribute('step','0.01');
        element.setAttribute('required','required');
        element.setAttribute('id','chargeAmount');
        element.classList.add('form-control');
        document.getElementById('showCharge').appendChild(element);
        restAmount = $('#restBlance').html();
        $('#chargeAmount').focus().on('keyup', function (){
            let amount = restAmount - $(this).val();
            if (amount >= 0){
                // rest(amount);
                $('#restBlance').html(amount);
            }else{
                $(this).val(Math.abs($('#restBlance').html() - restAmount));
            }
        });
    }

    function rest(amount){
        restAmount = amount;
    }
</script>
