@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>

                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-6"><h6 class="card-title">{{__($title)}}</h6></div>
                        <div class="col-6 text-right">
                            <a href="{{ route('admin.salary.index') }}">
                                <button type="button" class="btn btn-success text-capitalize rounded">
                                    {{__('Salary register')}}
                                </button>
                            </a>
                            <a href="{{ route('admin.salary.pay') }}">
                                <button type="button" class="btn btn-danger btn-sm rounded" data-toggle="tooltip" data-placement="top" title="{{ __('Go Back') }}"><i class="material-icons">reply</i></button>
                            </a>
                        </div>

                    </div>


                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8 col-sm-12 order-1">
                                <div class="card-body bg-dark">
                                    @if(!$employee)
                                        <form action="{{ route('admin.salary.pay') }}" method="get">
                                           <div class="form-row">
                                               <div class="col-md-10">
                                                   <div class="form-group">
                                                       <label for="search">{{ __('Employee ID') }}:</label>
                                                       <input type="text" name="employee" id="search" class="form-control" placeholder="{{ __('Type an employee id') }}">
                                                   </div>
                                               </div>
                                               <div class="col-md-2">
                                                   <div class="form-group">
                                                       <button type="submit" class="btn btn-danger w-100 mt-4"><span class="material-icons">search</span></button>
                                                   </div>
                                               </div>
                                           </div>

                                        </form>
                                    @else
                                        @include('backend.pages.accounts.salary.forms.salary-payment-form')
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 order-2">
                                <div class="card-body bg-dark text-center">
                                    @if(!$employee)
                                        <img class="card-img-top rounded-circle" src="{{ asset('backend/assets/img/profile/male.jpg')  }}" style="width: 80px" alt="Card image cap">
                                    @else
                                        @if(!$employee->user->profile_photo_path)
                                            <img class="card-img-top rounded-circle" src="{{ $employee->user->gender == 'male' ? asset('backend/assets/img/profile/male.jpg'):($employee->user->gender == 'female' ? asset('backend/assets/img/profile/female.jpg'):asset('backend/assets/img/profile/other.png'))  }}" style="width: 80px" alt="Card image cap">
                                        @else
                                            <img class="card-img-top rounded-circle" src="{{ asset('storage/'.$teacher->user->profile_photo_path)  }}" style="width: 80px" alt="Card image cap">
                                        @endif
                                    @endif
                                    <h5 class="mt-2">{{ $employee?$employee->user->name:__('No Employee selected') }}</h5>
                                    @if($employee)
                                            @foreach($roles as $role)
                                                @if($employee->user->hasRole($role->name))
                                                    @if($role->name == 'employee')
                                                        <span class="badge badge-success font-weight-bold rounded text-capitalize m-1">{{ __($employee->designation->name) }}</span>
                                                    @else
                                                        <span class="badge badge-success font-weight-bold rounded text-capitalize m-1">{{ __($role->name) }}</span>
                                                    @endif
                                                @endif

                                            @endforeach
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>{{ __('Name') }}</th>
                                                <th>{{ __('BDT') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th class="text-left">{{ __('Salary') }}:</th>
                                                <td class="text-right">{{ $basicSalary }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Previous Due') }}:</th>
                                                <td class="text-right">(+) {{ $balance?($balance->balance > 0?$balance->balance:0.00):0.00 }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('This month gift') }}:</th>
                                                <td class="text-right">(+) {{ $thisGift }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('This month bonus') }}:</th>
                                                <td class="text-right">(+) {{ $thisBonus }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Previous Advance') }}:</th>
                                                <td class="text-right">(-) {{ $balance?($balance->balance < 0?abs($balance->balance):0.00):0.00 }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Already Paid in this month') }}:</th>
                                                <td class="text-right">(-) {{ $payedAmount + $thisBonus + $thisGift - $thisCharge }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Already charge as fee in this month') }}:</th>
                                                <td class="text-right">(-) {{ $thisCharge }}</td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Total Salary'). (($basicSalary + ($balance?$balance->balance:0.00) - $payedAmount) > 0 ? ' ('.__('In Due').')':(($basicSalary + ($balance?$balance->balance:0.00) - $payedAmount) < 0 ? ' ('.__('In Advance').')':'')) }}:</th>
                                                <th class="text-right">{{ ($basicSalary + ($balance?$balance->balance:0.00) - $payedAmount) }}</th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @else
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>{{ __('Name') }}</th>
                                                <th>{{ __('BDT') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <th class="text-left">{{ __('Basic Salary') }}:</th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Previous Due') }}:</th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Previous Advance') }}:</th>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <th class="text-left">{{ __('Total Salary') }}:</th>
                                                <th>{{ __('0.00') }}/-</th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    @include('backend.pages.accounts.salary.insernal-css.payment-form')
@endsection
