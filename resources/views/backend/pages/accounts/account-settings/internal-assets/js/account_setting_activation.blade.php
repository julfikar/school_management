
<script>

    $('.accountSettingActivationBtn').on('change', function () {
        var selected = $(this).attr('id');
        if ($(this).is(':checked')){
            let url = "{{ route('admin.account-settings.get-form','') }}"+"/"+selected;
            $.ajax({
                type:'get',
                url: url,
                success:function (data) {
                    $('#feeInputModal .modal-content').empty().append(data);
                    $('#feeInputModal').modal('show').on('hidden.bs.modal', function (e) {
                        window.location.reload();
                    });
                }
            });
        }else {
            var url = "{{route('admin.account-settings.activation', '')}}" + "/" + selected;

            $.ajax({
                type: 'get',
                url: url,
                success: function (data) {
                    toastr.success("Account status has changed successfully.");
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            });
        }
    });

</script>
