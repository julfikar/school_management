
<script>

   /* $('.accountSettingActivationBtn').on('change', function () {
        var selected = $(this).attr('id');
        if ($(this).is(':checked')){
{{--            let url = "{{ route('admin.account-settings.get-form','') }}"+"/"+selected;--}}
            $.ajax({
                type:'get',
                url: url,
                success:function (data) {
                    $('#feeInputModal .modal-content').empty().append(data);
                    $('#feeInputModal').modal('show').on('hidden.bs.modal', function (e) {
                        window.location.reload();
                    });
                }
            });
        }else {
{{--            var url = "{{route('admin.account-settings.activation', '')}}" + "/" + selected;--}}

            $.ajax({
                type: 'get',
                url: url,
                success: function (data) {
                    toastr.success("Account status has changed successfully.");
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
            });
        }
    });*/


   const transaction_type = document.getElementById('transaction_type');

   transaction_type.addEventListener('change', function () {
       var transactionType = this.value;

      if(transactionType === 'Payable' || transactionType==='Receivable'){
          var url = "{{route('admin.cash-transactions.filter-by-type', '')}}"+"/"+transactionType;

          let myFetch = fetch(url);

          myFetch.then(res => res.json())
              .then(data => {
                  console.log(data);
                  const userId = document.getElementById('user_id');
                  // userId.innerHTML = '<option value="" selected disabled>Select a Subject</option>';
                  userId.innerHTML = '';
                  data.employees.forEach(employee => {
                      userId.innerHTML += `<option value="${employee.id}">${employee.user.name} (${employee.unique_id})</option>`
                  });

                  userId.removeAttribute('disabled');
                  userId.style.display = "block";
              }).catch(err => {
              console.log(err);
          })
      }else {
          const userId = document.getElementById('user_id');
          userId.style.display = "none";
      }
   });

</script>
