@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.wizards.sliders.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <form action="{{ route('admin.cash-transactions.store') }}" method="post">
                    @csrf
                    <table  class="table table-bordered bg-dark text-white" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th width="5%">{{__('Date')}}</th>
                            <th width="20%">{{__('Care Of Type')}}</th>
{{--                            <th width="20%">{{__('Care Of')}}</th>--}}
                            <th width="20%">{{__('Description')}}</th>
                            <th width="15%">{{__('Receive')}}</th>
                            <th width="15%"> {{__('Pay')}}</th>
                            <th width="15%"> {{__('Action')}}</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>
                                <input type="date" name="date" value="{{ date("Y-m-d") }}" class="form-control form-control-lg">
                            </td>
                            <td>
                                <select class="form-control form-control-lg" name="transaction_type" id="transaction_type">
                                    <option value="{{ null }}" selected disabled>{{__('Select one')}}</option>
                                    <option value="Income">{{ __('Income') }}</option>
                                    <option value="Expense">{{ __('Expense') }}</option>
                                </select>
                            </td>
{{--                            <td>--}}
{{--                                <select class="form-control form-control-lg" name="user_id" id="user_id" disabled>--}}
{{--                                    <option value="" selected disabled>{{__('Select payable or receivable')}}</option>--}}

{{--                                </select>--}}
{{--                            </td>--}}
                            <td>
                                <textarea name="input_description" id="" class="form-control"></textarea>
                            </td>
                            <td>
                                <input type="text" name="debit_amount" class="form-control form-control-lg">
                            </td>
                            <td>
                                <input type="text" name="credit_amount" class="form-control form-control-lg">
                            </td>
                            <td>
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Save')}}</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="col-12">
                <div class="card card-dark bg-dark">
                   {{-- <div class="card-header d-block">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <h6 class="card-title">{{__($title)}}</h6>
                            </div>
                        </div>
                    </div>--}}
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="15%">{{__('SL No.')}}</th>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Description')}}</th>
                                    <th>{{__('Debit')}}</th>
                                    <th>{{__('Credit')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cash_transactions as $key => $data)
                                    <tr>
                                        <th>{{__($loop->index+1)}}</th>
                                        <th>{{__($data->date)}}</th>
                                        <td>{{__($data->description)}}</td>
                                        <td>{{__($data->debit)}}</td>
                                        <td>{{__($data->credit)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="feeInputModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content"> </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.accounts.cash-transactions.internal-assets.js.cash_transaction_js')

@endsection
