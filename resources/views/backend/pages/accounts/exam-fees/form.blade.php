@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>

                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>


                    <form action="{{ route('admin.exam-fee.store') }}" method="POST" class="">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8 col-sm-12 order-1">
                                    <div class="card-body bg-dark">

                                        <p class="mb-1 text-uppercase"><label for="class_id">{{__('Class Name')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup></p>
                                        <div class="input-group input-group-lg mb-3 text-center">
                                            <select name="class_id" id="class_id" class="form-control" required>
                                                <option selected disabled value="{{ null }}">{{__('Select one')}}</option>
                                                @foreach($classRooms as $classRoom)
                                                    <option class="text-capitalize" value="{{ $classRoom->id }}">{{ $classRoom->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="exam_category_id">{{__('Exam Name')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup></p>
                                        <div class="input-group input-group-lg mb-3 text-center">
                                            <select name="exam_category_id" id="exam_category_id" class="form-control" required>
                                                <option selected disabled value="{{ null }}">{{__('Select one')}}</option>
                                                @foreach($exam_categories as $exam_category)
                                                    <option value="{{ $exam_category->id }}">{{ ucwords($exam_category->name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="student_id">{{__('Student Name')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup></p>
                                        <div class="input-group input-group-lg mb-3 text-center " id="getStudents2">
                                            <input type="text" name="student_id" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="student_id"
                                                   placeholder="{{__('Select a Class First')}}"  value="{{old('student_id')}}" readonly required>
                                        </div>
                                        <div class="input-group input-group-lg mb-3 text-center  d-none" id="getStudents">

                                            <select name="student_id" id="student_id" class="form-control selectpicker"  data-live-search="true" data-size="3" tabindex="-98" required>
                                                <option selected disabled value="" selected>{{ __('Select a Class First') }}</option>
                                                @foreach($allStudents as $student)
                                                    <option value="{{ $student->id }}">{{ $student->user->name.' ('.$student->unique_id.')' }}</option>
                                                @endforeach

                                            </select>


                                        </div>



                                        <p class="mb-1 text-uppercase"><label for="payed_amount">{{__('Payed Amount')}}</label>: <sup><i class="text-danger fas fa-star-of-life small"></i></sup><br><code>{{ __('Please don\'t use Bangla as your input.') }}</code></p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="payed_amount" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="payed_amount"
                                                   placeholder="{{__('Payed Amount')}}"  value="{{old('payed_amount')}}" required>
                                            <br>
                                            @if ($errors->has('payed_amount'))
                                                <span class="text-danger">{{ $errors->first('payed_amount') }}</span>
                                            @endif
                                        </div>

                                        <p class="mb-1 text-uppercase"><label for="discount_amou">{{__('Discount')}}</label>: <br><code>{{ __('Please don\'t use Bangla as your input.') }}</code></p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="discount" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="discount_amount"
                                                   placeholder="{{__('Discount Amount')}}" value="{{old('discount')}}">
                                            <br>
                                            @if ($errors->has('discount'))
                                                <span class="text-danger">{{ $errors->first('discount') }}</span>
                                            @endif
                                        </div>

                                        <input type="hidden" value="" id="cal_due_amount">

                                        <p class="mb-1 text-uppercase"><label for="due_amount">{{__('Due')}}</label>: <br><code>{{ __('Please don\'t use Bangla as your input.') }}</code></p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="due" class="form-control" aria-label="Large"
                                                   aria-describedby="inputGroup-sizing-sm" id="due_amount"
                                                   value="{{ null }}" readonly>
                                            <br>
                                            @if ($errors->has('due'))
                                                <span class="text-danger">{{ $errors->first('due') }}</span>
                                            @endif
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 order-2">
                                    <div class="card-body bg-dark ">
                                        <table class="table border border-2 border-info p-3">
                                            <thead>
                                            <tr>
                                                <th scope="col">{{ __('Name') }}</th>
                                                <th scope="col">{{ __('BDT') }}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($exam_fees as $exam_fee => $value)
                                                <tr>
                                                    <td>{{ __(str_replace('_', ' ', str_replace('_fee', '',$exam_fee))) }}</td>
                                                    <td>{{ __($value) }}/-</td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>

                            <div class="wizard-action card-body py-0 text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg px-4"
                                        type="submit">{{__('Submit')}}</button>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
    @include('backend.pages.accounts.exam-fees.internal-assets.js.exam_fee')
@endsection
