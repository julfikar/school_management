@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
    <style>
        .bootstrap-select.form-control-lg .dropdown-toggle {
            padding: 0 0 0  8px !important;
            background: #ffffff !important;
            box-shadow: none !important;
            margin-top: -8px;
            left: -8px;
        }
        .input-group .bootstrap-select.form-control .dropdown-toggle:focus{
            outline: none !important;
        }

    </style>
@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white"
                   href="{{ route('admin.transportations.index') }}">{{__('Transportations')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-6">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>

                    <div class="col-md-6 col-sm-12 text-right">
                        <!-- Button to Open the Modal -->
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addDwellerModal">
                            {{__('Add New Passenger')}}
                        </button>

                    </div>

                </div>
                <div class="card-body ">

                        @if($transportation->passengers->count()>0)
                            <div class="table-responsive style-scroll">

                                <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th width="10%">SL No.</th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('ID')}}</th>
                                        <th class="text-center">{{__('Option')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transportation->passengers as $key => $data)
                                        <tr>
                                            <th>{{__($loop->index+1)}}</th>
                                            <th>{{__($data->user->name)}}</th>
                                            <td>{{__($data->unique_id)}}</td>
                                            <td class="text-center">

                                                <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                                    <i class="material-icons">delete</i>
                                                    <form action="{{ route('admin.destroy-passenger',$data->id) }}" method="get" class="deleteForm"></form>
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        @else
                            <div class="row">
                            <div class="col-md-12">
                                <div class="text-warning p-3" >
                                    <h6 class="text-center h6">{{__('There is no Passenger found.')}}</h6>
                                </div>
                            </div>
                          </div>
                        @endif

                </div>
            </div>
        </div>
    </div>


    <!-- The Modal -->
    <div class="modal fade" id="addDwellerModal">
        <div class="modal-dialog">
            <div class="modal-content bg-dark text-white">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title h5">{{__('Add new Passenger')}}</h4>
                    <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="{{ route('admin.store-transportation-passenger') }}" id="add_new_passenger_form" method="post">
                        @csrf

                        <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Passenger:')}}</label> </p>
                        <div class="input-group input-group-lg mb-3">
                                <select class="form-control form-control-lg selectpicker" name="user_id" data-icon-base="fa" data-tick-icon="fa-check" data-live-search="true" data-size="5">
                                <option value="" selected disabled>{{__('Select a passenger')}}</option>
                                @foreach($users as $key => $data)
                                    @if(!$data->hasRole('super admin'))
                                    <option value="{{$data->id}}" class="text-capitalize">{{$data->name.' ('.($data->teacher?$data->teacher->unique_id:($data->student?$data->student->unique_id:($data->employee?$data->employee->unique_id:''))).')'}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br>
                            @if ($errors->has('user_id'))
                                <span class="text-danger">{{ $errors->first('user_id') }}</span>
                            @endif
                        </div>

                        <input type="hidden" name="transportation_id" value="{{$transportation->id}}">
                    </form>

                </div>


                <!-- Modal footer -->
                <div class="modal-footer border-top-0 pt-0">
                    <button type="button" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('add_new_passenger_form').submit();">{{__('Submit Form')}}</button>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
    @include('backend.pages.passengers.internal-assets.js.passenger-page-scripts')
@endsection
