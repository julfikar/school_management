@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white"
                    href="{{ route('admin.exams.index') }}">{{__($category->parent->name)}}</a>
                <a class="breadcrumb-item text-white"
                    href="{{ route('admin.exam.category.show',$category->parent->id) }}">{{__($category->name)}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-12">
                        <span class="card-title h6">{{__($category->name)}} {{__($title)}}</span>
                        <button type="button" class="btn btn-danger float-right" id="addExamCategoryBtn"><i class="material-icons">add</i>New</button>
                    </div>
                </div>
                <div class="card-body ">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10%" scope="col">#</th>
                                <th width="30%" scope="col">{{ __('Class') }}</th>
                                <th width="20%" scope="col">{{ __('Subject') }}</th>
                                <th width="20%" scope="col">{{ __('Exam') }}</th>
                                <th width="20%" scope="col">{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classRooms as $classRoom)
                            <tr>
                                <th>{{ $classRoom->index+1 }}</th>
                                <td>{{ __($classRoom->name)  }}</td>
                                <td>{{ __($classRoom->subjects->count()) }}</td>
                                <td>{{ __($classRoom->exams()->where('category_id', $category->id)->whereYear('created_at', '=', date('Y', time()))->count()) }}</td>
                                <td class="d-flex">
                                    <a href="javascript:void(0)" class="btn btn-success  btn-circle mr-2 addNewExam" data-toggle="tooltip" data-placement="left" title="{{ __('View all exam') }}">
                                        <i class="material-icons">remove_red_eye</i>
                                        <form action="{{ route('admin.exam-show') }}" method="get">
                                            <input type="hidden" name="class_room" value="{{ $classRoom->id }}">
                                            <input type="hidden" name="exam_category" value="{{ $category->id }}">
                                        </form>
                                    </a>

                                    <a href="javascript:void(0)" class="btn btn-danger  btn-circle mr-2 addNewExam" data-toggle="tooltip" data-placement="top" title="{{ __('Add new exam in this class') }}">
                                        <i class="material-icons">add</i>
                                        <form action="{{ route('admin.exams.create') }}" method="get">
                                            <input type="hidden" name="class_room" value="{{ $classRoom->id }}">
                                            <input type="hidden" name="exam_category" value="{{ $category->id }}">
                                        </form>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend/pages/exams/internal-assets/js/exam')
@endsection
