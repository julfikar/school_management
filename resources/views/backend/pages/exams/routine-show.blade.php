<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exam Routine</title>
</head>

<body>
    <div id="target">
        <div style="text-align: center">
            <h1>1st Term Examination</h1>
            <h2>Class: Two</h2>
        </div>
        <table style="width: 20px">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Subject</th>
                    <th>Start</th>
                    <th>End</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>12-12-2011</td>
                    <td>Bangla</td>
                    <td>12:30</td>
                    <td>03:30</td>
                </tr>
                <tr>
                    <td>12-12-2011</td>
                    <td>Bangla</td>
                    <td>12:30</td>
                    <td>03:30</td>
                </tr>
                <tr>
                    <td>12-12-2011</td>
                    <td>Bangla</td>
                    <td>12:30</td>
                    <td>03:30</td>
                </tr>
                <tr>
                    <td>12-12-2011</td>
                    <td>Bangla</td>
                    <td>12:30</td>
                    <td>03:30</td>
                </tr>
                <tr>
                    <td>12-12-2011</td>
                    <td>Bangla</td>
                    <td>12:30</td>
                    <td>03:30</td>
                </tr>
            </tbody>
        </table>
    </div>




    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"
        integrity="sha512-ToRWKKOvhBSS8EtqSflysM/S7v9bB9V0X3B1+E7xo7XZBEZCPL3VX5SFIp8zxY19r7Sz0svqQVbAOx+QcLQSAQ==" crossorigin="anonymous"
        referrerpolicy="no-referrer"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.7/dist/html2canvas.min.js"></script> --}}
    <script>
        // const { jsPDF } = window.jspdf;

        const doc = new jsPDF();
        // doc.html(document.body,{
        //     callback: function (doc) {
        //         doc.save();
        //     },
        //     x: 10,
        //     y: 10
        // })

        $(document).ready(function () {
            var specialElementHandler = {
                "#editor": function (element, renderer) {
                    return true;
                }
            }


            doc.fromHTML($('#target').html(),10,10,{
                // "width": 170,
                "elementHandlers": specialElementHandler
            });

            doc.save();
        });
    </script>
</body>

</html>