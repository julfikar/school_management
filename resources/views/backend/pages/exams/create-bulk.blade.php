@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white"
                   href="javascript:history.go(-1)">{{__('Go back')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header d-flex justify-content-between">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <form action="{{ route('admin.exams.store-bulk') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <p class="mb-1"><label for="class_id" class="card-title font-weight-bold">{{__('Class')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <select name="class_room_id" id="class_id" class="form-control" required>
                                <option selected value="{{ $classRoom->id }}">
                                    {{ Str::title($classRoom->name) }}</option>
                            </select>
                            <br>
                            @if ($errors->has('class_id'))
                                <span class="text-danger">{{ $errors->first('class_id') }}</span>
                            @endif
                        </div>

                        <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Exam Name')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                   placeholder="{{__('Exam Name')}}" required readonly value="{{ $category->name }}">
                            <input type="hidden" name="category_id" value="{{ $category->id }}">
                            <br>
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>


                        <table class="table">
                            <thead>
                            <tr>
                                <th>{{__('Subject Name')}}</th>
                                <th>{{__('Marks')}}</th>
                                <th>{{__('Date')}}</th>
                                <th>{{__('Start At')}}</th>
                                <th>{{__('End At')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($classRoom->subjects as $subject)
                                @if(App\Models\Exam::where(['class_room_id' => $classRoom->id, 'category_id' => $category->id, 'subject_id' => $subject->id])->whereYear('created_at', '=', date('Y', time()))->count() > 0)
                                    <tr class="subjects">
                                        <td>
                                            {{ __(App\Models\Exam::where(['class_room_id' => $classRoom->id, 'category_id' => $category->id, 'subject_id' => $subject->id])->whereYear('created_at', '=', date('Y', time()))->first()->subject->name) }}
                                        </td>
                                        <td>
                                            {{ __(App\Models\Exam::where(['class_room_id' => $classRoom->id, 'category_id' => $category->id, 'subject_id' => $subject->id])->whereYear('created_at', '=', date('Y', time()))->first()->marks) }}
                                        </td>
                                        <td>
                                            {{ __(App\Models\Exam::where(['class_room_id' => $classRoom->id, 'category_id' => $category->id, 'subject_id' => $subject->id])->whereYear('created_at', '=', date('Y', time()))->first()->date) }}
                                        </td>
                                        <td>
                                            {{ date('h:i a', strtotime(App\Models\Exam::where(['class_room_id' => $classRoom->id, 'category_id' => $category->id, 'subject_id' => $subject->id])->whereYear('created_at', '=', date('Y', time()))->first()->start_at)) }}
                                        </td>
                                        <td>
                                            {{ date('h:i a', strtotime(App\Models\Exam::where(['class_room_id' => $classRoom->id, 'category_id' => $category->id, 'subject_id' => $subject->id])->whereYear('created_at', '=', date('Y', time()))->first()->end_at)) }}
                                        </td>
                                    </tr>
                                @else
                                    <tr class="subjects">
                                        <td>
                                            <input type="text" value="{{ $subject->name }}" readonly class="subject-name">
                                        </td>
                                        <td>
                                            <input type="number" name="marks[]" class="subject-mark" required>
                                        </td>
                                        <td>
                                            <input type="date" name="date[]" class="exam-date" required>
                                        </td>
                                        <td>
                                            <input type="time" name="start_at[]" class="exam-start" required>
                                        </td>
                                        <td>
                                            <input type="time" name="end_at[]" class="exam-end" required>
                                        </td>
                                        <input type="hidden" name="subject_id[]" class="subject-id" value="{{ $subject->id }}">
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

@endsection
