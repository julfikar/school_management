@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white"
                   href="{{ route('admin.exams.index') }}">{{__($category->parent->name)}}</a>
                <a class="breadcrumb-item text-white"
                   href="{{ route('admin.exam.category.show',$category->parent->id) }}">{{__($category->name)}}</a>
                <a class="breadcrumb-item text-white"
                   href="{{ route('admin.exam.category.by-class',$category->id) }}">{{__($classRoom->name)}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <form action="{{ $exam ? route('admin.exams.update', $exam->id):route('admin.exams.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @if ($exam)
                    @method('patch')
                    @endif
                    <div class="card-body ">

                        <p class="mb-1"><label for="class_id" class="card-title font-weight-bold">{{__('Class')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <select name="class_room_id" id="class_id" class="form-control">
                                <option value="{{ $classRoom->id }}" selected>
                                    {{ Str::title($classRoom->name) }}</option>
                            </select>
                            <br>
                            @if ($errors->has('class_id'))
                            <span class="text-danger">{{ $errors->first('class_id') }}</span>
                            @endif
                        </div>

                        <p class="mb-1"><label for="subject_id" class="card-title font-weight-bold">{{__('Subject')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <select name="subject_id" id="subject_id" class="form-control">

                                @foreach ($classRoom->subjects as $subject)
                                <option value="{{ $subject->id }}" {{ $exam ? ($subject->id == $exam->subject_id ? 'selected':'') : '' }}>{{ $subject->name }}</option>
                                @endforeach
                            </select>
                            <br>
                            @if ($errors->has('subject_id'))
                            <span class="text-danger">{{ $errors->first('subject_id') }}</span>
                            @endif
                        </div>


                        <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="text" name="name" id="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                placeholder="{{__('Name')}}" value="{{ $category->name }}" required readonly>
                            <input type="hidden" name="category_id" value="{{ $category->id }}">
                            <br>
                            @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                        <p class="mb-1"><label for="date" class="card-title font-weight-bold">{{__('Date')}}:</label></p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="date" name="date" id="date" class="form-control" value="{{ $exam ? $exam->date : '' }}" required>
                            <br>
                            @if ($errors->has('date'))
                            <span class="text-danger">{{ $errors->first('date') }}</span>
                            @endif
                        </div>

                        <div class="form-row">
                            <div class="col-md-6">
                                <p class="mb-1"><label for="start_at" class="card-title font-weight-bold">{{__('Start Time')}}:</label></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="time" name="start_at" id="start_at" class="form-control" value="{{ $exam ? $exam->start_at : '' }}" required>
                                    <br>
                                    @if ($errors->has('start_at'))
                                        <span class="text-danger">{{ $errors->first('start_at') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-6">
                                <p class="mb-1"><label for="end_at" class="card-title font-weight-bold">{{__('End Time')}}:</label></p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="time" name="end_at" id="end_at" class="form-control" value="{{ $exam ? $exam->end_at : '' }}" required>
                            <br>
                            @if ($errors->has('end_at'))
                            <span class="text-danger">{{ $errors->first('end_at') }}</span>
                            @endif
                        </div>
                            </div>
                        </div>

                        <p class="mb-1"><label for="marks" class="card-title font-weight-bold">{{__('Marks')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="number" name="marks" id="marks" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                placeholder="{{__('Marks')}}" value="{{ $exam ? $exam->marks : '' }}" required>
                            <br>
                            @if ($errors->has('marks'))
                            <span class="text-danger">{{ $errors->first('marks') }}</span>
                            @endif
                        </div>

                        <p class="mb-1"><label for="question" class="card-title font-weight-bold">{{__('Question')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="file" id="question" name="question" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                {{ $exam ? '' : 'required' }}>
                            <br>
                            @if ($errors->has('question'))
                            <span class="text-danger">{{ $errors->first('question') }}</span>
                            @endif
                        </div>

                        <p class="mb-1"><label for="answer" class="card-title font-weight-bold">{{__('Answer')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="file" id="answer" name="answer" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                            <br>
                            @if ($errors->has('answer'))
                            <span class="text-danger">{{ $errors->first('answer') }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')
<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
<script src="{{ asset('backend/assets/js/exams.js') }}"></script>

@endsection
