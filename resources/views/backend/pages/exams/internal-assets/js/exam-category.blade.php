<script>
    $(function (){
        $('#addExamCategoryBtn').on('click', addNewExamCategory);
        $('.editExamCategoryBtn').on('click', editExamCategory);
    })

    function addNewExamCategory(){
        $('#examCategoryModal').modal('show');
    }

    function editExamCategory(){
        console.log();
        let url = '/admin/exams/category/'+$(this).data()['id']+'/edit';
            $.ajax({
            type:'get',
            url:url,
            success:function (data){
                $('#examCategoryModal .modal-content').empty().append(data);
                $('#examCategoryModal').modal('show');
            }
        })
    }
</script>
