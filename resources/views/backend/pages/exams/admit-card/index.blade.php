@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="card bg-dark col-lg-6 col-md-8 col-sm-11 mx-auto">
                <div class="card-body">
                    <form action="{{ route('admin.student-exams.admit-card.get') }}" method="get" target="_blank">
                        @csrf
                        <div class="form-group">
                            <label for="student" class="card-title text-light">{{ __('Student') }} <i class="text-danger fas fa-star-of-life"></i></label>
                            <select name="student" id="student" class="form-control selectpicker" data-live-search="true" data-size="3" tabindex="-98" required>
                                <option value="{{ null }}">{{ __('Select one') }}</option>
                                @foreach($students as $student)
                                    <option value="{{ $student->unique_id }}">{{ __($student->user->name).' ('.$student->unique_id.')' }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exam" class="card-title text-light">{{ __('Exam') }} <i class="text-danger fas fa-star-of-life"></i></label>
                            <select name="exam" id="exam" class="form-control selectpicker" data-live-search="true" data-size="3" tabindex="-98" required>
                                <option value="{{ null }}">{{ __('Select one') }}</option>
                                @foreach($exams as $exam)
                                    @if(count($exam->exam) > 0)
                                        <option value="{{ $exam->id }}">{{ __($exam->name) }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-danger w-50">{{ __('Get Admit Card') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
@endsection
