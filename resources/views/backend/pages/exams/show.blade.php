@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white"
                    href="{{ route('admin.exams.index') }}">{{__($category->name)}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-12">
                        <span class="card-title h6">{{__($category->name)}} {{__($title)}}</span>
                        <button type="button" class="btn btn-danger float-right" id="addExamCategoryBtn"><i class="material-icons">add</i>New</button>
                    </div>
                </div>
                <div class="card-body ">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="10%" scope="col">#</th>
                                <th width="50%" scope="col">{{ __('Name') }}</th>
                                <th width="20%" scope="col">{{ __('Marks at (%)') }}</th>
                                <th width="20%" scope="col">{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($exams as $exam)
                            <tr>
                                <th>{{ $loop->index+1 }}</th>
                                <td>{{ __($exam->name) }}</td>
                                <td>{{ __($exam->mark_at_percentage).' (%)' }}</td>
                                <td class="d-flex">
                                    <a href="{{ route('admin.exam.category.by-class',$exam->id) }}" class="btn btn-success  btn-circle mr-2">
                                        <i class="material-icons">remove_red_eye</i>
                                    </a>

                                    <a href="javascript:void(0)" class="btn btn-info  btn-circle mr-2 editExamCategoryBtn" data-id="{{ $exam->id }}">
                                        <i class="material-icons">edit</i>
                                    </a>

                                    <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                        <i class="material-icons">delete</i>
                                        <form action="{{ route('admin.exam.category.destroy',$exam->id) }}" method="post" class="deleteExam">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- exam category modal -->
<div class="modal fade" id="examCategoryModal" tabindex="-1" role="dialog" aria-labelledby="examCategoryModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-capitalize" id="examCategoryModalLongTitle">{{ __('add new Exam') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.exam.category.store') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="examName">{{ __('Exam Name') }}</label>
                        <input type="text" name="name" id="examName" class="form-control" required placeholder="{{ __('Exam Name') }}">
                    </div>
                    <div class="form-group">
                        <label for="examMark">{{ __('Exam Mark At (%)') }}</label>
                        <input type="number" step="0.01" name="mark_at_percentage" id="examName" class="form-control" required placeholder="{{ __('Exam Mark At (%)') }}" min="5" max="100">
                    </div>
                    @if($exams->count() > 0)
                        <div class="form-group">
                            <label for="examParent">{{ __('Main Term Exam') }}</label>
                            <select name="parent" id="examParent" class="form-control">
                                <option value="{{ null }}">{{ __('Select one if you need') }}...</option>
                                @foreach($parents as $exam)
                                    <option value="{{ $exam->id }}">{{ $exam->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('page-script')

<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@include('backend.pages.exams.internal-assets.js.delete-warning')
@include('backend.pages.exams.internal-assets.js.exam-category')

@endsection
