<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exam Routine</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <div style="margin-top: 20px">
        <p style="font-size: 40px; text-align: center">{{ $examName }}</p>
        <p style="font-size: 40px; text-align: center">{{ $class->name }}</p>
        <table class="table" style="border: 1px solid black;  border-collapse: collapse">
            <thead>
                <tr style="border: 1px solid black">
                    <th>{{__('Date')}}</th>
                    <th>{{__('Subject')}}</th>
                    <th>{{__('Start')}}</th>
                    <th>{{__('End')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($exams as $exam)
                <tr>
                    <td>{{ $exam->date }}</td>
                    <td>{{ $exam->subject ? $exam->subject->name : '' }}</td>
                    <td>{{ $exam->start_at }}</td>
                    <td>{{  $exam->end_at }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>
