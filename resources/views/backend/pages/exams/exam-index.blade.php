@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white"
                   href="{{ route('admin.exams.index') }}">{{__($category->parent->name)}}</a>
                <a class="breadcrumb-item text-white"
                   href="{{ route('admin.exam.category.show',$category->parent->id) }}">{{__($category->name)}}</a>
                <a class="breadcrumb-item text-white"
                   href="{{ route('admin.exam.category.by-class',$category->id) }}">{{__($classRoom->name)}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header d-flex justify-content-between">
                    <h6 class="card-title">{{__($title)}}</h6>
                    <a href="javascript:void(0)" class="btn btn-primary addNewExam">{{__('Create All Exam Once')}}
                        <form action="{{ route('admin.exams.create-bulk') }}" method="get">
                            <input type="hidden" name="class_room" value="{{ $classRoom->id }}">
                            <input type="hidden" name="exam_category" value="{{ $category->id }}">
                        </form>
                    </a>
                </div>
                <div class="card-body ">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Subject') }}</th>
                                <th scope="col">{{ __('Date') }}</th>
                                <th scope="col">{{ __('Marks') }}</th>
                                <th scope="col">{{ __('Question') }}</th>
                                <th scope="col">{{ __('Answer') }}</th>
                                <th scope="col">{{ __('Action') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($exams as $exam)
                            <tr>
                                <th>{{ $loop->index+1 }}</th>
                                <td>{{ __($exam->subject->name) }}</td>
                                <td>{{ date('d-M-y', strtotime($exam->date)) }}</td>
                                <td>{{ $exam->marks }}</td>
                                <td>
                                    {!! $exam->question ? '<a href="'.asset($exam->question).'" target="_blank" class=""> <i class="fa fa-download h5 mb-0"></i></a>':''!!}

                                </td>
                                <td>
                                    {!! $exam->answer ? '<a href="'.asset($exam->answer).'" target="_blank" class=""> <i class="fa fa-download h5 mb-0"></i></a>':'' !!}

                                </td>
                                <td class="d-flex">
                                    <a href="{{ route('admin.exams.edit',$exam->id) }}" class="btn btn-info  btn-circle mr-2">
                                        <i class="material-icons">edit</i>
                                    </a>

                                    <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                        <i class="material-icons">delete</i>
                                        <form action="{{ route('admin.exams.destroy',$exam->id) }}" method="post" id="deleteExam">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@include('backend.pages.exams.internal-assets.js.delete-warning')
@include('backend/pages/exams/internal-assets/js/exam')

@endsection
