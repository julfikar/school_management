@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <form action="{{ route('admin.exams.routine-download') }}" target="_blank">
                    <div class="card-header d-flex justify-content-between">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>


                    <div class="card-body ">

                        <p class="mb-1"><label for="class_id" class="card-title font-weight-bold">{{__('Class')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <select name="class_room_id" id="class_id" class="form-control">
                                <option value="" disabled selected>{{ __('Select a Class') }}</option>
                                @foreach ($classes as $class)
                                <option value="{{ $class->id }}">
                                    {{ Str::title($class->name) }}</option>
                                @endforeach
                            </select>
                            <br>
                            @if ($errors->has('class_id'))
                            <span class="text-danger">{{ $errors->first('class_id') }}</span>
                            @endif
                        </div>

                        <p class="mb-1"><label for="exam_id" class="card-title font-weight-bold">{{__('Exam')}}:</label> </p>
                        <div class="input-group input-group-lg mb-3">
                            <select name="exam_id" id="exam_id" class="form-control" disabled>
                                <option value="" disabled selected>{{ __('Select an Exam') }}</option>
                            </select>
                            <br>
                            @if ($errors->has('exam_id'))
                            <span class="text-danger">{{ $errors->first('exam_id') }}</span>
                            @endif
                        </div>

                        <div class="container" id="routine">
                            <div class="text-center">
                                <h1 id="exam_name"></h1>
                                <h2 id="class_name"></h2>
                            </div>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>{{__('Date')}}</th>
                                        <th>{{__('Subject')}}</th>
                                        <th>{{__('Start')}}</th>
                                        <th>{{__('End')}}</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>



                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit" id="download_button">{{__('Download Routine')}}</button>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')
<script src="{{ asset('backend/assets/js/pages/exams/exam-routine.js') }}"></script>

@endsection
