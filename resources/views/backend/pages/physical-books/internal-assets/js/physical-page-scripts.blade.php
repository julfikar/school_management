<script !src="">
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "100000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        'body-output-type': 'trustedHtml'
    };

    $(function () {
        $('.rowDataDeleteBtn').on('click', deleteRowAlert);
        $('.giveToMemberBtn').on('click', giveToMember);
        $('.takeFromMemberBtn').on('click', takeFromMember);
        $('.member').selectpicker();

        let bookHoldIn = document.getElementsByClassName('holdIn');
        let bookHoldInMember = document.getElementsByClassName('member');
        for (let bhi = 0; bhi < bookHoldIn.length; bhi++){
            let content = bookHoldIn[bhi];
            if (content.innerHTML != ''){
                bookHoldInMember[bhi].innerHTML = '';
            }
        }
    });

    function deleteRowAlert() {
        swal({
            title: "Are you sure?",
            text: "Once you delete, You can\'t recover this file",
            icon: "warning",
            dangerMode: true,
            buttons: {
                cancel: true,
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    closeModal: true
                },
            },
        }).then((value) => {
           if(value){
               $(this).find('form').submit();
            }
        });
    }

    function giveToMember() {
        let row = $(this).parent().parent().attr('data-browse');
        let outDate = $(this).parent().parent().find('.out_date').val();
        let selectedMember = $(this).parent().parent().find('.selectedMember').children("option:selected").val();
        if (outDate == ''){
            $(this).parent().parent().find('.out_date').focus();
        }
        if (selectedMember == ''){
            $(this).parent().parent().find('.selectedMember').focus();
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (selectedMember!= '' && outDate!= '') {
            $.ajax({
                type: 'post',
                url: 'physicalbooks/to-member',
                data: {
                    'row': row,
                    'out_date': outDate,
                    'member': selectedMember
                },
                success: function (data) {
                    if(data.message == null){
                        toastr.success('Success');
                        setTimeout(location.reload(), 1500);
                    }else{
                        toastr.warning(data.message);//you display your message in here
                    }
                },
                error: function (request, error) {
                    toastr.error(request.responseJSON.message);//you display your message in here
                    if (request.responseJSON.errors['member']){
                        toastr.error(request.responseJSON.errors['member'][0])
                    }
                    if (request.responseJSON.errors['out_date']){
                        toastr.error(request.responseJSON.errors['out_date'][0])
                    }
                },
            });
        }
    }

    function takeFromMember(){
        let row = $(this).parent().parent().attr('data-browse');
        let inDate = $(this).parent().parent().find('.in_date').val();
        if (inDate == ''){
            $(this).parent().parent().find('.in_date').focus();
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if (inDate!= '') {
            $.ajax({
                type: 'post',
                url: 'physicalbooks/from-member',
                data: {
                    'row': row,
                    'in_date': inDate
                },
                success: function (data) {
                    if(data.message == null){
                        toastr.success('Success');
                        // console.log(data);
                        setTimeout(location.reload(), 1000);
                    }else{
                        toastr.warning(data.message);//you display your message in here
                    }
                },
                error: function (request, error) {
                    toastr.error(request.responseJSON.message);//you display your message in here
                    if (request.responseJSON.errors['in_date']){
                        toastr.error(request.responseJSON.errors['in_date'][0])
                    }
                },
            });
        }
    }
</script>
