@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white" href="{{ route('admin.physicalbooks.index') }}">{{__('All Physical Books')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-12 pl-0">
                        <h6 class="card-title ">{{__($title)}}</h6>

                    </div>
                </div>
                <form class="" action="{{ $physicalBook?route('admin.physicalbooks.update',$physicalBook->id):route('admin.physicalbooks.store') }}" method="POST">
                        @csrf
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Book Name:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="name" id="name"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Book Name')}}" value="{{ $physicalBook?$physicalBook->name:old('name') }}">
                                    <br>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="author" class="card-title font-weight-bold">{{__('Author Name:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="author" id="author"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Author Name')}}" value="{{ $physicalBook?$physicalBook->author:old('author') }}">
                                    <br>
                                    @if ($errors->has('author'))
                                        <span class="text-danger">{{ $errors->first('author') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Insert Date:')}} ({{ __('optional') }})</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="datetime-local" name="insert_date" id="insert_date"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Insert date')}}" value="{{ $physicalBook?$physicalBook->insert_date:old('insert_date') }}">
                                    <br>
                                    @if ($errors->has('insert_date'))
                                        <span class="text-danger">{{ $errors->first('insert_date') }}</span>
                                    @endif
                                </div>

                            </div>
                        </div>


                        {{--<input type="hidden" name="class_id" value="{{$ebook->id}}">--}}
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

@endsection
