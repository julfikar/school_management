@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <div class="col-6">
                            <h6 class="card-title">{{__($title)}}</h6>
                        </div>

                        <div class="col-md-6 col-sm-12 text-right">
                            <a href="{{ route('admin.physicalbooks.create') }}" class="btn btn-success"> {{__('Add New Book')}}</a>
                        </div>

                    </div>
                    <div class="card-body ">

                        @if($physicalbooks->count()>0)
                            <div class="table-responsive style-scroll">

                                <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th width="3%">SL No.</th>
                                        <th width="10%">{{__('Insert Date')}}</th>
                                        <th width="15%">{{__('Name')}}</th>
                                        <th width="15%">{{__('Author')}}</th>
                                        <th width="15%">{{__('In Date')}}</th>
                                        <th width="15%">{{__('Out Date')}}</th>
                                        <th width="22%">{{__('Member')}}</th>
                                        <th width="5%">{{__('Option')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($physicalbooks as $data)
                                        <tr data-browse="{{ $data->id }}">
                                            <th>{{__($loop->index+1)}}</th>
                                            <td>{{date('d-M-y (h:i:s: a)', strtotime($data->insert_date))}}</td>
                                            <th>{{__($data->name)}}</th>
                                            <th>{{__($data->author)}}</th>
                                            <td>{!! $data->in_date? date('d-M-y (h:i:s: a)', strtotime($data->in_date)):'<input type="datetime-local" name="in_date"  class="form-control in_date" aria-label="Large" aria-describedby="inputGroup-sizing-sm"/>' !!}</td>
                                            <td>{!! $data->out_date?date('d-M-y (h:i:s: a)', strtotime($data->out_date)):'<input type="datetime-local" name="out_date"  class="form-control out_date" aria-label="Large" aria-describedby="inputGroup-sizing-sm"/>' !!}</td>
                                            <td>
                                                <span class="holdIn">{!! $data->libraryMember?$data->libraryMember->user->name.'</br>('.$data->libraryMember->unique_id.')':'' !!}</span>
                                                <span class="member">
                                               <select name="member" class="form-control selectpicker selectedMember" data-live-search="true" data-size="2" tabindex="-98">
                                                   <option value="{{ null }}" disabled selected data-tokens="{{ null }}">{{ __('Select one') }}</option>
                                                   @foreach($members as $member)
                                                       @if(!$member->book)
                                                           <option value="{{ $member->id }}" data-tokens="{{  $member->unique_id }}">{{ ucwords($member->user->name) .' ('. $member->unique_id .')' }}</option>
                                                       @endif
                                                   @endforeach
                                               </select>
                                                   </span>
                                            </td>
                                            <td>
                                                @if($data->libraryMember)
                                                    <a href="javascript:void(0)" title="Take from member" class="btn btn-success my-1 btn-circle takeFromMemberBtn" >
                                                        <i class="material-icons">check</i>
                                                    </a>
                                                @else
                                                    <a href="javascript:void(0)" title="Give to member" class="btn btn-success my-1 btn-circle giveToMemberBtn" >
                                                        <i class="material-icons">check</i>
                                                    </a>
                                                @endif
                                                <a href="{{ route('admin.physicalbooks.create',$data->id) }}" class="btn btn-info my-1 btn-circle" >
                                                    <i class="material-icons">edit</i>
                                                </a>
                                                <a href="javascript:void (0)" title="Delete"  class="btn btn-danger my-1 btn-circle rowDataDeleteBtn">
                                                    <i class="material-icons">delete</i>

                                                    <form action="{{ route('admin.physicalbooks.destroy',$data->id) }}" method="get" class="deleteForm"></form>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-warning p-3" >
                                        <h6 class="text-center h6">{{__('There is no subject found.')}}</h6>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        @endsection

        @section('page-script')
            <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
            <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
    @include('backend.pages.physical-books.internal-assets.js.physical-page-scripts')
@endsection
