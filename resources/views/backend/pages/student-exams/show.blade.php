@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ route('admin.student-exams.index') }}">{{ __('Exam Result') }}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <div class="table-responsive style-scroll">

                            <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th width="10%">{{__('Roll no')}}</th>
                                    <th width="50%">{{__('Name')}}</th>
                                    <th width="20%">{{__('Student ID')}}</th>
                                    <th width="20%">{{__('Option')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($classRoom->student as $data)
                                    <tr>
                                        <th>{{ $data->class_rol }}</th>
                                        <th>{{ $data->user->name }}</th>
                                        <th>{{ $data->unique_id }}</th>
                                        <td>
                                            <a href="javascript:void(0)" title="make result" class="btn btn-danger btn-circle makeReportBtn">
                                                <i class="material-icons">description</i>
                                                <form action="{{ route('admin.student-exams.create') }}" method="get">
                                                    <input type="hidden" name="student" value="{{ $data->id }}">
                                                    <input type="hidden" name="class" value="{{ $classRoom->id }}">
                                                    <input type="hidden" name="exam" value="{{ $examCategory->id }}">
                                                </form>
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
    @include('backend.pages.student-exams.internal-assets.js.student-exam')
@endsection
