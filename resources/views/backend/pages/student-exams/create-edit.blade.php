@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ route('admin.student-exams.index') }}">{{ __('Exam Result') }}</a>
                    <a class="breadcrumb-item text-white" href="javascript:history.go(-1)">{{ __($classRoom->name).__('\'s Student Exams Result') }}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <form action="{{ route('admin.student-exams.store') }}" method="post">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th width="10%">{{ __('Exam') }}:</th>
                                        <td>
                                            {{ __($examCategory->name) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="10%">{{ __('Name') }}:</th>
                                        <td>
                                            {{ $student->user->name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="10%">{{ __('Class') }}:</th>
                                        <td>
                                            {{ __($student->classRoom->name) }}
                                            <input type="hidden" name="class_id" value="{{ $student->classRoom->id }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="10%">{{ __('Student ID') }}:</th>
                                        <td>
                                            {{ $student->unique_id }}
                                            <input type="hidden" name="student_id" value="{{ $student->unique_id }}">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th width="5%">SL</th>
                                    <th width="30%">Subject</th>
                                    <th width="10%">Min. Mark</th>
                                    <th width="25%">Obtain Mark</th>
                                    <th width="10%">Max. Mark</th>
                                    <th width="10%">CGPA</th>
                                    <th width="10%">Remark</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($examCategory->exam()->whereYear('created_at', date('Y',time()))->where('class_room_id',$student->classRoom->id)->get() as $exam)
                                <tr>
                                    <th>{{ $loop->index+1 }} </th>
                                    <td>
                                        {{ __($exam->subject->name) }}
                                        <input type="hidden" name="subject[]" value="{{ $exam->subject->name }}">
                                        <input type="hidden" name="subject_id[]" value="{{ $exam->subject->id }}">
                                        <input type="hidden" name="exam_id[]" value="{{ $exam->id }}">
                                    </td>
                                    <td>{{ $exam->min_mark }}</td>
                                    <td>
                                        <input type="number" step="0.01" name="{{ str_replace(' ', '', $exam->subject->name) }}_got_mark" class="form-control" value="{{ $exam->studentExam()->where('student_id', $student->unique_id)->first() ? $exam->studentExam()->where('student_id', $student->unique_id)->first()->got_mark:''  }}">
                                    </td>
                                    <td>{{ $exam->marks }}</td>
                                    <td>
                                        <input type="number" step="0.01" name="{{ str_replace(' ', '', $exam->subject->name) }}_cgpa" class="form-control" value="{{ $exam->studentExam()->where('student_id', $student->unique_id)->first() ? $exam->studentExam()->where('student_id', $student->unique_id)->first()->cgpa:''  }}">
                                    </td>
                                    <td>
                                        <input type="text" name="{{ str_replace(' ', '', $exam->subject->name) }}_remark" class="form-control" value="{{ $exam->studentExam()->where('student_id', $student->unique_id)->first() ? $exam->studentExam()->where('student_id', $student->unique_id)->first()->remark:''  }}">

                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-danger w-25">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
