@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <form action="{{ route('admin.student-exams.show') }}" method="get">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exam" class="card-title">{{ __('Exam') }}</label>
                                <select name="exam" id="exam" class="form-control" required>
                                    <option selected disabled value="{{ null }}">{{ __('Select One') }}</option>
                                    @foreach($exams as $exam)
                                        <option class="text-capitalize" value="{{ $exam->id }}">{{ __($exam->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="class" class="card-title">{{ __('Class Room') }}</label>
                                <select name="class" id="class" class="form-control" required>
                                    <option selected disabled value="{{ null }}">{{ __('Select One') }}</option>
                                    @foreach($classes as $class)
                                        <option class="text-capitalize" value="{{ $class->id }}">{{ __($class->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-danger rounded w-25 text-capitalize">Get all student</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
