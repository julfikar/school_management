<script>
    $(function (){
        $('#deletePageBtn').on('click', deletePage);
        $('.copyBtn').on('click', coppyText);
    });

    function deletePage(){
        swal({
            title: "Are you sure?",
            text: "Once you delete, You can\'t recover this file",
            icon: "warning",
            dangerMode: true,
            buttons: {
                cancel: true,
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    closeModal: true
                },
            },
        }).then((value) => {
            if(value){
                $('#deletePageForm').submit();
            }
        });
    }

    function coppyText(){
        var copyText = document.getElementById('pageSlug');
        copyText.select();
        copyText.setSelectionRange(0,9999999999999);
        document.execCommand('copy');
    }
</script>
