@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    @include('backend.pages.settings.page-settings.internal-assets.css.page-css')
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-transparent">
                    <div class="card-header bg-dark">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <form action="{{ $page?route('admin.pages.update',$page->id):route('admin.pages.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @if($page)
                            @method('PATCH')
                        @endif
                        <div class="form-row">
                            <div class="col-md-8 col-sm-12 order-1">
                                <div class="card-body bg-dark">
                                    @if(!$page)
                                        <p class="mb-1 text-uppercase"><i class="text-danger fas fa-star-of-life"></i> <label for="pageTitle">{{__('page name')}}</label>: </p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="name" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" id="pageName"
                                                   placeholder="{{__('Page Name')}}" required>
                                            <br>
                                            @if ($errors->has('page_name'))
                                                <span class="text-danger">{{ $errors->first('page_name') }}</span>
                                            @endif
                                        </div>
                                    @endif

                                    <p class="mb-1 text-uppercase"><label for="pageTitle">{{__('page title')}}</label>: </p>
                                    <div class="input-group input-group-lg mb-3">
                                        <input type="text" name="page_title" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" id="pageTitle"
                                               placeholder="{{__('Page Title')}}" value="{{ $page?$page->title:'' }}">
                                        <br>
                                        @if ($errors->has('page_title'))
                                            <span class="text-danger">{{ $errors->first('page_title') }}</span>
                                        @endif
                                    </div>

                                    @if($page)
                                        @if($page->body_type)
                                            <p class="mb-1 text-uppercase"><i class="text-danger fas fa-star-of-life"></i> <label for="pageContent">{{__('page Content')}}</label>: </p>
                                            <div class="form-group mb-3">
                                                <textarea name="page_content" class="form-control" id="pageContent" placeholder="{{__('Page Content')}}" rows="10" required>{!! $page?$page->body:'' !!}</textarea>
                                                <br>
                                                @if ($errors->has('page_content'))
                                                    <span class="text-danger">{{ $errors->first('page_content') }}</span>
                                                @endif
                                            </div>
                                        @endif
                                    @else
                                        <p class="mb-1 text-uppercase"><i class="text-danger fas fa-star-of-life"></i> <label for="pageContent">{{__('page Content')}}</label>: </p>
                                        <div class="form-group mb-3">
                                            <textarea name="page_content" class="form-control" id="pageContent" placeholder="{{__('Page Content')}}" rows="10" required>{!! $page?$page->body:'' !!}</textarea>
                                            <br>
                                            @if ($errors->has('page_content'))
                                                <span class="text-danger">{{ $errors->first('page_content') }}</span>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-12 order-2">
                                <div class="card-body bg-dark">
                                    @if($page)
                                        @if($page->status)
                                            <div class="col-12 mb-3 text-center">
                                                <a href="{{ route('page.redirect',$page->slug) }}">
                                                    <button type="button" class="btn btn-warning w-100 rounded text-uppercase"><i class="far fa-eye"></i>&nbsp;
                                                        {{__('View on live')}}</button>
                                                </a>
                                            </div>
                                        @endif
                                    @endif

                                    <div class="col-12 mb-3 text-center">
                                        <button type="submit" class="btn btn-success w-100 rounded text-uppercase"><i class="material-icons">done_all</i>
                                            {{__('Save')}}</button>
                                    </div>

                                    @if($page)
                                        @if($page->deletable)
                                            <div class="col-12 mb-3 text-center">
                                                <button type="button" class="btn btn-danger w-100 rounded text-uppercase" id="deletePageBtn"><i class="material-icons">delete</i>
                                                    {{__('delete')}}
                                                </button>
                                            </div>
                                        @endif
                                    @endif

                                    <p class="mb-1 text-uppercase"><label for="pageStatus">{{__('Publish status')}}</label>: </p>
                                    <div class="input-group input-group-lg mb-3 text-center">
                                        <select name="page_status" id="pageStatus" class="form-control" required>
                                            <option selected disabled value="{{ null }}">{{__('Select one')}}</option>
                                            <option {{ $page?($page->status?'selected':''):'' }} value="on">{{__('Publish')}}</option>
                                            <option {{ $page?($page->status?'':'selected'):'' }} value="off">
                                                {{__('Un-publish')}}</option>
                                        </select>
                                    </div>

                                    @if($page)
                                        <p class="mb-1 text-uppercase copyBtn"><label for="pageSlug"><i class="material-icons">content_copy</i><code>{{__('Click here to copy this link for use in the menu url')}}</code></label>: </p>
                                        <div class="input-group input-group-lg mb-3 text-center">
                                            <input type="text" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm" id="pageSlug"
                                                   placeholder="{{__('Page Slug')}}" value="{{ '/pg/'.$page->slug }}" readonly>
                                        </div>
                                    @endif

                                    <p class="mb-1 text-uppercase"><label for="pageRightBar">{{__('Right-bar')}}</label>: </p>
                                    <div class="input-group input-group-lg mb-3 text-center">
                                        <label class="switch">
                                            <input type="checkbox" {{ $page?($page->right_bar?'checked':''):'checked' }} id="pageRightBar" name="page_right">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @if($page)
        @if($page->deletable)
            <form action="{{ route('admin.pages.destroy',$page->id) }}" method="post" id="deletePageForm">
                @csrf
                @method('DELETE')
            </form>
        @endif
    @endif
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/js/form-summerNote.js') }}"></script>
    @include('backend.pages.settings.page-settings.internal-assets.jss.page-js')
@endsection
