@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <form action="{{ route('admin.social-link.store') }}" method="POST" enctype="multipart/form-data">
                        <div class="card-body ">
                            @csrf
                            <p class="mb-1">{{__('Twitter')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="twitter" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('Twitter')}}" value="{{ setting('social-media.links.twitter') }}">
                                <br>
                                @if ($errors->has('twitter'))
                                    <span class="text-danger">{{ $errors->first('twitter') }}</span>
                                @endif
                            </div>

                            <p class="mb-1 text-capitalize">{{__('facebook')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="facebook" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('facebook')}}" value="{{ setting('social-media.links.facebook') }}">
                                <br>
                                @if ($errors->has('facebook'))
                                    <span class="text-danger">{{ $errors->first('facebook') }}</span>
                                @endif
                            </div>

                            <p class="mb-1 text-capitalize">{{__('whatsapp')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="whatsapp" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('whatsapp')}}" value="{{ setting('social-media.links.whatsapp') }}">
                                <br>
                                @if ($errors->has('whatsapp'))
                                    <span class="text-danger">{{ $errors->first('whatsapp') }}</span>
                                @endif
                            </div>

                            <p class="mb-1 text-capitalize">{{__('linkedin')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="linkedin" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('linkedin')}}" value="{{ setting('social-media.links.linkedin') }}">
                                <br>
                                @if ($errors->has('linkedin'))
                                    <span class="text-danger">{{ $errors->first('linkedin') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Skype')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="skype" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                       placeholder="{{__('Skype')}}" value="{{ setting('social-media.links.skype') }}">
                                <br>
                                @if ($errors->has('skype'))
                                    <span class="text-danger">{{ $errors->first('skype') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
