@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <div class="card-body ">
                    {{-- @if($customJs == null) --}}

                    <form action="{{ route('admin.settings.custom-js-save') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <p class="mb-1">{{__('Custom Js')}}:</p>
                        <div class="input-group  mb-3">
                            <textarea class="form-control" name="custom_js" aria-label="With textarea"
                                rows="15">{{ setting('backend.custom_js.custom_js') }}</textarea>
                            <br>
                            @if ($errors->has('custom-css'))
                            <span class="text-danger">{{ $errors->first('custom-css') }}</span>
                            @endif
                        </div>

                        <div class="wizard-action text-left">
                            <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                        </div>
                    </form>
                    {{--  @elseif($customCss != null)
                                <form action="{{route('admin.update-custom-css',$customCss->id)}}" method="post" enctype="multipart/form-data"
                    class="wma-form">
                    @csrf
                    <p class="mb-1">{{__('Custom Js:')}}</p>
                    <div class="input-group mb-3">
                        <textarea class="form-control" name="custom-css" aria-label="With textarea"
                            rows="15">{!! html_entity_decode($customCss->css) !!}</textarea>
                        <br>
                        @if ($errors->has('custom_css'))
                        <span class="text-danger">{{ $errors->first('custom_css') }}</span>
                        @endif
                    </div>

                    <div class="wizard-action text-left">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Update')}}</button>
                    </div>
                    </form>
                    @endif --}}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('page-script')

@endsection
