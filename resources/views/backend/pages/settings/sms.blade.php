@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <div class="card-body ">
                    {{--@if($smtpSettings == null) --}}
                    <form action="{{ route('admin.settings.sms-save') }}" method="POST" enctype="multipart/form-data" class="wma-form">
                        @csrf
                        <p class="mb-1">{{__('SMS API URL')}}: </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="text" name="url" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                placeholder="{{__('SMS API URL')}}" value="{{ setting('backend.sms.url') }}">
                            <br>
                            @if ($errors->has('url'))
                            <span class="text-danger">{{ $errors->first('url') }}</span>
                            @endif
                        </div>

                        <p class="mb-1">{{__('Auth Key')}}: </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="text" name="authkey" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                placeholder="{{__('Auth Key')}}" value="{{ setting('backend.sms.authkey') }}">
                            <br>
                            @if ($errors->has('authkey'))
                            <span class="text-danger">{{ $errors->first('authkey') }}</span>
                            @endif
                        </div>

                        <p class="mb-1">{{__('Sender ID')}}: </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="text" name="sender" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                placeholder="{{__('Sender ID')}}" value="{{ setting('backend.sms.sender') }}">
                            <br>
                            @if ($errors->has('sender'))
                            <span class="text-danger">{{ $errors->first('sender') }}</span>
                            @endif
                        </div>

                        <p class="mb-1">{{__('Route')}}: </p>
                        <div class="input-group input-group-lg mb-3">
                            <input type="text" name="route" class="form-control " aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                placeholder="{{__('Route')}}" value="{{ setting('backend.sms.route') }}">
                            <br>
                            @if ($errors->has('route'))
                            <span class="text-danger">{{ $errors->first('route') }}</span>
                            @endif
                        </div>

                        <div class="wizard-action text-left">
                            <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('page-script')

@endsection
