@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <div class="card-body px-5">
                    {{--@if($variable == null)--}}
                    <form action="{{ route('admin.settings.logo-favicon-save') }}" method="POST" enctype="multipart/form-data" class="wma-form">
                        @csrf
                        <p class="mb-1">{{__('Logo')}}: <code>{{__('expected size is 64x64px')}}</code></p>
                        <div class="form-row p-5">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <div class="px-5">
                                        <div class="site-logo" id="site-logo">
                                            <div class="input-images"></div>
                                        </div>
                                    </div>
                                   {{-- <div role="button" class="btn btn-primary mr-2">
                                        <input type="file" title='Click to add Files' name="logo" />
                                        <br>
                                        @if ($errors->has('logo'))
                                        <span class="text-danger">{{ $errors->first('logo') }}</span>
                                        @endif

                                    </div>--}}
                                </div>
                            </div>
                            <div class="col-md-6  d-md-block  d-sm-none">
                                <div class="img-favicon">
                                    <img src="{{ setting('backend.logo_favicon.logo') }}" alt="Og Meta Image" class="img-thumbnail img-fluid">
                                </div>

                            </div>
                        </div>

                        <p class="mb-1">{{__('Favicon')}}: <code>{{__('expected size is 32x32px')}}</code></p>
                        <div class="form-row p-5">
                            <div class="col-md-6  d-md-block  d-sm-none">
                                <div class="img-favicon float-left">
                                    <img src="{{ setting('backend.logo_favicon.favicon') }}" alt="Og Meta Image" class="img-thumbnail">
                                </div>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <div class="px-5">
                                        <div class="site-favicon" id="site-favicon">
                                            <div class="input-images"></div>
                                        </div>
                                    </div>
                                   {{-- <div role="button" class="btn btn-primary mr-2">
                                        <input type="file" title='Click to add Files' name="favicon" />
                                        <br>
                                        @if ($errors->has('favicon'))
                                        <span class="text-danger">{{ $errors->first('favicon') }}</span>
                                        @endif

                                    </div>--}}
                                </div>
                            </div>

                        </div>

                        <p class="mb-1">{{__('Site Tag Image')}}: <code>{{__('expected size is 64x64px')}}</code></p>
                        <div class="form-row p-5">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <div class="px-5">
                                        <div class="site_tag_image" id="site_tag_image">
                                            <div class="input-images"></div>
                                        </div>
                                    </div>
                                    {{--<div role="button" class="btn btn-primary mr-2">
                                        <input type="file" title='Click to add Files' name="site_tag_image" />
                                        <br>
                                        @if ($errors->has('site_tag_image'))
                                        <span class="text-danger">{{ $errors->first('site_tag_image') }}</span>
                                        @endif

                                    </div>--}}
                                </div>
                            </div>
                            <div class="col-md-6  d-md-block  d-sm-none">
                                <div class="img-favicon">
                                    <img src="{{ setting('backend.logo_favicon.site_tag_image') }}" alt="Og Meta Image" class="img-thumbnail">
                                </div>

                            </div>
                        </div>


                        <div class="wizard-action text-left">
                            <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit')}}</button>
                        </div>

                    </form>

                    {{-- @elseif($variable != null)
                               <form action="" method="POST" enctype="multipart/form-data" class="wma-form">
                                   @csrf
                                   <p class="mb-1">{{__('Logo:')}} <code>expected size is 32x32px</code></p>
                    <div class="form-row">
                        <div class="col-md-10 col-sm-12">
                            <div class="form-group">
                                <div role="button" class="btn btn-primary mr-2">
                                    <input type="file" title='Click to add Files' name="logo" />
                                    <br>
                                    @if ($errors->has('logo'))
                                    <span class="text-danger">{{ $errors->first('logo') }}</span>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2  d-md-block  d-sm-none">
                            <div class="img-favicon">
                                @if(!empty($logoFavicon->logo))
                                <img src="{{asset('upload/frontend/'.$logoFavicon->logo)}}" alt="{{$logoFavicon->logo}}" class="img-thumbnail">
                                @else
                                <img src="{{asset('upload/backend/noimage.jpg')}}" alt="Og Meta Image" class="img-thumbnail">
                                @endif
                            </div>

                        </div>
                    </div>

                    <p class="mb-1">{{__('Favicon:')}} <code>expected size is 32x32px</code></p>
                    <div class="form-row">
                        <div class="col-md-10 col-sm-12">
                            <div class="form-group">
                                <div role="button" class="btn btn-primary mr-2">
                                    <input type="file" title='Click to add Files' name="favicon" />
                                    <br>
                                    @if ($errors->has('favicon'))
                                    <span class="text-danger">{{ $errors->first('favicon') }}</span>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2  d-md-block  d-sm-none">
                            <div class="img-favicon">
                                @if(!empty($logoFavicon->favicon))
                                <img src="{{asset('upload/frontend/'.$logoFavicon->favicon)}}" alt="{{$logoFavicon->favicon}}" class="img-thumbnail">
                                @else
                                <img src="{{asset('upload/backend/noimage.jpg')}}" alt="Og Meta Image" class="img-thumbnail">
                                @endif
                            </div>

                        </div>
                    </div>

                    <p class="mb-1">{{__('Site Tag Image:')}} <code>expected size is 32x32px</code></p>
                    <div class="form-row">
                        <div class="col-md-10 col-sm-12">
                            <div class="form-group">
                                <div role="button" class="btn btn-primary mr-2">
                                    <input type="file" title='Click to add Files' name="site_tag_image" />
                                    <br>
                                    @if ($errors->has('site_tag_image'))
                                    <span class="text-danger">{{ $errors->first('site_tag_image') }}</span>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="col-md-2  d-md-block  d-sm-none">
                            <div class="img-favicon">
                                @if(!empty($logoFavicon->site_tag_image))
                                <img src="{{asset('upload/frontend/'.$logoFavicon->site_tag_image)}}" alt="{{$logoFavicon->site_tag_image}}"
                                    class="img-thumbnail">
                                @else
                                <img src="{{asset('upload/backend/noimage.jpg')}}" alt="Og Meta Image" class="img-thumbnail">
                                @endif
                            </div>

                        </div>
                    </div>


                    <div class="wizard-action text-left">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Update')}}</button>
                    </div>
                    </form>
                    @endif --}}
                </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('page-script')
    @include('backend.pages.settings.internal-assets.js.logo-page-scripts')
@endsection
