@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
@include('backend.pages.students.internal-assets.css.activation_slider_btn')
@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active text-capitalize">{{__($class_name)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header d-block">
                    <h6 class="card-title">{{__($title)}}</h6>
                </div>
                <div class="card-body ">
                    <div class="row my-3">
                        <div class="col-11 mx-auto">
                            <input type="checkbox" name="all_student" id="checkAllStudent" value="allStudent">
                            <label for="checkAllStudent">All Student</label>
                        </div>
                    </div>

                    <div class="table-responsive style-scroll">

                        <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Phone')}}</th>
                                    <th>{{__('Class Roll')}}</th>
                                    <th>{{__('Age')}}</th>
                                    <th>{{__('Height')}}</th>
                                    <th>{{__('Blood Group')}}</th>
                                    <th>{{__('Admission Date')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('ID Card')}}</th>
                                    <th>{{__('Option')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($students as $key => $data)
                                <tr>
                                    <td><input type="checkbox" name="student" class="studentCheckBox" value="{{ $data->id }}"></td>
                                    <th>{{__($data->unique_id)}}</th>
                                    <th>{{__($data->user->name)}}</th>
                                    <td>{{__($data->user->phone)}}</td>
                                    <td>{{__($data->class_rol)}}</td>
                                    <td>{{__($data->age)}}</td>
                                    <td>{{__($data->height)}}</td>
                                    <td>{{__($data->blood_group)}}</td>
                                    <td>{{__(date('D, d M Y',strtotime($data->admission_date)))}}</td>
                                    <td>
                                        <label class="switch float-right">
                                            <input type="checkbox" {{ $data->status?'checked':'' }} id="{{ $data->id }}" class="studentActivationBtn">
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.student.download',$data->id) }}" class="" target="_blank">
                                            <img src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="" class="img-fluid">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="javascript:void(0)" id="{{ $data->id }}" title="Promoted Next Class" class="btn btn-success btn-circle studentPromotionBtn">
                                            <i class="material-icons">call_made</i>
                                        </a>
                                        <a href="{{ route('admin.students.show',$data->id) }}" class="btn btn-info  btn-circle">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a href="javascript:void(0)" title="Delete" class="btn btn-danger btn-circle deleteRow">
                                            <i class="material-icons">delete</i>
                                            <form action="{{ route('admin.student.destroy',$data->id) }}" method="get">
                                                @csrf
                                            </form>
                                        </a>



                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-script')
<script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@include('backend.pages.students.internal-assets.js.student_activation')
@include('backend.pages.students.internal-assets.js.promotion-next')
@include('backend.pages.students.internal-assets.js.delete-warning')
@include('backend.pages.students.internal-assets.js.select-all-checkbox')

@endsection
