<script>

    $('.studentPromotionBtn').on('click', function () {
        let checkbox = document.getElementsByClassName('studentCheckBox');
        let selected = [];
        for (let cb = 0; cb < checkbox.length; cb++){
            if (checkbox[cb].checked){
                selected.push(checkbox[cb].value);
            }
        }
        if (checkbox.length != selected.length){
            document.getElementById("checkAllStudent").checked = false;
        }
        if (document.getElementById('checkAllStudent').checked){
            selected.push(document.getElementById('checkAllStudent').value);
        }
        // return;
        // var selected = $(this).attr('id');
        if (selected.length <= 0){
            swal({
                {{--title: "{{__('No user has been selected')}}",--}}
                text: "{{__('No user has been selected')}}",
                icon: "warning",
                buttons: {
                    cancel: true,
                    confirm: false,
                },
            })
        }
        var url = "{{route('admin.student-promotion', '')}}"+"/"+selected;
        $.ajax({
            type:'get',
            url: url,
            success:function (data) {
                console.log(data);
                // do nothing;
                if (data["alert-type"] == 'success'){
                    toastr.success(data["message"]);
                    setInterval('location.reload()', 1300);
                }else {
                    toastr.error(data["message"]);
                    // setInterval('location.reload()', 1300);
                }
             }
        });
    });
</script>
