<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ID Card</title>
</head>

<body>
<div style="background-image: url({{ $bg }}); height: 270px; width: 400px; border: 1px dashed lightgray; border-radius: 10px; background-position: center; background-repeat: no-repeat; background-size: cover; margin: 0 auto; overflow: hidden">
    <table style="width: 100%;">
        <tr>
            <td style="width: 20%; padding: 10px; text-align: right;">
                <img style="max-height: 65px; max-width: 45px;" src="{{ $logo }}"
                     alt="logo">
            </td>
            <td style="width: 80%; text-align: left;">
                <p style="font-size: 14pt; font-weight: bold; color: #efefef;">{{__(setting('backend.general.site_name'))}}</p>
            </td>
        </tr>
    </table>
    <table style="width: 100%;">
        <tr>
            <td style="width: 35%; float: left; text-align: center;">
                <img style="float: right; width: 120px; height: 130px; margin: -5px 0 5px 10px; text-align: center; border-radius: 10px; border: 1px solid #efefef; display: block;" src="{{ $avatar }}" width="100px" height="100px" alt="">
                <h3 style="color: #ffffff;">{{ __('ID CARD') }}</h3>
            </td>
            <td style="width: 65%;">
                <table style="width: 100%; font-size: 12pt; margin: -15px 0 0 0;">
                    <tr>
                        <td style="width: 35%; text-align: right;">
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold;text-transform: capitalize;">{{ __('Name') }} : </span>
                        </td>
                        <td style="width: 65%;">
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold; text-transform: capitalize;">{{ config('app.locale') == 'en'? $student->user->name:$student->user->profile->name_utf8 }}</span>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: right;">
                            <span style="color: rgba(35,14,132,0.99);"><span style="text-transform: capitalize; font-weight: bold;">{{ __('Student ID') }} : </span></span>
                        </td>
                        <td>
                            <span style="color: rgba(35,14,132,0.99);"><span style="font-weight: bold;">{{ $student->unique_id }}</span></span>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: right;">
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold; text-transform: capitalize;">{{ __('Class') }} : </span>
                        </td>
                        <td>
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold; text-transform: capitalize;">{{ __($student->classRoom->name) }}</span>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: right;">
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold;  text-transform: capitalize;">{{ __('Roll No') }} : </span>
                        </td>
                        <td>
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold;  text-transform: capitalize;">{{ __($student->class_rol) }}</span>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: right;">
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold; text-transform: capitalize;">{{ __('BG') }} : </span>
                        </td>
                        <td>
                            <span style="color: rgba(35,14,132,0.99); font-weight: bold; text-transform: capitalize;">{{ $student->blood_group }}</span>
                        </td>
                    </tr>
                </table>

                <table style="width: 100%;">
                    <tr>
                        <td style="width: 65%"></td>
                        <td style="width: 35%; border-top: 1px dotted black; text-align: center;"><h6 style="margin: 50px 0px 0px 100px;">{{ __('Head Master') }}</h6></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table  style="width: 100%;">
        <tr>
            <td style="width: 30%; padding-left: 45px; color: #efefef;">

            </td>
            <td style="width: 30%; color: #efefef;">
                <h6 style="text-transform: capitalize"> {{ __('Emergency call') }} : {{__($student->user->phone)}}</h6>
            </td>
        </tr>
    </table>
</div>
</body>

</html>
