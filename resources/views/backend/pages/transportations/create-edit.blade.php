@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white" href="{{ route('admin.transportations.index') }}">{{__('Transportations')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-12 pl-0">
                        <h6 class="card-title ">{{__($title)}}</h6>

                    </div>
                </div>
                <form class="" action="{{ $transportation?route('admin.transportation.update',$transportation->id):route('admin.transportations.store') }}" method="POST">
                        @csrf
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Vehicle reg no:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="vehicle_reg_no" id="name"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Vehicle reg no')}}" value="{{ $transportation ? $transportation->vehicle_reg_no : old('vehicle_reg_no') }}">
                                    <br>
                                    @if ($errors->has('vehicle_reg_no'))
                                        <span class="text-danger">{{ $errors->first('vehicle_reg_no') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Employee:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <select class="form-control form-control-lg" name="employee_id">
                                        <option value="">{{__('Select an Employee')}}</option>
                                        @foreach($employees as $key => $data)
                                            <option value="{{$data->id}}" {{ $transportation ? ($transportation->employee_id==$data->id?'selected':'') : '' }}>{{$data->user->name}}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                    @if ($errors->has('employee_id'))
                                        <span class="text-danger">{{ $errors->first('employee_id') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Vehicle no:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="vehicle_no" id="vehicle_no"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Vehicle no')}}" value="{{ $transportation ?$transportation->vehicle_no:old('vehicle_no') }}">
                                    <br>
                                    @if ($errors->has('vehicle_no'))
                                        <span class="text-danger">{{ $errors->first('vehicle_no') }}</span>
                                    @endif
                                </div>

                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Vehicle Type:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="vehicle_type" id="vehicle_type"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Vehicle type')}}" value="{{ $transportation ?$transportation->vehicle_type:old('vehicle_type') }}">
                                    <br>
                                    @if ($errors->has('vehicle_type'))
                                        <span class="text-danger">{{ $errors->first('vehicle_type') }}</span>
                                    @endif
                                </div>


                            </div>
                        </div>


                        {{--<input type="hidden" name="class_id" value="{{$ebook->id}}">--}}
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

@endsection
