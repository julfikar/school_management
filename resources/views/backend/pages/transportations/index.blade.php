@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-6">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>

                    <div class="col-md-6 col-sm-12 text-right">
                        <a href="{{ route('admin.transportations.create') }}" class="btn btn-success"> {{__('Add New Transportation')}}</a>
                    </div>

                </div>
                <div class="card-body ">

                        @if($transportations->count()>0)
                            <div class="table-responsive style-scroll">

                                <table id="bdcoder" class="table table-striped table-bordered miw-500" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th width="10%">SL No.</th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Incharge')}}</th>
                                        <th>{{__('Phone')}}</th>
                                        <th>{{__('Address')}}</th>
                                        <th>{{__('Option')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($transportations as $key => $data)
                                        <tr>
                                            <th>{{__($loop->index+1)}}</th>
                                            <th>{{__($data->vehicle_reg_no)}}</th>
                                            <td>{{__($data->employee->user->name)}}</td>
                                            <td>{{__($data->vehicle_no) }}</td>
                                            <td>{{$data->vehicle_type }}</td>
                                            <td>
                                                <a href="{{route('admin.transportations.show',$data->id)}}" class="btn btn-info  btn-circle" >
                                                    <i class="material-icons">edit</i>
                                                </a>
                                                <a href="javascript:void(0)" title="Delete"  class="btn btn-danger btn-circle deleteRow">
                                                    <i class="material-icons">delete</i>
                                                    <form action="{{ route('admin.transportation.destroy',$data->id) }}" method="get" class="deleteForm"></form>
                                                </a>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        @else
                            <div class="row">
                            <div class="col-md-12">
                                <div class="text-warning p-3" >
                                    <h6 class="text-center h6">{{__('There is no transportation found')}}</h6>
                                </div>
                            </div>
                          </div>
                        @endif

                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@include('backend.pages.transportations.internal-assets.js.transportation-page-scripts')
@endsection
