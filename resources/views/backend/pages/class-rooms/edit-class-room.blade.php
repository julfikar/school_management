@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <a class="breadcrumb-item text-white" href="{{ auth()->user()->userType->id < 3 ? route('admin.get-classrooms'):route('dashboard') }}">{{__('All Class')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-header">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>
                    <div class="card-body ">
                        <form action="{{ route('admin.update.classroom',$classroom->id) }}" method="POST">
                            @csrf
                            <p class="mb-1">{{__('Class Name')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="name" value="{{ $classroom?$classroom->name:'' }}" class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="Full Name" >
                                <br>
                                @if ($errors->has('name'))
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>

                            <p class="mb-1">{{__('Room No')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <input type="text" name="room_no" value="{{ __($classroom?$classroom->room_no:'') }}" class="form-control" aria-label="Large"
                                       aria-describedby="inputGroup-sizing-sm" placeholder="Room No">
                                <br>
                                @if ($errors->has('room_no'))
                                    <span class="text-danger">{{ $errors->first('room_no') }}</span>
                                @endif
                            </div>


                            <p class="mb-1">{{__('Teacher Name')}}: </p>
                            <div class="input-group input-group-lg mb-3">
                                <select class="selectpicker form-control" name="teacher_id" data-live-search="true" data-size="3" tabindex="-98" style="height: 50px;">
                                    <option {{ !$classroom->teacher?'selected':'' }} disabled value="{{ null }}" data-tokens="{{ null }}">{{__('Select Teacher')}}</option>
                                    @foreach($teachers as $key => $data)
                                        <option {{ $classroom->teacher?($classroom->teacher->id == $data->id?'selected':''):'' }}value="{{$data->id}}">{{$data->user->name}}</option>
                                    @endforeach
                                </select>
                                <br>
                                @if ($errors->has('teacher_id'))
                                    <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                                @endif
                            </div>


                            <div class="wizard-action text-left">
                                <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Update Class Room')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
@endsection
