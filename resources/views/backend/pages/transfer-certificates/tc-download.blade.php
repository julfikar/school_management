<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TC</title>
    <style>
       @page {
             size: 8.5in 11in;

         }
    </style>
</head>

<body style="margin:0;padding: 0;">
{{--style=" width: 595px; height: 842px; background-image: url('https://savarmanikchandra-gps.com/forntend/image/letter_bg.png'); background-repeat: no-repeat; background-size:cover; margin:0 auto;"--}}
<div style="padding: 10px; border: 10px solid #0FB2EF; background-image: url({{ $bg }}); background-repeat: no-repeat; background-position: center; background-size: cover; opacity: 0.05;">

    <table align="center" border="0" width="100%">
        <tr>
            <td style="text-align:right; width: 20%">
                <img align="right" src="{{ $logo }}"
                     alt="" width="80">
            </td>
            <td style="text-align: center; width: 65%">{{__('Government of the People\'s Republic of Bangladesh')}} <br>
                {{__('Ministry of Primary and Mass Education')}} <br>
                {{__('Upazila Education Office')}}</td>
            <td style="width: 15%">
                <img src="{{ $groupLogo }}" alt=""
                     width="80">
            </td>
            <td style="text-align: right; width: 10%"><img style="margin-top:-30px;" src="{{ $mogib100 }}" alt="" width="50"
                                                           align="right"></td>
        </tr>
    </table>
    <h1 style="font-size: 22pt; text-align: center;">{{ setting('backend.general.site_name') }}</h1>
    <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Post Office:')}} {{__('savar')}}, {{__('Upazila:')}} {{__('savar')}}, {{__('District:')}} {{__('Dhaka-1340')}} </p>
    <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Year of establishment:')}} {{ setting('backend.general.established') }} <samp
                style="font-size: 14px;font-weight: 600;">{{ 'EMIS Code' }}:{{ setting('backend.general.site_emis_code') }}</samp></p>
    <h3 style="text-align: center; border-radius: 5px; background-color: #000000; color: #fff; margin: 0px 32% 0px 32%; text-transform: uppercase;">
        {{ __('Transfer Certificate') }}</h3>
    @if(config('app.locale') != 'en')
    <br>
    @endif
    <table align="center" border="0" width="100%">
        <tr>
            <td style="font-size: 12pt; line-height: 12px; width: 33%">
                <table width="100%">
                    <tr>
                        <td width="85">{{__('Serial No:')}}-</td>
                        <td colspan="3" style="border-radius: 3px; height: 19px; width: 85px; border: 1px solid; text-align: center">{{__('TC-3101'.$transfer_certificate->id)}}</td>
                    </tr>
                    <tr>
                        <td width="85">{{__('Date:')}}-</td>
                        <td style="border-radius: 3px; height: 19px; width: 20px; border: 1px solid; text-align: center">{{__(date('d',strtotime($transfer_certificate->updated_at)))}}</td>
                        <td style="border-radius: 3px; height: 19px; width: 20px; border: 1px solid; text-align: center">{{__(date('m',strtotime($transfer_certificate->updated_at)))}}</td>
                        <td style="border-radius: 3px; height: 19px; width: 20px; border: 1px solid; text-align: center">{{__(date('Y',strtotime($transfer_certificate->updated_at)))}}</td>
                    </tr>
                </table>
            </td>
            <td style="width: 30%; text-align: center;">
            </td>
            <td style="text-align: right; color: brown; width: 36%">
                {{__('User ID')}}: {{$transfer_certificate->student->unique_id}}
                <img style="padding-right: 55px;" src="{{ $avatar }}" alt="" height="100px"
                     width="90px">
            </td>
        </tr>
    </table>
    <br>
    <p style="text-align: justify; font-size: 12pt; margin: 0px 15px 0px 15px;">{{__('To the fact that transfer certificate is being granted')}}, {{ config('app.locale') == 'en'?$student->user->name:$student->user->profile->name_utf8 }},
        {{__('Father:')}} {{ config('app.locale') == 'en'?$student->user->profile->father_name:$student->user->profile->father_name_utf8 }},
        {{__('Mother:')}} {{ config('app.locale') == 'en'?$student->user->profile->mother_name:$student->user->profile->mother_name_utf8 }},
        {{__('Village:')}} {{__($transfer_certificate->village)}},
        {{__('Post Office:')}} {{__($transfer_certificate->post_office)}},
        {{__('Upazila:')}} {{__($transfer_certificate->upazila)}}, {{__('District:')}} {{__($transfer_certificate->district)}}, {{__('Date of birth:')}} {{__($transfer_certificate->date_of_birth)}}, {{__('Roll No')}} {{__($transfer_certificate->roll)}}, {{__('in this school')}} {{__($transfer_certificate->academic_year)}}{{__('eng')}} {{__('of the academic year')}} {{__($transfer_certificate->class_name)}} {{__('studied and he passed/fail the annual examination.')}} {{__('He/She owes everything to the school')}} {{__($transfer_certificate->paid_month)}} {{__($transfer_certificate->paid_year)}}{{__('eng')}} {{__('paid up to date.')}} {{__('His/Her learning assessment is satisfactory.')}} {{__('His/Her moral character is good.')}}
    </p>
    <p style="text-align: center;">{{__('I wish him all the best.')}}</p>
    <table style="margin: 0px 0px 0px 50px;">
        <tr>
            <td> {{__('Reasons for dropping out of school')}} </td>
        </tr>
        <tr>
            <td> <input type="checkbox" {{ $transfer_certificate->by_guardian?'checked=""':'' }}> {{__('At the wish of the guardian')}}</td>
        </tr>
        <tr>
            <td> <input type="checkbox" {{ $transfer_certificate->by_complete?'checked=""':'' }}> {{__('Completion of schooling')}}</td>
        </tr>
        <tr>
            <td> <input type="checkbox" {{ $transfer_certificate->by_school_wish?'checked=""':'' }}> {{__('At the will of the school')}}</td>
        </tr>
        <tr>
            <td><input type="checkbox" {{ $transfer_certificate->by_home_change?'checked=""':'' }}> {{__('Change of residence')}} </td>
        </tr>
    </table>
    <br>
    <table width="100%" align="center">
        <tr>
            <td style="text-align: center; width: 33%">
                <p>{{ $headMaster }}</p>
                <p>{{__('Head Master')}}<br> {{__(setting('backend.general.site_name'))}}</p>
            </td>
            <td style="text-align: center; width: 33%">
                <div id="qr">
                    {!! $qrcode !!}
                </div>
            </td>
            <td style="text-align: right; width: 33%"><br><br>{{__('The seal of the school')}}</td>
        </tr>
    </table>
</div>

</body>

</html>
