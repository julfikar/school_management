@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')
    <link rel="stylesheet" href="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.css') }}">
@endsection

@section('content')
    <div id="wrapper-content">
        <div class="row">
            <div class="col">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                    <a class="breadcrumb-item text-white"
                       href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                    <span class="breadcrumb-item active">{{__($title)}}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card card-dark bg-dark">
                    <div class="card-body mt-4">

                        <form action="{{route('admin.transfer-certificate.store')}}" method="post" >
                                    @csrf
                            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="student_id">{{__('Student Name')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                            <div class="input-group input-group-lg mb-3 text-center">
                                <select name="student_id" id="student_id" class="selectpicker form-control" required name="teacher_id" data-live-search="true" data-size="3" tabindex="-98">
                                    <option selected="" disabled="" value="">Select one</option>
                                    @foreach($students as $student)
                                        <option value="{{ $student->id }}">{{ $student->user->name }} ({{$student->unique_id}})</option>
                                    @endforeach
                                </select>
                            </div>



                            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date_of_birth">{{__('Date of birth:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control border-md" value="{{old('date_of_birth')}}" id="date_of_birth" name="date_of_birth" placeholder="{{__('Date of birth as 16/07/2021')}}">
                                <div class="invalid-feedback">
                                    @if ($errors->has('date_of_birth'))
                                        <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="village">{{__('Village:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control border-md" value="{{old('village')}}" placeholder="{{__("Village Name")}}" id="village" name="village">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('village'))
                                                <span class="text-danger">{{ $errors->first('village') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="post_office">{{__('Post Office:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control border-md" value="{{old('post_office')}}" placeholder="{{__('Post Office')}}" id="post_office" name="post_office">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('post_office'))
                                                <span class="text-danger">{{ $errors->first('post_office') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="upazila">{{__('Upazila:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control border-md" value="{{old('upazila')}}" placeholder="{{__('Upazila name')}}" id="upazila" name="upazila">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('upazila'))
                                                <span class="text-danger">{{ $errors->first('upazila') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="district">{{__('District:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control border-md" value="{{old('district')}}" placeholder="{{__('District name')}}" id="district" name="district">
                                        <div class="invalid-feedback">
                                            @if ($errors->has('district'))
                                                <span class="text-danger">{{ $errors->first('district') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" class="form-control border-md" value="{{date('Y',time()) }}" placeholder="{{__('Academc year')}}" id="academic_year" name="academic_year">

                            <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Cause:')}}</label> </p>
                            <div class="">
                                <div class="form-check">
                                    <input name="reasone" type="radio" class="form-check-input" id="exampleCheck3" value="by_complete">
                                    <label class="form-check-label" for="exampleCheck3">{{__('By Completing of School')}}</label>
                                </div>
                                <div class="form-check">
                                    <input name="reasone" type="radio" class="form-check-input" id="exampleCheck5" value="by_school_wish">
                                    <label class="form-check-label" for="exampleCheck5">{{__('By School Wish')}}</label>
                                </div>
                                <div class="form-check">
                                    <input name="reasone" type="radio" class="form-check-input" id="exampleCheck4" value="by_guardian">
                                    <label class="form-check-label" for="exampleCheck4">{{__('By Guardian wish')}}</label>
                                </div>
                                <div class="form-check">
                                    <input name="reasone" type="radio" class="form-check-input" id="exampleCheck7" value="by_home_change">
                                    <label class="form-check-label" for="exampleCheck7">{{__('By home change')}}</label>
                                </div>
                            </div>


                            <div class="py-2 mb-4 text-center">
                                <button type="submit" class="btn btn-theme-green btn-lg bg-danger text-white">{{__('Submit the application')}}</button>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
            <script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>
    @include('backend.pages.subjects.internal-assets.js.subject-page-scripts')
@endsection
