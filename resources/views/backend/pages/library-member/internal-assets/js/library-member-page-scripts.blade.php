<script >
    $(function (){
        $('.deleteRow').on('click', deleteASingleRow)
    });

    function deleteASingleRow(){
        swal({
            title: "{{__('Are you sure?')}}",
            text: "{{__('Once you delete, You can\'t recover this data')}}",
            icon: "warning",
            dangerMode: true,
            buttons: {
                cancel: true,
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    closeModal: true
                },
            },
        }).then((value) => {
            if(value){
                $(this).find('form').submit();
            }
        });

    }
</script>
