<script !src="">
    $(function () {
        $('.ebook-thumbnail').imageUploader({
            imagesInputName: 'thumbnail',
            maxFiles: 1,
        });
        $('#ebookDeleteBtn').on('click', deleteAccountAlert);
    });

    function deleteAccountAlert() {
        swal({
            title: "Are you sure?",
            text: "Once you delete, You can\'t recover this file",
            icon: "warning",
            dangerMode: true,
            buttons: {
                cancel: true,
                confirm: {
                    text: "Delete",
                    value: true,
                    visible: true,
                    closeModal: true
                },
            },
        }).then((value) => {
           if(value){
                document.getElementById('deleteForm').submit();
            }
        });
    }
</script>
