@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-6">
                        <h6 class="card-title">{{__($title)}}</h6>
                    </div>

                    <div class="col-md-6 col-sm-12 text-right">
                        <a href="{{ route('admin.ebooks.create') }}" class="btn btn-success"> {{__('Add New E-Book')}}</a>
                    </div>

                </div>
                <div class="card-body ">

                    <div class="row">
                        @if($ebooks->count()>0)
                            @foreach($ebooks as $ebook)

                            <div class="col-md-3">
                                <div class="card text-white rounded">
                                    {{--https://i.postimg.cc/CxckQJW1/6.png--}}
                                    <img class="card-img-top rounded-top" src="{{ asset('upload/e_books/thumbnails/'.$ebook->thumbnail) }}" alt="Card image cap" height="180">
                                    <div class="card-body bg-dark">
                                        <h5 class="card-title text-center">{{ __($ebook->name) }}</h5>
                                        <div class="">
                                            <a href="{{ asset('upload/e_books/'.$ebook->e_book) }}" target="_blank" class="btn btn-primary btn-block">{{__('Read book')}}</a>
                                        </div>

                                        <div class="mt-3">
                                            <a href="{{ route('admin.ebooks.show',$ebook->id) }}" class="btn btn-success btn-block">Edit</a>
{{--                                            <div class="">--}}
{{--                                                <a href="{{ route('admin.destroy-subject',$subject->id) }}" class="btn btn-danger btn-sm">Delete</a>--}}
{{--                                            </div>--}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <div class="text-warning p-3" >
                                    <h6 class="text-center h6">{{__('There is no subject found.')}}</h6>
                                </div>
                            </div>
                        @endif

                    </div>



                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-script')
    <script src="{{asset('backend/assets/js/tables-datatable.js')}}"></script>
@endsection
