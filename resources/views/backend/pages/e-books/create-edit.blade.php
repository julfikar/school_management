@extends('backend.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
<div id="wrapper-content">
    <div class="row">
        <div class="col">
            <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-dark ">
                <a class="breadcrumb-item text-white"
                    href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{__('Home')}}</a>
                <a class="breadcrumb-item text-white" href="{{ route('admin.ebooks.index') }}">{{__('All E-Books')}}</a>
                <span class="breadcrumb-item active">{{__($title)}}</span>
                <span class="breadcrumb-info" id="time"></span>
            </nav>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card card-dark bg-dark">
                <div class="card-header">
                    <div class="col-12 pl-0">
                        <h6 class="card-title ">{{__($title)}}</h6>
                        @if($ebook)
                        <button type="button" class="btn btn-warning btn-sm float-right" id="ebookDeleteBtn">Delete</button>
                        @endif
                    </div>
                </div>
                <form class="" action="{{ $ebook?route('admin.ebook.update',$ebook->id):route('admin.ebooks.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-md-7">
                                <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Name:')}}</label> </p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="text" name="name" id="name"  class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm"
                                           placeholder="{{__('Name')}}" value="{{ $ebook ? $ebook->name : old('name') }}">
                                    <br>
                                    @if ($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>

                                @if($ebook)
                                    @if($ebook->e_book)
                                        <a href="{{ asset('upload/e_books/'.$ebook->e_book) }}" target="_blank">
                                            <img src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="" class="img-fluid my-1">
                                        </a>
                                    @endif
                                @endif

                                <p class="mb-1"><label for="cvFile" class="card-title font-weight-bold">{{__('E-Book')}}:</label>
                                    <code>{{ __('please upload E-book in PDF format') }}</code></p>
                                <div class="input-group input-group-lg mb-3">
                                    <input type="file" name="e_book" id="cvFile" class="form-control-file" value="{{ old('e_book') }}">
                                    <br>
                                    @if ($errors->has('e_book'))
                                        <span class="text-danger">{{ $errors->first('e_book') }}</span>
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-5">
                                <div class="mb-3">
                                    @if($ebook)
                                        @if($ebook->thumbnail)
                                            <img src="{{ asset('upload/e_books/thumbnails/'.$ebook->thumbnail) }}" alt="" class="img-fluid" height="100" width="100">
                                        @endif
                                    @endif
                                </div>
                                <p class="h6 mb-3">{{ __('Thumbnail') }}</p>
                                <div class="">
                                    <div class="ebook-thumbnail" id="subject-thumbnail">
                                        <div class="input-images"></div>
                                    </div>
                                </div>

                            </div>
                        </div>



                        {{--<input type="hidden" name="class_id" value="{{$ebook->id}}">--}}
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-wave-light btn-danger btn-lg" type="submit">{{__('Submit form')}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@if($ebook)
<form action="{{ route('admin.ebook.destroy',$ebook->id) }}" method="get" id="deleteForm"></form>
@endif
@endsection

@section('page-script')
    @include('backend.pages.e-books.internal-assets.js.ebook-page-scripts')
@endsection