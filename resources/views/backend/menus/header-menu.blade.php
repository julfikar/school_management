<!-- WRAPPER HEADER ------------------------------------------------------------------------------>
<div id="wrapper-header">
    <!-- NAVABR -->
    <nav class="navbar navbar-expand navbar-dark navbar-danger bg-dark">
        <!-- NAVABR NAV - LEFT -->
        <ul class="navbar-nav">
            <!-- NAV ITEM - SIDEBARTOGGLE -->
            <li class="nav-item">
                <a class="nav-link" href="javascript:void(0);" data-toggle="class" data-target="#wrapper" toggle-class="toggled" >
                    <i data-toggle="switch" data-iconFirst="menu" data-iconSecond="close" class="material-icons">menu</i>
                </a>
            </li>
        </ul>

        <!-- NAVABR NAV - RIGHT -->
        <ul class="navbar-nav ml-auto mr-5">
            <!-- NAV ITEM - LANG -->
            @if(count(config('app.languages')) > 1)
            <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:void(0);"  id="lang" data-toggle="dropdown">
                    <i class="material-icons">language</i>
                </a>
                <div class="dropdown-menu"  style="max-width: 120px">
                    @foreach(config('app.languages') as $language => $lang)
                    <a class="dropdown-item{{ App::currentLocale() == $language ? ' active':'' }}" href="{{ route('set-local',$language) }}">{{ strtoupper($language) }} ({{ $lang }})</a>
                    @endforeach
                </div>
            </li>
            @endif
            <!-- NAV ITEM - MESSAGES -->
            <li class="nav-item dropdown">
                <a class="nav-link {{ $complains->count() > 0?'dropdown-toggle':'' }} no-caret" href="javascript:void(0)" id="messages" data-toggle="dropdown" aria-expanded="false">
                    <i class="material-icons ">mail_outline</i>
                    {!! $complains->count() > 0?'<span class="badge badge-md">'.$complains->count().'</span>':'' !!}
                </a>

                @if($complains->count() > 0)
                    <div class="dropdown-menu">

                        <div class="dropdown-header py-2">
                            <h6 class="dropdown-title">{{ __('message') }}</h6>
                            <a  href="javascript:void(0);" class="dropdown-link ml-auto"><i class="material-icons">more_horiz</i></a>
                        </div>

                        <div class="dropdown-block p-0 style-scroll"  style="max-height: 303px">
                            <div class="box-message">
                                <ul class="message-list">
                                    @foreach($complains as $complain)
                                        <li class="message-item">
                                            <div class="message-img">
                                                <img src="{{ asset('backend/assets/img/profile/blank.png') }}" class=" img-fluid">
                                                <span class="badge badge-state bg-danger"></span>
                                            </div>
                                            <div class="message-content">
                                                <a href="{{ route('admin.wizards.complain.view',$complain->id) }}" class="message-link">{{ $complain->name }}</a>
                                                {{--                                            <span class="badge badge-md badge-info float-right">2</span>--}}
                                                <p class="message-text">{{ $complain->phone }}</p>
                                                <span class="message-time">{{ date('d-M-y h:i:s a', strtotime($complain->created_at)) }}</span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        {{--                    <div class="dropdown-footer py-2 justify-content-center">--}}
                        {{--                        <a class="dropdown-link" href="javascript:void(0);"><i class="material-icons">more_horiz</i></a>--}}
                        {{--                    </div>--}}
                    </div>
                @endif
            </li>
            <!-- NAV ITEM - NOTIFICATIONS -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle no-caret" href="javascript:void(0);" id="notifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">notifications_none</i>
                    <span class="badge badge-md">6</span>
                </a>

                <div class="dropdown-menu">

                    <div class="dropdown-header py-2">
                        <h6 class="dropdown-title">{{ __('notification') }}</h6>
                        <a  href="javascript:void(0);" class="dropdown-link ml-auto"><i class="material-icons">more_horiz</i></a>
                    </div>

                    <div class="dropdown-block p-0 style-scroll"  style="max-height: 303px">
                        <div class="box-notification" >
                            <ul class="notification-list">
                                <li class="notification-item">
                                    <div class="notification-icon bg-primary"><i class="material-icons">cloud_upload</i></div>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)" class="notification-link">file uploaded</a>
                                        <p class="notification-text">more</p>
                                    </div>
                                    <div class="notification-time">1 mn ago</div>
                                </li>
                                <li class="notification-item">
                                    <div class="notification-icon bg-warning"><i class="material-icons">person</i></div>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)" class="notification-link">new user</a>
                                        <p class="notification-text">send message</p>
                                    </div>
                                    <div class="notification-time">3 mn ago</div>
                                </li>
                                <li class="notification-item">
                                    <div class="notification-icon bg-danger"><i class="material-icons">access_time</i></div>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)" class="notification-link">new metting</a>
                                        <p class="notification-text">Jul,22<sup>th</sup> 10:30 AM</p>
                                    </div>
                                    <div class="notification-time">15 mn ago</div>
                                </li>
                                <li class="notification-item">
                                    <div class="notification-icon bg-dark"><i class="material-icons">insert_chart</i></div>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)" class="notification-link">new sondage</a>
                                        <p class="notification-text">vote</p>
                                    </div>
                                    <div class="notification-time">1 hour ago</div>
                                </li>
                                <li class="notification-item">
                                    <div class="notification-icon bg-info"><i class="material-icons">mail</i></div>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)" class="notification-link">new mail</a>
                                        <p class="notification-text">from joedhon@exemple.com</p>
                                    </div>
                                    <div class="notification-time">1 day ago</div>
                                </li>
                                <li class="notification-item">
                                    <div class="notification-icon bg-secondary"><i class="material-icons">toc</i></div>
                                    <div class="notification-content">
                                        <a href="javascript:void(0)" class="notification-link">complete task</a>
                                        <div class="progress">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 90%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div class="notification-time">3 day ago</div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="dropdown-footer py-2 justify-content-center">
                        <a class="dropdown-link" href="javascript:void(0);"><i class="material-icons">more_horiz</i></a>
                    </div>
                </div>
            </li>
            <!-- NAV ITEM - PARAMETRES -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle no-caret d-flex align-items-center" href="javascript:void(0);"  id="settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())

                        <img class="rounded-circle" width="32px" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                    @else
                        <img src="{{ Auth::user()->gender == 'male' ? asset('backend/assets/img/profile/male.jpg'):(Auth::user()->gender == 'female' ? asset('backend/assets/img/profile/female.jpg'):asset('backend/assets/img/profile/other.png'))  }}" class="rounded-circle " width="32px">
                    @endif

{{--                    <span class="d-sm-inline-block d-none pl-2 pr-1 text-capitalize">{{ Auth::user()->name }}</span>--}}
{{--                    <i class="d-sm-inline-block d-none material-icons icon-xs">keyboard_arrow_down</i>--}}
                </a>
                <div class="dropdown-menu" style="max-width: 120px">
                    <p class="text-center my-1"><small class="text-muted">{{ __('Manage Accounts') }}</small></p>
                    <a class="dropdown-item" href="{{ route('admin.profile') }}"><i class="material-icons">person</i> {{ __('Profile') }}</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                document.getElementById('logOutForm').submit();"><i class="material-icons">power_settings_new</i> {{ __('Log Out') }}
                        <form action="{{ route('logout') }}" method="post" id="logOutForm">
                            @csrf
                        </form>
                    </a>
                </div>
            </li>
        </ul>

    </nav>
</div>
<!-- END WRAPPER HEADER -------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------>
