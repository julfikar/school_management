<!-- WRAPPER LEFT -------------------------------------------------------------------------------->
<div id="wrapper-left">
    <!-- SIDEBAR -->
    <div class="sidebar sidebar-dark sidebar-danger bg-dark">
        <!-- SIDEBAR HEADER -->
        <div class="sidebar-header border-fade">
            <!-- SIDEBAR BRAND -->
            <a href="{{ '/' }}" class="sidebar-brand">
                <!-- SIDEBAR BRAND IMG -->
                <img class="sidebar-brand-img" src="{{ setting('backend.logo_favicon.logo') }}" />
                <!-- SIDEBAR BRAND TEXT -->
                <span class="sidebar-brand-text">{{ config('app.name') }}</span>
            </a>
            <!-- SIDEBAR CLOSE -->
            <a href="javascript:void(0);" class="sidebar-close d-md-none" data-toggle="class" data-target="#wrapper" toggle-class="toggled">
                <i class="material-icons icon-sm">close</i>
            </a>
        </div>
        <!-- SIDEBAR CONTAINER -->
        <div class="sidebar-container style-scroll-dark">
            <!-- SIDEBAR PROFILE -->
            <div class="sidebar-profile border-fade">
                <!-- SIDEBAR PROFILE IMG -->
                <div class="d-flex align-items-center">
                    @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                        <img src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" class="img-fluid img-thumbnail sidebar-profile-img" />
                    @else
                        <img src="{{ Auth::user()->gender == 'male' ? asset('backend/assets/img/profile/male.jpg'):(Auth::user()->gender == 'female' ? asset('backend/assets/img/profile/female.jpg'):asset('backend/assets/img/profile/other.png')) }}"
                             class="img-fluid img-thumbnail sidebar-profile-img" />
                    @endif
                </div>

                <!-- SIDEBAR PROFILE INFO -->
                <div class="sidebar-profile-info">
                    <h6>{{ auth()->user()->name }}</h6>
                    <!-- SIDEBAR ACTIONS -->
                    <div class="sidebar-actions">
                        <a href="{{ route('admin.profile') }}" class="keep"><i class="material-icons">person_outline</i></a>
                        <a href="javascript:void(0)" onclick="if (!document.getElementById('messages').parentElement.classList.contains('show')){
                           document.getElementById('messages').parentElement.classList.add('show');
                           document.getElementById('messages').parentElement.querySelector('.dropdown-menu').classList.add('show');
                           document.getElementById('messages').removeAttribute('aria-expanded');
                           document.getElementById('messages').setAttribute('aria-expanded', 'true');}else{document.getElementById('messages').parentElement.classList.remove('show');
                               document.getElementById('messages').parentElement.querySelector('.dropdown-menu').classList.remove('show');
                               document.getElementById('messages').removeAttribute('aria-expanded');
                               document.getElementById('messages').setAttribute('aria-expanded', 'false');}"><i class="material-icons"
                                                                                                                style="font-size: 21px">mail_outline</i></a>

                        <a href="javascript:void(0)" onclick="if (!document.getElementById('notifications').parentElement.classList.contains('show')){
                           document.getElementById('notifications').parentElement.classList.add('show');
                           document.getElementById('notifications').parentElement.querySelector('.dropdown-menu').classList.add('show');
                           document.getElementById('notifications').removeAttribute('aria-expanded');
                           document.getElementById('notifications').setAttribute('aria-expanded', 'true');}else{document.getElementById('notifications').parentElement.classList.remove('show');
                               document.getElementById('notifications').parentElement.querySelector('.dropdown-menu').classList.remove('show');
                               document.getElementById('notifications').removeAttribute('aria-expanded');
                               document.getElementById('notifications').setAttribute('aria-expanded', 'false');}"><i
                                class="material-icons">notifications_none</i></a>

                        <a href="javascript:void(0)" onclick="if(!document.getElementById('settingsMenuTab').classList.contains('open')){document.getElementById('settingsMenuTab').classList.add('open');
                        document.getElementById('settingsMenuTab').querySelector('ul').removeAttribute('style');
                        document.getElementById('settingsMenuTab').querySelector('ul').setAttribute('style', 'display: block');}else {document.getElementById('settingsMenuTab').classList.remove('open');
                               document.getElementById('settingsMenuTab').querySelector('ul').removeAttribute('style');
                               document.getElementById('settingsMenuTab').querySelector('ul').setAttribute('style', 'display: none');}"><i
                                class="material-icons" style="font-size: 21px">settings</i></a>
                    </div>
                </div>
            </div>
            <!-- SIDEBAR NAV -->
            <ul class="sidebar-nav">
                <!-- NAV ITEM -->
                <li class="nav-item {{ strpos(request()->path(), '/dashboard') ? 'active':'' }}">
                    <a href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}" class="nav-link">
                        {{--                        <i class="material-icons">dashboard</i>--}}
                        <i class="fas fa-tachometer-alt"></i>
                        <span class="link-text">{{__('dashboard')}}</span>
                    </a>
                </li>

                @can('teachers')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/teachers') ? 'active':'' }}">
                        <a href="javascript:void(0)" class="nav-link">
                            <i class="fas fa-chalkboard-teacher"></i>
                            <span class="link-text">{{__('Teachers')}}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.teachers.teacher.create') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Add New Teacher') }}</span></a></li>
                            <li><a href="{{ route('admin.teachers.teacher.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('All Teacher') }}</span></a></li>
                        </ul>
                    </li>
                @endcan

                @can('students')
                    <li class="nav-item has-dropdown {{ (request()->is('admin/student*')) ? 'active' : '' }}">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-user-graduate"></i>
                            <span class="link-text">{{ __('Students') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.students.create') }}" class="nav-link"> <i class="material-icons">add</i> <span
                                        class="link-text">{{ __('Add New Student') }}</span>
                                </a>
                            </li>
                            @foreach($classRooms as $classRoom)
                                <li><a href="{{ route('admin.get-class-students',$classRoom->id) }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                            class="link-text">{{ __($classRoom->name) }}</span>
                                        <span class="badge badge-md">{{__($classRoom->student->count())}}</span>
                                    </a>
                                </li>
                            @endforeach

                            <li><a href="{{ route('admin.admissions.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('New Admitted Students') }}</span></a></li>
                        </ul>
                    </li>
                @endcan

                @can('exam')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/exams') ? 'active':'' }}">
                        <a href="javascript:void(0)" class="nav-link">
                            {{--   <i class="material-icons">drive_file_rename_outline</i>--}}
                            <i class="fas fa-chalkboard"></i>
                            <span class="link-text">{{__('Exams')}}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.exams.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('All Exam') }}</span></a></li>
                            <li><a href="{{ route('admin.exams.routine') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Routine') }}</span></a></li>
                            <li><a href="{{ route('admin.student-exams.admit-card.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Admit Card') }}</span></a></li>
                        </ul>
                    </li>

                    <li class="nav-item {{ strpos(request()->path(), '/result') ? 'active':'' }}">
                        <a href="{{ route('admin.student-exams.index') }}" class="nav-link">
                            <i class="fas fa-diagnoses"></i>
                            <span class="link-text">{{__('Students result')}}</span>
                        </a>
                    </li>
                @endcan

                @can('class')
                    <li class="nav-item  has-dropdown {{ strpos(request()->path(), '/class') ? 'active':'' }}">
                        <a href="javascript:void(0)" class="nav-link">
                            {{--                        <i class="material-icons">class</i>--}}
                            <i class="fas fa-school"></i>
                            <span class="link-text">{{__('Classes')}}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.get-classrooms') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{__('All class') }}</span>
                                </a>
                            </li>
                            @foreach($classRooms as $classRoom)
                                <li><a href="{{ route('admin.get-class-subjects',$classRoom->id) }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                            class="link-text">{{ __($classRoom->name) }}</span>
                                        <span class="badge badge-md">{{ __($classRoom->subjects->count()) }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endcan

                @can('accounts')
                    <li class="nav-item has-dropdown {{ (request()->is('admin/accounts/*')) ? 'active':'' }}" id="settingsMenuTab">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="material-icons">account_balance_wallet</i>
                            <span class="link-text">{{ __('Accounts') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.account-settings.index') }}" class="nav-link">
                                    <i class="material-icons">add</i>
                                    <span class="link-text">{{ __('Account Settings') }}</span>
                                </a>
                            </li>
                            @foreach($accountSettings as $accountSetting)
                                @php( $url = 'admin.'.$accountSetting->slug.'.index')
                                <li>
                                    <a href="{{ route($url) }}" class="nav-link">
                                        <i class="material-icons">chevron_right</i>
                                        <span class="link-text">{{ __(str_replace('_',' ',$accountSetting->name)) }}</span>
                                    </a>
                                </li>
                            @endforeach
                            <hr>
                            @can('salary')
                                <li><a href="{{ route('admin.salary.pay') }}" class="nav-link">
                                        <i class="material-icons">chevron_right</i>
                                        <span class="link-text">{{ __('Salary') }}</span>
                                    </a>
                                </li>
                            @endcan
                            <li><a href="{{ route('admin.cash-transactions.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('Cash Transaction') }}</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endcan

                @can('reports')
                    <li class="nav-item has-dropdown {{ (request()->is('admin/reports/*')) ? 'active' : '' }}" id="settingsMenuTab">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fa fa-address-card"></i>
                            <span class="link-text">{{ __('Reports') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.student-report.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('Student Reports') }}</span>
                                </a>
                            </li>

                            <li><a href="{{ route('admin.teacher-report.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('Teacher Reports') }}</span>
                                </a>
                            </li>
                            <li><a href="{{ route('admin.employee-report.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('Employee Reports') }}</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endcan

                @can('app settings')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/settings/') ? 'active':'' }}" id="settingsMenuTab">
                        <a href="javascript:void(0);" class="nav-link">
                            {{--           <i class="material-icons">build</i>--}}
                            <i class="fas fa-cogs"></i>
                            <span class="link-text">{{ __('Settings') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.settings.general') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('General Settings') }}</span></a></li>

                            <li><a href="{{ route('admin.menu.category.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Menu Settings') }}</span></a></li>

                            <li><a href="{{ route('admin.settings.logo-favicon') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Logo Favicon') }}</span></a></li>
                            <li><a href="{{ route('admin.settings.seo') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Seo Settings') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.settings.smtp') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('smtp settings') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.settings.sms') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('sms settings') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.settings.custom-css') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('custom css') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.settings.custom-js') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('custom js') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.settings.insert-header-footer') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('insert header footer') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.social-link.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Social Link') }}</span></a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/pages/') ? 'active':'' }}" id="settingsMenuTab">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="material-icons">web_asset</i>
                            <span class="link-text">{{ __('Pages') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.pages.create') }}" class="nav-link"> <i class="material-icons">add</i> <span
                                        class="link-text">{{ __('Add new page') }}</span></a></li>
                            @foreach($pages as $page)
                                <li><a href="{{ route('admin.pages.show',$page->id) }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                            class="link-text">{{ __($page->name) }}</span></a></li>
                            @endforeach
                        </ul>
                    </li>
                @endcan

                @can('communication')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/communication/') ? 'active':'' }}" id="settingsMenuTab">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="material-icons">contact_phone</i>
                            <span class="link-text">{{ __('Communication') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.communication.phone') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Phone Call') }}</span></a></li>
                            <li><a href="{{ route('admin.communication.sms.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('SMS') }}</span></a></li>
                            <li><a href="{{ route('admin.communication.email.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('EMAIL') }}</span></a></li>
                            <li><a href="{{ 'https://meet.google.com/' }}" target="_blank" class="nav-link"> <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Online Meeting / Class') }}</span></a></li>
                        </ul>
                    </li>
                @endcan

                @can('wizards')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/wizards/') ? 'active':'' }}" id="settingsMenuTab">
                        <a href="javascript:void(0);" class="nav-link">
                            {{--           <i class="material-icons">build</i>--}}
                            <i class="fas fa-hat-wizard"></i>
                            <span class="link-text">{{ __('Wizard') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li>
                                <a href="{{ route('admin.wizards.sliders.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Slider') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.wizards.video_sliders.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Video Slider') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin.wizards.key_persons.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Key Person') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin.wizards.notice_board.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Notice Board') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{ route('admin.wizards.emergency_services.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Emergency Services') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{route('admin.wizards.sub-slider.index')}}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Sub Slider') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{route('admin.wizards.important_links.index')}}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Important Links') }}</span>
                                </a>
                            </li>

                            <li>
                                <a href="{{route('admin.wizards.complain.index')}}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('complain') }}</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endcan

                @can('app settings')
                    <li class="nav-item  {{ strpos(request()->path(), '/attendances') ? 'active':'' }}" id="settingsMenuTab">
                        <a href="javascript:void(0)" class="nav-link">
                            <i class="fas fa-atom"></i>
                            <span class="link-text">{{ __('Attendance') }}</span>
                        </a>
                    </li>
                @endcan

                <li class="nav-item has-dropdown {{ strpos(request()->path(), '/blog') ? 'active':'' }}" id="settingsMenuTab">
                    <a href="javascript:void(0)" class="nav-link">
                        <i class="fas fa-blog"></i>
                        <span class="link-text">{{ __('Blogs') }}</span>
                        <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                    </a>
                    <ul class="dropdown-list">
                        @can('blog settings')
                            <li>
                                <a href="{{ route('admin.blogfolders.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Blog Folder') }}</span>
                                </a>
                            </li>
                        @endcan

                        @can('blog')
                            <li>
                                <a href="{{ route('admin.blogposts.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i> <span class="link-text">{{ __('Blog Post') }}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>

                @can('galleries')
                    <li class="nav-item  {{ strpos(request()->path(), '/galleries/') ? 'active':'' }}" id="settingsMenuTab">
                        <a href="{{ route('admin.galleries.galleryfolder.index') }}" class="nav-link">
                            <i class="fas fa-images"></i>
                            <span class="link-text">{{ __('Gallery') }}</span>
                        </a>
                    </li>
                @endcan

                @can('testimonial & transfer certificate')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/testimonial') ? 'active':'' }}">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-file"></i>
                            <span class="link-text">{{ __('Testimonial') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li>
                                <a href="{{ route('admin.testimonial.create') }}" class="nav-link"> <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('Create new Testimonial') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.testimonial.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('All Testimonial') }}</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/transfer-certificate') ? 'active':'' }}" id="tc">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-certificate"></i>
                            <span class="link-text">{{ __('Transfer Certificate') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li>
                                <a href="{{ route('admin.transfer-certificate.create') }}" class="nav-link"> <i class="material-icons">add</i>
                                    <span class="link-text">{{ __('Create new TC') }}</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.transfer-certificate.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('All Transfer Certificate') }}</span>
                                </a>
                            </li>

                        </ul>
                    </li>
                @endcan

                @can('user')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/user/') ? 'active':'' }}">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-users-cog"></i>
                            <span class="link-text">{{ __('Users, roles and permissions') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.user.create-role') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Create new Role') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.user.all-role') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('User Roles') }}</span></a>
                            </li>
{{--                            <li><a href="{{ route('admin.user.create-admin') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span--}}
{{--                                        class="link-text">{{ __('Create new Admin') }}</span></a>--}}
{{--                            </li>--}}
                            <li><a href="{{ route('admin.user.get-admin') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Admin') }}</span></a></li>

                            <li><a href="{{ route('admin.user.get-guardian') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('All Guardians') }}</span></a>
                            </li>
                        </ul>
                    </li>
                @endcan

                @can('employee')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/employee') ? 'active':'' }}">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-briefcase"></i>
                            <span class="link-text">{{ __('Employees') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.designation.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Designations') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.employee.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('All Employees') }}</span></a>
                            </li>
                        </ul>
                    </li>
                @endcan

                @can('library')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/library') ? 'active':'' }}">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-book-reader"></i>
                            <span class="link-text">{{ __('Library') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.library-member.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('Members') }}</span></a>
                            </li>
                            <li><a href="{{ route('admin.physicalbooks.index') }}" class="nav-link">
                                    <i class="material-icons">chevron_right</i>
                                    <span class="link-text">{{ __('Physical Book') }}</span>
                                </a>
                            </li>
                            <li><a href="{{ route('admin.ebooks.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('E-Book') }}</span></a>
                            </li>
                        </ul>
                    </li>
                @endcan

                @can('hostel')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/hostel') ? 'active':'' }}">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-hotel"></i>
                            <span class="link-text">{{ __('Hostel') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list">
                            <li><a href="{{ route('admin.hostels.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('All Hostel') }}</span></a>
                            </li>
                            @foreach($hostels as $hostel)
                                <li>
                                    <a href="{{ route('admin.get-hostel-dwellers',$hostel->id) }}" class="nav-link">
                                        <i class="material-icons">chevron_right</i>
                                        <span class="link-text">{{ __($hostel->name) }}</span>
                                        <span class="badge badge-md">{{__($hostel->dwellers->count())}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endcan

                @can('transportation')
                    <li class="nav-item has-dropdown {{ strpos(request()->path(), '/transportation') ? 'active':'' }}">
                        <a href="javascript:void(0);" class="nav-link">
                            <i class="fas fa-truck-moving"></i>
                            <span class="link-text">{{ __('Transportations') }}</span>
                            <span class="badge badge-md"><span class="material-icons h6" >chevron_right</span></span>
                        </a>
                        <ul class="dropdown-list ">
                            <li><a href="{{ route('admin.transportations.index') }}" class="nav-link"> <i class="material-icons">chevron_right</i> <span
                                        class="link-text">{{ __('All Transportation') }}</span></a>
                            </li>
                            @foreach($transportations as $transportation)
                                <li>
                                    <a href="{{ route('admin.get-transportation-passengers',$transportation->id) }}" class="nav-link">
                                        <i class="material-icons">chevron_right</i>
                                        <span class="link-text">{{ __($transportation->vehicle_no) }}</span>
                                        <span class="badge badge-md">{{__($transportation->passengers->count())}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endcan
            </ul>

            <div style="height: 80px;"></div>
        </div>
    </div>
</div>
<!-- END WRAPPER LEFT ---------------------------------------------------------------------------->
