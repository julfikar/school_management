@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.blogs.internal-assets.css.blog-css')
    <link rel="stylesheet" href="{{ asset('forntend/css/pdf-viewer/pdfannotate.css') }}">
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $blog->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12 ">
                <div class="">
                    <nav class="breadcrumb notice-bg-color">
                        <h5><span class="breadcrumb-item active text-white">{{ __(Str::limit($title,30,'...')) }}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body shadow">
                            <div class="card-body p-2 barta-box-shadow-sm">
                                <a class="text-uppercase h6" href="{{ route('blog.single-folder',$blog->folder->id) }}">{{$blog->folder->name}}</a>
                                <div class="post-title-area pb-1 mb-2 border-bottom">
                                    <h2 class="post-title pt-2">{{$blog->title}}</h2>
                                    <div class="post-meta">
                                        <span class="post-author">By {{$blog->user->name}}</span>
                                        <span class="post-date"><i class="fa fa-clock-o"></i> {{ date('d M Y', strtotime($blog->created_at)) }}</span>
                                        <span class="post-date"><i class="fa fa-clock-o"></i>{{ Carbon\Carbon::parse($blog->created_at)->diffForHumans()}} </span>
                                        {{--<span class="post-hits"><i class="fa fa-eye"></i> 33</span>--}}
                                    </div>
                                </div>
                                <hr class="mt-0">
                                <div class="post-content-area">
                                    <div class="post-media post-featured-image">
                                        <img src="{{ asset('upload/blogs/post-thumbnails/'.$blog->thumbnail) }}" class="img-fluid w-100" alt="" >
                                    </div>
                                    <div class="entry-content pt-3">
                                        {!! html_entity_decode($blog->body) !!}
                                    </div><!-- Entery content end -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            @if($blog->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')

@endsection
