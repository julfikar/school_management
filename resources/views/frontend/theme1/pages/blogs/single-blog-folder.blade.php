@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.notice-board.internal-assets.css.notice-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-9 col-md-9">
                <div class="">
                    <nav class="breadcrumb  theme-bg-green">
                        <h5><span class="breadcrumb-item active text-white">{{ __($title) }}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>

                <div class="row">
                    @foreach($blogs as $blog)
                        <div class="col-md-4">
                            <a href="{{ route('blog.single',$blog->slug) }}" class="">
                                <div class="rounded bg-white shadow ">
                                    <img src="{{ asset('upload/blogs/post-thumbnails/'.$blog->thumbnail) }}" alt="" class="w-100 card-img-top">
                                    <div class="p-4 card-img-bottom">
                                        <div class="h6 font-weight-bold mb-3 text-dark">{{__(Str::limit($blog->title,46,''))}}</div>
                                        <p class="mb-2 text-small text-muted text-justify">{{ Str::limit(strip_tags($blog->body),113,'')}}</p>
                                        <a href="" class="text-primary font-weight-bold"> Read more.....</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>


            </div>

            {{--@if($page->right_bar)--}}
            <div class="col-lg-3 col-md-3 col-sm-12">
                @include('frontend.theme1.pages.home-page-sections.single-right-bar')
            </div>
            {{--@endif--}}
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    {{--    @include('frontend.theme1.pages.notice-board.internal-assets.js.notice-js')--}}
@endsection
