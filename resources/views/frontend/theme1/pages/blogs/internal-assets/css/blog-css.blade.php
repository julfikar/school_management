<style>
    /*single news details area*/

    .notice-bg-color{
        background-color:#609513 !important;
    }

   .post-title {
        font-size: 30px;
        line-height: 40px;
        padding: 15px 0 8px 0;
        margin: 0;
    }
    .post-meta span {
        font-size: 12px;
        color: #a3a3a3;
        padding-right: 5px;
        border-right: 1px solid #dedede;
        line-height: 12px;
        display: inline-block;
    }
    .post-meta span:last-child{
        border-right: none;
    }
    .post-meta span.post-date {

    }
    .post-meta span i {
        margin-right: 5px;
    }


</style>
