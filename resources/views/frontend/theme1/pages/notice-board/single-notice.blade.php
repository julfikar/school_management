@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.notice-board.internal-assets.css.notice-css')
    <link rel="stylesheet" href="{{ asset('forntend/css/pdf-viewer/pdfannotate.css') }}">
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12">
                <div class="">
                    <nav class="breadcrumb notice-bg-color">
                        <h5><span class="breadcrumb-item active text-white">{{ __($title) }}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>

                <div class="alert alert-secondary">
                    <h6 class="font-weight-bold text-success pt-2">{{$notice->heading}}</h6>
                    <hr>
                    <p>{{$notice->description}}</p>
                    <hr>
                    <div class="d-flex justify-content-center overflow-hidden">
                        {!! $notice->file ?'<div id="pdf-container" data-src="'.asset('/upload/notice-files/'.$notice->file).'"></div>':'' !!}

                    </div>
                </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @if($notice->file)
        <script src="{{ asset('forntend/js/pdf-viewer/pdf.min.js') }}"></script>
        <script src="{{ asset('forntend/js/pdf-viewer/fabric.min.js') }}"></script>
        <script src="{{ asset('forntend/js/pdf-viewer/arrow.fabric.js') }}"></script>
        <script src="{{ asset('forntend/js/pdf-viewer/pdfannotate.js') }}"></script>
        <script src="{{ asset('forntend/js/pdf-viewer/pdf-js-script.js') }}"></script>
    @endif
@endsection
