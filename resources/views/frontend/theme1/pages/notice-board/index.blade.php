@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.notice-board.internal-assets.css.notice-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12">
                <div class="">
                    <nav class="breadcrumb theme-bg-green">
                   <h5><span class="breadcrumb-item active text-white">{{ __($title) }}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
                @foreach($notice as $note)
                    <a href="{{ route('notice.single',$note->slug) }}">
                        <div class="alert alert-secondary">
                            <h6 class="font-weight-bold text-success pt-2">{{$note->heading}}</h6>
                            <p>{{$note->description}}</p>
                        </div>
                    </a>
                @endforeach

            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
{{--    @include('frontend.theme1.pages.notice-board.internal-assets.js.notice-js')--}}
@endsection
