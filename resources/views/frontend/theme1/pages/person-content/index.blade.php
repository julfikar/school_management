@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.person-content.internal-assets.css.content-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <div class="">
                    <nav class="breadcrumb  notice-bg-color">
                   <h5><span class="breadcrumb-item active text-white">{{ __($title) }}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>

                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($data as $content)
                            @if($content->person_content != null)
                                <div class="carousel-item {{$loop->first?'active':''}}">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="d-flex flex-column align-items-center text-center">
                                                        <img src="{{ asset('/upload/key-persons/'.$content->image) }}" alt="Admin" class="rounded-circle" width="150">
                                                        <div class="mt-3">
                                                            <h4 class="text-success">{{$content->name}}</h4>
                                                            <p class="text-secondary mb-1">{{$content->designation}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="">
                                                        <h4 class="text-success">{{__('Statement')}}:</h4><hr>
                                                        <p class="text-secondary mb-1" style="color: black!important;">{!! $content->person_content !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="fa fa-angle-left fa-3x icon-color" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="fa fa-angle-right fa-3x icon-color" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>

            <div class="col-lg-3 col-md-12">
                @include('frontend.theme1.pages.home-page-sections.single-right-bar')
            </div>
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.person-content.internal-assets.js.content-js')
@endsection
