<style>
    .filter-item{
        display: none;
    }
    .active-item{
        display: block;
    }
    .active-btn{
        color: #ffffff;
        background-color: #DC3545;
        border-color: #DC3545;
    }
    .active-btn:hover{
        color: #ffffff;
        background-color: #609513;
        border-color: #609513;
    }
    .notice-bg-color{
        background-color:#609513 !important;
    }
    .icon-color{
        color: #609513;
    }
    .Notice-image{
        height: 50px;
        width: 50px;
    }
</style>
