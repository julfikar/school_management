@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.notice-board.internal-assets.css.notice-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $page->right_bar?'col-lg-9':'col-lg-12' }} col-md-12">
                <div class="">
                    <nav class="breadcrumb theme-bg-green">
                        <h5><span class="breadcrumb-item active text-white">{{ __($title) }}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
                <div class="col-12">
                    {!! html_entity_decode($page->body) !!}
                </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    {{--    @include('frontend.theme1.pages.notice-board.internal-assets.js.notice-js')--}}
@endsection
