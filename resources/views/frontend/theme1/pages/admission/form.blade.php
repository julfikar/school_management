@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.admission.internal-assets.css.admission-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">

            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12 ">
               <div class="card shadow">
                   <div class="card-header">
                       <h2 class="text-center">{{__('Admission Form')}}</h2>
                   </div>
                    <div class="card-body">
                        <form action="{{ route('store-admission-form') }}" method="post" enctype="multipart/form-data">
                            @csrf

                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font-weight-bold" for="name">{{__('Student Name (in English)')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup> </label>
                                        <input type="text" name="name" value="{{old('name')}}" class="form-control border-md" id="name" placeholder="{{__('Student Name (in English) ')}}" required>
                                    </div>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('name'))
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font-weight-bold" for="name_utf8">{{__('Name In Bangla')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="name_utf8" value="{{old('name_utf8')}}" class="form-control border-md" id="name_utf8" placeholder="{{__('Name In Bangla')}}" required>
                                    </div>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('name_utf8'))
                                            <span class="text-danger">{{ $errors->first('name_utf8') }}</span>
                                        @endif
                                    </div>

                                </div>
                            </div>

                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font-weight-bold">{{__('Father Name:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="father_name" value="{{old('father_name')}}" class="form-control border-md" id="" placeholder="{{__('Father Name')}}" required>
                                    </div>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('father_name'))
                                            <span class="text-danger">{{ $errors->first('father_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="font-weight-bold">{{__('Father\'s Name In Bangla')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="father_name_utf8" value="{{old('father_name_utf8')}}" class="form-control border-md" id="" placeholder="{{__('Father\'s Name In Bangla')}}" required>
                                    </div>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('father_name_utf8'))
                                            <span class="text-danger">{{ $errors->first('father_name_utf8') }}</span>
                                        @endif
                                    </div>

                                </div>

                            </div>

                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <label for="mother_name" class="card-title font-weight-bold">{{__('Mother Name:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                    <input type="text" name="mother_name"  id="mother_name" class="form-control border-md" placeholder="{{__('Mother Name')}}" value="{{old('mother_name') }}">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('mother_name'))
                                            <span class="text-danger">{{ $errors->first('mother_name') }}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="mother_name" class="card-title font-weight-bold">{{__('Mother\'s Name In Bangla')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                    <input type="text" name="mother_name_utf8"  id="mother_name_utf8" class="form-control border-md" placeholder="{{__('বMother\'s Name In Bangla')}}" value="{{old('mother_name_utf8') }}">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('mother_name_utf8'))
                                            <span class="text-danger">{{ $errors->first('mother_name_utf8') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="birthday">{{__('Date of birth:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                    <input type="date" class="form-control border-md" value="{{old('date_of_birth')}}" id="birthday" name="date_of_birth">

                                    <div class="invalid-feedback">
                                        @if ($errors->has('date_of_birth'))
                                            <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="statew">{{__('Gender:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                    <select name="gender" class="custom-select form-control border-md d-block w-100" id="gender" required>
                                        <option selected disabled>{{__('Select Gender')}}</option>
                                        <option value="Male">{{__('Male')}}</option>
                                        <option value="Female">{{__('Female')}}</option>
                                        <option value="NA">{{__('Other')}}</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('gender'))
                                            <span class="text-danger">{{ $errors->first('gender') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="state">{{__('Admission class:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                    <select name="new_class" class="custom-select form-control border-md d-block w-100" id="new_class" required>
                                        <option value="" selected disabled>{{__('Select Class Name')}}</option>
                                        @foreach($classRooms as $key => $data)
                                            <option value="{{$data->id}}">{{__($data->name)}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('new_class'))
                                            <span class="text-danger">{{ $errors->first('new_class') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="state">{{__('Past class:')}} </label>
                                    <select name="old_class" class="custom-select form-control border-md d-block w-100" id="state" >
                                        <option value="" selected disabled>{{__('Select Class Name')}}</option>
                                        @foreach($classRooms as $key => $data)
                                            <option value="{{$data->id}}">{{__($data->name)}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('old_class'))
                                            <span class="text-danger">{{ $errors->first('old_class') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="age">{{__('Age:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                    <input type="text" class="form-control border-md" value="{{old('age')}}" id="age" name="age" placeholder="{{__('Age')}}" required>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('age'))
                                            <span class="text-danger">{{ $errors->first('age') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="state">{{__('Blood Group:')}} </label>
                                    <select name="blood_group" class="custom-select form-control border-md d-block w-100" id="state" >
                                        <option value="" selected disabled>{{__('Select Blood Group')}}</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        @if ($errors->has('blood_group'))
                                            <span class="text-danger">{{ $errors->first('blood_group') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center mb-4">
                                <div class="col-md-12">
                                    <label class="font-weight-bold" for="exampleInputEmail1">{{__('Marksheet of last class:')}} <code class="small">{{__('please upload marksheet in PDF format')}}</code></label>
                                    <div class="custom-file">
                                        <input type="file" name="mark_sheet" class="custom-file-input" id="eereee">
                                        <label class="custom-file-label" for="customFile">{{__('Attach past class marksheet (PDF)')}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row justify-content-center">
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="name">{{__('Mobile No')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                    <input type="text" name="phone" value="{{old('phone')}}" class="form-control border-md" id="name" placeholder="{{__('015XXXXX')}}"
                                           aria-describedby="emailHelp">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('phone'))
                                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold" for="email">{{__('Email')}}:</label>
                                    <input type="email" name="email" value="{{old('email')}}" class="form-control border-md" id="email" placeholder="{{__('example@example.com')}}"
                                           aria-describedby="emailHelp">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('email'))
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="row justify-content-center">
                                <div class="col-lg-6 col-md-6">
                                    <div class="py-2 mt-2 text-center">
                                        <h6 class="text-dark">{{__('present address')}}</h6>
                                    </div>

                                    <div class="form-group">
                                        <label for="presentDistrict" class="card-title font-weight-bold">{{__('District')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="present_district" id="district" list="districtList" class="form-control rounded" required
                                               value="{{ old('present_district') }}" placeholder="{{ __('Type your district name') }}">
                                        <datalist id="districtList">
                                            @foreach($districts as $district)
                                                <option>{{ $district->name }}</option>
                                            @endforeach
                                        </datalist>
                                    </div>

                                    <div class="form-group">
                                        <label for="presentThana" class="card-title font-weight-bold">{{__('Thana')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="present_thana" id="presentThana" list="thanaList" class="form-control rounded" required
                                               value="{{ old('present_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                        <datalist id="thanaList">
                                            @foreach($districts as $district)
                                                @foreach($district->thana as $thana)
                                                    <option value="{{ $thana->name }}">{{ $district->name }}</option>
                                                @endforeach
                                            @endforeach
                                        </datalist>
                                    </div>

                                    <div class="form-group">
                                        <label for="presentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="present_post_office" id="presentPostOffice" list="postOfficeList" class="form-control rounded"
                                               required value="{{ old('present_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                        <datalist id="postOfficeList">
                                            @foreach($districts as $district)
                                                @foreach($district->postOffice as $postOffice)
                                                    <optgroup label="{{ $district->name }}">
                                                        <option value="{{ $postOffice->post_code }}">{{ $postOffice->name }}</option>
                                                    </optgroup>
                                                @endforeach
                                            @endforeach
                                        </datalist>
                                    </div>

                                    <div class="form-group">
                                        <label for="presentCity" class="card-title font-weight-bold">{{__('City / Village')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="present_city" id="presentCity" class="form-control rounded" required
                                               value="{{ old('present_city') }}" placeholder="{{ __('Type your city') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="presentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="present_road" id="presentRoad" class="form-control rounded" required
                                               value="{{ old('present_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                    </div>


                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="py-2 mt-2 text-center">
                                        <h6 class="text-dark">{{__('Permanent Address')}}</h6>
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentDistrict" class="card-title font-weight-bold">{{__('District')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="permanent_district" id="permanentDistrict" list="districtList" class="form-control rounded"
                                               required value="{{ old('permanent_district') }}" placeholder="{{ __('Type your district name') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentThana" class="card-title font-weight-bold">{{__('Thana')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="permanent_thana" id="permanentThana" list="thanaList" class="form-control rounded" required
                                               value="{{ old('permanent_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="permanent_post_office" id="permanentPostOffice" list="postOfficeList" class="form-control rounded"
                                               required value="{{ old('permanent_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentCity" class="card-title font-weight-bold">{{__('City / Village')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="permanent_city" id="permanentCity" class="form-control rounded" required
                                               value="{{ old('permanent_city') }}" placeholder="{{ __('Type your city') }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="permanentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                        <input type="text" name="permanent_road" id="permanentRoad" class="form-control rounded" required
                                               value="{{ old('permanent_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                    </div>
                                </div>
                            </div>


                            <div class="row justify-content-center mb-4">
                               <div class="col-md-12">
                                   <label class="font-weight-bold" for="exampleInputEmail1">{{__('Photo')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup> <code class="small">{{__('expected size 300 x 300 pixels')}}</code></label>
                                   <div class="custom-file">
                                       <input type="file" name="profile_photo" class="custom-file-input" id="dfdssd" required>
                                       <label class="custom-file-label" for="customFile">{{__('Attach your picture')}}</label>
                                   </div>
                               </div>
                            </div>
                            <div class="py-2 mb-4 text-center">
                                <button type="submit" class="btn btn-theme-green btn-lg">{{__('Submit the application')}}</button>
                            </div>
                        </form>
                    </div>
               </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12 order-lg-3 order-md-3 order-3">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.admission.internal-assets.js.admission-js')
@endsection
