<style>
    .border-md {
        border-width: 2px;
    }

    .form-control:not(select) {
        padding: 1rem;
        border-radius: 5px;
    }
    .form-control{
        border-radius: 5px;
    }


    .form-control::placeholder {
        font-weight: bold;
        font-size: 0.9rem;
    }
    .form-control:focus {
        box-shadow: none;
        border-color:#609513;
    }
    .shadow {
        box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
    }
    textarea:focus, input:focus,select:focus {
        color: #000000 !important;
    }

    .custom-file {
        position: relative;
        display: inline-block;
        width: 100%;
        height: calc(2.25rem + 2px);
        margin-bottom: 0;
    }
    .custom-file-input {
        position: relative;
        z-index: 2;
        width: 100%;
        height: calc(2.25rem + 2px);
        margin: 0;
        opacity: 0;
    }
    .custom-file-label {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        z-index: 1;
        height: calc(3rem + 2px);
        padding: .8rem 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        border: 2px solid #d9d9d9;
        border-radius: .25rem;
    }

    .font-size-8px{
        font-size: 8px;
    }
</style>