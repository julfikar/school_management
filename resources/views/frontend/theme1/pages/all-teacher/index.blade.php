@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.all-teacher.internal-assets.css.content-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12">
                <div class="">
                    <nav class="breadcrumb bg-theme-green">
                   <h5><span class="breadcrumb-item bg-theme-green">{{$title}}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
              <div class="row mx-2">
                  @foreach($teachers as $teacher)
                      <div class="card col-lg-4 col-md-6 col-sm-12 px-0 my-2">
                          @if(!$teacher->user->profile_photo_path)
                              <img class="card-img-top" src="{{ $teacher->user->gender == 'male' ? asset('backend/assets/img/profile/male.jpg'):($teacher->user->gender == 'female' ? asset('backend/assets/img/profile/female.jpg'):asset('backend/assets/img/profile/other.png'))  }}" alt="Card image cap">
                          @else
                              <img class="card-img-top" src="{{ asset('storage/'.$teacher->user->profile_photo_path)  }}" alt="Card image cap">
                          @endif
                          <div class="card-body">
                              <h4 class="text-theme-green text-center text-roboto-bold mb-0">{{$teacher->user->name}}</h4>
                          <p class="text-roboto text-center text-uppercase">{{ '(expert in: '. $teacher->expert_in.')' }}</p>

                          </div>
                          <div class="card-footer text-center px-0 pb-0">
                              {!! $teacher->user->phone? '<a href="tel:'.$teacher->user->phone.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fas fa-phone"></i></a>':'' !!}
                              {!! $teacher->user->email? '<a href="email:'.$teacher->user->email.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fas fa-envelope"></i></a>':'' !!}
                              {!! $teacher->user->profile->facebook_link? '<a href="'.$teacher->user->profile->facebook_link.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fa fa-facebook"></i></a>':'' !!}
                              {!! $teacher->user->profile->whatsapp_link? '<a href="'.$teacher->user->profile->whatsapp_link.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fa fa-whatsapp"></i></a>':'' !!}
                              {!! $teacher->user->profile->linked_in? '<a href="'.$teacher->user->profile->linked_in.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fa fa-linkedin"></i></a>':'' !!}
                              <a href="{{route('teacher-explore',$teacher->id)}}">
                                  <button type="submit" class="btn btn-theme-green w-100 mt-1">{{__('Explore')}}</button>
                              </a>
                          </div>
                      </div>
                  @endforeach
              </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.all-teacher.internal-assets.js.content-js')
@endsection
