<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student Result</title>
    <style>
        @page {
            size: 8.5in 11in;
            /*background-image: url('https://savarmanikchandra-gps.com/forntend/image/letter_bg.png');*/
            background-repeat: no-repeat;
            background-position: center;
        }
        .text-dange{
            color: red;
        }
    </style>
</head>

<body style="margin:0;padding: 0;">
<div style="padding: 10px; border: 10px solid #0FB2EF; background-image: url({{ $bg }}); background-repeat: no-repeat; background-position: center; background-size: cover; opacity: 0.05;">

    <table align="center" border="0" width="100%">
        <tr>
            <td style="text-align:right; width: 20%">
                <img align="right" src="{{ $logo }}"
                     alt="" width="80">
            </td>
            <td style="text-align: center; width: 65%">{{__('Government of the People\'s Republic of Bangladesh')}} <br>
                {{__('Ministry of Primary and Mass Education')}} <br>
                {{__('Upazila Education Office')}}</td>
            <td style="width: 15%; text-align: left">
                <img src="{{ $groupLogo }}" alt=""
                     width="80">
            </td>
            <td style="text-align: right; width: 10%"><img style="margin-top:-30px;" src="{{ $mogib100 }}" alt="" width="50"
                                                           align="right"></td>
        </tr>
    </table>
    <h1 style="font-size: 22pt; text-align: center;">{{ setting('backend.general.site_name') }}</h1>
    <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Post Office:')}} {{__('savar')}}, {{__('Upazila:')}} {{__('savar')}}, {{__('District:')}} {{__('Dhaka-1340')}} </p>
    <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Year of establishment:')}} {{ setting('backend.general.established') }}<samp
            style="font-size: 14px;font-weight: 600;">{{ 'EMIS Code' }}:{{ setting('backend.general.site_emis_code') }}</samp></p>
    <h3 style="text-align: center; border-radius: 5px; background-color: #000000; color: #fff; margin: 0px 32% 0px 32%; text-transform: uppercase;">
        {{ __('Student Result') }}</h3>
    <br>
    <table align="center" border="0" width="100%">
        <tr>
            <td style="font-size: 12pt; line-height: 12px; width: 33%">
                <table width="100%">
                    <tr>
                        <td width="80">{{__('Date:')}}-</td>
                        <td style="border-radius: 3px; height: 19px; width: 30px; border: 1px solid; text-align: center"></td>
                        <td style="border-radius: 3px; height: 19px; width: 30px; border: 1px solid; text-align: center"></td>
                        <td style="border-radius: 3px; height: 19px; width: 60px; border: 1px solid; text-align: center"></td>
                    </tr>
                </table>
            </td>
            <td style="width: 30%; text-align: center;">
            </td>
            <td style="text-align: right; color: brown; width: 36%">
{{--                {{__('User ID')}}: {{$student->unique_id}}--}}
{{--                <img style="padding-right: 55px;" src="{{ $avatar }}" alt="" height="100px"--}}
{{--                     width="90px">--}}
            </td>
        </tr>
    </table>
    <table style="width: 100%;">
        <tr>
            <th style="width: 20%; text-align: left;">{{ __('Name') }}</th>
            <th style="width: 80%; text-align: left;">: {{ config('app.locale') == 'en'? $student->user->name:$student->user->profile->name_utf8 }}</th>
        </tr>
        <tr>
            <th style="width: 20%; text-align: left;">{{ __('Class') }}</th>
            <th style="width: 80%; text-align: left;">: {{ __($student->classRoom->name) }}</th>
        </tr>
        <tr>
            <th style="width: 20%; text-align: left;">{{ __('Roll') }}</th>
            <th style="width: 80%; text-align: left;">: {{ __($student->class_rol-1) }}</th>
        </tr>
        <tr>
            <th style="width: 20%; text-align: left;">{{ __('Student ID') }}</th>
            <th style="width: 80%; text-align: left;">: {{ __($student->unique_id) }}</th>
        </tr>
        <tr>
            <th style="width: 20%; text-align: left;">{{ __('Exam') }}</th>
            <th style="width: 80%; text-align: left;">: {{ __($exam->name) }}</th>
        </tr>
        <tr>
            <th style="width: 20%; text-align: left;">{{ __('Education Year') }}</th>
            <th style="width: 80%; text-align: left;">: {{ __($year-1) }}</th>
        </tr>
    </table>
    <br>
    <table style="width: 100%; text-align: left; border: 0.5px solid black;">
        <tr>
            <th style="width: 16%; border: .5px solid black;">Subject</th>
            <th style="width: 16%; border: .5px solid black;">Min. Mark</th>
            <th style="width: 16%; border: .5px solid black;">Obt. Mark</th>
            <th style="width: 16%; border: .5px solid black;">Max. Mark</th>
            <th style="width: 16%; border: .5px solid black;">CGPA</th>
            <th style="width: 16%; border: .5px solid black;">Remark</th>
        </tr>
        @php
            $fail = false;
        @endphp
        @foreach($results as $result)
            @if($result->cgpa == 0)
                @php
                    $fail = true;
                @endphp
            @endif
            <tr>
                <td style="text-align: center; border: .5px solid black;">{{ __($result->subject->name) }}</td>
                <td style="text-align: center; border: .5px solid black;">{{ __($result->exam->min_mark) }}</td>
                <td style="text-align: center; border: .5px solid black;">{{ __($result->got_mark) }}</td>
                <td style="text-align: center; border: .5px solid black;">{{ __($result->exam->marks) }}</td>
                <td style="text-align: center; border: .5px solid black;" {!! $result->cgpa == '0.00' ? 'class="text-danger"':'' !!}>{{ $result->cgpa }}</td>
                <td style="text-align: center; border: .5px solid black;" {!! $result->remark == 'F' ? 'class="text-danger"':'' !!}>{{ $result->remark }}</td>
            </tr>
        @endforeach
        <tr>
            <th colspan="4" style="text-align: right;">{{ __('Total') }}</th>
            @if($fail)
                <th class="text-danger">0.00</th>
                <th class="text-danger">F</th>
            @else
                <th style="text-align: center;">{{ number_format($results->sum('cgpa') / $results->count(),'2','.',',') }}</th>
                <th style="text-align: center;">{{ $results->sum('cgpa') / $results->count() > 4.99 ? 'A+': ($results->sum('cgpa') / $results->count() > 3.99 ? 'A': ($results->sum('cgpa') / $results->count() > 3.49 ? 'A-': ($results->sum('cgpa') / $results->count() > 2.99 ? 'B': ($results->sum('cgpa') / $results->count() > 1.99 ? 'C': ($results->sum('cgpa') / $results->count() > 0 ? 'D':'F'))))) }}</th>
            @endif
        </tr>
    </table>
    <br><br>
    <table width="100%" align="center">
        <tr>
            <td style="text-align: center; width: 33%">
                <p>{{ $headMaster }}</p>
                <p>{{__('Head Master')}}<br> {{__(setting('backend.general.site_name'))}}</p>
            </td>
            <td style="text-align: center; width: 33%">
                <div id="qr">
                    {!! $qrcode !!}
                </div>
            </td>
            <td style="text-align: right; width: 33%"><br><br>{{__('The seal of the school')}}</td>
        </tr>
    </table>
</div>

</body>

</html>
