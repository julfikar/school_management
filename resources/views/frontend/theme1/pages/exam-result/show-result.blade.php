@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.exam-result.internal-assets.css.exam-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12">
                <div class="">
                    <nav class="breadcrumb bg-theme-green">
                        <h5><span class="breadcrumb-item bg-theme-green">{{$title}}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
                <div class="card shadow p-5 mx-auto">
                    <div class="card-header bg-light">
                        <table class="table table-light">
                            <tbody>
                            <tr>
                                <th width="20%">{{ __('Name') }}:</th>
                                <th>{{ $student->user->name }}</th>
                            </tr>
                            <tr>
                                <th width="20%">{{ __('Class') }}:</th>
                                <th>{{ __($student->classRoom->name) }}</th>
                            </tr>
                            <tr>
                                <th width="20%">{{ __('Roll') }}:</th>
                                <th>{{ __($student->class_rol-1) }}</th>
                            </tr>
                            <tr>
                                <th width="20%">{{ __('Student ID') }}:</th>
                                <th>{{ __($student->unique_id) }}</th>
                            </tr>
                            <tr>
                                <th width="20%">{{ __('Exam') }}:</th>
                                <th>{{ __($exam->name) }}</th>
                            </tr>
                            <tr>
                                <th width="20%">{{ __('Education Year') }}:</th>
                                <th>{{ __($year-1) }}</th>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">{{ __('Subject') }}</th>
                                <th scope="col">{{ __('Min. MArk') }}</th>
                                <th scope="col">{{ __('Obt. Mark') }}</th>
                                <th scope="col">{{ __('Max. Mark') }}</th>
                                <th scope="col">{{ __('CGPA') }}</th>
                                <th scope="col">{{ __('Remark') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $fail = false;
                            @endphp
                            @foreach($results as $result)
                                @if($result->cgpa == 0)
                                    @php
                                        $fail = true;
                                    @endphp
                                @endif
                                <tr>
                                    <td>{{ __($result->subject->name) }}</td>
                                    <td>{{ __($result->exam->min_mark-1) }}</td>
                                    <td>{{ __($result->got_mark-1) }}</td>
                                    <td>{{ __($result->exam->marks-1) }}</td>
                                    <td {!! $result->cgpa == '0.00' ? 'class="text-danger"':'' !!}>{{ __($result->cgpa) }}</td>
                                    <td {!! $result->remark == 'F' ? 'class="text-danger"':'' !!}>{{ __($result->remark) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="4">{{ __('Total') }}</th>
                                @if($fail)
                                    <th class="text-danger">0.00</th>
                                    <th class="text-danger">F</th>
                                @else
                                    <th>{{ number_format($results->sum('cgpa') / $results->count(),'2','.',',') }}</th>
                                    <th>{{ $results->sum('cgpa') / $results->count() > 4.99 ? 'A+': ($results->sum('cgpa') / $results->count() > 3.99 ? 'A': ($results->sum('cgpa') / $results->count() > 3.49 ? 'A-': ($results->sum('cgpa') / $results->count() > 2.99 ? 'B': ($results->sum('cgpa') / $results->count() > 1.99 ? 'C': ($results->sum('cgpa') / $results->count() > 0 ? 'D':'F'))))) }}</th>
                                @endif
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer text-right">
{{--                        <a href="javascript:void(0)">--}}
                        <form action="{{ route('result.download') }}" method="get" target="_blank">
                            @csrf
                            <input type="hidden" name="student" value="{{ $student->id }}">
                            <input type="hidden" name="class" value="{{ $student->classRoom->id }}">
                            <input type="hidden" name="exam" value="{{ $exam->id }}">
                            <input type="hidden" name="year" value="{{ $year }}">
                            <button type="submit" class="btn btn-theme-green"><i class="fas fa-cloud-download-alt"></i> {{ __('Download Report') }}</button>
                        </form>

{{--                        </a>--}}
                    </div>
                </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.exam-result.internal-assets.js.exam-js')
@endsection
