@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.exam-result.internal-assets.css.exam-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12">
                <div class="">
                    <nav class="breadcrumb bg-theme-green">
                        <h5><span class="breadcrumb-item bg-theme-green">{{$title}}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
                <div class="card shadow col-md-6 p-5 mx-auto">
                    <form action="{{ route('result.show') }}" method="get" enctype="multipart/form-data">
                        @csrf
                            <div class="">
                                <div class="form-group">
                                    <label class="font-weight-bold" for="name">{{__('Student_id')}}:</label>
                                    <input type="text" name="unique_id" value="" class="form-control border-md" id="unique_id" placeholder="{{__('Input your id')}}" required>
                                </div>
                                <div class="invalid-feedback">
                                    @if ($errors->has('student_id'))
                                        <span class="text-danger">{{ $errors->first('student_id') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="">
                                <label class="font-weight-bold" for="state">{{__('Class')}}: </label>
                                <select name="class_id" class="custom-select form-control border-md d-block w-100" id="class_id" >
                                    <option value="" selected disabled>{{__('Select your class')}}</option>
                                    @foreach($classes as $class)
                                    <option value="{{$class->id}}">{{$class->name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    @if ($errors->has('class'))
                                        <span class="text-danger">{{ $errors->first('class') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="">
                                <label class="font-weight-bold" for="state">{{__('Year:')}} </label>
                                <select name="year" class="custom-select form-control border-md d-block w-100" id="year" >
                                    <option value="" selected disabled>{{__('Select year')}}</option>
                                    @for($year = date('Y',time()); $year > 2010; $year--)
                                    <option value="{{ $year }}">{{ $year }}</option>
                                    @endfor
                                </select>
                                <div class="invalid-feedback">
                                    @if ($errors->has('year'))
                                        <span class="text-danger">{{ $errors->first('year') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="">
                                <label class="font-weight-bold" for="state">{{__('Exam')}}: </label>
                                <select name="exam_id" class="custom-select form-control border-md d-block w-100" id="exam_id" >
                                    <option value="" selected disabled>{{__('Select exam')}}</option>
                                    @foreach($exams as $exam)
                                        <option value="{{$exam->id}}">{{$exam->name}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    @if ($errors->has('exam'))
                                        <span class="text-danger">{{ $errors->first('exam') }}</span>
                                    @endif
                                </div>
                            </div>
                        <div class="py-2 mb-4 text-center">
                            <button type="submit" class="btn btn-theme-green btn-lg">{{__('Submit the application')}}</button>
                        </div>
                    </form>
                </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.exam-result.internal-assets.js.exam-js')
@endsection
