@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.single-person-content.internal-assets.css.content-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12">
                <div class="">
                    <nav class="breadcrumb  notice-bg-color">
                   <h5><span class="breadcrumb-item active text-white">{{$data->name}}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="d-flex flex-column align-items-center text-center">
                                                <img src="{{ asset('/upload/key-persons/'.$data->image) }}" alt="Admin" class="rounded-circle" width="150">
                                                <div class="mt-3">
                                                    <h4 class="text-success">{{$data->name}}</h4>
                                                    <p class="text-secondary mb-1">{{$data->designation}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-8 col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="">
                                                <h4 class="text-success">{{__('Statement')}}:</h4><hr>
                                                <p class="text-secondary mb-1">{{$data->person_content}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <hr>

            </div>

            <div class="col-lg-3 col-md-12">
                @include('frontend.theme1.pages.home-page-sections.single-right-bar')
            </div>
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.single-person-content.internal-assets.js.content-js')
@endsection
