<style>
    .active-item{
        display: block;
    }
    .active-btn{
        color: #ffffff;
        background-color: #DC3545;
        border-color: #DC3545;
    }
    .active-btn:hover{
        color: #ffffff;
        background-color: #609513;
        border-color: #609513;
    }
</style>
