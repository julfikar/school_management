@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.library.internal-assets.css.library-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">

            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12 order-lg-2 order-md-1 order-1">
                <div class="filter filter-basic">
                    <div class="filter-nav text-center">
                        <a href="javascript:void(0)"><button class="btn btn-theme-green active-btn" data-filter="all_books">{{__('All')}}</button></a>
                        <a href="{{ route('library.storyBooks') }}"><button class="btn btn-theme-green"  data-filter="story_books">{{__('Story-books')}}</button></a>
                        <a href="{{ route('library.textBooks') }}"><button class="btn btn-theme-green" data-filter="text_books">{{__('Text-Books')}}</button></a>

                    </div>
                    <div class="filter-item active-item" id="all_books">
                        <div class="row mt-4">
                            @foreach($allBooks as $book)
                                <div class="col-sm-6 col-md-6 col-lg-4 ">
                                    <div class="" >
                                        <div class="item-content pb-3">
                                            <a href="{{ asset($book->e_book) }}" >
                                                <p class="bg-dark text-center text-white">{{$book->name}}</p>
                                                <img class="img-fluid book_height" src="{{ asset($book->thumbnail) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12 order-lg-3 order-md-3 order-3">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')

@endsection
