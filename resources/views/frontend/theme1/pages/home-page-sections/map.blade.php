<!--Google map-->
<div class="elementor-custom-embed">
    <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
            src="{{ setting('backend.general.location_map') }}"
            aria-label="Savar, Dhaka, Bangladesh"></iframe>
</div>
