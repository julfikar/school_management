
@foreach($right_persons as $right_person)
    <div class="card my-2 d-lg-block d-none">
        <div class="card-body text-center card-hed text-white">
            {{__($right_person->designation)}}
        </div>
        <img class="card-img-top" src="{{asset('upload/key-persons/'.$right_person->image)}}" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title text-center"> {{__($right_person->name)}}</h5>
        </div>
    </div>
@endforeach


<div class="card my-2">
    <section class="slider-wrapper">
        <div id="slider-style-ones" class="carousel slide bs-slider control-round indicators-line"
             data-ride="carousel" data-pause="hover" data-interval="5000">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#slider-style-one" data-slide-to="0" class="active"></li>
                <li data-target="#slider-style-one" data-slide-to="1"></li>
                <li data-target="#slider-style-one" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">
                <!-- Third Slide -->
                @foreach($sub_slider as $slider)
                    <div class="carousel-item {{ $loop->index == 0 ? 'active':'' }}">
                        <!-- Slide Background -->
                        <img src="{{asset('/upload/sub_slider/'.$slider->image)}}" alt="Slider Images" class="img-fluid"/>
                        <div class="bs-slider-overlay"></div>
                    </div>
            @endforeach
            <!-- End of Slide -->
            </div>
            <!-- End of Wrapper For Slides -->
        </div>
        <!-- End  slider-style-one Slider -->
    </section>
</div>

<div class="card my-2">
    <div class=" text-center card-hed ">
        <h5 class="text-white mb-0 pt-4">{{__('Emergency Services')}}</h5>
    </div>
    <div class="card-body text-white card-hed">
{{--        <ul class="list-unstyled text-center text-sm-left">--}}
{{--            @foreach($services as $service)--}}
{{--                <li>{{$service->name}}<br><i class="ml-1 fa fa-phone"></i><a class="text-white" href="#">{{$service->phone}}</a></li>--}}
{{--            @endforeach--}}

{{--        </ul>--}}
        <img alt="জরুরি হটলাইন" src="{{ asset('upload/settings/2020-01-29-15-38-1110062e77cba2e7d935d46121912483.jpg') }}" style="height:auto; width:100%" class="img-thumbnail">
    </div>
</div>
