<!-- Strat Slider Section -->
<section class="slider-wrapper">
    <div id="slider-style-one" class="carousel slide bs-slider control-round indicators-line"
         data-ride="carousel" data-pause="hover" data-interval="5000">
        <div class="carousel-inner m-0" role="listbox">

            @foreach($sliders as $slider)
            <!-- first Slide -->
            <div class="carousel-item {{$loop->first?'active':''}}">
                <!-- Slide Background -->
                <img src="{{ asset('upload/sliders/'.$slider->name) }}" alt="Slider Images" class="slide-image"/>
                <div class="bs-slider-overlay"></div>
            </div>
            <!-- End of Slide -->
            @endforeach
        </div>
        <!-- End of Wrapper For Slides -->
        <!-- Left Control -->
        <a class="left carousel-control" href="#slider-style-one" role="button" data-slide="prev">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <!-- Right Control -->
        <a class="right carousel-control" href="#slider-style-one" role="button" data-slide="next">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
    <!-- End  slider-style-one Slider -->
</section>
<!-- End Slider Section -->

<marquee behavior="scroll" onmouseover="this.stop();" onmouseout="this.start();" class="p-1 elementor-text-editor elementor-clearfix bg-danger text-light">@foreach($notice_board as $notice)
        @if($loop->index ==0)
            {!! '<i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i> '. $notice->heading .' <i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i><i class="fas fa-asterisk"></i>' !!}
        @endif
    @endforeach
</marquee>
