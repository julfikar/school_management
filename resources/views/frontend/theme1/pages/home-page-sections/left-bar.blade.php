

@foreach($left_persons as $left_person)
<div class="card my-2 d-lg-block d-none">
    <div class=" text-center card-body card-hed text-white">
        {{__($left_person->designation)}}
    </div>
    <img class="card-img-top" src="{{ asset('upload/key-persons/'.$left_person->image) }}"
         alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title text-center"> {{__($left_person->name)}}</h5>
    </div>
</div>
@endforeach

<div class="card my-2">
    <div class="card-body text-center card-hed pb-0">
        {!! config('app.locale') == 'en' ?'<h5 class="text-white text-capitalize text-roboto-bold">'.__('National anthem').'</h5>':'<h3 class="text-white text-capitalize text-galada">'.__('National anthem').'</h3>' !!}
    </div>
    <div class="card-body card-hed text-white text-kalpurush text-center pt-0">
        <p class="h6">আমার সোনার বাংলা, আমি তোমায় ভালোবাসি।
            <br>
            চিরদিন তোমার আকাশ, তোমার বাতাস, আমার প্রাণে বাজায় বাঁশি॥
            <br>
            ও মা, ফাগুনে তোর আমের বনে ঘ্রাণে পাগল করে,
            <br>
            মরি হায়, হায় রে—
            <br>
            ও মা, অঘ্রাণে তোর ভরা ক্ষেতে আমি কী দেখেছি মধুর হাসি॥
            <br>
            <br>
            কী শোভা, কী ছায়া গো, কী স্নেহ, কী মায়া গো—
            <br>
            কী আঁচল বিছায়েছ বটের মূলে, নদীর কূলে কূলে।
            <br>
            মা, তোর মুখের বাণী আমার কানে লাগে সুধার মতো,
            <br>
            মরি হায়, হায় রে—
            <br>
            মা, তোর বদনখানি মলিন হলে, ও মা, আমি নয়নজলে ভাসি॥</p>
    </div>
</div>
