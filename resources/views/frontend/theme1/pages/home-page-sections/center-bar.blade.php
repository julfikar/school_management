

<!-- notice -->
<div class="notice notice-board-bg ">
    <h2 class="notice-title {{ config('app.locale') == 'en' ? 'text-roboto-bold':'text-galada' }}">{{__('Notice Board')}}</h2>
    <div id="col-12 notice-board-ticker">
        <ul class="notice-content">
            @foreach($notice_board as $notice)
                @if($loop->index >0)
            <li class="list-style notice-item">
                <i class="fas fa-caret-right"></i>&nbsp;&nbsp;&nbsp;<a href="{{ route('notice.single',$notice->slug) }}">{{$notice->heading}}</a>
            </li>
                @endif
            @endforeach
        </ul>
        <a class="btn btn-theme-green float-right mr-2" href="{{route('notice.index')}}">{{__('All')}}</a>
    </div>
</div>
<!-- end notice -->
<!-- pm -->
<div class="row pm my-1">
    @foreach($videos as $video)
        @if($video->main_video == true)
            <div class="col-lg-12 col-md-6 col-sm-12">
                <div class="position-relative">
                   <img class="img-thumbnail videoFrame" src="{{ asset('upload/video-sliders/'.$video->thumbnail) }}" data-file="{{$video->url}}">
                    <div class="videoFrame playIcon text-center" data-file="{{$video->url}}"><i class="text-danger fas fa-play fa-3x"></i></div>
                </div>
            </div>
        @endif
    @endforeach
</div>
<!-- end pm -->

<!-- text books -->
<div class="galari">
    <div class="lightbox-gallery">
        <div class="text-center">
            <a href="javascript:void(0)">
                <h3 class="mb-0 py-4 {{ config('app.locale') == 'en' ? 'text-roboto-bold':'text-galada' }}">{{__('Library')}}</h3>
            </a>
        </div>

        <div class="row photos d-none d-lg-flex">
            @foreach($subjects as $subject)
            <div class="col-lg-4 my-2">
                <a href="{{ asset('upload/subjects/e_books/'.$subject->e_book) }}" target="_blank" data-lightbox="photos">
                    <p class="bg-dark text-center">{{ __($subject->name) }}</p>
                    <img class="img-fluid  book_height_welcome" src="{{asset('upload/subjects/thumbnails/'.$subject->thumbnail)}}">
                </a>
            </div>
            @endforeach
        </div>

        <div class="row">
            <div class="library-mobile photos d-lg-none">
                @foreach($subjects as $subject)
                    <div class="col-10 col-md-5">
                        <a href="{{ asset('upload/subjects/e_books/'.$subject->e_book) }}" target="_blank" data-lightbox="photos">
                            <p class="bg-dark text-center">{{ __($subject->name) }}</p>
                            <img class="img-fluid  book_height_welcome" src="{{asset('upload/subjects/thumbnails/'.$subject->thumbnail)}}">
                        </a>
                    </div>
                @endforeach
                @if($subjects->count() > 5)
                    <div class="col-sm-12">
                        <div class="d-block float-right" style="margin-top: -20px">
                            <a href="{{route('library')}}" class="btn btn-sm btn-theme-green">{{__('See More')}}</a>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="row row my-2">
{{--            @if($subjects->count() > 5)--}}
                <div class="col-sm-12">
                    <div class="d-block float-right" style="margin-top: -20px">
                        <a href="{{route('library')}}" class="btn btn-sm btn-theme-green">{{__('See More')}}</a>
                    </div>
                </div>
{{--            @endif--}}
        </div>
    </div>
</div>
<!-- end text books -->

<!-- calender -->
<div class="row">
    <div class="calendar-wrapper"></div>
</div>
<!-- end calender -->

<!-- library -->
<div class="galari">
    <div class="lightbox-gallery">
        <div class="text-center">
            <a href="javascript:void(0)">
                <h3 class="mb-0 py-4 {{ config('app.locale') == 'en' ? 'text-roboto-bold':'text-galada' }}">{{__('Story Book')}}</h3>
            </a>
        </div>

        <div class="row photos d-none d-lg-flex">
            @foreach($ebooks as $ebook)
            <div class="col-lg-4 my-2">
                <a href="{{ route('admin.ebooks.show',$ebook->id) }}" target="_blank"  data-lightbox="photos">
                    <p class="bg-dark text-center">{{$ebook->name}}</p>
                    <img class="img-fluid book_height_welcome"
                         src="{{asset('/upload/e_books/thumbnails/'.$ebook->thumbnail) }}">
                </a>
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="library-mobile photos d-lg-none">
                @foreach($ebooks as $ebook)
                    <div class="col-10 col-md-5">
                        <a href="{{ route('admin.ebooks.show',$ebook->id) }}" target="_blank"  data-lightbox="photos">
                            <p class="bg-dark text-center">{{$ebook->name}}</p>
                            <img class="img-fluid book_height_welcome"
                                 src="{{asset('/upload/e_books/thumbnails/'.$ebook->thumbnail) }}">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row my-2">
{{--            @if($subjects->count() > 5)--}}
                <div class="col-sm-12">
                    <div class="d-block float-right" style="margin-top: -20px">
                        <a href="{{route('library')}}" class="btn btn-sm btn-theme-green">{{__('See More')}}</a>
                    </div>
                </div>
{{--                Ì@endif--}}
        </div>
    </div>
</div>
<!-- end library -->

<!-- videos -->
<div class="row my-3">
    <div class="w-100 my-2 ">
        <div class="" style="position: relative">
            <img id="mainPlayerThumbnail" class="d-block img-fluid" src="">
            <div class="playIcon" id="videoScrollerPlayIcon" data-file=""><i class="text-danger fas fa-play fa-3x"></i></div>
        </div>
    </div>
    <div id="videosCarousel" class="carousel slide w-100" data-ride="carousel">
        <div class="carousel-inner videosCarousel-inner w-100" role="listbox">
            @foreach($videos as $video)
                @if($video->main_video == false)
                    <div class="carousel-item {{ $loop->index == 1 ? 'active':'' }}" data-file="{{$video->url}}">
                         <img width="900" height="1200" class="d-block col-3 img-fluid" src="{{ asset('upload/video-sliders/'.$video->thumbnail) }}">
                    </div>
                @endif
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#videosCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#videosCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

    </div>
</div>

<!--key persons for mobile view-->
<div class="row my-3 d-lg-none d-block">
    <div id="keyPersonCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($keyPersons as $keyPerson)
            <div class="carousel-item {{ $loop->index == 0?'active':'' }}">
                <div class="card my-2">
                    <div class=" text-center card-body card-hed text-white">
                        {{__($keyPerson->designation)}}
                    </div>
                    <img class="card-img-top" src="{{ asset('upload/key-persons/'.$keyPerson->image) }}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title text-center"> {{__($keyPerson->name)}}</h5>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#keyPersonCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#keyPersonCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>


