<style>
    .playIcon{
        position: absolute!important;
        top: 45%;
        left: 45%;
        cursor: pointer;
    }

    .videosCarousel-inner .carousel-item.active,
    .videosCarousel-inner .carousel-item-next,
    .videosCarousel-inner .carousel-item-prev {
        display: flex;
    }

    .videosCarousel-inner .carousel-item-right.active,
    .videosCarousel-inner .carousel-item-next {
        transform: translateX(25%);
    }

    .videosCarousel-inner .carousel-item-left.active,
    .videosCarousel-inner .carousel-item-prev {
        transform: translateX(-25%);
    }

    .videosCarousel-inner .carousel-item-right,
    .videosCarousel-inner .carousel-item-left{
        transform: translateX(0);

    }
    .videosCarousel-inner .carousel-item img{
        border: 1px solid #609513;
        border-radius: 15px;
        margin: 0 1px;
        cursor: -moz-grab;
        cursor: -webkit-grab;
    }
    img#mainPlayerThumbnail{
        border: 1px solid #609513;
        border-radius: 15px;
    }
</style>

