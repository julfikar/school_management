<script>
    $(function (){
        $('.filter-btn').on('click', activeSection);
        var el = $('.zoom-gallery-{{$galleryFolder->id}}');

       /* // For image popup
       el.magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
                verticalFit: true,
            },
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function(element) {
                    return element.find('img');
                }
            }

        });*/

    });

    function activeSection(){
        $('.filter-btn').removeClass('active-btn');
        $('.filter-item').removeClass('active-item');
        let selectSection =  $(this).attr('data-filter');
        $('#'+selectSection).addClass('active-item');
        $(this).addClass('active-btn');
    }
</script>
