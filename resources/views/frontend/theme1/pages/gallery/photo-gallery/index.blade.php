@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.gallery.photo-gallery.internal-assets.css.photo-css')
    <!-- Import Custome CSS -->
    <link rel="stylesheet" href="{{ asset('forntend/theme1/css/yBox.min.css') }}">
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">

            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12 order-lg-2 order-md-1 order-1">

                <div class="filter filter-basic">
                    <div class="filter-nav text-center">
                        @foreach($galleryFolders as $galleryFolder)
                        <button class="btn btn-theme-green {{$loop->first?'active-btn':''}} filter-btn" data-filter="photo{{$galleryFolder->id}}">{{__(ucwords($galleryFolder->name))}}</button>
                        @endforeach

                    </div>

                    @foreach($galleryFolders as $galleryFolder)
                    <div class="filter-item {{$loop->first?'active-item':''}}" id="photo{{$galleryFolder->id}}">
                        <div class="row mt-4 zoom-gallery-{{$galleryFolder->id}}">
                            @foreach($galleryFolder->galleries as $gallery)
                                <div class="col-sm-6 col-md-6 col-lg-4 ">
                                    <div class="">
                                        <div class="item-content group-wrap pb-3">
                                            <a class="yBox" href="{{ asset('upload/galleries/'.$gallery->name) }}" data-ybox-group="photo{{$galleryFolder->id}}" >
                                                <img class="img-fluid book_height img-thumbnail" src="{{ asset('upload/galleries/'.$gallery->name) }}">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endforeach

                </div>

            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12 order-lg-3 order-md-3 order-3">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.gallery.photo-gallery.internal-assets.js.photo-gallery')
    <!-- Import Videopopup Js -->
    <script type="text/javascript" src="{{ asset('forntend/theme1/js/directive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('forntend/theme1/js/yBox.min.js') }}"></script>
@endsection
