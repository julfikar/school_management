@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.gallery.video-gallery.internal-assets.css.video-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">

            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12 order-lg-2 order-md-1 order-1">
                <div class="">
                    <nav class="breadcrumb theme-bg-green">
                        <h5><span class="breadcrumb-item active text-white">{{$title}}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
                    <div class="row">
                        @foreach($videos as $video)
                            @if($video->main_video == false)
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="position-relative">
                                        <img class="img-thumbnail videoFrame" src="{{ asset('upload/video-sliders/'.$video->thumbnail) }}" data-file="{{$video->url}}">
                                        <div class="playIcon videoFrame" data-file="{{$video->url}}"><i class="text-danger fas fa-play fa-3x"></i></div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12 order-lg-3 order-md-3 order-3">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->
<!--popup video player-->
    <div id="popupVideoPlayerModal" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="video-frame">
                        <iframe id="player" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--popup video player-->
@endsection

@section('page-script')
    @include('frontend.theme1.pages.gallery.video-gallery.internal-assets.js.video-gallery')
@endsection
