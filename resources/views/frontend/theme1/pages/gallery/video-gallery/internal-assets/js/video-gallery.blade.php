<script>
    $(function (){
        $('.videoFrame').on('click', showVideoOnPopup)
    });

    function showVideoOnPopup(e){
        e.preventDefault();
        let videoData = $(this).data();
        $('#player').attr('src', videoData.file);
        $('#popupVideoPlayerModal').modal('show');
    }

    $(document).on('click', function(e){
        $('#popupVideoPlayerModal').on('hidden.bs.modal', function (e) {
            $('#player').removeAttr('src');
        })
    })
</script>
