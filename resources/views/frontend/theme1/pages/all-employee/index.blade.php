@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.all-employee.internal-assets.css.content-css')
@endsection

@section('content')

    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="{{ $page->right_bar?'col-lg-9 col-md-9':'col-lg-12 col-md-12' }} col-sm-12">
                <div class="">
                    <nav class="breadcrumb bg-theme-green">
                   <h5><span class="breadcrumb-item bg-theme-green">{{$title}}</span></h5>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
              <div class="row mx-2">
                  @foreach($employees as $employee)
                      <div class="card col-lg-4 col-md-6 col-sm-12 px-0">
                          @if(!$employee->user->profile_photo_path)
                              <img class="card-img-top" src="{{ $employee->user->gender == 'male' ? asset('backend/assets/img/profile/male.jpg'):($employee->user->gender == 'female' ? asset('backend/assets/img/profile/female.jpg'):asset('backend/assets/img/profile/other.png'))  }}" class="rounded-circle " alt="Card image cap">
                          @else
                          @endif
                          <div class="card-body">
                              <h4 class="text-theme-green text-center text-roboto-bold mb-0">{{$employee->user->name}}</h4>
                          <p class="text-roboto text-center text-uppercase">{{ '(Designation: '. $employee->designation->name.')' }}</p>

                          </div>
                          <div class="card-footer text-center px-0 pb-0">
                              {!! $employee->user->phone? '<a href="tel:'.$employee->user->phone.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fas fa-phone"></i></a>':'' !!}
                              {!! $employee->user->email? '<a href="email:'.$employee->user->email.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fas fa-envelope"></i></a>':'' !!}
                              {!! $employee->user->profile->facebook_link? '<a href="'.$employee->user->profile->facebook_link.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fa fa-facebook"></i></a>':'' !!}
                              {!! $employee->user->profile->whatsapp_link? '<a href="'.$employee->user->profile->whatsapp_link.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fa fa-whatsapp"></i></a>':'' !!}
                              {!! $employee->user->profile->linked_in? '<a href="'.$employee->user->profile->linked_in.'" target="_blank" class="btn btn-theme-outline-green btn-card-social-circle"><i class="fa fa-linkedin"></i></a>':'' !!}
                              <a href="{{route('employee-explore',$employee->id)}}">
                                  <button type="submit" class="btn btn-theme-green w-100 mt-1">{{__('Explore')}}</button>
                              </a>
                          </div>
                      </div>
                  @endforeach
              </div>
            </div>

            @if($page->right_bar)
                <div class="col-lg-3 col-md-3 col-sm-12">
                    @include('frontend.theme1.pages.home-page-sections.single-right-bar')
                </div>
            @endif
        </div>
    </div>
    <!-- End About Us -->

@endsection

@section('page-script')
    @include('frontend.theme1.pages.all-employee.internal-assets.js.content-js')
@endsection
