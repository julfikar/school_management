<footer class="bg-faded footer-copy-right">
    <div class="container">
        <div class="section-content">
            <div class="row">
                @include('frontend.theme1.menus.bottom-menu')

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer-item">
                        <div class="footer-title">
                            <h4 class="text-white text-capitalize">{{__('complain')}}</h4>

                            <div class="border-style-1"></div>
                        </div>
                        <form action="{{route('complain')}}" method="post" >
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <input type="text" name="name" class="form-control"
                                           placeholder="{{__('Enter Name')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text" name="phone" class="form-control"
                                           placeholder="{{__('Enter phone no')}}">
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="contact-textarea">
                                        <textarea class="form-control" rows="4" placeholder="{{__('Write Message')}}" name="message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-theme text-uppercase">{{__('Send Message')}} </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row justify-content-between">
                <div class="col-md-5">
                    <form action="{{route('subscribe')}}" method="post">
                        @csrf
                        <div class="footer-item">
                            <h5 class="newsletter-text text-white">{{__('Newsletter')}}</h5>
                            <div class="input-group subscribe-style-two">
                                <input type="email" name="email" class="form-control" placeholder="{{__('Email')}}">
                                <span class="input-group-btn">
                                    <button class="btn btn-subscribe" type="submit">{{__('Subscribe')}}</button>
                                </span>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-md-4">
                    <div class="footer-item">
                        <h5 class="newsletter-text text-white">{{__('Social Link')}}</h5>
                        <ul class="social-icon bg-transparent text-white bordered">
                            {!! setting('social-media.links.twitter') ? '<li><a href="'.setting('social-media.links.twitter').'"><i class="fa fa-twitter text-white" aria-hidden="true"></i></a></li>':'' !!}

                            {!! setting('social-media.links.facebook') ? '<li><a href="'.setting('social-media.links.facebook').'"><i class="fa fa-facebook text-white" aria-hidden="true"></i></a></li>':'' !!}

                            {!! setting('social-media.links.whatsapp') ? '<li><a href="'.setting('social-media.links.whatsapp').'"><i class="fa fa-whatsapp text-white" aria-hidden="true"></i></a></li>':'' !!}

                            {!! setting('social-media.links.linkedin') ? '<li><a href="'.setting('social-media.links.linkedin').'"><i class="fa fa-linkedin text-white" aria-hidden="true"></i></a></li>':'' !!}

                            {!! setting('social-media.links.skype') ? '<li><a href="'.setting('social-media.links.skype').'"><i class="fa fa fa-skype text-white" aria-hidden="true"></i></a></li>':'' !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copy-right py-3 row">
    <div class="col-12">
        <p class="text-white text-center">© {{ date('Y',time()) }}, All Rights Reserved By :<a href="{{ '/' }}">{{ config('app.name','laravel') }}</a></p>
    </div>
</div>
