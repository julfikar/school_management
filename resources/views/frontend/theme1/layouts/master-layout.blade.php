<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('frontend.theme1.layouts.head')

<body id="body">

<div class="layout-boxed">

    @include('frontend.theme1.layouts.pre-loader')

    @include('frontend.theme1.menus.header-menu')

    @yield('content')

    @include('frontend.theme1.layouts.footer')
</div>

@include('frontend.theme1.layouts.script')
</body>

</html>
