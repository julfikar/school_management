<div class="l-sidebar">
    <div class="logo">
        <div class="logo__txt">
            <a href="{{ '/' }}">
                <img class="sidebar-brand-img" src="{{ setting('backend.logo_favicon.logo') }}" height="40" width="40">
            </a>
        </div>
    </div>
    <div class="l-sidebar__content">
        <nav class="c-menu js-menu">
            <ul class="u-list">
                <li class="c-menu__item {{ strpos(request()->path(),'/dashboard')?'is-active':'' }}" data-toggle="tooltip" title="Proposals">
                    <a href="{{ route('dashboard') }}" class="text-white">
                        <div class="c-menu__item__inner">
                            <i class="fa fa-dashboard"></i>
                            <div class="c-menu-item__title"><span>{{__('Dashboard')}}</span></div>
                        </div>
                    </a>
                </li>

                <li class="c-menu__item has-submenu {{ strpos(request()->path(),'/profile')?'is-active':'' }}" data-toggle="tooltip" title="Accounts">
                    <a href="{{ route('user.profile') }}" class="text-white">
                        <div class="c-menu__item__inner">
                            <i class="fa fa-address-book-o"></i>
                            <div class="c-menu-item__title"><span>{{__('Profile')}}</span></div>
                        </div>
                    </a>
                </li>


                @if(auth()->user()->user_type_id == 3)
                    <li class="c-menu__item has-submenu {{ strpos(request()->path(),'/my-all-applications')?'is-active':'' }}" data-toggle="tooltip" title="My Application">
                        <a href="{{ route('user.my-all-applications') }}" class="text-white">
                            <div class="c-menu__item__inner">
                                <i class="fa fa-envelope-open"></i>
                                <div class="c-menu-item__title"><span>{{__('My All Applications')}}</span></div>
                            </div>
                        </a>
                    </li>


                    <li class="c-menu__item has-submenu {{ strpos(request()->path(),'/application')?'is-active':'' }}" data-toggle="tooltip" title="Application">
                        <a href="{{ route('user.application') }}" class="text-white">
                            <div class="c-menu__item__inner">
                                <i class="fa fa-envelope"></i>
                                <div class="c-menu-item__title"><span>{{__('Application')}}</span></div>
                            </div>
                        </a>
                    </li>
                @endif

                <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Settings">
                    <div class="c-menu__item__inner"><i class="fa fa-cogs"></i>
                        <div class="c-menu-item__title"><span>{{__('Settings')}}</span></div>
                    </div>
                </li>
                <li class="c-menu__item has-submenu" data-toggle="tooltip" title="Logout" onclick="event.preventDefault();
                                                document.getElementById('logOutForm').submit();">
                    <div class="c-menu__item__inner"><i class="fa fa-power-off"></i>
                        <div class="c-menu-item__title"><span>Logout</span></div>
                    </div>
                    <form action="{{ route('logout') }}" method="post" id="logOutForm">
                        @csrf
                    </form>
                </li>

            </ul>
        </nav>
    </div>
</div>
