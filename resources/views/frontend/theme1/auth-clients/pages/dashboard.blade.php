
@extends('frontend.theme1.auth-clients.layouts.master-layout')

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-light ">
                    <a class="breadcrumb-item text-dark" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{ __('Home') }}</a>
                    <span class="breadcrumb-item active">{{ __('Dashboard') }}</span>
                    <span class="breadcrumb-info" id="time"></span>
                </nav>
            </div>
        </div>

        <div class="row">
            @if (Auth::user()->userType->id == 5)
                <div class="col-md-12">
                    <a target="_blank" href="{{ route('user.download-admit-card') }}" class="btn btn-lg btn-theme-green"><i class="fa fa-download"></i> Download Admit Card</a>
                </div>
            @endif


        <div class="col-md-12">
            @if (Auth::user()->userType->id == 3)
                <div class="row mt-4">
                    @foreach($textBook as $ebook)
                        <div class="col-sm-3 col-md-3 col-lg-3 ">
                            <div class="" >
                                <div class="item-content pb-3">
                                    <a href="{{ asset('upload/subjects/e_books/'.$ebook->e_book) }}" >
                                        <p class="bg-dark text-center text-white py-1">{{$ebook->name}}</p>
                                        <img class="img-fluid book_height w-100" src="{{ asset('upload/subjects/thumbnails/'.$ebook->thumbnail) }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-12 pagination">
                    {{ $textBook->links() }}
                </div>
            @else
                <div class="row mt-4">
                    @foreach($storryBooks as $ebook)
                        <div class="col-sm-6 col-md-6 col-lg-4 ">
                            <div class="" >
                                <div class="item-content pb-3">
                                    <a href="{{ asset('upload/e_books/'.$ebook->e_book) }}" >
                                        <p class="bg-dark text-center text-white">{{$ebook->name}}</p>
                                        <img class="img-fluid book_height" src="{{ asset('upload/e_books/thumbnails/'.$ebook->thumbnail) }}" width="100%">
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="col-12 pagination">
                    {{ $storryBooks->links() }}
                </div>
            @endif

        </div>


        </div>
    </div>

@endsection

