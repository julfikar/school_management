<div class="row">
    <div class="col-12 mb-4">
        <div class="card  bg-white py-1">

            <div class="card-body p-4">
                <div class="row">
                    <div class="col-md-4 py-5">
                        <div class="h5">{{ __('Profile Information') }}</div>
                        <p class="">{{ __('Update your account profile information.') }}</p>
                    </div>
                    <div class="col-md-8">
                        <form action="{{ route('user.profile-update') }}" method="post" enctype="multipart/form-data" class="wma-form">
                            @csrf
                            <div class="">

                                <p class="h6 mb-3">{{ __('Photo') }}</p>
                                <div class="row">
                                    <div class="col-md-6 text-center">
                                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())

                                            <img class="rounded-circle" width="150" src="{{ Auth::user()->profile_photo_url }}" alt="{{ Auth::user()->name }}" />
                                        @else
                                            <img src="{{ Auth::user()->gender == 'male' ? asset('backend/assets/img/profile/male.jpg'):(Auth::user()->gender == 'female' ? asset('backend/assets/img/profile/female.jpg'):asset('backend/assets/img/profile/other.png'))  }}" class="rounded-circle " width="150">
                                        @endif
                                    </div>

                                    <div class="col-md-6">
                                        <div class="">
                                            <div class="admin-image" id="admin_image">
                                                <div class="input-images"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col">

                                        <p class="mb-1 font-weight-bold"><label for="name">{{ __('Name') }}:</label> </p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="name" id="name" value="{{Auth::user()->name}}" class="form-control rounded"
                                                   aria-label="Large" placeholder="{{__('Name in English')}}" aria-describedby="inputGroup-sizing-sm">
                                            <br>
                                            @if ($errors->has('name'))
                                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>

                                        <p class="mb-1 font-weight-bold"><label for="name_utf8">বাংলায় নাম :</label> </p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="text" name="name_utf8" id="name_utf8" value="{{Auth::user()->profile->name_utf8}}" class="form-control rounded"
                                                   aria-label="Large" placeholder="বাংলায় নাম"  aria-describedby="inputGroup-sizing-sm">
                                            <br>
                                            @if ($errors->has('name_utf8'))
                                                <span class="text-danger">{{ $errors->first('name_utf8') }}</span>
                                            @endif
                                        </div>

                                        <p class="mb-1 font-weight-bold"><label for="phone">{{ __('Phone') }}:</label> </p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="tel" name="phone" id="phone" value="{{Auth::user()->phone}}" class="form-control rounded"
                                                   aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                                            <br>
                                            @if ($errors->has('phone'))
                                                <span class="text-danger">{{ $errors->first('phone') }}</span>
                                            @endif
                                        </div>

                                        <p class="mb-1 font-weight-bold"><label for="email">{{ __('Email') }}:</label> </p>
                                        <div class="input-group input-group-lg mb-3">
                                            <input type="email" name="email" id="email" value="{{Auth::user()->email}}" class="form-control rounded"
                                                   aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                                            <br>
                                            @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="float-right">
                                    <button class="btn btn-wave-light rounded btn-theme-green btn-lg" type="submit">{{ __('Save') }}</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-12 mb-4">
        <div class="card card-white bg-white py-1">
            <div class="card-body p-4">
                <form action="{{ route('user.profile-info') }}" method="post">
                    @csrf

                    <div class="form-row">
                        <div class="col-md-6 px-5">
                            <div class="form-group">
                                <label for="fathersName" class="card-title font-weight-bold">{{__('Father Name')}}</label>
                                <input type="text" name="father_name" id="fathersName" class="form-control" required value="{{ auth()->user()->profile? auth()->user()->profile->father_name:'' }}" placeholder="{{ __('Father name in english') }}">
                            </div>
                        </div>
                        <div class="col-md-6 px-5">
                            <div class="form-group">
                                <label for="mothersName" class="card-title font-weight-bold">{{__('Mother Name')}}</label>
                                <input type="text" name="mother_name" id="mothersName" class="form-control" required value="{{ auth()->user()->profile? auth()->user()->profile->mother_name:'' }}" placeholder="{{ __('Mother name in english') }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-md-6 px-5">
                            <div class="form-group">
                                <label for="father_name_utf8" class="card-title font-weight-bold">{{__('পিতার নাম')}}</label>
                                <input type="text" name="father_name_utf8" id="father_name_utf8" class="form-control"  required value="{{ auth()->user()->profile? auth()->user()->profile->father_name_utf8:'' }}" placeholder="{{ __('বাংলায় পিতার নাম') }}">
                            </div>
                        </div>
                        <div class="col-md-6 px-5">
                            <div class="form-group">
                                <label for="mother_name_utf8" class="card-title font-weight-bold">{{__('মাতার নাম')}}</label>
                                <input type="text" name="mother_name_utf8" id="mothersName" class="form-control" required value="{{ auth()->user()->profile? auth()->user()->profile->mother_name_utf8:'' }}" placeholder="{{ __('বাংলায় মাতার নাম') }}">
                            </div>
                        </div>
                    </div>


                    @if(!auth()->user()->profile)
                        <div class="form-row">
                            <div class="col-md-6 px-5">
                                <h6 class="h6 text-center text-left card-title">{{__('present address')}}</h6>
                                <div class="form-group">
                                    <label for="presentDistrict" class="card-title font-weight-bold">{{__('District')}}</label>
                                    <input type="text" name="present_district" id="district" list="districtList" class="form-control rounded" required value="{{ old('present_district') }}" placeholder="{{ __('Type your district name') }}">
                                    <datalist id="districtList">
                                        @foreach($districts as $district)
                                            <option>{{ $district->name }}</option>
                                        @endforeach
                                    </datalist>
                                </div>

                                <div class="form-group">
                                    <label for="presentThana" class="card-title font-weight-bold">{{__('Thana')}}</label>
                                    <input type="text" name="present_thana" id="presentThana" list="thanaList" class="form-control rounded" required value="{{ old('present_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                    <datalist id="thanaList">
                                        @foreach($districts as $district)
                                            @foreach($district->thana as $thana)
                                                <option value="{{ $thana->name }}">{{ $district->name }}</option>
                                            @endforeach
                                        @endforeach
                                    </datalist>
                                </div>

                                <div class="form-group">
                                    <label for="presentCity" class="card-title font-weight-bold">{{__('City / Village')}}</label>
                                    <input type="text" name="present_city" id="presentCity" class="form-control rounded" required value="{{ old('present_city') }}" placeholder="{{ __('Type your city') }}">
                                </div>

                                <div class="form-group">
                                    <label for="presentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}</label>
                                    <input type="text" name="present_post_office" id="presentPostOffice" list="postOfficeList" class="form-control rounded" required value="{{ old('present_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                    <datalist id="postOfficeList">
                                        @foreach($districts as $district)
                                            @foreach($district->postOffice as $postOffice)
                                                <optgroup label="{{ $district->name }}">
                                                    <option value="{{ $postOffice->post_code }}">{{ $postOffice->name }}</option>
                                                </optgroup>
                                            @endforeach
                                        @endforeach
                                    </datalist>
                                </div>

                                <div class="form-group">
                                    <label for="presentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}</label>
                                    <input type="text" name="present_road" id="presentRoad" class="form-control rounded" required value="{{ old('present_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                </div>
                            </div>
                            <div class="col-md-6 px-5">
                                <h6 class="h6 text-center text-left card-title">{{__('permanent address')}}</h6>
                                <div class="form-group">
                                    <label for="permanentDistrict" class="card-title font-weight-bold">{{__('District')}}</label>
                                    <input type="text" name="permanent_district" id="permanentDistrict" list="districtList" class="form-control rounded" required value="{{ old('permanent_district') }}" placeholder="{{ __('Type your district name') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentThana" class="card-title font-weight-bold">{{__('Thana')}}</label>
                                    <input type="text" name="permanent_thana" id="permanentThana" list="thanaList" class="form-control rounded" required value="{{ old('permanent_thana') }}" placeholder="{{ __('Type your thana name') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentCity" class="card-title font-weight-bold">{{__('City / Village')}}</label>
                                    <input type="text" name="permanent_city" id="permanentCity" class="form-control rounded" required value="{{ old('permanent_city') }}" placeholder="{{ __('Type your city') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentPostOffice" class="card-title font-weight-bold">{{__('Post Office')}}</label>
                                    <input type="text" name="permanent_post_office" id="permanentPostOffice" list="postOfficeList" class="form-control rounded" required value="{{ old('permanent_post_office') }}" placeholder="{{ __('Type your post code') }}">
                                </div>

                                <div class="form-group">
                                    <label for="permanentRoad" class="card-title font-weight-bold">{{__('Road / Block / Sector')}}</label>
                                    <input type="text" name="permanent_road" id="permanentRoad" class="form-control rounded" required value="{{ old('permanent_road') }}" placeholder="{{ __('Type your home / road / block / sector') }}">
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="form-group px-5">
                            <label for="fullPresentAddress" class="card-title font-weight-bold">{{__('present address')}}</label>
                            <input type="text" name="present_address" id="fullPresentAddress" class="form-control rounded" value="{{ auth()->user()->profile->present_address }}" required>
                        </div>
                        <div class="form-group px-5">
                            <label for="fullPermanentAddress" class="card-title font-weight-bold">{{__('permanent address')}}</label>
                            <input type="text" name="permanent_address" id="fullPermanentAddress" class="form-control rounded" value="{{ auth()->user()->profile->permanent_address }}" required>
                        </div>
                    @endif


                    <div class="form-group px-5 text-center">
                        <button type="submit" class="btn btn-theme-green w-50">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
