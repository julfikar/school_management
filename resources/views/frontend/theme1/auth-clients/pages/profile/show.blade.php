@extends('frontend.theme1.auth-clients.layouts.master-layout')

@section('title', config('app.name', 'laravel').' | '.$title)

@section('page-css')

@endsection

@section('content')
    <!-- WRAPPER CONTENT ----------------------------------------------------------------------------->
    <div id="wrapper-content">

        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-dark bg-white ">
                        <a class="breadcrumb-item text-dark" href="{{ auth()->user()->userType->id < 3 ? route('user.dashboard'):route('dashboard') }}">{{ __('Home') }}</a>
                        <span class="breadcrumb-item active">{{ __($title) }}</span>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
            </div>

            <div class="row">
                @if (Laravel\Fortify\Features::canUpdateProfileInformation())
                    <div class="col-12">
                        @include('frontend.theme1.auth-clients.pages.profile.update-profile-information-form')
                    </div>
                @endif

                @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                    <div class="col-12">
                        @include('frontend.theme1.auth-clients.pages.profile.update-password-form')
                    </div>
                @endif

            </div>
        </div>
    </div>
    <!-- END WRAPPER CONTENT ------------------------------------------------------------------------->
@endsection

@section('page-script')
    @include('frontend.theme1.auth-clients.pages.profile.internal-assets.js.profile-page-scripts')
@endsection
