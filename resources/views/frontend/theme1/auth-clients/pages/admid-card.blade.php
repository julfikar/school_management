<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admit card</title>
    <style>
        @page {
            size: 8.5in 11in;
            /*background-image: url('aa.png');*/
            background-repeat: no-repeat;
            background-position: center;
        }
        body {
            color: #2E3092;
        }
    </style>
</head>


<body>
<div style="padding: 10px; border: 10px solid #0FB2EF; background-image: url({{ $bg }}); background-repeat: no-repeat; background-position: center; background-size: cover;; opacity: 0.05;">

    <table align="center" border="0" width="100%">
        <tr>
            <td style="width: 15%">
                <img
                    src="{{ $logo }}" alt=""
                    width="80">
            </td>
            <td style="text-align: center; width: 60%"><h1 style="font-size: 22pt; text-align: center;">{{ __(setting('backend.general.site_name')) }}</h1>
                <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Post Office:')}} {{__('savar')}}, {{__('Upazila:')}} {{__('savar')}}, {{__('District:')}} {{__('Dhaka-1340')}} </p>
                <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Year of establishment:')}} {{ __(setting('backend.general.established')) }}
                    <br><samp
                        style="font-size: 14px;font-weight: 600;"> {{ 'EMIS Code' }}: {{ setting('backend.general.site_emis_code') }}</samp></p>
            </td>
            <td style="width: 20%">
                <img align="right" src="{{ $groupLogo }}" alt=""
                     width="80">
            </td>
        </tr>
    </table>
    <br>
    <h2
        style="text-align: center; border-radius: 5px; background-color: #035606; color: #fff; margin: 0px 32% 0px 32%; text-transform: uppercase;">
        {{ __('Admit Card') }}</h2>
    <br>

    <table width="100%">
        <tr>
            <td style="width: 70%">
                <table width="100%">
                    <tr>
                        <th style="text-align: left;">{{ __('Enrolment ID') }}</th>
                        <th style="border-radius: 3px; border: 2px solid #0FB2EF; padding: 5px 10px; background-color: #E2F4FC; text-align: left">{{ $user->applicant->unique_id }}</th>
                    </tr>
                    <tr>
                        <th style="text-align: left;">{{ __('Center Name') }}</th>
                        <td>: {{ setting('backend.general.site_name') }}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;">{{ __('Center Code') }}</th>
                        <td>: {{ setting('backend.general.site_emis_code') }}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;">{{ __('Name') }}</th>
                        <td>: {{ config('app.locale') == 'en'?$user->name:$user->profile->name_utf8 }}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;">{{ __('Father Name') }}</th>
                        <td>: {{ config('app.locale') == 'en'?$user->profile->father_name:$user->profile->father_name_utf8 }}</td>
                    </tr>
                    <tr>
                        <th style="text-align: left;">{{ __('Mother Name') }}</th>
                        <td>: {{ config('app.locale') == 'en'?$user->profile->mother_name:$user->profile->mother_name_utf8 }}</td>
                    </tr>
                </table>
                <br>
                <table style="border-collapse: collapse; width: 100%;">
                    <tr>
                        @if(count($class->subjects) > 6)
                            <th rowspan="4"
                                style="width: 10px; border: 1px solid #2E3092; writing-mode: vertical-rl;  -webkit-transform: rotate(-180deg); -moz-transform: rotate(-180deg);">
                                {{ __('Subject') }} <br> {{ __('Time & Date') }}
                            </th>
                        @else
                            <th rowspan="2"
                                style="width: 10px; border: 1px solid #2E3092; writing-mode: vertical-rl;  -webkit-transform: rotate(-180deg); -moz-transform: rotate(-180deg);">
                                {{ __('Subject') }} <br> {{ __('Time & Date') }}
                            </th>
                        @endif
                            @foreach($class->subjects as $subject)
                                <th style="border: 1px solid #2E3092;">{{ __($subject->name) }}</th>
                                @if($loop->index > 4)
                                    @break;
                                @endif
                            @endforeach
                    </tr>
                    <tr>
                        @foreach($class->subjects as $subject)
                            <th style="border: 1px solid #2E3092;  height: 70px;"></th>
                            @if($loop->index > 4)
                                @break;
                            @endif
                        @endforeach
                    </tr>
                    @if(count($class->subjects) > 6)
                        <tr>
                            @foreach($class->subjects as $subject)
                                @if($loop->index > 5)
                                <th style="border: 1px solid #2E3092;">{{ __($subject->name) }}</th>
                                @endif
                            @endforeach
                        </tr>
                        <tr>
                            @foreach($class->subjects as $subject)
                                @if($loop->index > 5)
                                    <th style="border: 1px solid #2E3092;  height: 70px;"></th>
                                @endif
                            @endforeach
                        </tr>
                    @endif
                </table>
            </td>

            <td style="width: 30%">
                <table width="100%">
                    <tr>
                        <th style="text-align: left;">{{ __('Roll') }}</th>
                        <th style="border-radius: 3px; border: 2px solid #0FB2EF; padding: 5px 10px; background-color: #E2F4FC; text-align: left"></th>
                    </tr>
                    <tr>
                        <th style="text-align: left;">{{ __('DOB') }}</th>
                        <th style="border-radius: 3px; border: 2px solid #0FB2EF; padding: 5px 10px; background-color: #E2F4FC; text-align: left">{{ date('d-M-y', strtotime($user->applicant->date_of_birth)) }}</th>
                    </tr>
                    <tr>
                        <th style="text-align: left;">{{ __('Sex') }}</th>
                        <th style="border-radius: 3px; border: 2px solid #0FB2EF; padding: 5px 10px; background-color: #E2F4FC; text-align: left; text-transform: uppercase">{{$user->gender }}</th>
                    </tr>
                </table>
                <br>
                <table width="100%">
                    <tr>
                        <td style="padding-right: 5px; text-align: right">
                            <img style="border: 1px solid #2E3092;" height="150px" width="150px" src="{{ $avatar }}" alt="">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br><br>
    <table width="100%" align="center">
        <tr>
            <td style="text-align: left; width: 33%">
                <p style="border-top: 1px solid #2E3092; padding: 1px 5px; text-transform: capitalize; font-weight: bold;"> {{ __('student\'s signature') }}</p>
            </td>
            <td style="text-align: center; width: 33%">
                <p style="border-top: 1px solid #2E3092; padding: 1px 5px; text-transform: capitalize; font-weight: bold;">{{ __('headmaster\'s signature') }}</p>
            </td>
            <td style="text-align: right; width: 33%">
                <p style="border-top: 1px solid #2E3092; padding: 1px 5px; text-transform: capitalize; font-weight: bold;">{{ __('examiner\'s signature') }}</p>
            </td>
        </tr>
    </table>
</div>
</body>

</html>
