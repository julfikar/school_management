<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Testimonial</title>
    <style>
        @page {
            size: 8.5in 11in;
            /*background-image: url('https://savarmanikchandra-gps.com/forntend/image/letter_bg.png');*/
            background-repeat: no-repeat;
            background-position: center;
        }
    </style>
</head>

<body style="margin:0;padding: 0;">
{{--style=" width: 595px; height: 842px; background-image: url('https://savarmanikchandra-gps.com/forntend/image/letter_bg.png'); background-repeat: no-repeat; background-size:cover; margin:0 auto;"--}}
<div style="padding: 10px; border: 10px solid #0FB2EF; background-image: url({{ $bg }}); background-repeat: no-repeat; background-position: center; background-size: cover; opacity: 0.05;">

    <table align="center" border="0" width="100%">
        <tr>
            <td style="text-align:right; width: 20%">
                <img align="right" src="{{ $logo }}"
                     alt="" width="80">
            </td>
            <td style="text-align: center; width: 65%">{{__('Government of the People\'s Republic of Bangladesh')}} <br>
                {{__('Ministry of Primary and Mass Education')}} <br>
                {{__('Upazila Education Office')}}</td>
            <td style="width: 15%">
                <img src="{{ $groupLogo }}" alt=""
                     width="80">
            </td>
            <td style="text-align: right; width: 10%"><img style="margin-top:-30px;" src="{{ $mogib100 }}" alt="" width="50"
                                                           align="right"></td>
        </tr>
    </table>
    <h1 style="font-size: 22pt; text-align: center;">{{ setting('backend.general.site_name') }}</h1>
    <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Post Office:')}} {{__('savar')}}, {{__('Upazila:')}} {{__('savar')}}, {{__('District:')}} {{__('Dhaka-1340')}} </p>
    <p style="font-size: 12pt; text-align: center; line-height: 1px;">{{__('Year of establishment:')}} {{ setting('backend.general.established') }} <samp
            style="font-size: 14px;font-weight: 600;">{{ 'EMIS Code' }}:{{ setting('backend.general.site_emis_code') }}</samp></p>
    <h3 style="text-align: center; border-radius: 5px; background-color: #000000; color: #fff; margin: 0px 32% 0px 32%; text-transform: uppercase;">
        {{ __('Testimonial') }}</h3>
    @if(config('app.locale') != 'en')
        <br>
    @endif
    <table align="center" border="0" width="100%">
        <tr>
            <td style="font-size: 12pt; line-height: 12px; width: 33%">
                <table width="100%">
                    <tr>
                        <td width="85">{{__('Serial No:')}}-</td>
                        <td colspan="3" style="border-radius: 3px; height: 19px; width: 85px; border: 1px solid; text-align: center">{{__('TR-3101'.$testimonial->id)}}</td>
                    </tr>
                    <tr>
                        <td width="85">{{__('Date:')}}-</td>
                        <td style="border-radius: 3px; height: 19px; width: 20px; border: 1px solid; text-align: center">{{__(date('d',strtotime($testimonial->updated_at)))}}</td>
                        <td style="border-radius: 3px; height: 19px; width: 20px; border: 1px solid; text-align: center">{{__(date('m',strtotime($testimonial->updated_at)))}}</td>
                        <td style="border-radius: 3px; height: 19px; width: 20px; border: 1px solid; text-align: center">{{__(date('Y',strtotime($testimonial->updated_at)))}}</td>
                    </tr>
                </table>
            </td>
            <td style="width: 30%; text-align: center;">
            </td>
            <td style="text-align: right; color: brown; width: 36%">
                {{__('User ID')}}: {{$testimonial->student->unique_id}}
                <img style="padding-right: 55px;" src="{{ $avatar }}" alt="" height="100px"
                     width="90px">
            </td>
        </tr>
    </table>
    <br>
    <p style="text-align: justify;line-height: 20px; font-size: 12pt; margin: 0px 15px 0px 15px;">{{__('This is going to certify that')}}, {{ config('app.locale') == 'en'?$student->user->name:$student->user->profile->name_utf8 }}
        {{__('Father:')}} {{ config('app.locale') == 'en'?$student->user->profile->father_name:$student->user->profile->father_name_utf8 }}
        {{__('Mother:')}} {{ config('app.locale') == 'en'?$student->user->profile->mother_name:$student->user->profile->mother_name_utf8 }}
        {{__('Village:')}} {{__($testimonial->village)}}
        {{__('Post Office:')}} {{__($testimonial->post_office)}}
        {{__('Upazila:')}} {{__($testimonial->upazila)}} {{__('District:')}} {{__($testimonial->district)}} {{__('Date of birth:')}} {{__($testimonial->date_of_birth)}} {{__('Roll No')}} {{__($testimonial->roll)}} {{__('in this school')}} {{__($testimonial->education_year)}}{{__('eng')}} {{__('of the academic year')}} {{__($testimonial->class)}} {{__('studied.')}}  {{__('His/Her learning assessment is satisfactory.')}} {{__('His/Her moral character is good.')}}
    </p>
    <p style="text-align: center;">{{__('I wish him all the best.')}}</p>
    <br><br><br>
    <table width="100%" align="center">
        <tr>
            <td style="text-align: center; width: 33%">
                <p>{{ $headMaster }}</p>
                <p>{{__('Head Master')}}<br> {{__(setting('backend.general.site_name'))}}</p>
            </td>
            <td style="text-align: center; width: 33%">
                <div id="qr">
                    {!! $qrcode !!}
                </div>
            </td>
            <td style="text-align: right; width: 33%"><br><br>{{__('The seal of the school')}}</td>
        </tr>
    </table>
</div>

</body>

</html>
