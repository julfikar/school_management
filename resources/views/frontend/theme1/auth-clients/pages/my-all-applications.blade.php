
@extends('frontend.theme1.auth-clients.layouts.master-layout')

@section('content')
    <!-- WRAPPER CONTENT ----------------------------------------------------------------------------->
    <div id="wrapper-content">

        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-light ">
                        <a class="breadcrumb-item text-dark" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{ __('Home') }}</a>
                        <span class="breadcrumb-item active">{{ __($title) }}</span>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="row mt-3">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><h6 class="mb-0">{{__('Application')}}</h6></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><h6 class="mb-0">{{__('Testimonial')}}</h6></a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><h6 class="mb-0">{{__('Transfer Certificate')}}</h6></a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                            <div class="row mt-4">
                                                @forelse($applications as $application)
                                                    <div class="col-md-2">
                                                        <div class="card shadow border-0" >
                                                            <div class="card-body">
                                                                <img class="card-img-top" src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="Card image cap">

                                                                <h6 class="text-center my-3">{{Str::limit($application->subject,'28','...')}}</h6>

                                                                <a href="{{ route('user.download-application-form',$application->id) }}" target="_blank" class="text-decoration-none">
                                                                    <button class="btn btn-theme-green btn-block">{{__('Download')}}</button>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                @empty
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <h4 class="mb-0 alert alert-warning">{{__('No application found.')}}</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforelse

                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <div class="row mt-4">
                                                @forelse($testimonials as $testimonial)
                                                    <div class="col-md-2">
                                                        <div class="card shadow border-0" >
                                                            <div class="card-body">
                                                                <img class="card-img-top" src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="Card image cap">

                                                                <h6 class="text-center my-3">{{Str::limit($testimonial->name,'28','...')}}</h6>

                                                                <a href="{{ route('user.download-testimonial',$testimonial->id) }}" class="text-decoration-none">
                                                                    <button class="btn btn-theme-green btn-block">{{__('Download')}}</button>
                                                                </a>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    @empty
                                                        <div class="col-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <h4 class="mb-0 alert alert-warning">{{__('No testimonials found.')}}</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforelse
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                            <div class="row mt-4">
                                                @forelse($transfer_certificates as $transfer_certificate)
                                                    <div class="col-md-2">
                                                        <div class="card shadow border-0" >
                                                            <div class="card-body">
                                                                <img class="card-img-top" src="{{ asset('backend/assets/img/file/pdf.png') }}" alt="Card image cap">

                                                                <h6 class="text-center my-3">{{Str::limit($transfer_certificate->name,'28','...')}}</h6>

                                                                @if($transfer_certificate->status=='1')
                                                                    <a href="{{ route('user.download-transfer-certificate-form',$transfer_certificate->id) }}" class="text-decoration-none">
                                                                        <button class="btn btn-theme-green btn-block">{{__('Download')}}</button>
                                                                    </a>
                                                                @else
                                                                    <a href="javascript:void()" class="text-decoration-none">
                                                                        <button class="btn btn-theme-green btn-block">{{__('Pending...')}}</button>
                                                                    </a>
                                                                @endif
                                                            </div>

                                                        </div>
                                                    </div>

                                                @empty
                                                    <div class="col-12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <h4 class="mb-0 alert alert-warning">{{_('No transfer certificates found.')}}</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

