
<form action="{{ route('user.transfer-certificate.store') }}" method="post" >
    @csrf

    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="name">{{__('Name:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
    <div class="input-group mb-3">
        <input type="text" class="form-control border-md" value="{{config('app.locale') == 'en'?auth()->user()->name:auth()->user()->profile->name_utf8}}" id="name" name="name" placeholder="{{__('Name')}}" disabled>
        <div class="invalid-feedback">
            @if ($errors->has('name'))
                <span class="text-danger">{{ $errors->first('name') }}</span>
            @endif
        </div>
    </div>

    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="father_name">{{__('Father Name:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
    <div class="input-group mb-3">
        <input type="text" class="form-control border-md" value="{{config('app.locale') == 'en'?auth()->user()->profile->father_name:auth()->user()->profile->father_name_utf8}}" id="father_name" name="father_name" placeholder="{{__('Father Name')}}" disabled>
        <div class="invalid-feedback">
            @if ($errors->has('father_name'))
                <span class="text-danger">{{ $errors->first('father_name') }}</span>
            @endif
        </div>
    </div>

    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="mother_name">{{__('Mother Name:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
    <div class="input-group mb-3">
        <input type="text" class="form-control border-md" value="{{ config('app.locale') == 'en'?auth()->user()->profile->mother_name:auth()->user()->profile->mother_name_utf8 }}" id="mother_name" name="mother_name" placeholder="{{__('Mother name')}}" disabled>
        <div class="invalid-feedback">
            @if ($errors->has('mother_name'))
                <span class="text-danger">{{ $errors->first('mother_name') }}</span>
            @endif
        </div>
    </div>

    <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date_of_birth">{{__('Date of birth:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
    <div class="input-group mb-3">
        <input type="text" class="form-control border-md" value="{{old('date_of_birth')}}" id="date_of_birth" name="date_of_birth" placeholder="{{__('Date of birth as 16/07/2021')}}">
        <div class="invalid-feedback">
            @if ($errors->has('date_of_birth'))
                <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="village">{{__('Village:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
            <div class="input-group mb-3">
                <input type="text" class="form-control border-md" value="{{old('village')}}" placeholder="{{__("Village Name")}}" id="village" name="village">
                <div class="invalid-feedback">
                    @if ($errors->has('village'))
                        <span class="text-danger">{{ $errors->first('village') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="post_office">{{__('Post Office:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
            <div class="input-group mb-3">
                <input type="text" class="form-control border-md" value="{{old('post_office')}}" placeholder="{{__('Post Office')}}" id="post_office" name="post_office">
                <div class="invalid-feedback">
                    @if ($errors->has('post_office'))
                        <span class="text-danger">{{ $errors->first('post_office') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="upazila">{{__('Upazila:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
            <div class="input-group mb-3">
                <input type="text" class="form-control border-md" value="{{old('upazila')}}" placeholder="{{__('Upazila name')}}" id="upazila" name="upazila">
                <div class="invalid-feedback">
                    @if ($errors->has('upazila'))
                        <span class="text-danger">{{ $errors->first('upazila') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="district">{{__('District:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
            <div class="input-group mb-3">
                <input type="text" class="form-control border-md" value="{{old('district')}}" placeholder="{{__('District name')}}" id="district" name="district">
                <div class="invalid-feedback">
                    @if ($errors->has('district'))
                        <span class="text-danger">{{ $errors->first('district') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-6">
            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="class_name">{{__('Class Name')}}: </label></p>
            <div class="input-group mb-3">
                <input type="text" class="form-control border-md" value="{{auth()->user()->student->classRoom->name}}" placeholder="{{__('Class name')}}" id="class_name" name="class_name" disabled="">
                <div class="invalid-feedback">
                    @if ($errors->has('class_name'))
                        <span class="text-danger">{{ $errors->first('class_name') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="academic_year">{{__('Academic Year:')}} </label></p>
            <div class="input-group mb-3">
                <input type="text" class="form-control border-md" value="{{date('Y',time())}}" placeholder="{{__('Academc year')}}" id="academic_year" name="academic_year">
                <div class="invalid-feedback">
                    @if ($errors->has('academic_year'))
                        <span class="text-danger">{{ $errors->first('academic_year') }}</span>
                    @endif
                </div>
            </div>
        </div>
    </div>



    <p class="mb-1"><label for="name" class="card-title font-weight-bold">{{__('Cause:')}}</label> </p>
    <div class="">
        <div class="form-check">
            <input name="by_home_change" type="checkbox" class="form-check-input" id="exampleCheck3">
            <label class="form-check-label" for="exampleCheck3">{{__('By home change')}}</label>
        </div>
    </div>

    <input type="hidden" name="student_id" value="{{auth()->user()->student->id}}">

    <div class="py-2 mb-4 text-center">
        <button type="submit" class="btn btn-theme-green btn-lg">{{__('Submit the application')}}</button>
    </div>
</form>