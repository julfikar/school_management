
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Application Form</title>
</head>
<body>


<div>
    <p>{{ __('Date:') }} {{ __(date('d',strtotime($application->date))).'/'.__(date('m',strtotime($application->date))).'/'.__(date('Y',strtotime($application->date))) }}
        <br>
        {{ __('To,') }}
        <br>
        {{$application->to}}
        <br>
        {{ setting('backend.general.site_name') }},
        <br>
        {{__('Post Office:')}} {{__('savar')}}, {{__('Upazila:')}} {{__('savar')}}, {{__('District:')}} {{__('Dhaka-1340')}}
    </p>
    <br>
    <strong style="font-size: 20px">{{ __('Subject') }}: {{ $application->subject }}</strong>
    <br>
    <p style="text-align: justify;">{{ __('Dear Sir,') }}
        <br>{{ $application->request_body }}</p>
    <p style="text-align: justify;"> {{ $application->request_summary }}</p>
    <br>
    <span class="">{{ __('Yours Sincerely') }}</span>
    @if(config('app.locale') != 'en')
        <br>
    <span class="">{{ __('_0_') }},</span>
    @endif
    <p style="margin-bottom: 8px;margin-top: 0;">{{ __('Name') }}: {{ config('app.locale') == 'en'?$student->user->name:$student->user->profile->name_utf8 }}
        <br>
        {{ __('Class') }}: {{ __($student->classRoom->name) }}
        <br>
        {{ __('Roll') }}: {{ __($student->class_rol) }}
        <br>
        {{ setting('backend.general.site_name') }},
        <br>
        {{__('Post Office:')}} {{__('savar')}}, {{__('Upazila:')}} {{__('savar')}}, {{__('District:')}} {{__('Dhaka-1340')}}
    </p>
</div>

</body>
</html>
