
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Application Form</title>
</head>
<body>

<div style="width: 970px;margin:20px auto 0 auto;">
    <p>তারিখঃ {{ date('d/m/Y',strtotime($application->date)) }} ইং</p>
    <p style="margin-bottom: 8px;margin-top: 0;">বরাবর,</p>
    <p style="margin-bottom: 8px;margin-top: 0;">{{$application->to}}</p>
    <p style="margin-bottom: 8px;margin-top: 0;"> {{ setting('backend.general.site_name') }},</p>
    <p style="margin-bottom: 8px;margin-top: 0;">সাভার, ঢাকা</p>
    <br>
    <br>
    <strong>বিষয়: {{ $application->subject }}</strong>
    <br>
    <br>
    <p style="margin-bottom: 35px">জনাব, </p>

    <p  style="text-align: justify;margin-bottom: 0;line-height: 32px;">{{ $application->request_body }}</p>

    <p  style="text-align: justify;line-height: 32px;">{{ $application->request_summary }}</p>
    <br>
    <span>বিনীত নিবেদক,</span>
    <br>

    <p style="margin-bottom: 8px;">নাম: &nbsp;{{$application->name}}</p>
    <p style="margin-bottom: 8px;margin-top: 0">শ্রেণিঃ &nbsp;{{ $application->class_name }}</p>
    <p style="margin-bottom: 8px;margin-top: 0">রোলঃ &nbsp;{{ $application->roll }}</p>
    <p style="margin-bottom: 8px;margin-top: 0"> {{ setting('backend.general.site_name') }},</p>
    <p style="margin-bottom: 8px;margin-top: 0">সাভার, ঢাকা</p>

</div>

</body>
</html>