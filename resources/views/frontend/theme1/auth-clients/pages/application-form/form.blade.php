
@extends('frontend.theme1.auth-clients.layouts.master-layout')

@section('page-css')

@endsection


@section('content')
    <!-- WRAPPER CONTENT ----------------------------------------------------------------------------->
    <div id="wrapper-content">

        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <nav class="breadcrumb justify-content-sm-start justify-content-center text-center text-light bg-light ">
                        <a class="breadcrumb-item text-dark" href="{{ auth()->user()->userType->id < 3 ? route('index.dashboard'):route('dashboard') }}">{{ __('Home') }}</a>
                        <span class="breadcrumb-item active">{{ __($title) }}</span>
                        <span class="breadcrumb-info" id="time"></span>
                    </nav>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><h5 class="mb-0">{{__('Application')}}</h5></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><h5 class="mb-0">{{__('Transfer Certificate')}}</h5></a>
                                </li>

                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                                    <div class="card  bg-white border-0 mt-4">

                                        <form action="{{ route('user.application.store') }}" method="post" >
                                                @csrf

                                                <p class="mb-1 font-weight-bold"><label for="teacher_id">{{ __('Send to') }}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label> </p>
                                                <div class="input-group mb-3">
                                                    <select name="teacher_id" class="custom-select form-control d-block w-100" id="teacher_id" >
                                                        <option value="{{ null }}" selected disabled>{{__('Select for send')}}</option>
                                                        @foreach($teachers as $teacher)
                                                            <option value="{{$teacher->id}}">{{$teacher->user->name}} {{$teacher->user->getRoleNames()}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        @if ($errors->has('teacher_id'))
                                                            <span class="text-danger">{{ $errors->first('teacher_id') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="date">{{__('Date:')}} <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                                <div class="input-group mb-3">
                                                    <input type="date" class="form-control border-md" value="" id="date" name="date" placeholder="Select Date">
                                                    <div class="invalid-feedback">
                                                        @if ($errors->has('date'))
                                                            <span class="text-danger">{{ $errors->first('date') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <p class="mb-1 font-weight-bold"><label class="font-weight-bold" for="to">{{__('To')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label></p>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control border-md" value="" placeholder="Ex: Headmaster..." id="to" name="to">
                                                    <div class="invalid-feedback">
                                                        @if ($errors->has('to'))
                                                            <span class="text-danger">{{ $errors->first('to') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <label class="font-weight-bold" for="subject">{{__('Subject')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                                <div class="input-group mb-3">
                                                    <input type="text" class="form-control border-md" value="" placeholder="Ex: Prayer for leave of absence..." id="subject" name="subject">
                                                    <div class="invalid-feedback">
                                                        @if ($errors->has('subject'))
                                                            <span class="text-danger">{{ $errors->first('subject') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <label class="font-weight-bold" for="request_body">{{__('Request')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                                <div class="input-group mb-3">
                                                    <textarea class="form-control" name="request_body" id="request_body" rows="3">{{ __('With due respect, i beg to state that') }}...</textarea>
                                                    <div class="invalid-feedback">
                                                        @if ($errors->has('request_body'))
                                                            <span class="text-danger">{{ $errors->first('request_body') }}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <label class="font-weight-bold" for="request_summary">{{__('Request Summary')}}: <sup><i class="text-danger fa fa-star font-size-8px"></i></sup></label>
                                                <div class="input-group mb-3">
                                                    <textarea class="form-control" name="request_summary" id="request_summary" rows="3">{{ __('Therefore') }},...</textarea>
                                                    <div class="invalid-feedback">
                                                        @if ($errors->has('request_summary'))
                                                            <span class="text-danger">{{ $errors->first('request_summary') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="py-2 mb-4 text-center">
                                                    <button type="submit" class="btn btn-theme-green btn-lg">{{__('Submit the application')}}</button>
                                                </div>
                                            </form>

                                    </div>

                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="card  bg-white border-0">
                                        <div class="mt-4">
                                            @include('frontend.theme1.auth-clients.pages.transfer-certificates.form')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection
