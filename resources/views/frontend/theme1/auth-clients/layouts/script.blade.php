<script type="text/javascript" src="{{ asset('forntend/theme1/js/jquery-3.2.1.min.js') }}"></script>
<!-- Import Proper Js -->
<script type="text/javascript" src="{{ asset('forntend/theme1/js/popper.min.js') }}"></script>
<!-- Import Bootstrap 4 Js  -->
<script type="text/javascript" src="{{ asset('forntend/theme1/js/bootstrap.min.js') }}"></script>
<script src='{{ asset('forntend/js/2188c74ac9.js') }}'></script>

<!-- toastr alert -->
<script src="{{asset('notification_assets/js/toastr.min.js')}}"></script>
<!-- IMAGE UPLOADER -->
<script src="{{asset('backend/assets/js/image-uploader.min.js')}}"></script>
<script src='{{ asset('forntend/theme1/dashboard/js/script.js') }}'></script>

<script src="{{ asset('backend/assets/plugin/bootstrap-sllect-picker/bootstrap-select.min.js') }}"></script>

@yield('page-script')
<script !src="">
            @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @elseif(count($errors) > 0)
    @foreach($errors->all() as $error)
    toastr.error("{{ $error }}");
    @endforeach
    @endif
</script>
