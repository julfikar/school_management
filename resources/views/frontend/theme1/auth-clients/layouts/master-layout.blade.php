
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <title>Client Dashboard</title>
    <link rel='stylesheet' href='{{asset('forntend/theme1/dashboard/css/bootstrap.min.css')}}'>
    <!-- IMAGE UPLOADER -->
    <link rel="stylesheet" href="{{asset('backend/assets/css/image-uploader.min.css')}}" />
    <!-- toastr alert -->
    <link rel="stylesheet" href="{{asset('notification_assets/css/toastr.min.css')}}" />
    <link rel='stylesheet' href='{{ asset('forntend/theme1/dashboard/css/animate.css') }}'>
    <link rel="stylesheet" href="{{ asset('forntend/theme1/dashboard/css/style.css') }}">
    @yield('page-css')
</head>

<body class="sidebar-is-reduced">

<header class="l-header">
    <div class="l-header__inner clearfix">
        <div class="c-header-icon js-hamburger">
            <div class="hamburger-toggle"><span class="bar-top"></span><span class="bar-mid"></span><span
                        class="bar-bot"></span></div>
        </div>
{{--        <div class="c-header-icon has-dropdown"><span--}}
{{--                    class="c-badge c-badge--header-icon animated shake">12</span><i class="fa fa-bell"></i>--}}
{{--            <div class="c-dropdown c-dropdown--notifications">--}}
{{--                <div class="c-dropdown__header"></div>--}}
{{--                <div class="c-dropdown__content"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
</header>

    @include('frontend.theme1.auth-clients.menus.left-menu')

    <main class="l-main">
        <div class="content-wrapper content-wrapper--with-bg">
            @yield('content')
        </div>
    </main>

    @include('frontend.theme1.auth-clients.layouts.script')

</body>
</html>
