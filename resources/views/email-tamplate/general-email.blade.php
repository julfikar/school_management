<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" style="width:100%;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <!--    <title>New email template 2020-09-21</title> -->
    <!--[if (mso 16)]>
    <style type="text/css">
        a {text-decoration: none;}
    </style>
    <![endif]-->

    <!--[if gte mso 9]>
    <style>
        sup { font-size: 100% !important; }
    </style>
    <![endif]-->

    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG></o:AllowPNG>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body style="width:100%;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0">
<div class="es-wrapper-color" style="background-color:#EEEEEE">
    <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t"> <v:fill type="tile" color="#eeeeee"></v:fill> </v:background>
    <![endif]-->
    <div style="padding: 20px 15px;">
        <p style="font-size: 14pt;">
            {!! $message !!}
        </p>
    </div>
    <br>
    <br>
    <div style="padding: 20px 15px;">
        <span>
            <span>-----------------------------------------------</span>
            <br>
            <b style="font-size: 16pt;">&thinsp;&thinsp;&thinsp; Thanks & Best Regards</b>
            <br>
            <img align="right" src="https://savarmanikchandra-gps.com/upload/settings/1627575990gew34vDAC.PNG"
                 alt="" width="80">
            <br>
            <b style="font-size: 16pt;">{{ setting('backend.general.site_name') }}</b>
        </span>

{{--        <p style="font-size: 18pt"><b>{{ $companyInfo->name }}</b></p>--}}
{{--        <p style="font-size: 15pt"><b>{{ $companyInfo->email }}</b></p>--}}
{{--        <p style="font-size: 12pt; width: 200px;"><b>{!! $companyInfo->address !!}</b></p>--}}
{{--        <p style="font-size: 12pt;"><b>Mob. {{ $companyInfo->phone }}</b></p>--}}
    </div>
</div>
</body>
</html>
