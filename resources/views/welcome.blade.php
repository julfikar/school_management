@extends('frontend.theme1.layouts.master-layout')

@section('title', config('app.name', 'laravel'). ' | '.$title)

@section('page-css')
    @include('frontend.theme1.pages.home-page-sections.internal-assets.css.home-page-css')
@endsection

@section('content')
    @include('frontend.theme1.pages.home-page-sections.slider')
    <!-- Strat About Us -->
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12 col-sm-12 order-lg-1 order-md-2 order-2">
                @include('frontend.theme1.pages.home-page-sections.left-bar')
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 order-lg-2 order-md-1 order-1">
                @include('frontend.theme1.pages.home-page-sections.center-bar')
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 order-lg-3 order-md-3 order-3">
                @include('frontend.theme1.pages.home-page-sections.right-bar')
            </div>
        </div>
    </div>
    <!-- End About Us -->
    @include('frontend.theme1.pages.home-page-sections.map')

    <!--modal-->
    <div id="popupVideoPlayer" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <iframe src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    @include('frontend.theme1.pages.home-page-sections.internal-assets.js.home-page-js')
    <script src="{{asset('forntend/js/video-carousel.js')}}"></script>
    <script src="{{asset('forntend/js/jquery.touchSwipe.min.js')}}"></script>

@endsection
