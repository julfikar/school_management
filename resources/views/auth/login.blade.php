<x-guest-layout>
    <div class="main-content">
        <div class="container">
            <div class="box log-max-show">
                <div class="row box-content">
                    <div class="col-xs-12 col-sm-12 col-md-4 p-0 log-max-visible">
                        <div class="login-bg text-center text-white">
                            <div class="content">
                                <div class="logo">
                                    <div class="login-img">
                                        <a href="{{ '/' }}">
                                            <img src="{{ asset(setting('backend.logo_favicon.logo')) }}" alt="" width="100%">
                                        </a>
                                    </div>
                                </div>
                                <h1> <strong> {{ '-'.__('Welcome').'-' }} </strong> </h1>
                                <h5>{{ __('Login Window') }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 bg-white">

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active  " id="login" role="tabpanel" aria-labelledby="login-tab">
                                @if (session('status'))
                                    <div class="mb-4 font-medium text-sm text-green-600">
                                        {{ session('status') }}
                                    </div>
                                @endif


                                <form action="{{ route('login') }}" method="POST" class="form login-form d-inline">
                                    @csrf
                                    <div class="heading mt-5">
                                        <h3 class="text-center h4 text-capitalize">{{ __('Log in your account') }}</h3>

                                        <p class="text-center">{{ __('Haven\'t any account').'?' }}
                                            <a class="text-thm flipBtn" href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                                        </p>
                                        <x-jet-validation-errors class="mb-4" />
                                    </div>

                                    <div>
                                        {{--                                        <x-jet-label for="phone" value="{{ __('Phone') }}" />--}}
                                        <x-jet-input id="phone" class="block mt-1 w-full" type="tel" name="phone" :value="old('phone')" required autofocus placeholder="{{ __('Phone') }}"/>
                                    </div>

                                    <div class="mt-4">
                                        {{--                                        <x-jet-label for="password" value="{{ __('Password') }}" />--}}
                                        <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" placeholder="{{ __('Password') }}"/>
                                    </div>

                                    <div class="custom-control custom-checkbox my-4">
                                        <input class="custom-control-input" name="rememberme" type="checkbox" id="modal_rememberme" value="forever">
                                        <label class="custom-control-label" for="modal_rememberme">{{ __('Show Password') }}</label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        @if (Route::has('password.request'))
                                            <a class="underline text-gray-600 hover:text-gray-900 forget-btn" href="{{ route('password.request') }}">
                                                {{ __('Forgot your password?') }}
                                            </a>
                                        @endif
                                    </div>

                                    <div class="text-center items-center justify-end mt-4">
                                        <button type="submit" class="log-icon rounded py-2 px-3 text-white mt-3 text-uppercase">
                                            {{ __('Login') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</x-guest-layout>
