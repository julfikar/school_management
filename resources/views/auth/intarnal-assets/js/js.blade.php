<script !src="">
    // for (var fb = 0; fb < document.getElementsByClassName('flipBtn').length; fb++){
    //     var btn = document.getElementsByClassName('flipBtn')[fb];
    //     btn.addEventListener('click', flipWindow);
    // }
    // function flipWindow(event) {
    //     var button = event.target;
    //     var content = button.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement.parentElement;
    //     for (var rdn = 0; rdn < content.parentElement.children.length; rdn++){
    //         content.parentElement.children[rdn].classList.remove('d-none');
    //     }
    //     var btnClass = content.classList.contains('d-none');
    //     if(!btnClass){
    //         content.classList.add('d-none');
    //     }
    // }

    let showPasswordBtn = document.getElementById('modal_rememberme');
    showPasswordBtn.addEventListener('change',function (){
        let type = document.querySelector('#password').getAttribute('type');
        if (type == 'password'){
            document.querySelector('#password').setAttribute('type', 'text');
            if (document.getElementById('password_confirmation')){
                document.querySelector('#password_confirmation').setAttribute('type', 'text')
            }
        }else {
            document.querySelector('#password').setAttribute('type', 'password');
            if (document.getElementById('password_confirmation')){
                document.querySelector('#password_confirmation').setAttribute('type', 'password')
            }
        }
    })
</script>
