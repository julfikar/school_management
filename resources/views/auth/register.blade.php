<x-guest-layout>
    <div class="main-content" style="margin-top: 50px!important;">
        <div class="container">
            <div class="box log-max-show">
                <div class="row box-content">
                    <div class="col-xs-12 col-sm-12 col-md-4 p-0 log-max-visible">
                        <div class="login-bg text-center text-white">
                            <div class="content">
                                <div class="logo">
                                    <div class="login-img">
                                        <a href="{{ '/' }}">
                                            <img src="{{ asset(setting('backend.logo_favicon.logo')) }}" alt="" width="100%">
                                        </a>
                                    </div>
                                </div>
                                <h1> <strong> {{ '-'.__('Welcome').'-' }} </strong> </h1>
                                <h5 class="text-capitalize">{{ __('Registration window') }}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 bg-white">

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active  " id="login" role="tabpanel" aria-labelledby="login-tab">
                                <form method="POST" action="{{ route('register') }}" class="form login-form d-inline">
                                    <div class="heading ">
                                        <h3 class="text-center h4 mt-3">{{ __('Create A New Guardian Account') }}</h3>

                                        <p class="text-center">{{ __('Already registered').'?' }}
                                            <a class="text-thm flipBtn" href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </p>
                                        <x-jet-validation-errors class="mb-4" />
                                    </div>
                                    @csrf

                                    <div>
                                        {{--                                            <x-jet-label for="name" value="{{ __('Name') }}" />--}}
                                        <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" placeholder="{{ __('Name') }}"/>
                                    </div>

                                    <div class="mt-4">
                                        {{--                                            <x-jet-label for="phone" value="{{ __('Phone') }}" />--}}
                                        <x-jet-input id="phone" class="block mt-1 w-full" type="tel" name="phone" :value="old('phone')" required placeholder="{{ __('Phone') }}"/>
                                    </div>

                                    <div class="mt-4">
                                        {{--                                            <x-jet-label for="email" value="{{ __('Email (optional)') }}" />--}}
                                        <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" placeholder="{{ __('Email (optional)') }}"/>
                                    </div>

                                    <div class="mt-4">
                                        {{--                                            <x-jet-label for="password" value="{{ __('Password') }}" />--}}
                                        <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}" />
                                    </div>

                                    <div class="mt-4">
                                        {{--                                            <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />--}}
                                        <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}" />
                                    </div>

                                    <div class="custom-control custom-checkbox mt-4">
                                        <input class="custom-control-input" name="rememberme" type="checkbox" id="modal_rememberme" value="forever">
                                        <label class="custom-control-label" for="modal_rememberme">{{ __('Show Password') }}</label>
                                    </div>

                                    @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())

                                        <div class="mt-2">
                                            <x-jet-label for="terms">
                                                <div class="flex items-center">
                                                    <x-jet-checkbox name="terms" id="terms" class="my-0"/>

                                                    <div class="ml-2">
                                                        {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                                                'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                                                'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                                        ]) !!}
                                                    </div>
                                                </div>
                                            </x-jet-label>
                                        </div>
                                    @endif


                                    <div class="text-center items-center justify-end mt-4">
                                        {{--                                            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">--}}
                                        {{--                                                {{ __('Already registered?') }}--}}
                                        {{--                                            </a>--}}
                                        <button type="submit" class="log-icon rounded py-2 px-3 text-white mt-3">{{ __('Register') }}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</x-guest-layout>
