<?php

use App\Actions\Fortify\UpdateUserPassword;
use App\Http\Controllers\AdminControllers\AccountsExamFeeController;
use App\Http\Controllers\AdminControllers\AccountsHostelFeeController;
use App\Http\Controllers\AdminControllers\AccountsLibraryFeeController;
use App\Http\Controllers\AdminControllers\AccountsTransportationFeeController;
use App\Http\Controllers\AdminControllers\AccountsTutionFeeController;
use App\Http\Controllers\AdminControllers\AdminAccountSettingsController;
use App\Http\Controllers\AdminControllers\AdminAdmissionController;
use App\Http\Controllers\AdminControllers\AdminBlogFolderController;
use App\Http\Controllers\AdminControllers\AdminBlogPostController;
use App\Http\Controllers\AdminControllers\AdminCashTransactionController;
use App\Http\Controllers\AdminControllers\AdminClassRoutine;
use App\Http\Controllers\AdminControllers\AdminEbookController;
use App\Http\Controllers\AdminControllers\AdminEmployeeReportsController;
use App\Http\Controllers\AdminControllers\AdminGalleryController;
use App\Http\Controllers\AdminControllers\AdminGalleryFolderController;
use App\Http\Controllers\AdminControllers\AdminHostelController;
use App\Http\Controllers\AdminControllers\AdminLibraryMemberController;
use App\Http\Controllers\AdminControllers\AdminPageController;
use App\Http\Controllers\AdminControllers\AdminPassengerController;
use App\Http\Controllers\AdminControllers\AdminPhysicalBookController;
use App\Http\Controllers\AdminControllers\AdminSalaryController;
use App\Http\Controllers\AdminControllers\AdminStudentReportsController;
use App\Http\Controllers\AdminControllers\AdminTeacherReportsController;
use App\Http\Controllers\AdminControllers\AdminTransportationController;
use App\Http\Controllers\AdminControllers\CommunicationController;
use App\Http\Controllers\AdminControllers\ExamCategoryController;
use App\Http\Controllers\AdminControllers\StudentExamController;
use App\Http\Controllers\AdminControllers\AddminController;
use App\Http\Controllers\AdminControllers\AdminResourceController;
use App\Http\Controllers\AdminControllers\AdminStudentController;
use App\Http\Controllers\AdminControllers\AdminSubjectController;
use App\Http\Controllers\AdminControllers\AdminTeacherController;
use App\Http\Controllers\AdminControllers\AppSettingsController;
use App\Http\Controllers\AdminControllers\DesignationController;
use App\Http\Controllers\AdminControllers\EmployeeController;
use App\Http\Controllers\AdminControllers\ExamController;
use App\Http\Controllers\AdminControllers\TransferCertificateController;
use App\Http\Controllers\AdminControllers\UserSettingController;
use App\Http\Controllers\AdminControllers\AdminDwellerController;
use App\Http\Controllers\AdminControllers\WizardKeyPersonController;
use App\Http\Controllers\AdminControllers\WizardSliderController;
use App\Http\Controllers\AdminControllers\WizardVideoSliderController;
use App\Http\Controllers\GuestViewControllers\GuestViewController;
use App\Http\Controllers\MenuSettings\MenuCategoryController;
use App\Http\Controllers\MenuSettings\MenuItemController;
use App\Http\Controllers\UserConlrollers\UserController;
use App\Http\Controllers\AdminControllers\WizardEmergencyServiceController;
use App\Http\Controllers\AdminControllers\WizardSudSliderController;
use App\Http\Controllers\AdminControllers\WizardImportantLinksController;
use App\Http\Controllers\AdminControllers\WizardNoticeBoardController;
use App\Http\Controllers\AdminControllers\WizardComplainController;
use App\Http\Controllers\AdminControllers\TestimonialController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/setup', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    echo 'done';
    return redirect()->route('home');
});

Route::get('/', [GuestViewController::class, 'index'])->name('home');
Route::get('/pg/{slug}', [GuestViewController::class, 'redirectPage'])->name('page.redirect');
Route::prefix('library')->group(function (){
    Route::get('/', [GuestViewController::class, 'library'])->name('library');
    Route::get('/story-books', [GuestViewController::class, 'storyBooks'])->name('library.storyBooks');
    Route::prefix('text-books')->group(function (){
        Route::get('/', [GuestViewController::class, 'textBooks'])->name('library.textBooks');
        Route::get('/{classRoom}', [GuestViewController::class, 'textBooksByClass'])->name('library.textBooks.byClass');
    });

});

Route::get('/photo-gallery', [GuestViewController::class, 'photoGallery'])->name('photo-gallery');
Route::get('/admission-form', [GuestViewController::class, 'admissionForm'])->name('admission-form');
Route::post('/admission-form-store', [GuestViewController::class, 'storeAdmissionForm'])->name('store-admission-form');
Route::prefix('notice')->as('notice.')->group(function (){
    Route::get('/', [GuestViewController::class, 'notice'])->name('index');
    Route::get('/view/{slug}', [GuestViewController::class, 'singleNoticeView'])->name('single');
});

Route::prefix('blog')->as('blog.')->group(function (){
    Route::get('/', [GuestViewController::class, 'blog'])->name('index');
    Route::get('/view/{slug}', [GuestViewController::class, 'singleBlogView'])->name('single');
    Route::get('/folder/view/{id}', [GuestViewController::class, 'singleBlogFolderView'])->name('single-folder');
});

Route::get('/content', [GuestViewController::class, 'personContent'])->name('person-content');
Route::get('/single-content/{id}', [GuestViewController::class, 'singlePersonContent'])->name('single-person-content');
Route::get('/all-teachers', [GuestViewController::class, 'allTeachers'])->name('all-teachers');
Route::get('/all-employees', [GuestViewController::class, 'allEmployees'])->name('all-employees');
Route::get('/employee-explore/{id}', [GuestViewController::class, 'employeeExplore'])->name('employee-explore');

Route::post('/complain', [GuestViewController::class, 'complain'])->name('complain');
Route::get('/teacher-explore/{id}', [GuestViewController::class, 'teacherExplore'])->name('teacher-explore');
Route::post('/subscribe', [GuestViewController::class, 'Subscribe'])->name('subscribe');

Route::prefix('result')->as('result.')->group(function (){
    Route::get('/', [GuestViewController::class, 'examResult'])->name('index');
    Route::get('/show', [GuestViewController::class, 'examResultStore'])->name('show');
    Route::get('/download', [GuestViewController::class, 'examResultDownload'])->name('download');
});

// For transfer-certificate
Route::get('/download-transfer-certificate-form/{id}', [GuestViewController::class, 'downloadTransferCertificateForm'])->name('download-transfer-certificate-form');

// For Testimonial
Route::get('/download-testimonial-form/{id}', [GuestViewController::class, 'downloadTestimonial'])->name('download-testimonial');
//Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//
//    return view('dashboard');
//})->name('dashboard');

Route::group(['prefix' => 'admin', 'middleware' => ['auth:sanctum', 'admin']], function () {
    // dashboard rout
    Route::get('/dashboard', [AddminController::class, 'index'])->name('index.dashboard');
    // profile routs
    Route::get('/profile', [AddminController::class, 'profile'])->name('admin.profile');
    Route::post('/profile', [AddminController::class, 'profileUpdate'])->name('admin.profile');
    Route::post('/profile-info', [AddminController::class, 'profileInfoUpdate'])->name('admin.profile-info');
    Route::post('/password-update', [UpdateUserPassword::class, 'updateAdminPassword'])->name('admin.password-update');
    Route::post('/delete-account', [AddminController::class, 'adminDelete'])->name('admin.delete-account');

    //classRooms, routine, Subjects & Subjects Resources,
    Route::middleware('permission:class')->group(function (){
        //classRooms
        Route::get('/classes', [AddminController::class, 'getAllClassRoom'])->name('admin.get-classrooms');
        Route::get('/show-classes/{classroom}', [AddminController::class, 'showClassRoom'])->name('admin.show-classroom');
        Route::post('/update-classroom/{classroom}', [AddminController::class, 'classRoomUpdate'])->name('admin.update.classroom');

        //class routine
        Route::prefix('classes')->as('admin.')->group(function () {
            Route::get('/routine/{classRoom}', [AdminClassRoutine::class, 'index'])->name('class-routine');
            Route::get('/routine-create/{classRoom}', [AdminClassRoutine::class, 'create'])->name('class-routine.create');
            Route::post('/routine/{classRoom}', [AdminClassRoutine::class, 'store'])->name('class-routine.store');
            Route::post('/routine-update/{period}', [AdminClassRoutine::class, 'update'])->name('class-routine.update');
            Route::delete('/routine-destroy/{period}', [AdminClassRoutine::class, 'destroy'])->name('class-routine.destroy');
            Route::get('/routine-download/{classRoom}', [AdminClassRoutine::class, 'download'])->name('class-routine.download');
        });

        // Subjects
        Route::get('/class-subjects-show/{classRoom}/', [AdminSubjectController::class, 'index'])->name('admin.get-class-subjects');
        Route::get('/add-class-subjects/{classRoom}', [AdminSubjectController::class, 'addClassSubject'])->name('admin.add-class-subject');
        Route::post('/store-class-subjects/', [AdminSubjectController::class, 'storeClassSubject'])->name('admin.store-class-subject');
        Route::get('/show-subjects-update/{subject}/', [AdminSubjectController::class, 'showUpdateForm'])->name('admin.show-update-subject-form');
        Route::post('/update-subject', [AdminSubjectController::class, 'subjectUpdate'])->name('admin.update.subject');
        Route::get('destroy-subject/{subject}', [AdminSubjectController::class, 'destroySubject'])->name('admin.destroy-subject');
        Route::get('/subjects/{class_id}', [AdminSubjectController::class, 'filterByClass']);

        // Subjects Resources
        Route::get('/show-subject-resources/{subject}', [AdminResourceController::class, 'index'])->name('admin.get-subject-resources');
        Route::get('/add-subject-resource/{subject}', [AdminResourceController::class, 'create'])->name('admin.add-subject-resource');
        Route::post('/store-subject-resource/', [AdminResourceController::class, 'store'])->name('admin.store-subject-resource');
        Route::get('/update-subject-resource/{resource}/', [AdminResourceController::class, 'showUpdateForm'])->name('admin.show-update-resource-form');
        Route::post('/update-resource', [AdminResourceController::class, 'update'])->name('admin.update.resource');
        Route::post('destroy-resource/', [AdminResourceController::class, 'destroy'])->name('admin.destroy-resource');
    });

    // library, Ebooks, Physical Book & book give to member
    Route::prefix('library')->as('admin.')->middleware('permission:library')->group(function () {
    // library
        Route::resource('library-member', AdminLibraryMemberController::class, ['only' => ['index', 'store', 'destroy']]);

        // Library Ebooks
        Route::resource('ebooks', AdminEbookController::class, ['only' => ['index', 'create', 'store', 'show']]);
        Route::post('/ebooks/update/{ebook}', [AdminEbookController::class, 'update'])->name('ebook.update');
        Route::get('/ebooks/destroy/{ebook}', [AdminEbookController::class, 'destroy'])->name('ebook.destroy');

        // Library Physical Book
        Route::resource('physicalbooks', AdminPhysicalBookController::class, ['only' => ['index', 'store']]);
        Route::get('/physicalbook/create/{physicalBook?}', [AdminPhysicalBookController::class, 'create'])->name('physicalbooks.create');
        Route::post('/physicalbook/update/{physicalBook}', [AdminPhysicalBookController::class, 'update'])->name('physicalbooks.update');
        Route::get('/physicalbook/destroy/{physicalBook}', [AdminPhysicalBookController::class, 'destroy'])->name('physicalbooks.destroy');

        // book give to member
        Route::post('/physicalbooks/to-member', [AdminPhysicalBookController::class, 'assignToMember']);
        Route::post('/physicalbooks/from-member', [AdminPhysicalBookController::class, 'revokeToMember']);
    });

    // Hostel section
    Route::prefix('hostel')->as('admin.')->middleware('permission:hostel')->group(function () {
        Route::resource('hostels', AdminHostelController::class, ['only' => ['index', 'create', 'store', 'show']]);
        Route::post('/hostel/update/{hostel}', [AdminHostelController::class, 'update'])->name('hostel.update');
        Route::get('/hostel/destroy/{hostel}', [AdminHostelController::class, 'destroy'])->name('hostel.destroy');
    });

    // Hostel Dwellers
    Route::as('admin.')->middleware('permission:hostel')->group(function () {
        Route::get('/hostel-dwellers-show/{hostel}/', [AdminDwellerController::class, 'index'])->name('get-hostel-dwellers');
        Route::post('/store-hostel-dwellers/', [AdminDwellerController::class, 'store'])->name('store-hostel-dweller');
        Route::get('destroy-dweller/{dweller}', [AdminDwellerController::class, 'destroy'])->name('destroy-dweller');
    });

    // transportation section
    Route::as('admin.')->middleware('permission:transportation')->group(function () {
        Route::resource('transportations', AdminTransportationController::class, ['only' => ['index', 'create', 'store', 'show']]);
        Route::post('/transportation/update/{transportation}', [AdminTransportationController::class, 'update'])->name('transportation.update');
        Route::get('/transportation/destroy/{transportation}', [AdminTransportationController::class, 'destroy'])->name('transportation.destroy');
    });

    // transportation passenger
    Route::as('admin.')->middleware('permission:transportation')->group(function () {
        Route::get('/transportation-passengers-show/{transportation}/', [AdminPassengerController::class, 'index'])->name('get-transportation-passengers');
        Route::post('/store-transportation-passengers/', [AdminPassengerController::class, 'store'])->name('store-transportation-passenger');
        Route::get('destroy-passenger/{passenger}', [AdminPassengerController::class, 'destroy'])->name('destroy-passenger');
    });

    // teachers
    Route::prefix('teachers')->as('admin.teachers.')->middleware('permission:teachers')->group(function () {
        Route::post('update/{teacher}', [AdminTeacherController::class, 'update'])->name('update');
        Route::get('destroy/{teacher}', [AdminTeacherController::class, 'destroy'])->name('destroy');
        Route::get('create/bulk', [AdminTeacherController::class, 'createBulk'])->name('create-bulk');
        Route::get('/create-bulk/demo-file', [AdminTeacherController::class, 'downloadDemoFile'])->name('create-bulk.demo-file');
        Route::post('store/bulk', [AdminTeacherController::class, 'storeBulk'])->name('store-bulk');
        Route::resource('/teacher', AdminTeacherController::class, ['only' => ['index', 'create', 'store', 'show']]);
        Route::get('/teacher/activation/{teacher}', [AdminTeacherController::class, 'teacherActivation'])->name('teacher.activation');
    });

    //Exam category, exams, Exam routine & result
    Route::as('admin.')->middleware('permission:exam')->group(function (){
        // Exam category
        Route::prefix('exams')->as('exam.')->group(function (){
            Route::resource('/category', ExamCategoryController::class, ['only'=>['store','show','edit','update','destroy']]);
        });
        Route::get('/exams/exam-by-class/{category}', [ExamCategoryController::class, 'showExamByClass'])->name('exam.category.by-class');

        Route::get('exams/filter_by_class/{class_id}', [ExamController::class, 'filterByClass']);
        Route::get('exams/distinct/filter_by_class/{class_id}', [ExamController::class, 'getDistinctValueByClass']);
        Route::get('exams/create-bulk', [ExamController::class, 'createBulk'])->name('exams.create-bulk');
        Route::post('exams/store-bulk', [ExamController::class, 'storeBulk'])->name('exams.store-bulk');
        Route::get('exams/routine', [ExamController::class, 'selectExamView'])->name('exams.routine');
        Route::post('exams/routine-download', [ExamController::class, 'routineDownload'])->name('exams.routine-download');
        Route::get('exams/filter', [ExamController::class, 'filter']);
        //Exams
        Route::resource('/exams',ExamController::class, ['only'=>['index','create','store','edit','update','destroy']]);
        Route::get('/exams/show-exams-by', [ExamController::class, 'showExams'])->name('exam-show');

        // Exam routine
        Route::prefix('exams')->group(function () {
            Route::get('/routine-download', [ExamController::class, 'downloadRoutine'])->name('routine-download');
            Route::get('/routine/show', [ExamController::class, 'routineShow'])->name('routine-show');
        });

        // Exam result
        Route::prefix('result')->as('student-exams.')->group(function () {
            Route::get('/', [StudentExamController::class, 'index'])->name('index');
            Route::get('/student-exams', [StudentExamController::class, 'show'])->name('show');
            Route::get('/student-exams-result', [StudentExamController::class, 'create'])->name('create');
            Route::post('/student-exams-result', [StudentExamController::class, 'store'])->name('store');
        });

        Route::prefix('admit-card')->as('student-exams.admit-card.')->group(function (){
            Route::get('/', [ExamController::class, 'admitCardIndex'])->name('index');
            Route::get('/get', [ExamController::class, 'getAdmitCard'])->name('get');
        });
    });

    // employee & designation
    Route::prefix('employee')->middleware('permission:employee')->group(function () {
        // designation
        Route::get('/designation', [DesignationController::class, 'index'])->name('admin.designation.index');
        Route::get('/designation/create-form/{designation?}', [DesignationController::class, 'createForm'])->name('admin.designation.create');
        Route::post('store-designation/{designation?}', [DesignationController::class, 'store'])->name('admin.designation.store');
        Route::get('destroy-designation/{designation}', [DesignationController::class, 'destroy'])->name('admin.designation.destroy');

        // employee
        Route::get('/', [EmployeeController::class, 'index'])->name('admin.employee.index');
        Route::get('/create-form/{employee?}', [EmployeeController::class, 'create'])->name('admin.employee.create');
        Route::get('/create-bulk', [EmployeeController::class, 'createBulk'])->name('admin.employee.create-bulk');
        Route::get('/create-bulk/demo-file', [EmployeeController::class, 'downloadDemoFile'])->name('admin.employee.create-bulk.demo-file');
        Route::post('store-employee/', [EmployeeController::class, 'store'])->name('admin.employee.store');
        Route::post('store-bulk/', [EmployeeController::class, 'storeBulk'])->name('admin.employee.store-bulk');
        Route::post('/employee/update/{employee}', [EmployeeController::class, 'update'])->name('admin.employee.update');
        Route::get('/destroy/{employee}', [EmployeeController::class, 'destroy'])->name('admin.employee.destroy');
        Route::get('/activation/{employee}', [EmployeeController::class, 'employeeActivation'])->name('employee.activation');
    });

    //pages
    Route::as('admin.')->middleware('permission:app settings')->group(function (){
        Route::resource('/pages',AdminPageController::class, ['only'=>['create','store','show','update','destroy']]);
    });

    //Blog Folders
    Route::as('admin.')->middleware('permission:blog settings')->group(function (){
        Route::resource('/blogfolders',AdminBlogFolderController::class, ['only'=>['index','create','store','show','update','destroy']]);
    });

    //Blog post
    Route::prefix('blogpost')->middleware('permission:blog')->name('admin.blogposts.')->group(function (){
        Route::get('/{blogFolder?}', [AdminBlogPostController::class, 'index'])->name('index');
        Route::get('/create-post/{blogFolder?}', [AdminBlogPostController::class, 'create'])->name('create');
        Route::get('/show-post/{blogpost}', [AdminBlogPostController::class, 'show'])->name('show');
        Route::post('/store/', [AdminBlogPostController::class, 'store'])->name('store');
        Route::post('/update/{blogpost}', [AdminBlogPostController::class, 'update'])->name('update');
        Route::get('/destroy/{blogpost}', [AdminBlogPostController::class, 'destroy'])->name('destroy');
        Route::get('/activation/{blogpost}', [AdminBlogPostController::class, 'blogpostActivation'])->name('activation');
//        Route::resource('/blogposts',AdminBlogPostController::class, ['only'=>['index','create','store','show','update','destroy']]);
    });

    // app settings
    Route::prefix('settings')->middleware('permission:app settings')->group(function () {
        Route::get('general', [AppSettingsController::class, 'getGeneralSettings'])->name('admin.settings.general');
        Route::post('general', [AppSettingsController::class, 'saveGeneralSettings'])->name('admin.settings.general-save');
        Route::get('logo-favicon', [AppSettingsController::class, 'getLogoFaviconSettings'])->name('admin.settings.logo-favicon');
        Route::post('logo-favicon', [AppSettingsController::class, 'saveLogoFaviconSettings'])->name('admin.settings.logo-favicon-save');
        Route::get('seo', [AppSettingsController::class, 'getSeoSettings'])->name('admin.settings.seo');
        Route::post('seo', [AppSettingsController::class, 'saveSeoSettings'])->name('admin.settings.seo-save');
        Route::get('smtp', [AppSettingsController::class, 'getSmtpSettings'])->name('admin.settings.smtp');
        Route::post('smtp', [AppSettingsController::class, 'saveSmtpSettings'])->name('admin.settings.smtp-save');
        Route::get('sms', [AppSettingsController::class, 'getSmsSettings'])->name('admin.settings.sms');
        Route::post('sms', [AppSettingsController::class, 'saveSmsSettings'])->name('admin.settings.sms-save');
        Route::get('custom-css', [AppSettingsController::class, 'getCustomCssSettings'])->name('admin.settings.custom-css');
        Route::post('custom-css', [AppSettingsController::class, 'saveCustomCssSettings'])->name('admin.settings.custom-css-save');
        Route::get('custom-js', [AppSettingsController::class, 'getCustomJsSettings'])->name('admin.settings.custom-js');
        Route::post('custom-js', [AppSettingsController::class, 'saveCustomJsSettings'])->name('admin.settings.custom-js-save');
        Route::get('insert-header-footer', [AppSettingsController::class, 'getInsertHeaderFooterSettings'])->name('admin.settings.insert-header-footer');
        Route::post('insert-header-footer', [AppSettingsController::class, 'saveInsertHeaderFooterSettings'])->name('admin.settings.insert-header-footer-save');
        // language settings
        Route::get('/greeting/{locale}', [AppSettingsController::class, 'languageSettings'])->name('set-local');

        Route::prefix('menu')->as('admin.menu.')->group(function (){
            Route::resource('/category', MenuCategoryController::class, ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('/item', MenuItemController::class, ['only' => ['index', 'create', 'store']]);
        });

        Route::prefix('social-link')->as('admin.social-link.')->group(function (){
            Route::get('/',[AppSettingsController::class, 'socialLinkSettings'])->name('index');
            Route::post('/',[AppSettingsController::class, 'socialLinkSettingsStore'])->name('store');
        });
    });

    // galleries
    Route::prefix('galleries')->middleware('permission:galleries')->name('admin.galleries.')->group(function () {

        // Gallery Folder section
        Route::prefix('gallery-folder')->name('galleryfolder.')->group(function () {
            Route::get('/', [AdminGalleryFolderController::class, 'index'])->name('index');
            Route::get('/create-gallery-folder/{galleryFolder?}', [AdminGalleryFolderController::class, 'create'])->name('create');
            Route::post('/store/{galleryFolder?}', [AdminGalleryFolderController::class, 'store'])->name('store');
            Route::get('/destroy/{galleryFolder}', [AdminGalleryFolderController::class, 'destroy'])->name('destroy');
            Route::get('/activation/{galleryFolder}', [AdminGalleryFolderController::class, 'galleryFolderActivation'])->name('activation');

        });

        // Gallery image section
        Route::prefix('gallery-image')->name('galleryimage.')->group(function () {
            Route::get('/{galleryFolder}', [AdminGalleryController::class, 'index'])->name('index');
            Route::get('/create-gallery/{galleryFolder}', [AdminGalleryController::class, 'create'])->name('create');
            Route::get('/show-gallery/{gallery}', [AdminGalleryController::class, 'show'])->name('show');
            Route::post('/store/', [AdminGalleryController::class, 'store'])->name('store');
            Route::post('/update/{gallery}', [AdminGalleryController::class, 'update'])->name('update');
            Route::get('/destroy/{gallery}', [AdminGalleryController::class, 'destroy'])->name('destroy');
            Route::get('/activation/{gallery}', [AdminGalleryController::class, 'galleryActivation'])->name('activation');
        });


    });

    // wizards
    Route::prefix('wizards')->as('admin.wizards.')->middleware('permission:wizards')->group(function () {

        // slider image section
        Route::prefix('sliders')->name('sliders.')->group(function () {
            Route::get('/', [WizardSliderController::class, 'index'])->name('index');
            Route::get('/create-slider/', [WizardSliderController::class, 'create'])->name('create');
            Route::get('/show-slider/{slider}', [WizardSliderController::class, 'show'])->name('show');
            Route::post('/store/', [WizardSliderController::class, 'store'])->name('store');
            Route::post('/update/{slider}', [WizardSliderController::class, 'update'])->name('update');
            Route::get('/destroy/{slider}', [WizardSliderController::class, 'destroy'])->name('destroy');
            Route::get('/activation/{slider}', [WizardSliderController::class, 'sliderActivation'])->name('activation');
        });

        // slider Video section
        Route::prefix('video-sliders')->name('video_sliders.')->group(function () {
            Route::get('/', [WizardVideoSliderController::class, 'index'])->name('index');
            Route::get('/show-slider/{videoSlider?}', [WizardVideoSliderController::class, 'create'])->name('create');
            Route::post('/store/{videoSlider?}', [WizardVideoSliderController::class, 'store'])->name('store');
            Route::get('/destroy/{videoSlider}', [WizardVideoSliderController::class, 'destroy'])->name('destroy');
            Route::get('/activation/{videoSlider}', [WizardVideoSliderController::class, 'active_main_slider'])->name('activation');
        });

        // key person
        Route::prefix('key-person')->name('key_persons.')->group(function () {
            Route::get('/', [WizardKeyPersonController::class, 'index'])->name('index');
            Route::get('/show-slider/{keyPerson?}', [WizardKeyPersonController::class, 'create'])->name('create');
            Route::post('/store/{keyPerson?}', [WizardKeyPersonController::class, 'store'])->name('store');
            Route::get('/destroy/{keyPerson}', [WizardKeyPersonController::class, 'destroy'])->name('destroy');
        });

        // emergency services section
        Route::prefix('emergency_services')->name('emergency_services.')->group(function () {
            Route::get('/', [WizardEmergencyServiceController::class, 'index'])->name('index');
            Route::get('/create', [WizardEmergencyServiceController::class, 'create'])->name('create');
            Route::post('/store', [WizardEmergencyServiceController::class, 'store'])->name('store');
            Route::get('/destroy/{id}', [WizardEmergencyServiceController::class, 'destroy'])->name('destroy');
            Route::get('/edit/{id}', [WizardEmergencyServiceController::class, 'edit'])->name('edit');
            Route::post('/update/{id}', [WizardEmergencyServiceController::class, 'update'])->name('update');
        });

        //sub slider section
        Route::prefix('sub-slider')->name('sub-slider.')->group(function () {
            Route::get('/', [WizardSudSliderController::class, 'index'])->name('index');
            Route::get('/create', [WizardSudSliderController::class, 'create'])->name('create');
            Route::post('/store', [WizardSudSliderController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [WizardSudSliderController::class, 'edit'])->name('edit');
            Route::post('/update/{id}', [WizardSudSliderController::class, 'update'])->name('update');
            Route::get('/delete/{id}', [WizardSudSliderController::class, 'delete'])->name('delete');
        });
       //important links section
        Route::prefix('important_links')->name('important_links.')->group(function () {
            Route::get('/', [WizardImportantLinksController::class, 'index'])->name('index');
            Route::get('/create', [WizardImportantLinksController::class, 'create'])->name('create');
            Route::post('/store', [WizardImportantLinksController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [WizardImportantLinksController::class, 'edit'])->name('edit');
            Route::post('/update/{id}', [WizardImportantLinksController::class, 'update'])->name('update');
            Route::get('/delete/{id}', [WizardImportantLinksController::class, 'delete'])->name('delete');
        });

        //notice board section
        Route::prefix('notice_board')->name('notice_board.')->group(function () {
            Route::get('/', [WizardNoticeBoardController::class, 'index'])->name('index');
            Route::get('/create', [WizardNoticeBoardController::class, 'create'])->name('create');
            Route::post('/store', [WizardNoticeBoardController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [WizardNoticeBoardController::class, 'edit'])->name('edit');
            Route::post('/update/{id}', [WizardNoticeBoardController::class, 'update'])->name('update');
            Route::get('/delete/{id}', [WizardNoticeBoardController::class, 'delete'])->name('delete');
        });

        //complain section
        Route::prefix('complain')->name('complain.')->group(function () {
            Route::get('/', [WizardComplainController::class, 'index'])->name('index');
            Route::get('/delete/{id}', [WizardComplainController::class, 'delete'])->name('delete');
            Route::get('/view/{id}', [WizardComplainController::class, 'view'])->name('view');
        });
    });

    // User settings
    Route::prefix('user')->middleware('permission:user')->group(function () {
        Route::get('get-admin', [UserSettingController::class, 'getAdmin'])->name('admin.user.get-admin');
        Route::get('get-guardian', [UserSettingController::class, 'getGuardian'])->name('admin.user.get-guardian');

//        Route::get('create-admin', [UserSettingController::class, 'createAdmin'])->name('admin.user.create-admin');
        Route::get('/show/{user}', [UserSettingController::class, 'show'])->name('admin.user.show-admin');
        Route::post('/show/{user}', [UserSettingController::class, 'updateAdmin']);

        Route::get('all-role', [UserSettingController::class, 'allRole'])->name('admin.user.all-role');
        Route::get('create-role/{role?}', [UserSettingController::class, 'createRole'])->name('admin.user.create-role');
        Route::post('create-role', [UserSettingController::class, 'storeRole']);
        Route::post('update-role/{role}', [UserSettingController::class, 'updateRole'])->name('admin.user.update-role');
        Route::get('destroy-role/{role}', [UserSettingController::class, 'destroyRole'])->name('admin.user.destroy-role');
    });

    //students
    Route::prefix('student')->middleware('permission:students')->group(function () {
        Route::get('/', [UserSettingController::class, 'getStudent'])->name('admin.user.get-student');
        Route::get('create/bulk', [AdminStudentController::class, 'createBulk'])->name('admin.students.create-bulk');
        Route::get('/create-bulk/demo-file', [AdminStudentController::class, 'downloadDemoFile'])->name('admin.students.create-bulk.demo-file');
        Route::post('store/bulk', [AdminStudentController::class, 'storeBulk'])->name('admin.students.store-bulk');
        Route::get('create/{student?}', [UserSettingController::class, 'createStudent'])->name('admin.user.create-student');
        Route::post('store/{student?}', [UserSettingController::class, 'storeStudent'])->name('admin.user.store-student');
        Route::get('destroy/{student}', [UserSettingController::class, 'destroyStudent'])->name('admin.user.destroy-student');
        Route::get('/activation/{student}', [UserSettingController::class, 'studentActivation'])->name('admin.user.active-student');
        Route::get('filter_by_class/{class_id}', [UserSettingController::class, 'getStudentByClass']);

        Route::get('/promotion/{student}', [AdminStudentController::class, 'promotedToStudent'])->name('admin.student-promotion');

        Route::as('admin.')->group(function () {
            Route::get('/clasroom-students-show/{classRoom}/', [AdminStudentController::class, 'index'])->name('get-class-students');
            Route::resource('students', AdminStudentController::class, ['only' => ['create', 'store', 'show']]);
            Route::post('/students/update/{student}', [AdminStudentController::class, 'update'])->name('student.update');
            Route::get('/students/destroy/{student}', [AdminStudentController::class, 'destroy'])->name('student.destroy');
            Route::get('students/activation/{student}', [AdminStudentController::class, 'studentActivation'])->name('student.activation');
            Route::get('/download/{student}', [AdminStudentController::class, 'download'])->name('student.download');


        });

        Route::as('admin.')->group(function () {
            Route::resource('admissions', AdminAdmissionController::class, ['only' => ['index', 'create', 'store', 'show']]);
            Route::get('/admission/destroy/{admission}', [AdminAdmissionController::class, 'destroy'])->name('admission.destroy');
            Route::get('admission/activation/{admission}', [AdminAdmissionController::class, 'admittedToStudent'])->name('admission.activation');
        });

    });

    // Accounts section
    Route::prefix('accounts')->as('admin.')->middleware('permission:accounts')->group(function () {
       Route::prefix('account-settings')->as('account-settings.')->group(function (){
           Route::get('/', [AdminAccountSettingsController::class, 'index'])->name('index');
           Route::get('/get-form/{accountSetting}', [AdminAccountSettingsController::class, 'getForm'])->name('get-form');
           Route::post('/store-fees', [AdminAccountSettingsController::class, 'storeFee'])->name('store-fees');
           Route::get('/activation/{id}', [AdminAccountSettingsController::class, 'accountSettingActivation'])->name('activation');
       });

        Route::prefix('tution-fee')->as('tution-fee.')->group(function (){
            Route::get('/', [AccountsTutionFeeController::class, 'index'])->name('index');
            Route::get('/filter-student/{class_id}', [AccountsTutionFeeController::class, 'filterStudentByClass'])->name('class-student');
            Route::post('/store', [AccountsTutionFeeController::class, 'storeTutionFee'])->name('store');

        });

        Route::prefix('exam-fee')->as('exam-fee.')->group(function (){
            Route::get('/', [AccountsExamFeeController::class, 'index'])->name('index');
            Route::get('/filter-student/{class_id}', [AccountsExamFeeController::class, 'filterStudentByClass'])->name('class-student');
            Route::post('/store', [AccountsExamFeeController::class, 'store'])->name('store');

        });

        Route::prefix('liibrary-fee')->as('library-fee.')->group(function (){
            Route::get('/', [AccountsLibraryFeeController::class, 'index'])->name('index');
            Route::post('/store', [AccountsLibraryFeeController::class, 'storeLibraryFee'])->name('store');
        });

        Route::prefix('hoostel-fee')->as('hostel-fee.')->group(function (){
            Route::get('/', [AccountsHostelFeeController::class, 'index'])->name('index');
            Route::post('/store', [AccountsHostelFeeController::class, 'store'])->name('store');
        });

        Route::prefix('trransportation-fee')->as('transportation-fee.')->group(function (){
            Route::get('/', [AccountsTransportationFeeController::class, 'index'])->name('index');
            Route::post('/store', [AccountsTransportationFeeController::class, 'store'])->name('store');
        });

        // For cash transactions
        Route::prefix('cash-transactions')->as('cash-transactions.')->group(function (){
            Route::get('/', [AdminCashTransactionController::class, 'index'])->name('index');
            Route::post('/store', [AdminCashTransactionController::class, 'store'])->name('store');
            Route::get('/filter-user/{type}', [AdminCashTransactionController::class, 'filterUserByType'])->name('filter-by-type');

        });
    });

    // Reports section
    Route::prefix('reports')->as('admin.')->middleware('permission:reports')->group(function () {
        Route::prefix('student-report')->as('student-report.')->group(function () {
            Route::get('/', [AdminStudentReportsController::class, 'index'])->name('index');
            Route::post('/attendance/calculate', [AdminStudentReportsController::class, 'attendanceCalculate'])->name('attendance-calculate');
            Route::post('/exam-result/calculate', [AdminStudentReportsController::class, 'examResultCalculate'])->name('exam-result-calculate');

            Route::post('/accounts/calculate', [AdminStudentReportsController::class, 'accountsCalculate'])->name('accounts-calculate');

        });
        // Teacher Reports
        Route::prefix('teacher-report')->as('teacher-report.')->group(function () {
            Route::get('/', [AdminTeacherReportsController::class, 'index'])->name('index');
            Route::post('/attendance/calculate', [AdminTeacherReportsController::class, 'attendanceCalculate'])->name('attendance-calculate');

            Route::post('/accounts/calculate', [AdminTeacherReportsController::class, 'accountsCalculate'])->name('accounts-calculate');
            Route::post('/salary/calculate', [AdminTeacherReportsController::class, 'salaryCalculate'])->name('salary-calculate');

        });


        Route::prefix('employe-report')->as('employee-report.')->group(function () {
            Route::get('/', [AdminEmployeeReportsController::class, 'index'])->name('index');
            Route::post('/attendance/calculate', [AdminEmployeeReportsController::class, 'attendanceCalculate'])->name('attendance-calculate');

            Route::post('/accounts/calculate', [AdminEmployeeReportsController::class, 'accountsCalculate'])->name('accounts-calculate');
            Route::post('/salary/calculate', [AdminEmployeeReportsController::class, 'salaryCalculate'])->name('salary-calculate');
        });
    });

    //testimonial & Transfer Certificate
    Route::as('admin.')->middleware('permission:testimonial & transfer certificate')->group(function () {
        //testimonial
        Route::prefix('testimonial')->name('testimonial.')->group(function () {
            Route::get('/', [TestimonialController::class, 'index'])->name('index');
            Route::get('/create', [TestimonialController::class, 'create'])->name('create');
            Route::post('/store', [TestimonialController::class, 'store'])->name('store');
            Route::get('/destroy/{testimonial}', [TestimonialController::class, 'destroy'])->name('destroy');
            Route::get('/download/{testimonial}', [TestimonialController::class, 'download'])->name('download');
        });

        //Transfer Certificate
        Route::prefix('transfer-certificate')->name('transfer-certificate.')->group(function () {
            Route::get('/', [TransferCertificateController::class, 'index'])->name('index');
            Route::get('/create', [TransferCertificateController::class, 'create'])->name('create');
            Route::post('/store', [TransferCertificateController::class, 'store'])->name('store');
            Route::get('/download/{id}', [TransferCertificateController::class, 'download'])->name('download');
            Route::get('/destroy/{transfercertificate}', [TransferCertificateController::class, 'destroy'])->name('destroy');
            Route::get('/approve/{transfercertificate}', [TransferCertificateController::class, 'approve'])->name('approve');
        });
    });

    //salary
    Route::prefix('salary')->as('admin.salary.')->middleware('permission:salary')->group(function (){
        Route::get('/', [AdminSalaryController::class, 'index'])->name('index');
        Route::get('/create/{user}', [AdminSalaryController::class, 'create'])->name('create');
        Route::post('/create/{user}', [AdminSalaryController::class, 'store'])->name('store');
        Route::get('/pay', [AdminSalaryController::class, 'pay'])->name('pay');
        Route::post('/pay', [AdminSalaryController::class, 'storePayment'])->name('pay');
    });

    //communication
    Route::prefix('communication')->as('admin.communication.')->middleware('permission:communication')->group(function (){
        Route::get('/', [CommunicationController::class, 'phoneCall'])->name('phone');
        Route::prefix('sms')->as('sms.')->group(function (){
            Route::get('/index', [CommunicationController::class, 'smsIndex'])->name('index');
            Route::post('/sent', [CommunicationController::class, 'sendSms'])->name('sent');
        });
        Route::prefix('email')->as('email.')->group(function (){
            Route::get('/index', [CommunicationController::class, 'emailIndex'])->name('index');
            Route::post('/sent', [CommunicationController::class, 'sendEmail'])->name('sent');
        });
    });
});

Route::group(['prefix' => 'user','middleware' => ['auth:sanctum', 'user']], function () {
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard');
    // profile routs
    Route::as('user.')->group(function () {
        Route::get('/profile', [UserController::class, 'profile'])->name('profile');
        Route::post('/profile-update', [UserController::class, 'profileUpdate'])->name('profile-update');
        Route::post('/profile-info', [UserController::class, 'profileInfoUpdate'])->name('profile-info');

        Route::get('/my-all-applications', [UserController::class, 'myAllApplications'])->name('my-all-applications');

        // For Testimonial
        Route::get('/download-testimonial-form/{id}', [UserController::class, 'downloadTestimonial'])->name('download-testimonial');
        // For application
        Route::get('/application', [UserController::class, 'Application'])->name('application');
        Route::post('/application/store', [UserController::class, 'applicationStore'])->name('application.store');
        Route::get('/download-application-form/{id}', [UserController::class, 'downloadApplicationForm'])->name('download-application-form');
        //transfer-certificate
        Route::post('/transfer-certificate/store', [UserController::class, 'transferCertificateStore'])->name('transfer-certificate.store');
        Route::get('/download-transfer-certificate-form/{id}', [UserController::class, 'downloadTransferCertificateForm'])->name('download-transfer-certificate-form');

        Route::post('/password-update', [UpdateUserPassword::class, 'updateAdminPassword'])->name('password-update');

        Route::get('/download-admit-card', [UserController::class, 'downloadAdmitCard'])->name('download-admit-card');

    });

});


