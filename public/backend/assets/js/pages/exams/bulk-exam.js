// const classInput = document.getElementById('class_id');
const submitButton = document.getElementById('submit_button');

// classInput.addEventListener('change', function () {
//     const classId = this.value;
//
//     fetch(`/admin/subjects/${classId}`)
//         .then(res => res.json())
//         .then(data => {
//             // const subjectInput = document.getElementById('subject_id');
//             // // subjectInput.innerHTML = '<option value="" selected disabled>Select a Subject</option>';
//             // subjectInput.innerHTML = '';
//             // data.subjects.forEach(subject => {
//             //     subjectInput.innerHTML += `<option value="${subject.id}">${subject.name}</option>`
//             // });
//
//             // subjectInput.removeAttribute('disabled');
//
//             const tableBody = document.querySelector('tbody');
//             tableBody.innerHTML = '';
//
//             data.subjects.forEach(subject => {
//                 tableBody.innerHTML += `
//                     <tr class="subjects">
//                         <td>
//                             <input type="text" name="" id="" value="${subject.name}" readonly class="subject-name">
//                         </td>
//                         <td>
//                             <input type="number" name="" id="" class="subject-mark">
//                         </td>
//                         <td>
//                             <input type="date" name="" id="" class="exam-date">
//                         </td>
//                         <td>
//                             <input type="time" name="" id="" class="exam-start">
//                         </td>
//                         <td>
//                             <input type="time" name="" id="" class="exam-end">
//                         </td>
//                         <input type="hidden" name="" id="" class="subject-id" value="${subject.id}">
//                     </tr>
//                 `
//             });
//
//
//         }).catch(err => {
//             console.log(err);
//         })
// })

submitButton.addEventListener('click', function () {
    const subjects = [];
    document.querySelectorAll('.subjects').forEach(subject => {
        subjects.push({
            'id': subject.querySelector('.subject-id').value,
            'name': subject.querySelector('.subject-name').value,
            'mark': subject.querySelector('.subject-mark').value,
            'date': subject.querySelector('.exam-date').value,
            'start_at': subject.querySelector('.exam-start').value,
            'end_at': subject.querySelector('.exam-end').value,
        });
    })

    axios({
        'method': 'post',
        'url': '/admin/exams/store-bulk',
        'data': {
            'class_id': document.getElementById('class_id').value,
            'exam_name': document.getElementById('name').value,
            'subjects': subjects
        }
    }).then(res => {
        toastr.success('Saved Successfully!');
        location.reload();
    }).catch(err => {
        toastr.error(err.data.message);
    });

})
