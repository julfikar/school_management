

const classInput = document.getElementById('class_id');
const examInput = document.getElementById('exam_id');


classInput.addEventListener('change', function () {
    const classId = this.value;

    fetch(`/admin/exams/distinct/filter_by_class/${classId}`)
        .then(res => res.json())
        .then(data => {
            const examInput = document.getElementById('exam_id');
            // examInput.innerHTML = '<option value="" selected disabled>Select a Subject</option>';
            examInput.innerHTML = '';
            for (const key in data.exams) {
                if (Object.hasOwnProperty.call(data.exams, key)) {
                    const element = data.exams[key];
                    examInput.innerHTML += `<option value="${element.name}">${element.name}</option>`
                }
            }

            examInput.removeAttribute('disabled');



            // get current selected class name
            let selectedClassName = classInput.querySelector(`[value="${classInput.value}"]`).textContent
            document.getElementById('class_name').textContent = selectedClassName;

            //get current selected exam name
            document.getElementById('exam_name').textContent = document.getElementById('exam_id').value



            getExams();

        }).catch(err => {
            console.log(err);
        })
})

examInput.addEventListener('change', function () {
    //call admin/exams/filter?class_id=3&exam_name='lksdjf'
    getExams();

    //get current selected exam name
    document.getElementById('exam_name').textContent = document.getElementById('exam_id').value
});





function getExams() {
    fetch(`/admin/exams/filter?class_id=${classInput.value}&exam_name=${examInput.value}`)
        .then(res => res.json())
        .then(data => {
            const tableBody = document.querySelector('tbody');
            tableBody.innerHTML = '';



            data.exams.forEach(exam => {
                tableBody.innerHTML += `
                <tr>
                    <td>${exam.date}</td>
                    <td>${exam.subject ? exam.subject.name : ''}</td>
                    <td>${exam.start_at}</td>
                    <td>${exam.end_at}</td>
                </tr>
                `;
            });


        }).catch(err => {
            console.log(err);
        })
}