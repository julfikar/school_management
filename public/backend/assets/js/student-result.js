const classInput = document.getElementById('class_id');

classInput.addEventListener('change', function () {
    const classId = this.value;

    fetch(`/admin/exams/filter_by_class/${classId}`)
        .then(res => res.json())
        .then(data => {
            const examInput = document.getElementById('exam_id');
            // examInput.innerHTML = '<option value="" selected disabled>Select a exam</option>';
            examInput.innerHTML = '';
            data.exams.forEach(exam => {
                examInput.innerHTML += `<option value="${exam.id}">${exam.name}</option>`
            });

            examInput.removeAttribute('disabled');

        }).catch(err => {
            console.log(err);
        })

    fetch(`/admin/student/filter_by_class/${classId}`)
        .then(res => res.json())
        .then(data => {
            const studentInput = document.getElementById('student_id');
            // studentInput.innerHTML = '<option value="" selected disabled>Select a exam</option>';
            studentInput.innerHTML = '';
            data.students.forEach(student => {
                studentInput.innerHTML += `<option value="${student.id}">${student.user.name}</option>`
            });

            studentInput.removeAttribute('disabled');

        }).catch(err => {
            console.log(err);
        })
})