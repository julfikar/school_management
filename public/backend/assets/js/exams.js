const classInput = document.getElementById('class_id');

classInput.addEventListener('change', function () {
    const classId = this.value;

    fetch(`/admin/subjects/${classId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            const subjectInput = document.getElementById('subject_id');
            // subjectInput.innerHTML = '<option value="" selected disabled>Select a Subject</option>';
            subjectInput.innerHTML = '';
            data.subjects.forEach(subject => {
                subjectInput.innerHTML += `<option value="${subject.id}">${subject.name}</option>`
            });

            subjectInput.removeAttribute('disabled');

        }).catch(err => {
            console.log(err);
        })
});